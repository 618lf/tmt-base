package com.tmt.base.common.web;

import com.tmt.base.common.utils.SpringContextHolder;

/**
 * 自定义信息  + 国际化的消息机制
 * @author lifeng
 */
public class Message {
	private Message.Type messageType;
	private String content;

	public Message() {}

	public Message(Message.Type type, String content) {
		this.messageType = type;
		this.content = content;
	}

	public Message(Message.Type type, String content, Object[] args) {
		this.messageType = type;
		this.content = SpringContextHolder.getMessage(content, args);
	}

	public static Message success(String content, Object[] args) {
		return new Message(Message.Type.success, content, args);
	}

	public static Message warn(String content, Object[] args) {
		return new Message(Message.Type.warn, content, args);
	}

	public static Message error(String content, Object[] args) {
		return new Message(Message.Type.error, content, args);
	}

	public Message.Type getType() {
		return this.messageType;
	}

	public void setType(Message.Type type) {
		this.messageType = type;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String toString() {
		return SpringContextHolder.getMessage(this.content, new Object[0]);
	}

	public enum Type {
		success, warn, error;
	}
}
