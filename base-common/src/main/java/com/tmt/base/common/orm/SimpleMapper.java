package com.tmt.base.common.orm;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tmt.base.common.orm.annotion.Column;
import com.tmt.base.common.orm.annotion.TableAlias;
import com.tmt.base.common.utils.ReflectUtil;

/**
 * 简单的erm映射，通过entity的name映射ralation的name
 * @author LL
 *
 */
public class SimpleMapper implements IERMapper {
	
	/**
	 *Constructor 
	 */
	public SimpleMapper(){
		
	}
	/** 表名前缀 */
	private String tablePrefix;

	/** 表的别名 */
	private String tableAlias;
	
	/** 表名 */
	private String tableName;
	
	public String getTablePrefix() {
		return tablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

	public String getTableAlias() {
		return tableAlias;
	}

	public void setTableAlias(String tableAlias) {
		this.tableAlias = tableAlias;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public SimpleMapper(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String, String> loadMaps(Class<?> clazz) {
		Field[] fields = clazz.getDeclaredFields();
		String alias = this.getTableAlias(clazz);
		this.setTableAlias(alias);
		Map map = new HashMap<Object,Object>();
		for(Field field:fields){
			String fieldName = field.getName();
			Class<?> fieldType = field.getType();
			if(ReflectUtil.isSimpleType(fieldType)){
				map.put(fieldName, this.getColumn(field));
			}
		}
		return map;
	}
	
	/**
	 * 将属性转换为字段
	 * @param field
	 * @return 如果有注解标记，则返回注解的标记，否则返回默认值
	 */
	private String getColumn(Field field){
		Column column = field.getAnnotation(Column.class);
		if(column!=null){
			return column.value();
		}else{
			String tableAlias = this.getTableAlias();
			return tableAlias+"."+this.convertProperty2Column(field.getName());
		}
	}
	
	/**
	 * 将属性转换为默认字段
	 * @param property
	 * @return
	 */
	private String convertProperty2Column(String property){
		StringBuilder column = new StringBuilder();
		for(int i=0;i<property.length();i++){
			char c = property.charAt(i);
			if(Character.isUpperCase(c)){
				column.append("_");
			}
			column.append(Character.toUpperCase(c));
		}
		return column.toString();
	}
	
	/**
	 * 得到实体对应的表别名
	 * @param 实体名entity
	 * @return 如果有注解标记，则返回注解的标记，否则返回默认值
	 */
	public String getTableAlias(Class<?> entity){
		TableAlias tableAlias = entity.getAnnotation(TableAlias.class);
		if(tableAlias!=null){
			return tableAlias.value();
		}
		String entityName = StringUtils.substringAfterLast(entity.getName(), ".");
		StringBuffer alias = new StringBuffer(30);
		alias.append(Character.toUpperCase(entityName.charAt(0)));
		for(int i=1;i<entityName.length();i++){
			char c = entityName.charAt(i);
			if(Character.isUpperCase(c)){
				alias.append(Character.toUpperCase(c));
			}
		}
		return alias.toString();
	}
}
