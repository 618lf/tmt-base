package com.tmt.base.common.persistence.datasource;

/**
 * 数据源管理器
 * @author liFeng 2014年6月23日
 */
public class DataSourceHolder {

	private static final ThreadLocal<String> dataSourceHolder = new ThreadLocal<String>();

	public static void setDataSourceType(DataSourceType dataSourceType) {
		dataSourceHolder.set(dataSourceType.name());
	}

	public static DataSourceType getDataSourceType() {
		return dataSourceHolder.get()==null?null:DataSourceType.valueOf(dataSourceHolder.get());
	}

	public static void clearDataSourceType() {
		dataSourceHolder.remove();
	}

}
