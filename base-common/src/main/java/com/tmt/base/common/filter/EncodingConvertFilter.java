package com.tmt.base.common.filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

/**
 * GET参数编码格式转换
 * @author liFeng
 * 2014年8月17日
 */
public class EncodingConvertFilter extends OncePerRequestFilter{

	private String fromEncoding = "ISO-8859-1";
	private String toEncoding = "UTF-8";
	
	/**
	 * 处理get 参数的编码转换，Tomcat 等服务器中不需要配置编码转换
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		if("GET".equalsIgnoreCase(request.getMethod())) {
			Iterator<String[]> localIterator = request.getParameterMap().values().iterator();
			while (localIterator.hasNext()){
				String[] arrayOfString = localIterator.next();
				for (int i = 0; i < arrayOfString.length; i++) {
					try {
						arrayOfString[i] = new String(
								arrayOfString[i].getBytes(this.fromEncoding),
								this.toEncoding);
					} catch (UnsupportedEncodingException localUnsupportedEncodingException) {
						localUnsupportedEncodingException.printStackTrace();
					}
				}
			}
		}
	}
	
	public String getFromEncoding() {
		return fromEncoding;
	}

	public void setFromEncoding(String fromEncoding) {
		this.fromEncoding = fromEncoding;
	}

	public String getToEncoding() {
		return toEncoding;
	}

	public void setToEncoding(String toEncoding) {
		this.toEncoding = toEncoding;
	}

}
