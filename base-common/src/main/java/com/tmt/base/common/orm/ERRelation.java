package com.tmt.base.common.orm;

import java.lang.reflect.Field;
import java.util.Map;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tmt.base.common.config.Globals;

/**
 * @author LL
 *
 */
@Component
public class ERRelation {

	private static Logger logger = LoggerFactory.getLogger(ERRelation.class);
	
	@Autowired
	private CacheManager ehCacheManager; 
	
	@Autowired
	private SimpleMapper mapper;
	/**
	 * 通过属性明的到实体名
	 * @param clazz
	 * @param property
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public String getFieldNameByProperty(Class<?> clazz,String property){
		Cache cache = ehCacheManager.getCache(Globals.ERM_CACHE_NAME);
		Element element = cache.get(clazz.getName());
		if(element == null){
			element = new Element(clazz.getName(), mapper.loadMaps(clazz));
			cache.put(element);
		}
		if(property.indexOf('.')!=-1){
			String fieldName = property.substring(0, property.indexOf('.'));
			String subProerty = property.substring(property.indexOf('.')+1, property.length());
			try{
				Field field = clazz.getDeclaredField(fieldName);
				return getFieldNameByProperty(field.getType(),subProerty);
			} catch (NoSuchFieldException e) {
				logger.error("找不到属性"+fieldName,e);
				return null;
			}
		}else{
			return (String)((Map)element.getObjectValue()).get(property);
		}
	}
}
