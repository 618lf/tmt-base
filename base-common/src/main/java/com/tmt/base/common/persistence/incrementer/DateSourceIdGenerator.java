package com.tmt.base.common.persistence.incrementer;

import javax.sql.DataSource;

/**
 * dataSource 适配器
 * 
 * @author liFeng 2014年6月9日
 */
public interface DateSourceIdGenerator extends IdGenerator {
	public void setDataSource(DataSource dataSource);
}
