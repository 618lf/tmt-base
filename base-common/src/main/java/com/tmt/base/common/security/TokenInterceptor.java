package com.tmt.base.common.security;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.tmt.base.common.utils.CookieUtils;

/**
 * POST:
 *       Ajax 提交的数据中 请求头中需要存放 token
 *       form 提交的参数中需要提供 token
 * GET:  不处理
 * @author lifeng
 */
public class TokenInterceptor extends HandlerInterceptorAdapter {

	private static Logger logger = LoggerFactory.getLogger(TokenInterceptor.class);
	private static final String TOKEN_KEY = "token";
	private static final String ACCESS_DENIED_MSG = "Bad or missing token!";
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		String tokenId = CookieUtils.getCookie(request, TOKEN_KEY);
		if ( request.getMethod().equalsIgnoreCase("POST") ) {
			String str2 = request.getHeader("X-Requested-With");//Ajax 处理
			if ((str2 != null) && (str2.equalsIgnoreCase("XMLHttpRequest"))) {
				if ((tokenId != null) && (tokenId.equals(request.getHeader(TOKEN_KEY)))) {
					return true;	
				}
				response.addHeader("tokenStatus", "accessDenied");
			} else if ((tokenId != null) && (tokenId.equals(request.getParameter(TOKEN_KEY)))) {
				return true;
			}
			if (tokenId == null) {
				tokenId = UUID.randomUUID().toString();
				CookieUtils.setCookie(response, TOKEN_KEY, tokenId);
			}
			//token 失效
			try{
				response.sendError(403, ACCESS_DENIED_MSG);
			}catch(Exception e) {
				logger.error("token error", e); 
			}
			return false;
		}
		if (tokenId == null) {
			tokenId = UUID.randomUUID().toString();
			CookieUtils.setCookie(response, TOKEN_KEY, tokenId);
		}
		request.setAttribute(TOKEN_KEY, tokenId);
		return Boolean.TRUE; 
	}
}
