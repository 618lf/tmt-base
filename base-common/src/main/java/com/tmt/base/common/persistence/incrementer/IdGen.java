/**
 * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.tmt.base.common.persistence.incrementer;

import java.io.Serializable;

import javax.sql.DataSource;

import com.tmt.base.common.utils.SpringContextHolder;

/**
 * 封装各种生成唯一性ID算法的工具类.
 * 
 * @author ThinkGem
 * @version 2013-01-15
 */
public final class IdGen implements IdGenerator{

	private static IdGen instance = null;

	public static IdGen getInstance() {
		if (instance == null) {
			instance = SpringContextHolder.getBean(IdGen.class);
		}
		return instance;
	}

	private IdGenerator idGenerator;

	/**
	 * 无效的id
	 */
	public static final String INVALID_ID = "-1";

	/**
	 * 默认的RootID，一般作为默认值
	 */
	public static final String ROOT_ID = "0";

	/**
	 * 判断一个id是否有效 全局定义 id 为-1无效
	 * 
	 * @return
	 */
	public static Boolean isInvalidId(String id) {
		if (id == null || "".equals(id.trim())
				|| IdGen.INVALID_ID.equals(id.trim())) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * 初始的根目录不能删除 根据Id = 0 判断是否是根路劲
	 * 
	 * @param id
	 * @return
	 */
	public static Boolean isRoot(String id) {
		if (id != null && IdGen.ROOT_ID.equals(id)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	/**
	 * 自动获取主键
	 * 
	 * @param key
	 * @return
	 */
	public static Serializable key(Serializable key) {
		return IdGen.getInstance().generateId(key);

	}

	public Serializable generateId(Serializable key) {
		if (idGenerator == null) { // 默认
			return UUIdGenerator.uuid();
		}
		return idGenerator.generateId(key);
	}
	
	/**
	 * 设置策略
	 * @param strategy
	 */
	public void setStrategy(String strategy) {
		if (strategy == null || "UUID".equals(strategy)) {
			idGenerator = new UUIdGenerator();
		} else if ("NUMBER".equals(strategy)) {
			idGenerator = new NumberIdGenerator();
		}
	}
	
	/**
	 * NUMBER 需要设置主键保存的数据源
	 */
	public void setDataSource(DataSource dataSource) {
		if( idGenerator instanceof DateSourceIdGenerator){
			((DateSourceIdGenerator) idGenerator).setDataSource(dataSource);
		}
	}
}
