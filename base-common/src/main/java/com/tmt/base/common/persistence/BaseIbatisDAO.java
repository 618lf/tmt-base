package com.tmt.base.common.persistence;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.util.StringUtils;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapExecutor;
import com.ibatis.sqlmap.engine.execution.SqlExecutor;
import com.ibatis.sqlmap.engine.impl.ExtendedSqlMapClient;
import com.tmt.base.common.entity.IdEntity;
import com.tmt.base.common.exception.DataAccessException;
import com.tmt.base.common.persistence.ibatis.LimitSqlExecutor;
import com.tmt.base.common.persistence.ibatis.support.SqlMapClientDaoSupport;
import com.tmt.base.common.utils.ReflectUtil;

/**
 * 数据库操作支持类
 * @author TMT
 */
@SuppressWarnings({ "unchecked", "deprecation" })
public abstract class BaseIbatisDAO<T, PK> extends SqlMapClientDaoSupport {

	protected static Logger logger = LoggerFactory.getLogger(BaseIbatisDAO.class);
	
	protected final static String INSERT = "insert";
    protected final static String UPDATE = "update";
    protected final static String DELETE = "delete";
    protected final static String DELETE_BY_CONDITION = "deleteByCondition";
    protected final static String FIND_BY_PK = "findByPk";
    protected final static String FIND_ALL = "findAll";
    protected final static String FIND_BY_CONDITION = "findByCondition";
    protected final static String COUNT_BY_CONDITION = "findByConditionStat";
    protected final static String FIND_BY_PK_WITHLOCK = "findByPkWithLock";
    protected final static String UPDATE_SELECTIVE = "updateSelective";

    @Autowired
    private SqlExecutor sqlExecutor;
    private boolean readonly = false;
    
    public void initialize() throws Exception {
        if (sqlExecutor != null) {
            SqlMapClient sqlMapClient = this.getSqlMapClientTemplate().getSqlMapClient();
            if (sqlMapClient instanceof ExtendedSqlMapClient &&((ExtendedSqlMapClient) sqlMapClient)
                    .getDelegate().getSqlExecutor() != sqlExecutor ) {
                ReflectUtil.setFieldValue(((ExtendedSqlMapClient) sqlMapClient)
                        .getDelegate(), "sqlExecutor", SqlExecutor.class,
                        sqlExecutor);
            }
        }
    }
    protected abstract String getNamespace();
    
	protected String getClassShortName(Class<T> theClass) {
        String className = theClass.getName();
        return className.substring(className.lastIndexOf('.') + 1);
    }

    public boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    /**
     * statement name.
     * 
     * @param operate
     * @return
     */
    protected String getStatementName(String operate) {
        String namespace = getNamespace();

        return StringUtils.hasText(namespace) ? (namespace + "." + operate)
                : namespace;
    }

    protected SqlMapClientTemplate getSqlRunner() {
        return this.getSqlMapClientTemplate();
    }

    protected int execute(String statementName, Object entity) {
        return this.getSqlRunner().update(getStatementName(statementName), entity);
    }
    
    protected int execute(String statementName) {
        return this.getSqlRunner().update(getStatementName(statementName));
    }
    
    /***********CURD***************/
	public PK insert(String statementName, T entity) {
    	try{
    		PK pk = null;
    		if( entity instanceof IdEntity ) {
    			pk = ((IdEntity<PK>)entity).prePersistEntity(this.getNamespace());
    		}
    		this.getSqlRunner().insert(getStatementName(statementName), entity);
    		return pk;
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public PK insert(T entity) {
    	try{
    		return this.insert(INSERT, entity);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public int delete(T entity) {
    	try{
    		return this.getSqlRunner().delete(getStatementName(DELETE), entity);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }

    public int delete(String statementName, T entity) {
    	try{
    		return this.getSqlRunner().delete(getStatementName(statementName),
                entity);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public int delete(String statementName, Map<String,?> params) {
    	try{
    		return this.getSqlRunner().delete(getStatementName(statementName), params);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    	
    }
    
    public int deleteByCondition(QueryCondition qc) {
    	try{
    		return this.getSqlRunner().delete(getStatementName(DELETE_BY_CONDITION), qc);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public int deleteByCondition(Map<String,Object> qc) {
    	try{
    		return this.getSqlRunner().delete(getStatementName(DELETE_BY_CONDITION), qc);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public int update(String statementName, T entity) {
    	try{
            if( entity instanceof IdEntity ) {
            	((IdEntity<PK>)entity).preUpdate();
            }
    		return this.getSqlRunner().update(getStatementName(statementName), entity);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public int update(T entity) {
    	return this.update(UPDATE,entity);
    }
    
    public int updateSelective(T entity) {
    	return this.update(UPDATE_SELECTIVE, entity);
    }
    
    public int update(String statementName, Map<String,?> params) {
    	try{
    		return this.getSqlRunner().update(getStatementName(statementName), params);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public T findByPk(PK id) {
    	try{
    		return (T) this.getSqlRunner().queryForObject(getStatementName(FIND_BY_PK),id);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public T findByPk(String sql , PK id) {
    	try{
    		return (T) this.getSqlRunner().queryForObject(getStatementName(sql),id);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public T findByPkWithLock(PK id) {
    	try{
	        return (T) this.getSqlRunner().queryForObject(
	                getStatementName(FIND_BY_PK_WITHLOCK), id);
	    }catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
    }
    
    public boolean isExist(PK id) {
        return findByPk(id) != null;
    }
    
    public List<T> findAll() {
    	try{
    		return this.getSqlRunner().queryForList(getStatementName(FIND_ALL));
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public List<T> queryByCondition(QueryCondition qc){
    	try{
    		return this.getSqlRunner().queryForList(getStatementName(FIND_BY_CONDITION),qc);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public List<T> queryForList(String statementName) {
    	try{
	        return this.getSqlRunner().queryForList(getStatementName(statementName), null);
	    }catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
    }
    
    public List<T> queryForList(String statementName, Object entity) {
    	try{
	    	return this.getSqlRunner().queryForList(
	                    getStatementName(statementName), entity);
	    }catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
    }
    
    public List<T> queryForList(String statementName, QueryCondition qc) {
    	try{
	        return this.getSqlRunner().queryForList(getStatementName(statementName), qc);
	    }catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
    }
    
    public List<T> queryForList(String statementName, Map<String,?> params) {
    	try{
	    	return this.getSqlRunner().queryForList(getStatementName(statementName), params);
	    }catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
    }
    
    public <E> List<E> queryForGenericsList(String statementName, Map<String,?> params) {
    	try{
	    	return this.getSqlRunner().queryForList(getStatementName(statementName), params);
	    }catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
    }
    
    public <E> List<E> queryForGenericsList(String statementName, Object entity) {
    	try{
	    	return this.getSqlRunner().queryForList(
	                    getStatementName(statementName), entity);
	    }catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
    }
    
    public List<Map<String,Object>> queryForMapList(String statementName, Map<String,?> params) {
    	try{
	    	return (List<Map<String,Object>>)this.getSqlRunner().queryForList(getStatementName(statementName), params);
	    }catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
    }
    
    public List<String> queryForStringList(String statementName, Map<String,?> params) {
    	try{
	    	return (List<String>)this.getSqlRunner().queryForList(
	    			getStatementName(statementName), params);
	    }catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
    }

    public T queryForObject(String statementName) {
    	try{
            return (T) this.getSqlRunner().queryForObject(getStatementName(statementName), null);
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }

    public T queryForObject(String statementName, Object entity) {
    	try{
	    	return (T) this.getSqlRunner().queryForObject(getStatementName(statementName), entity);
	    }catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
    }
    
    public T queryForObject(String statementName, Map<String,?> params) {
    	try{
    	    return (T)(this.getSqlRunner().queryForObject( getStatementName(statementName), params));
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }
    
    public T queryForObject(QueryCondition qc) {
    	try{
    		return (T)(this.getSqlRunner().queryForObject(getStatementName(FIND_BY_CONDITION), qc));
    	}catch(Exception ex){
    		logger.error("sql执行错误：", ex);
    		throw new DataAccessException(ex);
    	}
    }

    /**
     * 提供分页功能的数据查询 注意:本功能的SQL语句需要有对应统计记录数方法 命名规则为:功能SQL语句名称+Stat
     * 
     * @param sql
     * @param args
     * @param pageSize
     * @param pageNum
     * @return
     */
    public Page queryForPageList(String sql, Map<String,?> args,
            PageParameters pageParameters) {
        int pageNum = pageParameters.getPageIndex();
        int pageSize = pageParameters.getPageSize();
        Integer count = 0;
        List<T> lst = null;
        if (pageNum == PageParameters.NO_PAGINATION
                || pageSize == PageParameters.NO_PAGINATION) {
            lst = this.getSqlRunner().queryForList(sql, args);
        } else {
            count = (Integer) this.getSqlRunner().queryForObject(getStatementName(sql+"Stat"),
                    args);
            //防止pageNum超过最大值
            int pageCount = getPageCount(count, pageSize);
            if(pageNum > pageCount){
                pageNum = pageCount;
            }
            lst = this.getSqlRunner().queryForList(getStatementName(sql), args,
                    (pageNum - 1) * pageSize, pageSize);
        }
        pageParameters.setRecordCount(count);
        return new Page(pageParameters, lst);
    }
    
    public Page queryForMapPageList(String sql, Map<String,?> args,
            PageParameters pageParameters) {
        int pageNum = pageParameters.getPageIndex();
        int pageSize = pageParameters.getPageSize();
        Integer count = 0;
        List<Map<String,Object>> lst = null;
        if (pageNum == PageParameters.NO_PAGINATION
                || pageSize == PageParameters.NO_PAGINATION) {
            lst = this.getSqlRunner().queryForList(sql, args);
        } else {
            count = (Integer) this.getSqlRunner().queryForObject(getStatementName(sql+"Stat"),
                    args);
            //防止pageNum超过最大值
            int pageCount = getPageCount(count, pageSize);
            if(pageNum > pageCount){
                pageNum = pageCount;
            }
            lst = this.getSqlRunner().queryForList(getStatementName(sql), args,
                    (pageNum - 1) * pageSize, pageSize);
        }
        pageParameters.setRecordCount(count); 
        return new Page(pageParameters, lst);
    }
    
    /**
     * 提供分页功能的数据查询 注意:本功能的SQL语句需要有对应统计记录数方法 命名规则为:功能SQL语句名称+Stat
     * 
     * @param sql
     * @param args
     * @param pageSize
     * @param pageNum
     * @return
     */
    public Page queryForPageList(String sql, QueryCondition args,
            PageParameters pageParameters) {
        int pageNum = pageParameters.getPageIndex();
        int pageSize = pageParameters.getPageSize();
        Integer count = 0;
        if(sql==null){
        	sql = FIND_BY_CONDITION;
        }
        List<T> lst = null;
        if (pageNum == PageParameters.NO_PAGINATION
                || pageSize == PageParameters.NO_PAGINATION) {
            lst = this.getSqlRunner().queryForList(sql, args);
        } else {
        	try{
	            count = (Integer) this.getSqlRunner().queryForObject(getStatementName(sql+"Stat"),args);
	            //防止pageNum超过最大值
	            int pageCount = getPageCount(count, pageSize);
	            if(pageNum > pageCount){
	                pageNum = pageCount;
	                pageParameters.setPageIndex(pageNum);
	            }
	            lst = this.getSqlRunner().queryForList(getStatementName(sql), args,
	                    (pageNum - 1) * pageSize, pageSize);
        	}catch(Exception ex){
        		logger.error("sql执行错误：", ex);
        		throw new DataAccessException(ex);
        	}
        }
        pageParameters.setRecordCount(count);
        return new Page(pageParameters, lst);
    }
    
    /**
     * 提供分页功能的数据查询 注意:本功能的SQL语句需要有对应统计记录数方法 命名规则为:功能SQL语句名称+Stat
     * 
     * @param sql
     * @param args
     * @param pageSize
     * @param pageNum
     * @return
     */
    public Page queryForPage(QueryCondition args,
            PageParameters pageParameters) {
        int pageNum = pageParameters.getPageIndex();
        int pageSize = pageParameters.getPageSize();
        Integer count = 0;
        String sql = FIND_BY_CONDITION;
        List<T> lst = null;
        if (pageNum == PageParameters.NO_PAGINATION
                || pageSize == PageParameters.NO_PAGINATION) {
            lst = this.getSqlRunner().queryForList(sql, args);
        } else {
        	try{
	            count = (Integer) this.getSqlRunner().queryForObject(getStatementName(sql+"Stat"),args);
	            //防止pageNum超过最大值
	            int pageCount = getPageCount(count, pageSize);
	            if(pageNum > pageCount){
	                pageNum = pageCount;
	                pageParameters.setPageIndex(pageNum);
	            }
	            lst = this.getSqlRunner().queryForList(getStatementName(sql), args,
	                    (pageNum - 1) * pageSize, pageSize);
        	}catch(Exception ex){
        		logger.error("sql执行错误：", ex);
        		throw new DataAccessException(ex);
        	}
        }
        pageParameters.setRecordCount(count);
        return new Page(pageParameters, lst);
    }
    
    public void setEnableLimit(boolean enableLimit) {
        if (sqlExecutor instanceof LimitSqlExecutor) {
            ((LimitSqlExecutor) sqlExecutor).setEnableLimit(enableLimit);
        }
    }
    
    /**
     * 计算页数
     * @param recordCount
     * @param pageSize
     * @return
     */
    private int getPageCount(int recordCount, int pageSize) {
        if(recordCount == 0)
            return 0;
        int pageCount = recordCount/pageSize;
        if(recordCount%pageSize > 0)
            pageCount ++;
        return pageCount;
    }
    
    /** 根据条件查询个数
	 * @param qc
	 * @return 个数
	 * @author sea
	 */
	public Integer countByCondition(QueryCondition qc){
		try{
			return (Integer) this.getSqlRunner().queryForObject(getStatementName(COUNT_BY_CONDITION),qc);
		}catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
	}
	
	/**
	 * 批量新增 
	 * @Title: batchInsert 
	 * @Description: 批量新增 
	 * @return void    返回类型 
	 */
	public void batchInsert(final List<T> entitys){
		batchInsert(INSERT, entitys);
	}
	
	/**
	 * 批量新增 
	 * @Title: batchInsert 
	 * @Description: 批量新增 
	 * @return void    返回类型 
	 */
	public void batchInsert(final String statementName, final List<T> entitys){
		SqlMapClientCallback<Object> callback = new SqlMapClientCallback<Object>() {
            public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
                executor.startBatch();
                for (T entity : entitys) { 
                	//设置主键
                	if( entity instanceof IdEntity ) {
            			((IdEntity<PK>)entity).prePersistEntity(BaseIbatisDAO.this.getNamespace());
            		}
                    executor.insert(getStatementName(statementName), entity); // statement在*MapSql.xml一条语句的id    
                }
                executor.executeBatch();           
                return null;
            }
		};
		this.getSqlRunner().execute(callback);
	}
	
	/**
	 * 批量修改
	 * @Title: batchInsert 
	 * @Description: 批量新增 
	 * @return void    返回类型 
	 */
	public void batchUpdate(final List<T> entitys){
		batchUpdate(UPDATE,entitys);
	}
	
	/**
	 * 批量修改
	 * @Title: batchInsert 
	 * @Description: 批量新增 
	 * @return void    返回类型 
	 */
	public void batchUpdate(final String statementName, final List<T> entitys){
		SqlMapClientCallback<Object> callback = new SqlMapClientCallback<Object>() {
            public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
                executor.startBatch();
                for (T entity : entitys) {               
                    executor.update(getStatementName(statementName), entity); // statement在*MapSql.xml一条语句的id    
                }
                executor.executeBatch();           
                return null;
            }
		};
		this.getSqlRunner().execute(callback);
	}
	
	/**
	 * 批量删除
	 * @Title: batchInsert 
	 * @Description: 批量新增 
	 * @return void    返回类型 
	 */
	public void batchDelete(final List<T> entitys){
		batchDelete(DELETE,entitys);
	}
	
	/**
	 * 批量删除
	 * @Title: batchInsert 
	 * @Description: 批量新增 
	 * @return void    返回类型 
	 */
	public void batchDelete(final String statementName, final List<T> entitys){
		SqlMapClientCallback<Object> callback = new SqlMapClientCallback<Object>() {
            public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
                executor.startBatch();
                for (T entity : entitys) {               
                    executor.delete(getStatementName(statementName), entity); // statement在*MapSql.xml一条语句的id    
                }
                executor.executeBatch();           
                return null;
            }
		};
		this.getSqlRunner().execute(callback);
	}
	
    /** 根据条件查询个数
	 * @param qc
	 * @return 个数
	 * @author sea
	 */
	public Integer countByCondition(String sqlName, QueryCondition qc){
		try{
			return (Integer) this.getSqlRunner().queryForObject(getStatementName(sqlName),qc);
		}catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
	}
	
	/** 根据条件查询个数
	 * @param qc
	 * @return 个数
	 * @author sea
	 */
	public Integer countByCondition(String sqlName, Object qc){
		try{
			return (Integer) this.getSqlRunner().queryForObject(getStatementName(sqlName),qc);
		}catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
	}
	
	 /** 根据条件查询个数
	 * @param qc
	 * @return 个数
	 * @author sea
	 */
	public Integer countByCondition(String sqlName, Map<String,?> params){
		try{
			return (Integer) this.getSqlRunner().queryForObject(getStatementName(sqlName),params);
		}catch(Exception ex){
			logger.error("sql执行错误：", ex);
			throw new DataAccessException(ex);
		}
	}
}
