package com.tmt.base.common.exception;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import com.tmt.base.common.utils.ExceptionUtil;

/**
 * 异常信息处理类,默认是不会打印springmvc 内部的错误信息，
 * 我统一打印出来
 * @author liFeng
 * 2014年6月23日
 */
public class DefaultExceptionHandler extends DefaultHandlerExceptionResolver {

	private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);
	
	private String defaultErrorView;//默认的错误页面  
	private int defaultErrorStatus;
	private List<HttpMessageConverter<?>> messageConverters;//jSon消息转化器

	@Override
	protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
		//先打印异常
		String msg = ExceptionUtil.getExceptionMessage(ex);
		logger.error(msg);
		//处理默认的异常信息,这部分信息放到web.xml 中配置出来,进行管理 主要是：404 和 400
		//默认的异常是用：sendError来处理,会跳转到一个新的页面,通过异常码来展示错误的页面
		ModelAndView modelAndView = super.doResolveException(request, response, handler, ex);
		if(modelAndView != null && modelAndView.getView() == null) {//如果默认的异常
			return modelAndView;
		}
		if( modelAndView == null) {
			modelAndView = new ModelAndView();
		}
		//页面访问的异常
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("msg", msg);
		modelAndView.addAllObjects(model);
		Method method = null;
		//Ajax 访问的异常
		if( handler instanceof HandlerMethod ) {
			method = ((HandlerMethod)handler).getMethod();
			//是否是异步读取的异常  -- 不处理这个异常 500
			ResponseBody responseBodyAnn = AnnotationUtils.findAnnotation(method, ResponseBody.class);
			if (responseBodyAnn != null) {  
				return handleResponseBody(modelAndView, request, response);
			}
		}
		model.put("ex", ex);
		modelAndView.setViewName(defaultErrorView);
		return modelAndView;
	}
	
	/**
	 * Ajax 异常 默认的异常码是500，这样jQuery Ajax 才能 调用error
	 * @param returnValue
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings({"rawtypes", "unchecked", "resource" })
	private ModelAndView handleResponseBody(ModelAndView returnValue, HttpServletRequest request, HttpServletResponse response){
		//设置默认的错误状态吗
        response.setStatus(defaultErrorStatus);
        
		Map value = returnValue.getModelMap();  
        HttpInputMessage inputMessage = new ServletServerHttpRequest(request);  
        List<MediaType> acceptedMediaTypes = inputMessage.getHeaders().getAccept();  
        if (acceptedMediaTypes.isEmpty()) {  
            acceptedMediaTypes = Collections.singletonList(MediaType.ALL);  
        }  
        MediaType.sortByQualityValue(acceptedMediaTypes);  
        HttpOutputMessage outputMessage = new ServletServerHttpResponse(response);  
        Class<?> returnValueType = value.getClass();  
        List<HttpMessageConverter<?>> messageConverters = this.getMessageConverters();  
        try{
        	if (messageConverters != null) {  
                for (MediaType acceptedMediaType : acceptedMediaTypes) {  
                    for (HttpMessageConverter messageConverter : messageConverters) {  
                        if (messageConverter.canWrite(returnValueType, acceptedMediaType)) {  
                            messageConverter.write(value, acceptedMediaType, outputMessage);  
                            return new ModelAndView();  
                        }  
                    }  
                }  
            }
        }catch(Exception e){
        	try {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "服务器错误");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        }
        return returnValue; 
	}
	
	public String getDefaultErrorView() {  
        return defaultErrorView;  
    }  
  
    public void setDefaultErrorView(String defaultErrorView) {  
        this.defaultErrorView = defaultErrorView;  
    }  
    
    public int getDefaultErrorStatus() {
		return defaultErrorStatus;
	}

	public void setDefaultErrorStatus(int defaultErrorStatus) {
		this.defaultErrorStatus = defaultErrorStatus;
	}
    
    public List<HttpMessageConverter<?>> getMessageConverters() {
		return messageConverters;
	}

	public void setMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
		this.messageConverters = messageConverters;
	}
	
}
