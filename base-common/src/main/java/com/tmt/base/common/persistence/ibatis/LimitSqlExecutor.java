package com.tmt.base.common.persistence.ibatis;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibatis.sqlmap.engine.execution.SqlExecutor;
import com.ibatis.sqlmap.engine.mapping.statement.RowHandlerCallback;
import com.ibatis.sqlmap.engine.scope.StatementScope;
import com.tmt.base.common.persistence.BaseIbatisDAO;

public class LimitSqlExecutor extends SqlExecutor {
	
	private static Logger logger = LoggerFactory.getLogger(BaseIbatisDAO.class);
    
    private Dialect dialect;

    private boolean enableLimit = true;

    public Dialect getDialect() {
        return dialect;
    }

    public void setDialect(Dialect dialect) {
        this.dialect = dialect;
    }

    public boolean isEnableLimit() {
        return enableLimit;
    }

    public void setEnableLimit(boolean enableLimit) {
        this.enableLimit = enableLimit;
    }
    
    /**
     * Long form of the method to execute a query
     *
     * @param statementScope     - the request scope
     * @param conn        - the database connection
     * @param sql         - the SQL statement to execute
     * @param parameters  - the parameters for the statement
     * @param skipResults - the number of results to skip
     * @param maxResults  - the maximum number of results to return
     * @param callback    - the row handler for the query
     * @throws SQLException - if the query fails
     */
    public void executeQuery(StatementScope requestScope, Connection connection, String sql,
            Object[] parameters, int skipResults, int maxResults, RowHandlerCallback callback)
            throws SQLException {
        if ((skipResults != NO_SKIPPED_RESULTS || maxResults != NO_MAXIMUM_RESULTS)
                && supportsLimit()) {
        	
            sql = dialect.getLimitString(sql, skipResults, maxResults);
            logger.debug(sql);
            
//            //最后两个参数是分页参数
//            Object[] copyParams = null;
//    		if( parameters != null) {
//    			copyParams = Arrays.copyOf(parameters, parameters.length + 2);
//    		} else {
//    			copyParams = new Object[2];
//    		}
//    		
//			copyParams[copyParams.length-2] = maxResults;
//			copyParams[copyParams.length-1] = skipResults;
//			
//			parameters = copyParams;
			
            skipResults = NO_SKIPPED_RESULTS;
            maxResults = NO_MAXIMUM_RESULTS;
        }
        super.executeQuery(requestScope, connection, sql, parameters, skipResults,
                maxResults, callback);
    }
    
    public boolean supportsLimit() {
        if (enableLimit && dialect != null) {
            return dialect.supportsLimit();
        }
        return false;
    }

}
