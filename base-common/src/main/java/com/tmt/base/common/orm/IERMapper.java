package com.tmt.base.common.orm;

import java.util.Map;

/**
 * @author LL
 *
 */
public interface IERMapper {
	Map<String,String> loadMaps(Class<?> clazz);
}
