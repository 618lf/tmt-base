package com.tmt.base.common.persistence;

import org.apache.commons.lang3.StringUtils;

public class PageParameters {
	
	// 当前页面的页数
	private int pageIndex;
	// 页面大小
	private int pageSize;
	// 数据总量
	private int recordCount = -1;
	/** 排序参数 */
    private String sortField;
    /** 排序类型 */
    private String sortType;
    
    /** 排序名称(数据库中统一管理排序) */
    private String sortName;
    
    public final static String DESC = "desc";
    
    public final static String ASC = "asc";

	// 默认当前页面页数为第一页
	private static final int DEFAULT_PAGE_INDEX = 1;
	
	// 默认页面大小为10
	public static final int DEFAULT_PAGE_SIZE = 15;
	
	//表示不分页
	public static final int NO_PAGINATION = -1;
	
	
	private int first;// 首页索引
	private int last;// 尾页索引
	private int prev;// 上一页索引
	private int next;// 下一页索引
	
	private int length = 8;// 显示页面长度
	private int slider = 1;// 前后显示页面长度
	
	private String funcName = "page"; // 设置点击页码调用的js函数名称，默认为page，在一页有多个分页对象时使用。
		
    private String message = ""; // 设置提示消息，显示在“共n条”之后
	
	/**
	 * 以默认当前页面和页面大小构造一个分页对象。 其中，默认当前页数为1，默认页面大小为18。
	 */
	public PageParameters() {
		this.pageIndex = DEFAULT_PAGE_INDEX;
		this.pageSize = DEFAULT_PAGE_SIZE;
	}
	
	public boolean isOrderEmptiy(){
		return StringUtils.isEmpty(this.getSortField())||StringUtils.isEmpty(this.getSortType());
	}
	
	/**
	 * 以指定的当前页面页数和页面大小构造一个分页对象。
	 * 
	 * @param pageIndex
	 *            当前页数，若参数值不大于0，则使用默认值1。
	 * @param pageSize
	 *            页面大小，若参数值不大于0，则使用默认值10。
	 */
	public PageParameters(int pageIndex, int pageSize,int recordCount) {
		this.pageIndex = pageIndex > 0 ? pageIndex : DEFAULT_PAGE_INDEX;
		this.pageSize = pageSize > 0 ? pageSize : DEFAULT_PAGE_SIZE;
		this.recordCount = recordCount;
	}

    public String getSortField() {
        return sortField;
    }
    
    public void setSortField(String sortFields) {
        this.sortField = sortFields;
    }
    
    public String getSortType() {
        return sortType;
    }
    
    public void setSortType(String orderType) {
        this.sortType = orderType;
    }
    
    public String getSortName() {
        return sortName;
    }
    
    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

	
	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageCount() {
		if(this.recordCount == 0)
			return 0;
		int pageCount = this.recordCount/this.pageSize;
		if(this.recordCount%this.pageSize > 0)
			pageCount ++;
		return pageCount;
	}
	
	public int getRecordCount() {
		return recordCount;
	}
	
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

	/**
	 * 初始化参数
	 */
	public void initialize(){
				
		this.first = 1;
		
		this.last = (int)(recordCount / (this.pageSize < 1 ? 20 : this.pageSize) + first - 1);
		
		if (this.recordCount % this.pageSize != 0 || this.last == 0) {
			this.last++;
		}

		if (this.last < this.first) {
			this.last = this.first;
		}
		
		if (this.pageIndex <= 1) {
			this.pageIndex = this.first;
		}

		if (this.pageIndex >= this.last) {
			this.pageIndex = this.last;
		}

		if (this.pageIndex < this.last - 1) {
			this.next = this.pageIndex + 1;
		} else {
			this.next = this.last;
		}

		if (this.pageIndex > 1) {
			this.prev = this.pageIndex - 1;
		} else {
			this.prev = this.first;
		}
		
		//2
		if (this.pageIndex < this.first) {// 如果当前页小于首页
			this.pageIndex = this.first;
		}

		if (this.pageIndex > this.last) {// 如果当前页大于尾页
			this.pageIndex = this.last;
		}
	}
	
	/**
	 * 分页条
	 * @return
	 */
	public String getPagination(){
        
		initialize();
		
		StringBuilder sb = new StringBuilder();
		
		if (pageIndex == first) {// 如果是首页
			sb.append("<li class=\"disabled\"><a href=\"javascript:\">&#171; 上一页</a></li>\n");
		} else {
			sb.append("<li><a href=\"javascript:"+funcName+"("+prev+","+pageSize+");\">&#171; 上一页</a></li>\n");
		}

		int begin = pageIndex - (length / 2);

		if (begin < first) {
			begin = first;
		}

		int end = begin + length - 1;

		if (end >= last) {
			end = last;
			begin = end - length + 1;
			if (begin < first) {
				begin = first;
			}
		}

		if (begin > first) {
			int i = 0;
			for (i = first; i < first + slider && i < begin; i++) {
				sb.append("<li><a href=\"javascript:"+funcName+"("+i+","+pageSize+");\">"
						+ (i + 1 - first) + "</a></li>\n");
			}
			if (i < begin) {
				sb.append("<li class=\"disabled\"><a href=\"javascript:\">...</a></li>\n");
			}
		}

		for (int i = begin; i <= end; i++) {
			if (i == pageIndex) {
				sb.append("<li class=\"active\"><a href=\"javascript:\">" + (i + 1 - first)
						+ "</a></li>\n");
			} else {
				sb.append("<li><a href=\"javascript:"+funcName+"("+i+","+pageSize+");\">"
						+ (i + 1 - first) + "</a></li>\n");
			}
		}

		if (last - end > slider) {
			sb.append("<li class=\"disabled\"><a href=\"javascript:\">...</a></li>\n");
			end = last - slider;
		}

		for (int i = end + 1; i <= last; i++) {
			sb.append("<li><a href=\"javascript:"+funcName+"("+i+","+pageSize+");\">"
					+ (i + 1 - first) + "</a></li>\n");
		}

		if (pageIndex == last) {
			sb.append("<li class=\"disabled\"><a href=\"javascript:\">下一页 &#187;</a></li>\n");
		} else {
			sb.append("<li><a href=\"javascript:"+funcName+"("+next+","+pageSize+");\">"
					+ "下一页 &#187;</a></li>\n");
		}
		sb.append("<li class=\"disabled controls\"><a href=\"javascript:\">当前 ");
		sb.append("<input type=\"text\" value=\""+pageIndex+"\" onkeypress=\"var e=window.event||this;var c=e.keyCode||e.which;if(c==13)");
		sb.append(funcName+"(this.value,"+pageSize+");\" onclick=\"this.select();\"/> / ");
		sb.append("<input type=\"text\" value=\""+this.getPageCount()+"\" readonly='readonly' onkeypress=\"var e=window.event||this;var c=e.keyCode||e.which;if(c==13)");
		sb.append(funcName+"("+pageIndex+",this.value);\" onclick=\"this.select();\"/> 页，");
		sb.append("共 " + recordCount + " 条"+(message!=null?message:"")+"</a><li>\n");
		sb.insert(0,"<ul class='pagination'>\n").append("</ul>\n");
		sb.append("<div style=\"clear:both;\"></div>");

		return sb.toString();
	}
	
	public String getSimplePagination(){
        initialize();
		
		StringBuilder sb = new StringBuilder();
		
		if (pageIndex == first) {// 如果是首页
			sb.append("<li class=\"disabled\"><a href=\"javascript:\">&#171; 上一页</a></li>\n");
		} else {
			sb.append("<li><a href=\"javascript:"+funcName+"("+prev+","+pageSize+");\">&#171; 上一页</a></li>\n");
		}

		int begin = pageIndex - (length / 2);

		if (begin < first) {
			begin = first;
		}

		int end = begin + length - 1;

		if (end >= last) {
			end = last;
			begin = end - length + 1;
			if (begin < first) {
				begin = first;
			}
		}

		if (begin > first) {
			int i = 0;
			for (i = first; i < first + slider && i < begin; i++) {
				sb.append("<li><a href=\"javascript:"+funcName+"("+i+","+pageSize+");\">"
						+ (i + 1 - first) + "</a></li>\n");
			}
			if (i < begin) {
				sb.append("<li class=\"disabled\"><a href=\"javascript:\">...</a></li>\n");
			}
		}

		for (int i = begin; i <= end; i++) {
			if (i == pageIndex) {
				sb.append("<li class=\"active\"><a href=\"javascript:\">" + (i + 1 - first)
						+ "</a></li>\n");
			} else {
				sb.append("<li><a href=\"javascript:"+funcName+"("+i+","+pageSize+");\">"
						+ (i + 1 - first) + "</a></li>\n");
			}
		}

		if (last - end > slider) {
			sb.append("<li class=\"disabled\"><a href=\"javascript:\">...</a></li>\n");
			end = last - slider;
		}

		for (int i = end + 1; i <= last; i++) {
			sb.append("<li><a href=\"javascript:"+funcName+"("+i+","+pageSize+");\">"
					+ (i + 1 - first) + "</a></li>\n");
		}

		if (pageIndex == last) {
			sb.append("<li class=\"disabled\"><a href=\"javascript:\">下一页 &#187;</a></li>\n");
		} else {
			sb.append("<li><a href=\"javascript:"+funcName+"("+next+","+pageSize+");\">"
					+ "下一页 &#187;</a></li>\n");
		}
		sb.insert(0,"<ul class='pagination'>\n").append("</ul>\n");
		sb.append("<div style=\"clear:both;\"></div>");

		return sb.toString();
	}
}
