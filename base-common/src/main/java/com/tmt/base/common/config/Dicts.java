package com.tmt.base.common.config;

public class Dicts {
	/** 邮件服务器相关  - 用于配置发送邮件的用户名和密码 */
	public static final String COM_SYSTEM_MAIL_HOST = "com.system.config.mail.host";
	public static final String COM_SYSTEM_MAIL_USERNAME = "com.system.config.mail.username";
	public static final String COM_SYSTEM_MAIL_PASSWORD = "com.system.config.mail.password";
	/** 定时任务服务类Key */
	public static final String COM_SYSTEM_TASK_ITEMS = "com.system.config.task.items";
}
