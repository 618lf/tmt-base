package com.tmt.base.common.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;

/**
 * @author TMT
 * 封装了公用操作数据的，但查询的只能在子类内部使用
 * 目的是不让子类外部直接传入sql就可以查询，应在子类内部实现一个sql的方法，在方法中
 * 调用基础的方式来查询数据
 */
public abstract class BaseService<T, PK> {
	
	protected static Logger logger = LoggerFactory.getLogger(BaseService.class);
    
    /**
     * 在子类实现此函数,为下面的CRUD操作提供DAO.
     */
    protected abstract BaseIbatisDAO<T, PK> getEntityDao();
    
    @Transactional
    public PK insert(final T entity) {
        return getEntityDao().insert(entity);
    }
    @Transactional
    public int update(final T entity) {
        return getEntityDao().update(entity);
    }
    @Transactional
    public int updateSelective(final T entity) {
        return getEntityDao().updateSelective(entity);
    }
    @Transactional
    public int delete(final T entity) {
        return getEntityDao().delete(entity);
    }
    public Page queryForPage(QueryCondition qc, PageParameters param) {
    	return this.getEntityDao().queryForPage(qc, param);
    }
    public T get(final PK id) {
        return getEntityDao().findByPk(id);
    }
    public List<T> findAll() {
        return getEntityDao().findAll();
    }
    @Transactional
	public void batchUpdate( List<T> entities ){
		this.getEntityDao().batchUpdate(entities);
	}
    @Transactional
	public void batchInsert( List<T> entities ){
		this.getEntityDao().batchInsert(entities);
	}
    @Transactional
	public void batchDelete( List<T> entities ){
		this.getEntityDao().batchDelete(entities);
	}
    public List<T> queryByCondition(QueryCondition qc ){
    	return getEntityDao().queryByCondition(qc);
    }
    //**********下列方法只能在子类内部使用****************
    @Transactional
    protected int update( String statementName, T entity) {
    	return getEntityDao().update(statementName , entity);
    }
    protected Page queryForPageList(String sql, QueryCondition qc, PageParameters param) {
    	return this.getEntityDao().queryForPageList(sql, qc, param);
    }
    protected Page queryForPageList(String sql, Map<String,?> qc, PageParameters param) {
    	return this.getEntityDao().queryForPageList(sql, qc, param);
    }
    protected Page queryForMapPageList(String sql, Map<String,?> qc, PageParameters param) {
    	return this.getEntityDao().queryForMapPageList(sql, qc, param);
    }
    /** 根据条件查询个数
	 * @param qc
	 * @return 个数
	 * @author sea
	 */
	protected Integer countByCondition(QueryCondition qc){
		return   this.getEntityDao().countByCondition(qc);
	}
	
	/** 根据条件查询个数
	 * @param qc
	 * @return 个数
	 * @author sea
	 */
	protected Integer countByCondition(String sql,Object qc){
		return   this.getEntityDao().countByCondition(sql,qc);
	}
	
    /** 根据条件查询个数
	 * @param qc
	 * @return 个数
	 * @author sea
	 */
	protected Integer countByCondition(String sql,QueryCondition qc){
		return   this.getEntityDao().countByCondition(sql,qc);
	}
	
	/** 根据条件查询个数
	 * @param qc
	 * @return 个数
	 * @author sea
	 */
	protected Integer countByCondition(String sql, Map<String,?> params){
		return   this.getEntityDao().countByCondition(sql,params);
	}
	
	protected List<T> queryForList( String statementName, Map<String,?> params ){
		return this.getEntityDao().queryForList(statementName, params);
	}
	
	protected List<T> queryForList( String statementName, QueryCondition qc ){
		return this.getEntityDao().queryForList(statementName, qc);
	}
	
	protected List<T> queryForList( String statementName, Object o ){
		return this.getEntityDao().queryForList(statementName, o);
	}
	
	protected <E> List<E> queryForGenericsList( String statementName, Object o ){
		return this.getEntityDao().queryForGenericsList(statementName, o);
	}
	
	protected <E> List<E> queryForGenericsList( String statementName, Map<String,?> params ){
		return this.getEntityDao().queryForGenericsList(statementName, params);
	}
	
	protected List<Map<String,Object>> queryForMapList( String statementName, Map<String,?> params ){
		return this.getEntityDao().queryForMapList(statementName, params);
	}
	
	protected T queryForObject( String statementName, Object params ){
		return this.getEntityDao().queryForObject(statementName, params);
	}
	
	protected T queryForObject( String statementName, Map<String,?> params ){
		return this.getEntityDao().queryForObject(statementName, params);
	}
	
	protected T queryForObject( String statementName, QueryCondition qc ){
		return this.getEntityDao().queryForObject(qc);
	}
	
	@Transactional
	protected void batchDelete(String sql, List<T> entities ){
		this.getEntityDao().batchDelete(sql, entities);
	}
	
	@Transactional
	protected void batchInsert(String sql, List<T> entities ){
		this.getEntityDao().batchInsert(sql, entities);
	}
	
	@Transactional
	protected void batchUpdate(String sql,  List<T> entities ){
		this.getEntityDao().batchUpdate(sql, entities);
	}
}
