package com.tmt.base.common.persistence.datasource;

import java.util.Map;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.jdbc.datasource.lookup.DataSourceLookup;

/**
 * 动态数据源
 * @author liFeng
 * 2014年6月23日
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

	private Object defaultTargetDataSourceBak;
	
	@Override
	protected Object determineCurrentLookupKey() {
		return DataSourceHolder.getDataSourceType()== null?this.defaultTargetDataSourceBak:DataSourceHolder.getDataSourceType();
	}
	
	public void setDataSourceLookup(DataSourceLookup dataSourceLookup) {
		super.setDataSourceLookup(dataSourceLookup);
	}
	
	public void setDefaultTargetDataSource(Object defaultTargetDataSource) {
		super.setDefaultTargetDataSource(defaultTargetDataSource);
		this.defaultTargetDataSourceBak = defaultTargetDataSource;
	}

	
	public void setTargetDataSources(Map<Object, Object> targetDataSources) {
		super.setTargetDataSources(targetDataSources);
	}

}
