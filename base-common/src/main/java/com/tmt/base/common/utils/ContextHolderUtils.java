package com.tmt.base.common.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.tmt.base.common.config.Globals;
/**
* @ClassName: ContextHolderUtils 
* @Description: TODO(上下文工具类) 
* @author lifeng 
* @date 2012-12-15 下午11:27:39 
*
 */
public class ContextHolderUtils {
	
	/**
	 * SpringMvc下获取request
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return request;
	}
	/**
	 * SpringMvc下获取session
	 * 
	 * @return
	 */
	public static HttpSession getSession() {
		HttpSession session = getRequest().getSession();
		return session;
	}
	
	public static String getWebRoot() {
		return ContextHolderUtils.getSession().getServletContext().getContextPath();
	}
	
	/**
	 *  WebRoot + Globals.ADMIN_PATH
	 * @return
	 */
	public static String getCtx() {
		return ContextHolderUtils.getSession().getServletContext().getContextPath() + Globals.getAdminPath();
	}
}
