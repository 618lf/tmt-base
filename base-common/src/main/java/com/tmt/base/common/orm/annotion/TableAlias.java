package com.tmt.base.common.orm.annotion;

/**
 * 定义实体对应的表别名
 * @author LL
 *
 */
@java.lang.annotation.Target(value={java.lang.annotation.ElementType.TYPE})
@java.lang.annotation.Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Documented
public abstract @interface TableAlias{
  public abstract java.lang.String value() default "";
}
