package com.tmt.base.common.orm;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.tmt.base.common.persistence.QueryCondition.Criteria;

/**
 * @author LLS
 *
 * @param <T>
 */
public class QueryItem<T>{
	@Autowired
	private static final ERRelation relation;
	
	static{
		WebApplicationContext context = ContextLoader.getCurrentWebApplicationContext();
		relation = context.getBean(ERRelation.class);
	}
	
	private Class<T> clazz;
	
	private Criteria criteria;
	
	public QueryItem(Criteria criteria,Class<T> clazz){
		this.criteria = criteria;
		this.clazz=clazz;
		
	}
	
	public boolean isValid() {
		return criteria.isValid();
	}

	
	public List<String> getCriteriaWithoutValue() {
		return criteria.getCriteriaWithoutValue();
	}

	
	public List<Map<String, Object>> getCriteriaWithSingleValue() {
		return criteria.getCriteriaWithSingleValue();
	}

	
	public List<Map<String, Object>> getCriteriaWithListValue() {
		return criteria.getCriteriaWithListValue();
	}

	
	public List<Map<String, Object>> getCriteriaWithBetweenValue() {
		return criteria.getCriteriaWithBetweenValue();
	}

	
	public Criteria andIsNull(String property) {
		return criteria.andIsNull(relation.getFieldNameByProperty(clazz, property));
	}

	
	public Criteria andIsNotNull(String property) {
		return criteria.andIsNotNull(relation.getFieldNameByProperty(clazz, property));
	}

	
	public Criteria andEqualTo(String property, Object value) {
		return criteria.andEqualTo(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andNotEqualTo(String property, Object value) {
		return criteria.andNotEqualTo(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andGreaterThan(String property, Object value) {
		return criteria.andGreaterThan(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andGreaterThanOrEqualTo(String property,
			Object value) {
		return criteria.andGreaterThanOrEqualTo(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andLessThan(String property, Object value) {
		return criteria.andLessThan(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andLessThanOrEqualTo(String property,
			Object value) {
		return criteria.andLessThanOrEqualTo(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andIn(String property, List<Object> values) {
		return criteria.andIn(relation.getFieldNameByProperty(clazz, property), property, values);
	}

	
	public Criteria andNotIn(String property, List<Object> values) {
		return criteria.andNotIn(relation.getFieldNameByProperty(clazz, property), property, values);
	}

	
	public Criteria andBetween(String property, Object value1,
			Object value2) {
		return criteria.andBetween(relation.getFieldNameByProperty(clazz, property), property, value1, value2);
	}

	
	public Criteria andNotBetween(String property,
			Object value1, Object value2) {
		return criteria.andNotBetween(relation.getFieldNameByProperty(clazz, property), property, value1, value2);
	}

	
	public Criteria andLike(String property, Object value) {
		return criteria.andLike(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andNotLike(String property, Object value) {
		return criteria.andNotLike(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andDateEqualTo(String property, Date value) {
		return criteria.andDateEqualTo(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andDateNotEqualTo(String property, Date value) {
		return criteria.andDateNotEqualTo(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andDateGreaterThan(String property,
			Date value) {
		return criteria.andDateGreaterThan(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andDateGreaterThanOrEqualTo(String property,
			Date value) {
		return criteria.andDateGreaterThanOrEqualTo(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andDateLessThan(String property, Date value) {
		return criteria.andDateLessThan(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andDateLessThanOrEqualTo(String property,
			Date value) {
		return criteria.andDateLessThanOrEqualTo(relation.getFieldNameByProperty(clazz, property), property, value);
	}

	
	public Criteria andDateIn(String property, List<Date> values) {
		return criteria.andDateIn(relation.getFieldNameByProperty(clazz, property), property, values);
	}

	
	public Criteria andDateNotIn(String property,
			List<Date> values) {
		return criteria.andDateNotIn(relation.getFieldNameByProperty(clazz, property), property, values);
	}

	
	public Criteria andDateBetween(String property, Date value1,
			Date value2) {
		return criteria.andDateBetween(relation.getFieldNameByProperty(clazz, property), property, value1, value2);
	}

	
	public Criteria andDateNotBetween(String property,
			Date value1, Date value2) {
		return criteria.andDateNotBetween(relation.getFieldNameByProperty(clazz, property), property, value1, value2);
	}
	
}
