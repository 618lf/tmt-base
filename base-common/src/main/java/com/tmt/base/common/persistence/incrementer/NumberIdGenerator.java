package com.tmt.base.common.persistence.incrementer;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.LinkedList;

import javax.sql.DataSource;

import org.springframework.jdbc.support.JdbcUtils;

import com.tmt.base.common.exception.DataAccessException;
import com.tmt.base.common.utils.DataSourceUtils;

/**
 * 利用 sys_id_store 来记录主键的增长
 * @author liFeng
 * 2014年6月9日
 */
public class NumberIdGenerator implements DateSourceIdGenerator{

	/**数据表主键缓存*/
	private static HashMap<String,LinkedList<Long>> keyCache = new HashMap<String,LinkedList<Long>>();
	private static final String VALUE_SQL = "{? = call F_NEXT_VALUE(?) }";
	private static final String VALUE_INIT_SQL = "INSERT INTO SYS_ID_STORE(TABLE_NAME,MIN_VALUE,CURRENT_VALUE,MAX_VALUE,STEP,REMARK) VALUES(?,?,?,?,?,?)";
	private static final String STEP_SEQ_SEPARATOR = "-";
	
	private DataSource dataSource;
	
	@Override
	public Serializable generateId( Serializable key) {
		return String.valueOf(getNextKey(String.valueOf(key)));
	}
	
	private synchronized Long getNextKey( String tableName ) {
		String mapKey = tableName.toUpperCase();
		if ( !keyCache.containsKey(mapKey) || keyCache.get(mapKey).isEmpty()) {
			LinkedList<Long> keyList = keyCache.get(mapKey);
			if (keyList== null || keyList.size() == 0) { // 缓存键值已用完
				loadFromDB(mapKey);
			}
		}
		return keyCache.get(mapKey).remove(0);
	}
	
	private void loadFromDB(String tableName){
		Connection con = DataSourceUtils.getConnection(getDataSource());
		CallableStatement stmt = null;
		try {
			stmt = con.prepareCall(VALUE_SQL);
			DataSourceUtils.applyTransactionTimeout(stmt, getDataSource());
			stmt.registerOutParameter(1, Types.VARCHAR);
			stmt.setString(2, tableName);
			stmt.execute();
			String keyAndStep = stmt.getString(1);
			if( keyAndStep != null ) {
				initKeyCache(tableName,keyAndStep);
			} else {
				initTableKey(con, tableName);
				stmt.execute();
				keyAndStep = stmt.getString(1);
				initKeyCache(tableName,keyAndStep);
			}
		}catch (SQLException ex) {
			throw new DataAccessException("Could not call function F_NEXT_VALUE", ex);
		}finally {
			JdbcUtils.closeStatement(stmt);
			DataSourceUtils.releaseConnection(con, getDataSource());
		}
	}
	
	private void initTableKey(Connection con, String tableName){
		PreparedStatement  stmt = null;
		try {
			stmt = con.prepareStatement(VALUE_INIT_SQL);
			stmt.setString(1, tableName);
			stmt.setLong(2, 1L);
			stmt.setLong(3, 0L);
			stmt.setLong(4, 99999999999L);
			stmt.setInt(5, 20);
			stmt.setString(6, "TABLE " + tableName + " ID");
			stmt.executeUpdate();
		} catch (SQLException ex) {
			throw new DataAccessException("Could not insert EM_ID_STORE ,tableName:" + tableName, ex);
		} finally {
			JdbcUtils.closeStatement(stmt);
		}
	}
	
	private void initKeyCache( String keyName, String keyAndStep) {
		String[] seqAndStep = keyAndStep.split(STEP_SEQ_SEPARATOR);
		//更新下一个主键值 当前值加步长
		if (seqAndStep != null && seqAndStep.length == 2) {
			long seq = Long.parseLong(seqAndStep[0]);
			int step = Integer.parseInt(seqAndStep[1]);
			LinkedList<Long> keyList = (LinkedList<Long>) keyCache.get(keyName);
			if (keyList == null) {
				keyList = new LinkedList<Long>();
			}
			// 生成缓存主键值并放在列表里
			for (int i = 0; i < step; i++) {
				keyList.add(seq + i);
			}
			keyCache.put(keyName, keyList);
		}
	}
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public DataSource getDataSource() {
		return this.dataSource;
	}
	
}
