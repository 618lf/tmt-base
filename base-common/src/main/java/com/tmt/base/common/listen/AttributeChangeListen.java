package com.tmt.base.common.listen;

import java.util.EventListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 实体属性变化监听器
 * @author lifeng
 */
public interface AttributeChangeListen<T> extends EventListener{

	public static Logger logger = LoggerFactory.getLogger(EventListener.class);
	/**
	 * 执行属性变化事件
	 * @param event
	 */
	public void fireAttributeChange(AttributeChangeEvent<T> event);
}
