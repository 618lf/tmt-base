package com.tmt.base.common.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.LocaleResolver;

/**
 * 字符串工具类, 继承org.apache.commons.lang3.StringUtils类
 * @author ThinkGem
 * @version 2013-05-22
 */
public class StringUtil3 extends org.apache.commons.lang3.StringUtils {

	/**
	 * 替换掉HTML标签方法
	 */
	public static String replaceHtml(String html) {
		if (isBlank(html)){
			return "";
		}
		String regEx = "<.+?>";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(html);
		String s = m.replaceAll("");
		return s;
	}

	/**
	 * 缩略字符串（不区分中英文字符）
	 * @param str 目标字符串
	 * @param length 截取长度
	 * @return
	 */
	public static String abbr(String str, int length) {
		if (str == null) {
			return "";
		}
		try {
			StringBuilder sb = new StringBuilder();
			int currentLength = 0;
			for (char c : str.toCharArray()) {
				currentLength += String.valueOf(c).getBytes("GBK").length;
				if (currentLength <= length - 3) {
					sb.append(c);
				} else {
					sb.append("...");
					break;
				}
			}
			return sb.toString();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * 转换为Double类型
	 */
	public static Double toDouble(Object val){
		if (val == null){
			return 0D;
		}
		try {
			return Double.valueOf(trim(val.toString()));
		} catch (Exception e) {
			return 0D;
		}
	}

	/**
	 * 转换为Float类型
	 */
	public static Float toFloat(Object val){
		return toDouble(val).floatValue();
	}

	/**
	 * 转换为Long类型
	 */
	public static Long toLong(Object val){
		return toDouble(val).longValue();
	}

	/**
	 * 转换为Integer类型
	 */
	public static Integer toInteger(Object val){
		return toLong(val).intValue();
	}
	
	/**
	 * 获得i18n字符串
	 */
	public static String getMessage(String code, Object[] args) {
		LocaleResolver localLocaleResolver = (LocaleResolver) SpringContextHolder.getBean(LocaleResolver.class);
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
		Locale localLocale = localLocaleResolver.resolveLocale(request);
		return SpringContextHolder.getApplicationContext().getMessage(code, args, localLocale);
	}
	
	/**
	 * 获得用户远程地址
	 */
	public static String getRemoteAddr(HttpServletRequest request){
		String remoteAddr = request.getHeader("X-Real-IP");
        if (isNotBlank(remoteAddr)) {
        	remoteAddr = request.getHeader("X-Forwarded-For");
        }else if (isNotBlank(remoteAddr)) {
        	remoteAddr = request.getHeader("Proxy-Client-IP");
        }else if (isNotBlank(remoteAddr)) {
        	remoteAddr = request.getHeader("WL-Proxy-Client-IP");
        }
        return remoteAddr != null ? remoteAddr : request.getRemoteAddr();
	}
	
	/**
	 * StringBuffer .. append
	 */
	public static StringBuilder appendTo(StringBuilder appendable, Iterable<?> parts) throws IOException{
		return appendTo(appendable, parts.iterator());
	}
	
	/**
	 * StringBuffer .. append
	 */
	public static StringBuilder appendTo(StringBuilder appendable, Object... parts) throws IOException {
	    return appendTo(appendable, Arrays.asList(parts));
	}
	
	/**
	 * StringBuffer .. append
	 */
	public static StringBuilder appendTo(StringBuilder appendable, Object first, Object second, Object... rest)
			throws IOException {
		return appendTo(appendable, iterable(first, second, rest));
	}
	
	/**
	 * StringBuffer .. append
	 */
    public static StringBuilder appendTo(StringBuilder appendable, Iterator<?> parts) throws IOException{
		if (parts.hasNext()) {
			appendable.append(toString(parts.next()));
			while (parts.hasNext()) {
				appendable.append(toString(parts.next()));
			}
		}
		return appendable;
	}
    
    public static CharSequence toString(Object part) {
		return (part instanceof CharSequence) ? (CharSequence) part : part.toString();
	}
    
    private static Iterable<Object> iterable(final Object first, final Object second, final Object[] rest) {
		return new AbstractList<Object>() {
			@Override
			public int size() {
				return rest.length + 2;
			}

			@Override
			public Object get(int index) {
				switch (index) {
				case 0:
					return first;
				case 1:
					return second;
				default:
					return rest[index - 2];
				}
			}
		};
	}
	
	/**
	 * 信息格式化
	 * @param template
	 * @param args
	 * @return
	 */
	public static String format(String template, Object... args) {
		template = String.valueOf(template); // null -> "null"
		StringBuilder builder = new StringBuilder(template.length() + 16* args.length);
		int templateStart = 0;
		int i = 0;
		while (i < args.length) {
			int placeholderStart = template.indexOf("%s", templateStart);
			if (placeholderStart == -1) {
				break;
			}
			builder.append(template.substring(templateStart, placeholderStart));
			builder.append(args[i++]);
			templateStart = placeholderStart + 2;
		}
		builder.append(template.substring(templateStart));

		if (i < args.length) {
			builder.append(" [");
			builder.append(args[i++]);
			while (i < args.length) {
				builder.append(", ");
				builder.append(args[i++]);
			}
			builder.append(']');
		}
		return builder.toString();
	}
	
	/**
	 * 在固定位置插入指定字符
	 * @param postion
	 * @param insert
	 * @return
	 */
	public static String insert(String src, int postion, String insert) {
		if(StringUtil3.isBlank(src) || StringUtil3.isEmpty(insert) || postion < 0){
			return src;
		}
		if( postion > StringUtil3.length(src)) {
			postion = StringUtil3.length(src);
		}
		return new StringBuilder(src).insert(postion, insert).toString();
	}
	
	/**
	 * 在每固定位置插入指定字符
	 * @param postion
	 * @param insert
	 * @return
	 */
	public static String insertEach(String src, int postion, String insert) {
		if(StringUtil3.isBlank(src) || StringUtil3.isEmpty(insert)){
			return src;
		}
		if( postion > StringUtil3.length(src)) {
			postion = StringUtil3.length(src);
		}
		if( postion == 0) {
			return insert(src,postion,insert);
		}
		int each = StringUtil3.length(src) / postion;
		StringBuffer sbSrc = new StringBuffer(src);
		for(int i =1, j = each; i<=j; i++ ){
			sbSrc.insert(i*postion+(i-1), insert);
		}
		return sbSrc.toString();
	}
	
	
}