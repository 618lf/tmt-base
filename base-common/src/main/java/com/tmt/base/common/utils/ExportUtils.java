package com.tmt.base.common.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

/**
 * 文件操作类
 * @ClassName: ExportUtils 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author 李锋
 * @date 2013-3-9 下午04:39:30 
 *
 */
public class ExportUtils {

	
	/**
	 * bDelete -- 下载完之后是否删除 ，对于普通的导出列表 ，建议删除
	 * @Title: downloadFile 
	 * @return void    返回类型
	 */
	public static void downloadFile( String srcFilePath , String srcFileName ,  HttpServletResponse response , String encoding, Boolean bDelete ){
		
		BufferedInputStream objBufferedInputStream = null;
        BufferedOutputStream objBufferedOutputStream = null;
        OutputStream objOutputStream = null;
        InputStream objInputStream = null;
        File objFile = new File(srcFilePath);
        try {
            response.reset();
            response.setHeader("Content-disposition", "attachment;success=true;filename=" + URLEncoder.encode(srcFileName,encoding ));
            response.setContentType("text/html;charset=UTF-8");
            objInputStream = new FileInputStream(objFile);
            objBufferedInputStream = new BufferedInputStream(objInputStream);
            objOutputStream = response.getOutputStream();
            objBufferedOutputStream = new BufferedOutputStream(objOutputStream);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = objBufferedInputStream.read(buffer, 0, 8192)) != -1) {
                objBufferedOutputStream.write(buffer, 0, bytesRead);
            }
            objBufferedOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
				objInputStream.close();
				objBufferedInputStream.close();
	            objOutputStream.close();
	            objBufferedOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			 // 下载文件完成后删除文件
			if(bDelete) {
	            objFile.delete();	
			}
        }
	}
	/**
	 * bDelete -- 下载完之后是否删除 ，对于普通的导出列表 ，建议删除
	 * @Title: downloadFile 
	 * @return void    返回类型
	 */
	public static void downloadFile( File srcFile , String srcFileName ,  HttpServletResponse response , String encoding , Boolean bDelete){
		
		BufferedInputStream objBufferedInputStream = null;
        BufferedOutputStream objBufferedOutputStream = null;
        OutputStream objOutputStream = null;
        InputStream objInputStream = null;
        File objFile = srcFile;
        try {
            response.reset();
            //url encode会将空格转换为+号
            response.setHeader("Content-disposition", "attachment;success=true;filename=" + URLEncoder.encode(srcFileName,encoding ).replaceAll("\\+", ""));
            response.setContentType("text/html;charset=UTF-8");
            objInputStream = new FileInputStream(objFile);
            objBufferedInputStream = new BufferedInputStream(objInputStream);
            objOutputStream = response.getOutputStream();
            objBufferedOutputStream = new BufferedOutputStream(objOutputStream);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = objBufferedInputStream.read(buffer, 0, 8192)) != -1) {
                objBufferedOutputStream.write(buffer, 0, bytesRead);
            }
            objBufferedOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
				objInputStream.close();
				objBufferedInputStream.close();
	            objOutputStream.close();
	            objBufferedOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			 // 下载文件完成后删除文件
			if(bDelete) {
	            objFile.delete();	
			}
        }
	}
}
