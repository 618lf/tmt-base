package com.tmt.base.common.listen;

import java.util.EventObject;

/**
 * 属性变化事件
 * @author lifeng
 *
 */
public class AttributeChangeEvent<T>  extends EventObject{

	private static final long serialVersionUID = 4223088380546027795L;
	
	public AttributeChangeEvent(T source) {
		super(source);
	}
	
	/**
	 * 得到事件源
	 */
	@SuppressWarnings("unchecked")
	public T getSource() {
        return (T) source;
    }
}
