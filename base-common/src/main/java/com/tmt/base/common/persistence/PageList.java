package com.tmt.base.common.persistence;

import java.util.List;

/**
 * 数据库查询翻页结果集
 * @author bob created at 2007-9-15
 */
public interface PageList {
	
	/**
	 * 返回页面参数
	 * @return
	 */
	public PageParameters getParam();

	/**
	 * 返回当前页数据
	 * @return
	 */
	public<T> List<T> getData();
}
