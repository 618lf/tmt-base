package com.tmt.base.common.editor;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.tmt.base.common.config.Globals;
import com.tmt.base.common.utils.DateUtil3;

/**
 * 日期的属性编辑器 -- request 日期的转化
 * 
 * @author liFeng 2014年6月19日
 */
public class DateEditor extends PropertyEditorSupport {
	
	private static final String DEFAULT_DATE_FPRMAT = "yyyy-MM-dd HH:mm:ss";
	private boolean emptyAsNull;
	private String dateFormat = DEFAULT_DATE_FPRMAT;

	public DateEditor(boolean emptyAsNull) {
		this.emptyAsNull = emptyAsNull;
	}

	public DateEditor(boolean emptyAsNull, String dateFormat) {
		this.emptyAsNull = emptyAsNull;
		this.dateFormat = dateFormat;
	}

	public String getAsText() {
		Date localDate = (Date) getValue();
		return localDate != null ? new SimpleDateFormat(this.dateFormat) .format(localDate) : "";
	}

	public void setAsText(String text) {
		if (text == null) {
			setValue(null);
		} else {
			String str = text.trim();
			if ((this.emptyAsNull) && ("".equals(str)))
				setValue(null);
			else
				try {
					setValue(DateUtil3.parseDate(str, Globals.DATE_PATTERNS));
				} catch (ParseException localParseException) {
					setValue(null);
				}
		}
	}
}
