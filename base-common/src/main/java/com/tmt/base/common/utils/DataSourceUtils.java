package com.tmt.base.common.utils;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.util.Assert;

/**
 * 不在同一个线程中获取数据库的连接
 * @author liFeng
 * 2014年6月10日
 */
public class DataSourceUtils extends
		org.springframework.jdbc.datasource.DataSourceUtils {

	private static Logger logger = LoggerFactory.getLogger(DataSourceUtils.class);

	public static Connection getConnection(DataSource dataSource)
			throws CannotGetJdbcConnectionException {
		try {
			Assert.notNull(dataSource, "No DataSource specified");
			logger.debug("Fetching resumed JDBC Connection from DataSource");
			return dataSource.getConnection();
		} catch (SQLException ex) {
			throw new CannotGetJdbcConnectionException(
					"Could not get JDBC Connection", ex);
		}
	}
	
}
