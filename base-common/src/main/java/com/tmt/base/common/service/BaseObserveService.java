package com.tmt.base.common.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.common.listen.AttributeChangeEvent;
import com.tmt.base.common.listen.AttributeChangeListen;

/**
 * 具体监听功能的
 * 主要见监听 数据库操作的方法：例如 insert、update
 * @author lifeng
 *
 * @param <T>
 * @param <PK>
 */
public abstract class BaseObserveService<T, PK> extends BaseService<T, PK>{

	private List<AttributeChangeListen<T>> listens;
	
	/**
	 * 添加监听器
	 * @param listen
	 */
	public synchronized void addAttributeChangeListen( AttributeChangeListen<T> listen) {
		if( listens == null) {
			listens = new ArrayList<AttributeChangeListen<T>>();
		}
		listens.add(listen);
	}
	
	/**
	 * 执行监听事件
	 * @param event
	 */
	private void fireAttributeChange(AttributeChangeEvent<T> event){
		if(listens != null && listens.size() != 0) {
			for( AttributeChangeListen<T> listen: listens){
				listen.fireAttributeChange(event);
			}
		}
	}
	
	// --- 需要监听的CURD方法
	@Transactional
	@Override
	public PK insert(T entity) {
		PK pk = super.insert(entity);
		//定义事件
		AttributeChangeEvent<T> event = new AttributeChangeEvent<T>(entity);
		//执行事件
		this.fireAttributeChange(event);
		return pk;
	}

	@Transactional
	@Override
	public int update(T entity) {
		int iCount = super.update(entity);
		//定义事件
		AttributeChangeEvent<T> event = new AttributeChangeEvent<T>(entity);
		//执行事件
		this.fireAttributeChange(event);
		return iCount;
	}
	
}
