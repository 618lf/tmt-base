package com.tmt.base.common.persistence.incrementer;

import java.io.Serializable;

public interface IdGenerator {

	/**
	 * Generates a new ID
	 * @return
	 */
	Serializable generateId( Serializable key );
	
}
