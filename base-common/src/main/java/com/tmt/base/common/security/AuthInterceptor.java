package com.tmt.base.common.security;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.tmt.base.common.config.Globals;

/**
 * 权限控制  -- 防止访问无权限的页面
 * @author lifeng
 */
public class AuthInterceptor extends HandlerInterceptorAdapter {

	private static Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);
	private String loginPath = Globals.getAdminPath() +"/login";
	
	@Value("${url_escaping_charset}")
	private String encoding;
    
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		//判断用户是否登录
		if(SecurityUtils.getSubject().getPrincipal() != null ) {
			return Boolean.TRUE;
		}
		// Ajax 访问
		String requestedType = request.getHeader("X-Requested-With");
		if ((requestedType != null) && (requestedType.equalsIgnoreCase("XMLHttpRequest")))
	    {
	      response.addHeader("loginStatus", "accessDenied");
	      try{
	    	 response.sendError(403); 
	      }catch( Exception e){
	    	 logger.error("access Denied", e); 
	      }
	      return Boolean.FALSE;
	    }
		try{
			//重登录后跳转到当前页面 可以实现页面的跳转
			if (request.getMethod().equalsIgnoreCase("GET")) {
				String str2 = request.getQueryString() != null ? request
						.getRequestURI() + "?" + request.getQueryString() : request
						.getRequestURI();
				response.sendRedirect(request.getContextPath() + this.loginPath
						+ "?" + "redirectUrl" + "="
						+ URLEncoder.encode(str2, this.encoding));
			} else {
				//重新登录
				response.sendRedirect(request.getContextPath() + this.loginPath);
			}
		}catch(Exception e) {
			logger.error("access Denied", e); 
		}
		return Boolean.TRUE;
	}
	//该方法在action执行后，生成视图前执行。在这里，我们有机会修改视图层数据
	//处理一些公用页面的数据 暂时不需要
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) {
	}
	
	//最后执行，通常用于释放资源，处理异常。我们可以根据ex是否为空，来进行相关的异常处理。
	//因为我们在平时处理异常时，都是从底层向上抛出异常，最后到了spring框架从而到了这个方法中
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		String uri = request.getRequestURI();
		String uriPrefix = request.getContextPath() + Globals.getAdminPath();
		// 拦截所有来自管理端的POST请求
		if ("POST".equals(request.getMethod()) && StringUtils.startsWith(uri, uriPrefix)){
			
		}
	}
}
