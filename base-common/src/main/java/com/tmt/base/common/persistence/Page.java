package com.tmt.base.common.persistence;

import java.util.List;

/**
 * 
 * @author TMT
 */
@SuppressWarnings("rawtypes")
public class Page implements PageList {
	
	private PageParameters param;
	
	private List data;

	public  Page(){
		super();
	}

	public <T> Page(int pageSize,int pageNum,int recordCount,List<T> page){
		this.param = new PageParameters(pageNum,pageSize,recordCount);
		this.data = page;
	}
	
	public <T> Page(PageParameters pageParameters,List<T> page){
		this.param = new PageParameters(pageParameters.getPageIndex(),pageParameters.getPageSize(),pageParameters.getRecordCount());
		this.param.setSortField(pageParameters.getSortField());
		this.param.setSortType(pageParameters.getSortType());		
		this.data = page;
	}

	public void setPage(PageParameters pageParameters) {
		this.param = pageParameters;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getData() {
		return data;
	}
	
	public void setData(List data) {
		this.data = data;
		if(this.param == null && this.data != null) {
			param = new PageParameters();
			param.setRecordCount(this.data.size());
		}
	}

	public PageParameters getParam() {
		if(this.param == null) {
			param = new PageParameters();
		}
		return param;
	}
	
	/**
	 * 分页条<div class="page">${page.pagination}</div>
	 * @return
	 */
	public String getPagination(){
		return getParam().getPagination();
	}
	
	/**
	 * 分页条<div class="page">${page.simplePagination}</div>
	 * @return
	 */
	public String getSimplePagination(){
		return getParam().getSimplePagination();
	}
}
