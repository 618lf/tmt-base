package com.tmt.base.common.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Ibatis查询条件构造类
 * @author liliang
 *
 */
public class QueryCondition {

	protected String orderByClause;

	protected List<Criteria> oredCriteria;

	public QueryCondition() {
		oredCriteria = new ArrayList<Criteria>();
	}

	protected QueryCondition(QueryCondition example) {
		this.orderByClause = example.orderByClause;
		this.oredCriteria = example.oredCriteria;
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}
	
	public String getOrderByClause() {
		return orderByClause;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
	}

	public static class Criteria {
		protected List<String> criteriaWithoutValue;
		protected List<Map<String,Object>> criteriaWithSingleValue;
		protected List<Map<String,Object>> criteriaWithListValue;
		protected List<Map<String,Object>> criteriaWithBetweenValue;

		protected Criteria() {
			super();
			criteriaWithoutValue = new ArrayList<String>();
			criteriaWithSingleValue = new ArrayList<Map<String,Object>>();
			criteriaWithListValue = new ArrayList<Map<String,Object>>();
			criteriaWithBetweenValue = new ArrayList<Map<String,Object>>();
		}

		public boolean isValid() {
			return criteriaWithoutValue.size() > 0
					|| criteriaWithSingleValue.size() > 0
					|| criteriaWithListValue.size() > 0
					|| criteriaWithBetweenValue.size() > 0;
		}

		public List<String> getCriteriaWithoutValue() {
			return criteriaWithoutValue;
		}

		public List<Map<String,Object>> getCriteriaWithSingleValue() {
			return criteriaWithSingleValue;
		}

		public List<Map<String,Object>> getCriteriaWithListValue() {
			return criteriaWithListValue;
		}

		public List<Map<String,Object>> getCriteriaWithBetweenValue() {
			return criteriaWithBetweenValue;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteriaWithoutValue.add(condition);
		}

		protected void addCriterion(String condition, Object value,
				String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property
						+ " cannot be null");
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("condition", condition);
			map.put("value", value);
			criteriaWithSingleValue.add(map);
		}

		protected void addCriterion(String condition, List<Object> values,
				String property) {
			if (values == null || values.size() == 0) {
				throw new RuntimeException("Value list for " + property
						+ " cannot be null or empty");
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("condition", condition);
			map.put("values", values);
			criteriaWithListValue.add(map);
		}

		protected void addCriterion(String condition, Object value1,
				Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property
						+ " cannot be null");
			}
			List<Object> list = new ArrayList<Object>();
			list.add(value1);
			list.add(value2);
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("condition", condition);
			map.put("values", list);
			criteriaWithBetweenValue.add(map);
		}

		protected void addCriterionForJDBCDate(String condition, Date value,
				String property) {
			addCriterion(condition, new java.sql.Date(value.getTime()),property);
		}

		protected void addCriterionForJDBCDate(String condition, List<Date> values,
				String property) {
			if (values == null || values.size() == 0) {
				throw new RuntimeException("Value list for " + property
						+ " cannot be null or empty");
			}
			List<Date> dateList = new ArrayList<Date>();
			Iterator<Date> iter = values.iterator();
			while (iter.hasNext()) {
				dateList.add(new java.sql.Date(((Date) iter.next()).getTime()));
			}
			addCriterion(condition, dateList, property);
		}

		protected void addCriterionForJDBCDate(String condition, Date value1,
				Date value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property
						+ " cannot be null");
			}
			addCriterion(condition, new java.sql.Date(value1.getTime()),
					new java.sql.Date(value2.getTime()), property);
		}

		public Criteria andIsNull(String column) {
			addCriterion(column+" is null");
			return this;
		}

		public Criteria andIsNotNull(String column) {
			addCriterion(column+" is not null");
			return this;
		}
		
		public Criteria andConditionSql(String conditionSql) {
			addCriterion(conditionSql);
			return this;
		}

		public Criteria andEqualTo(String column,String property,Object value) {
			addCriterion(column+" =", value, property);
			return this;
		}

		public Criteria andNotEqualTo(String column,String property,Object value) {
			addCriterion(column+" <>", value, property);
			return this;
		}

		public Criteria andGreaterThan(String column,String property,Object value) {
			addCriterion(column+" >", value, property);
			return this;
		}

		public Criteria andGreaterThanOrEqualTo(String column,String property,Object value) {
			addCriterion(column+" >=", value, property);
			return this;
		}

		public Criteria andLessThan(String column,String property,Object value) {
			addCriterion(column+" <", value, property);
			return this;
		}

		public Criteria andLessThanOrEqualTo(String column,String property,Object value) {
			addCriterion(column+" <=", value, property);
			return this;
		}

		@SuppressWarnings("unchecked")
		public Criteria andIn(String column,String property,@SuppressWarnings("rawtypes") List values) {
			addCriterion(column+ " in", values, property);
			return this;
		}

		@SuppressWarnings("unchecked")
		public Criteria andNotIn(String column,String property,@SuppressWarnings("rawtypes") List values) {
			addCriterion(column+" not in", values, property);
			return this;
		}

		public Criteria andBetween(String column,String property,Object value1, Object value2) {
			addCriterion(column+" between", value1, value2, property);
			return this;
		}

		public Criteria andNotBetween(String column,String property,Object value1, Object value2) {
			addCriterion(column+" not between", value1, value2, property);
			return this;
		}
        /**
         * 存储到 WithoutValue 中
         * @param column
         * @param property
         * @param value
         * @return
         */
		public Criteria andLike(String column,String property,Object value) {
			addCriterion(column + " LIKE CONCAT(CONCAT('%','" + value + "'),'%')");
			return this;
		}

		public Criteria andNotLike(String column,String property,Object value) {
			addCriterion(column + " NOT LIKE CONCAT(CONCAT('%','" + value + "'),'%')");
			return this;
		}

		public Criteria andDateEqualTo(String column,String property,Date value) {
			addCriterionForJDBCDate(column+" =", value, property);
			return this;
		}

		public Criteria andDateNotEqualTo(String column,String property,Date value) {
			addCriterionForJDBCDate(column+" <>", value, property);
			return this;
		}

		public Criteria andDateGreaterThan(String column,String property,Date value) {
			addCriterionForJDBCDate(column+" >", value, property);
			return this;
		}

		public Criteria andDateGreaterThanOrEqualTo(String column,String property,Date value) {
			addCriterionForJDBCDate(column+" >=", value, property);
			return this;
		}

		public Criteria andDateLessThan(String column,String property,Date value) {
			addCriterionForJDBCDate(column+" <", value, property);
			return this;
		}

		public Criteria andDateLessThanOrEqualTo(String column,String property,Date value) {
			addCriterionForJDBCDate(column+" <=", value, property);
			return this;
		}

		public Criteria andDateIn(String column,String property,List<Date> values) {
			addCriterionForJDBCDate(column+" in", values, property);
			return this;
		}

		public Criteria andDateNotIn(String column,String property,List<Date> values) {
			addCriterionForJDBCDate(column+" not in", values, property);
			return this;
		}

		public Criteria andDateBetween(String column,String property,Date value1, Date value2) {
			addCriterionForJDBCDate(column+" between", value1, value2,property);
			return this;
		}

		public Criteria andDateNotBetween(String column,String property,Date value1, Date value2) {
			addCriterionForJDBCDate(column+" not between", value1, value2,property);
			return this;
		}

		
	}
}