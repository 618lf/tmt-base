package com.tmt.base.common.orm.annotion;

/**
 * 定义实体对应的列名
 * @author LL
 *
 */
@java.lang.annotation.Target(value={java.lang.annotation.ElementType.FIELD})
@java.lang.annotation.Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Documented
public abstract @interface Column{
  public abstract java.lang.String value() default "";
}
