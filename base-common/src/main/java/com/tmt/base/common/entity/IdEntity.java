/**
 * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.tmt.base.common.entity;

import java.io.Serializable;

import com.tmt.base.common.persistence.incrementer.IdGen;

/**
 * 数据Entity类
 * @author ThinkGem
 * @version 2013-05-28
 */
public abstract class IdEntity<PK> implements Serializable {

	private static final long serialVersionUID = -4759823746568422807L;
	
	protected PK id; // 编号
	
	public IdEntity() {
		super();
	}
	
	public PK prePersistEntity( Serializable key){
		return this.prePersist(key);
	}
	
	@SuppressWarnings("unchecked")
	public PK prePersist( Serializable key ){
		this.id = (PK) IdGen.key(key);
		return this.id;
	}
	
	public void preUpdate() {
		
	}
	
	public PK getId() {
		return id;
	}

	public void setId(PK id) {
		this.id = id;
	}
	
}
