package com.tmt.base.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author TMT
 */
public class RegexUtil {
	
	public static final String DATE_REGEX = "^[0-9]{4}\\-[0-9]{1,2}\\-[0-9]{1,2}$";
	public static final String DIGIT_REGEX = "^\\d+(\\.\\d+)?$"; 
	
	/**校验日期
	 * @param date
	 * @return
	 */
	public static boolean isDate(String date){
		Pattern p = Pattern.compile(DATE_REGEX);
		Matcher m = p.matcher(date);
		return  m.matches();
	}
	
	
	/**校验数字
	 * @param num
	 * @return
	 */
	public static boolean isNumber(String num){
		Pattern p = Pattern.compile(DIGIT_REGEX);
		Matcher m = p.matcher(num);
		return  m.matches();
	}
	
	
}
