package com.tmt.base.common.exception;

/**
 * 业务层同一抛出异常
 * @author liliang
 *
 */
@SuppressWarnings("serial")
public class BusinessServiceException extends BaseAppException {

    public BusinessServiceException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     */
    public BusinessServiceException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public BusinessServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
