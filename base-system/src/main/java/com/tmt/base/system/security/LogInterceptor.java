package com.tmt.base.system.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.tmt.base.common.config.Globals;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.entity.Log;
import com.tmt.base.system.entity.User;
import com.tmt.base.system.service.LogService;
import com.tmt.base.system.utils.UserUtils;

/**
 * 系统访问日志记录
 * @author lifeng
 */
public class LogInterceptor extends HandlerInterceptorAdapter{
   
	protected static Logger logger = LoggerFactory.getLogger(LogInterceptor.class);
	
	LogService LogService = SpringContextHolder.getBean(LogService.class);
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, 
			Object handler, Exception ex) throws Exception {
		
		String requestRri = request.getRequestURI();
		String uriPrefix = request.getContextPath() + Globals.getAdminPath();
		if((StringUtil3.startsWith(requestRri, uriPrefix) && (StringUtil3.endsWith(requestRri, "/save")
				|| StringUtil3.endsWith(requestRri, "/delete") || StringUtil3.endsWith(requestRri, "/import"))
				) || ex!=null){ //添 删 改 查
			User user = UserUtils.getUser();
			if( user != null && user.getId() != null ) {
				
				StringBuilder params = new StringBuilder();
				int index = 0;
				for (Object param : request.getParameterMap().keySet()){ 
					params.append((index++ == 0 ? "" : "&") + param + "=");
					params.append(StringUtil3.abbr(StringUtil3.endsWithIgnoreCase((String)param, "password")
							? "" : request.getParameter((String)param), 100));
				}
				Log log = new Log();
				log.setType(ex == null ? Log.TYPE_ACCESS : Log.TYPE_EXCEPTION);
				log.setRemoteAddr(StringUtil3.getRemoteAddr(request)); 
				log.setUserAgent(request.getHeader("user-agent"));
				log.setRequestUri(request.getRequestURI());
				log.setMethod(request.getMethod()); 
				log.setParams(params.toString());
				String exDesc = (ex != null ? ex.toString() : " ");
				log.setException(exDesc.substring(0, exDesc.length()>255?255:exDesc.length()));
				LogService.insert(log);
				
				//logger.debug("access log {type: {}, loginName: {}, uri: {}}, ", log.getType(), user.getLoginName(), log.getRequestUri());
			}
		}
	}
}
