package com.tmt.base.system.entity;

/**
 * 机构
 * @author lifeng
 */
public class Office extends BaseParentEntity<String>{
	
	private static final long serialVersionUID = -3991917224409935175L;
    private String areaId;
    private String code;
    private String name;
    private String type;// 机构类型（1：公司；2：部门；3：小组）
    private String address;
    private String zipCode;
    private String master;
    private String phone;
    private String fax;//传真
    private String email;
    private String path;
    private String parentName;
    private String areaName;
    
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getMaster() {
		return master;
	}
	public void setMaster(String master) {
		this.master = master;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * 将父区域中的属性添加到子区域中
	 * 
	 * @param parent
	 */
	public void fillByParent(Office parent) {
		super.fillByParent(parent);
		this.setPath((parent == null?"":parent.getPath())  + PATH_SEPARATE + this.getName());
	}

	/**
	 * 父节点的Path修改之后，所有的子节点都要修改
	 * 
	 * @param parent
	 * @param oldParentIds
	 */
	public void updatePathByParent(Office parent, String oldParentIds) {
		super.updatePathByParent(parent, oldParentIds);
		this.setPath(parent.getPath() + PATH_SEPARATE + this.getName());
	}
}