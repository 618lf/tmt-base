package com.tmt.base.system.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.UserRole;

@Repository
public class UserRoleDao extends BaseIbatisDAO<UserRole, String> {

	@Override
	protected String getNamespace() {
		return "SYS_USER_ROLE";
	}
	
	public List<UserRole> findByRoleId(String roleId){
		return this.queryForList("findByRoleId", roleId);
	}
}
