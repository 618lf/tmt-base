package com.tmt.base.system.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.Office;

@Repository
public class OfficeDao extends BaseIbatisDAO<Office, String>{

	@Override
	protected String getNamespace() {
		return "SYS_OFFICE";
	}
}
