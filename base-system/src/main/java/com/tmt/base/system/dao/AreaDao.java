package com.tmt.base.system.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.Area;

@Repository
public class AreaDao extends BaseIbatisDAO<Area, String>{

	@Override
	protected String getNamespace() {
		return "SYS_AREA";
	}
}
