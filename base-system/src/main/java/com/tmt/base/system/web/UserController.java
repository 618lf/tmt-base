package com.tmt.base.system.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.entity.Office;
import com.tmt.base.system.entity.Role;
import com.tmt.base.system.entity.TreeVO;
import com.tmt.base.system.entity.User;
import com.tmt.base.system.enums.UserStatus;
import com.tmt.base.system.service.OfficeService;
import com.tmt.base.system.service.RoleService;
import com.tmt.base.system.service.SystemService;
import com.tmt.base.system.service.UserService;
import com.tmt.base.system.utils.UserUtils;

@Controller
@RequestMapping(value = "${adminPath}/system/user")
public class UserController extends BaseController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private OfficeService officeService;
	@Autowired
	private RoleService roleService;
	
	/**
	 * 以Frame 格式展示（不提倡了）
	 */
	@RequestMapping(value = {"treeFrame"})
	public String treeFrame(Model model){
		model.addAttribute("treeUrl", "/system/user/orgTree");
		return "/system/TreeFrame";
	}
	@RequestMapping(value = {"orgTree"})
	public String orgTree(Model model){
		List<TreeVO> trees = this.officeService.findTreeList(new HashMap<String,Object>());
		model.addAttribute("items", trees);
		model.addAttribute("checked", Boolean.FALSE);
		model.addAttribute("treeUrl", "/system/user/initList?officeId=");
		return "/system/UserOfficeTree";
	}
	@RequestMapping(value = {"userNav"})
	public String userNav(Model model,Page page){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		page  = userService.queryForPage(qc, param);
		model.addAttribute("page", page);
		return "/system/UserListNav";
	}
	
	@RequestMapping(value = {"form"})
	public String form(User user,Model model) {
		if( user != null && user.getId() != null && StringUtil3.isNotBlank(user.getId())) {
			user = this.userService.get(user.getId());
			Office office = officeService.get(user.getOfficeId());
			user.setOffice(office);
			
			List<Role> roles = roleService.findByUserId(user.getId());
			if( roles != null && roles.size() != 0) {
				StringBuffer roleIds = new StringBuffer();
				StringBuffer roleNames = new StringBuffer();
				for( Role r:roles ) {
					roleIds.append(r.getId()).append(",");
					roleNames.append(r.getName()).append(",");
				}
				user.setRoleIds(roleIds.toString());
				user.setRoleNames(roleNames.toString());
			}
		} else {
			if(user == null) {
				user = new User();
			}
			if( user.getOfficeId() != null) {
				Office office = officeService.get(user.getOfficeId());
				user.setOffice(office);
			} else {
				Office office = new Office();
				user.setOffice(office);
			}
			user.setId(IdGen.INVALID_ID);
		}
		model.addAttribute("user", user);
		model.addAttribute("officeId", user.getOfficeId());
		return "/system/UserForm";
	}
	
	/**
	 * 个人信息
	 * @param user
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"selfInfo"})
	public String selfInfo(User user,Model model) {
		if(user == null || IdGen.isInvalidId(user.getId())) {
			user = UserUtils.getUser();
		}
		this.form(user, model);
		return "/system/UserSelfInfo";
	}
	
	/**
	 * 个人信息 - 保存
	 * @param user
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"selfInfoSave"})
	public String selfInfoSave(User user,Model model, RedirectAttributes redirectAttributes) {
		this.save(user, model, redirectAttributes);
		return "redirect:"+Globals.getAdminPath()+"/system/user/selfInfo";
	}
	
	@RequestMapping(value = {"save"})
	public String save(User user,Model model, RedirectAttributes redirectAttributes){
		if ( !beanValidator(model, user) ){
			return form(user, model);
		}
		this.userService.save(user);
		addMessage(redirectAttributes, StringUtil3.format("%s'%s'%s", "修改用户", user.getName(), "成功"));
		redirectAttributes.addAttribute("id", user.getId());
		return "redirect:"+Globals.getAdminPath()+"/system/user/form";
	}
	
	@RequestMapping(value = {"initList"})
	public String initList(User user,Model model){
		if( user != null && user.getOfficeId() != null) {
			model.addAttribute("officeId", user.getOfficeId());
		}
		return "/system/UserList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(User user,Model model,Page page) {
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = qc.createCriteria();
		if(user!=null && user.getOfficeId() != null && !IdGen.isInvalidId(user.getOfficeId())) {
			c.andEqualTo("U.OFFICE_ID", "OFFICE_ID", user.getOfficeId());
		}
		if(user!=null && StringUtil3.isNotEmpty(user.getName())) {
			c.andLike("U.NAME", "NAME", "%"+user.getName()+"%");
		}
		if(user!=null && StringUtil3.isNotEmpty(user.getNo())) {
			c.andLike("U.NO", "NO", "%"+user.getNo()+"%");
		}
		page  = this.userService.queryForPage(qc, param);
		return page;
	}
	
	@ResponseBody
	@RequestMapping(value = {"treeSelect"})
	public List<Map<String, Object>> treeSelect(@RequestParam(required=false)String extId, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<TreeVO> orgTrees = this.officeService.findTreeList(new HashMap<String,Object>());
		List<TreeVO> userTrees = this.userService.findTreeList(new HashMap<String,Object>());
		for (int i=0; i<orgTrees.size(); i++){
			TreeVO e = orgTrees.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", "O_"+e.getId());
			map.put("pId", "O_"+e.getParent());
			map.put("name", e.getTreeName());
			map.put("chkDisabled", Boolean.TRUE);
			int iCount = this.userService.officeSelectCheck(e.getId());
			if( iCount > 0 ) {
				map.put("chkDisabled", Boolean.FALSE);
			}
			mapList.add(map);
		}
		for (int i=0; i<userTrees.size(); i++){
			TreeVO e = userTrees.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", e.getId());
			map.put("pId", "O_"+e.getParent());
			map.put("name", e.getTreeName());
			map.put("type", 1);
			mapList.add(map);
		}
		return mapList;
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<User> users = Lists.newArrayList();
		Boolean bFalg = Boolean.TRUE;
		for( String id:idList ) {
			 User user = new User();
			 user.setId(id);
			 if( IdGen.isRoot(user.getId()) ) {
				 bFalg = Boolean.FALSE; break;
			 }
			 users.add(user);
		}
		if(!bFalg){
			return AjaxResult.error("不能删除超级管理员");
		}
		this.userService.batchDelete(users);
		return AjaxResult.success();
	}
	
	@ResponseBody
	@RequestMapping(value = {"lockUser"})
	public Boolean lockUser(String[] idList , Model model,HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<User> users = Lists.newArrayList();
		for( String id : idList ) {
			 User user = this.userService.get(id);
			 user.setUserStatus(UserStatus.LOCK_U);
			 users.add(user);
		}
		this.userService.lockUser(users);
		return Boolean.TRUE;
	}
	
	@ResponseBody
	@RequestMapping(value = {"unLockUser"})
	public Boolean unLockUser(String[] idList , Model model,HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<User> users = Lists.newArrayList();
		for( String id:idList ) {
			 User user = this.userService.get(id);
			 user.setUserStatus(UserStatus.NARMAL);
			 users.add(user);
		}
		this.userService.unLockUser(users);
		return Boolean.TRUE;
	}
	
	@ResponseBody
	@RequestMapping(value = {"checkAccount"})
	public Boolean checkAccount(User user){
		int iCount = this.userService.checkAccount(user);
		if( iCount > 0 ) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	@ResponseBody
	@RequestMapping(value = {"initPassword"})
	public Boolean initPassword(User user, String newPassWord ){
		if( user != null && !IdGen.isInvalidId(user.getId()) && !StringUtil3.isBlank(newPassWord) ) {
			user.setPassword(SystemService.entryptPassword(newPassWord));
			user.setUserStatus(UserStatus.MD_P);
			this.userService.updatePassWord(user);
			user = this.userService.get(user.getId());
			
			Map<String,Object> context = Maps.newHashMap();
			context.put("newPassWord", newPassWord);
		}
		return Boolean.TRUE;
	}
	
	@ResponseBody
	@RequestMapping(value = {"checkNo"})
	public Boolean checkNo(User user){
		int iCount = this.userService.checkUserCode(user);
		if( iCount > 0 ) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
}
