package com.tmt.base.system.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.system.dao.ExcelTemplateDao;
import com.tmt.base.system.entity.ExcelItem;
import com.tmt.base.system.entity.ExcelTemplate;

@Service
public class ExcelTemplateService extends BaseService<ExcelTemplate,String>{

	@Autowired
	private ExcelTemplateDao excelTemplateDao;
	@Autowired
	private ExcelItemService excelItemService;
	
	@Override
	public BaseIbatisDAO<ExcelTemplate,String> getEntityDao() {
		return this.excelTemplateDao;
	}
	
	@Transactional(readOnly = false)
	public String saveTemplate( ExcelTemplate template ) {
		if(IdGen.isInvalidId(template.getId())) {
			this.insert(template);
		} else {
			this.update(template);
		}
		this.excelItemService.save(template);
		return template.getId();
	}
	
	@Transactional(readOnly = false)
	public void delete(List<ExcelTemplate> templates){
		List<ExcelItem> allItems  = Lists.newArrayList();
		for( ExcelTemplate template: templates){
			List<ExcelItem> oldItems = excelItemService.findByTempleteId(template.getId());
			allItems.addAll(oldItems);
		}
		this.batchDelete(templates);
		this.excelItemService.batchDelete(allItems);
	}
	
	/**
	 * 查询模版和详情
	 * @param templateId
	 * @return
	 */
	public ExcelTemplate getWithItems(String templateId){
		ExcelTemplate template = this.get(templateId);
		if(template != null) {
			List<ExcelItem> items = excelItemService.findByTempleteId(templateId);
			template.setItems(items);
 		}
		return template;
	}
}
