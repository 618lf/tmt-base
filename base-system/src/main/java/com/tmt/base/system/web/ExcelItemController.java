package com.tmt.base.system.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.entity.ExcelItem;
import com.tmt.base.system.entity.ExcelTemplate;
import com.tmt.base.system.service.ExcelItemService;
import com.tmt.base.system.service.ExcelTemplateService;

@Controller
@RequestMapping(value = "${adminPath}/system/excel/item")
public class ExcelItemController extends BaseController{

	@Autowired
	private ExcelTemplateService excelTemplateService;
	
	@Autowired
	private ExcelItemService excelItemService;
	
	@ResponseBody
	@RequestMapping(value = {"items",""})
	public AjaxResult items(ExcelTemplate template, Model model) {
		List<ExcelItem> items = this.excelItemService.findByTempleteId(template.getId());
		AjaxResult result = AjaxResult.success();
		result.setObj(items);
		return result;
	}
}
