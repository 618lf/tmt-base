package com.tmt.base.system.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.User;

@Repository
public class UserDao extends BaseIbatisDAO<User, String> {

	@Override
	protected String getNamespace() {
		return "SYS_USER";
	}
	
	public User findByAccount(String account){
		return this.queryForObject("findUserByAccount", account);
	}
}
