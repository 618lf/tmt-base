package com.tmt.base.system.entity;

import java.util.Date;

import com.tmt.base.system.enums.UserStatus;

/**
 * 系统用户
 * @author lifeng
 *
 */
public class User extends BaseEntity<String>{
	
	private static final long serialVersionUID = 8719349633839252847L;
	private String officeId;
    private String loginName;
    private String password;
    private String no;
    private String name;
    private Date birthday;
    private String duty;
    private String email;//电子邮件
    private String phone;//固定电话
    private String mobile;//移动电话
    private String address;
    private String userType;
    private String loginIp;
    private Date loginDate;
    private Integer status = 1;//用户状态(1247 模式)：1 待修改密码 ,2 锁定 ,4 正常  （默认 1）
    private Office office;
    private String roleIds;
    private String roleNames;
    
    public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getDuty() {
		return duty;
	}
	public void setDuty(String duty) {
		this.duty = duty;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Office getOffice() {
		return office;
	}
	public void setOffice(Office office) {
		this.office = office;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
    
	public String getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}
	public String getRoleNames() {
		return roleNames;
	}
	public void setRoleNames(String roleNames) {
		this.roleNames = roleNames;
	}
	public String getOfficeId() {
		return officeId;
	}
	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getLoginIp() {
		return loginIp;
	}
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	public Date getLoginDate() {
		return loginDate;
	}
	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}
	
	public String getUserStatus() {
		if( status == null) {
			return "未知状态";
		}
		return UserStatus.valueOf(status);
	}
	
	/**
	 * 账户是否锁定
	 * @return
	 */
	public Boolean isLocked(){
		return (status != null && (status == UserStatus.MD_P_A_LOCK.getValue()
				|| status == UserStatus.LOCK_U.getValue()));
	}
	
	/**
	 * 设置用户的状态
	 * @param status
	 */
	public void setUserStatus( UserStatus status ) {
		if( status == UserStatus.MD_P ) {
			if( this.status == UserStatus.LOCK_U.getValue() ||
				this.status == UserStatus.MD_P_A_LOCK.getValue()
				) {
				this.status = UserStatus.MD_P_A_LOCK.getValue();
			} else if( this.status == UserStatus.MD_P.getValue() ||
					   this.status == UserStatus.NARMAL.getValue() ) {
				this.status = UserStatus.MD_P.getValue();
			}
		} else if( status == UserStatus.LOCK_U ) {
			if( this.status == UserStatus.LOCK_U.getValue() ||
				this.status == UserStatus.NARMAL.getValue()
				) {
				this.status = UserStatus.LOCK_U.getValue();
			} else if( this.status == UserStatus.MD_P.getValue() ||
					   this.status == UserStatus.MD_P_A_LOCK.getValue() ) {
				this.status = UserStatus.MD_P_A_LOCK.getValue();
			}
		} else if( status == UserStatus.NARMAL ) {
			if( this.status == UserStatus.MD_P.getValue() ||
				this.status == UserStatus.MD_P_A_LOCK.getValue()
				) {
				this.status = UserStatus.MD_P.getValue();
			} else if( this.status == UserStatus.LOCK_U.getValue() ||
					   this.status == UserStatus.NARMAL.getValue() ) {
				this.status = UserStatus.NARMAL.getValue();
			}
		}
		if( this.status != UserStatus.MD_P.getValue() &&
			this.status != UserStatus.LOCK_U.getValue() &&
			this.status != UserStatus.MD_P_A_LOCK.getValue() &&
			this.status != UserStatus.NARMAL.getValue() ) {
			this.status = status.getValue();
		}
	}

}