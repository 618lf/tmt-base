package com.tmt.base.system.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.entity.Office;
import com.tmt.base.system.entity.Role;
import com.tmt.base.system.entity.TreeVO;
import com.tmt.base.system.service.OfficeService;
import com.tmt.base.system.service.RoleService;

@Controller
@RequestMapping(value = "${adminPath}/system/role")
public class RoleController extends BaseController {

	@Autowired
	private RoleService roleService;
	@Autowired
	private OfficeService officeService;
	
	@RequestMapping(value = {"treeFrame"})
	public String treeFrame(Model model){
		model.addAttribute("treeUrl", "/system/role/orgTree");
		return "/system/TreeFrame";
	}
	@RequestMapping(value = {"orgTree"})
	public String orgTree(Model model){
		List<TreeVO> trees = this.officeService.findTreeList(new HashMap<String,Object>());
		model.addAttribute("items", trees);
		model.addAttribute("checked", Boolean.FALSE);
		model.addAttribute("treeUrl", "/system/role/initList?officeId=");
		return "/system/RoleOfficeTree";
	}
	@RequestMapping(value = {"initList"})
	public String initList(Role role,Model model){
		if( role != null && role.getOfficeId() != null && StringUtil3.isNotBlank(role.getOfficeId())) {
			model.addAttribute("officeId", role.getOfficeId());
		}
		return "/system/RoleList";
	}
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Role role,Model model,Page page) {
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = qc.createCriteria();
		if( role!=null && role.getOfficeId() != null && !StringUtil3.isBlank(role.getOfficeId()) ) {
			c.andEqualTo("R.OFFICE_ID", "OFFICE_ID", role.getOfficeId());
		}
		if( role!=null && role.getName() != null && !StringUtil3.isBlank(role.getName()) ) {
			c.andLike("R.NAME", "NAME", role.getName());
		}
		page = this.roleService.queryForPage(qc, param);
		return page;
	}
	@RequestMapping(value = {"form"})
	public String form(Role role,Model model) {
		if( role != null && role.getId() != null && StringUtil3.isNotBlank(role.getId())) {
			role = this.roleService.get(role.getId());
			//组织结构
			Office office = officeService.get(role.getOfficeId());
			role.setOffice(office);
		} else {
			if(role == null) {
				role = new Role();
			}
			if( role.getOfficeId() != null) {
				Office office = officeService.get(role.getOfficeId());
				role.setOffice(office);
			} else {
				Office office = new Office();
				role.setOffice(office);
			}
			role.setId(IdGen.INVALID_ID);
		}
		model.addAttribute("role", role);
		model.addAttribute("officeId", role.getOfficeId());
		return "/system/RoleForm";
	}
	@RequestMapping(value = {"save"})
	public String save( Role role,Model model, RedirectAttributes redirectAttributes ){
		//后台验证
		if ( !beanValidator(model, role) ){
			return form(role, model);
		}
		roleService.save(role);
		addMessage(redirectAttributes, "保存角色'" + role.getName() + "'成功");
		return "redirect:"+Globals.getAdminPath()+"/system/role/initList";
	}
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model,HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Role> roles = Lists.newArrayList();
		for( String id:idList ) {
			 Role role = new Role();
			 role.setId(id);
			 roles.add(role);
		}
		this.roleService.batchDelete(roles);
		return AjaxResult.success();
	}
	
	/**
	 * 角色树形选择
	 * @param extId
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"treeSelect"})
	public List<Map<String, Object>> treeSelect(@RequestParam(required=false)String extId, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Map<String, Object>> mapList = Lists.newArrayList();
		//组织结构树
		List<TreeVO> orgTrees = this.officeService.findTreeList(new HashMap<String,Object>());
		//角色树
		List<TreeVO> userTrees = this.roleService.findTreeList(new HashMap<String,Object>());
		for (int i=0; i<orgTrees.size(); i++){
			TreeVO e = orgTrees.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", "O_"+e.getId());
			map.put("pId", "O_"+e.getParent());
			map.put("name", e.getTreeName());
			map.put("chkDisabled", Boolean.TRUE);
			int iCount = this.roleService.officeSelectCheck(e.getId());
			if( iCount > 0 ) {
				map.put("chkDisabled", Boolean.FALSE);
			}
			mapList.add(map);
		}
		for (int i=0; i<userTrees.size(); i++){
			TreeVO e = userTrees.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", e.getId());
			map.put("pId", "O_"+e.getParent());
			map.put("name", e.getTreeName());
			map.put("type", 1);
			mapList.add(map);
		}
		return mapList;
	}
}
