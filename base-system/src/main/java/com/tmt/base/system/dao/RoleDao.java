package com.tmt.base.system.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.Role;

@Repository
public class RoleDao extends BaseIbatisDAO<Role,String> {

	@Override
	protected String getNamespace() {
		return "SYS_ROLE";
	}
}
