package com.tmt.base.system.entity;

import java.util.Map;

import com.google.common.collect.Maps;
import com.tmt.base.common.orm.annotion.Entity;
import com.tmt.base.common.utils.StringUtil3;

/**
 * $.ajax后需要接受的JSON
 * 
 * 此类返回的信息，特别是错误的信息，应该可以设定不同的格式模版，
 * 例如 错误类别：详细信息，系统应提供几个错误模版
 * 成功也是一样的，也可以提供一个模版，可以专门提供一个消息处理类
 * @author
 * 
 */
@Entity(noCache=true)
public class AjaxResult {
	
	private Boolean success = Boolean.TRUE;// 是否成功
	private String msg = "操作成功";// 提示信息
	private Object obj = null;// 返回对象
	private Map<String, Object> attributes;// 返回其他参数
	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}
	
	public Map<String, Object> setAttribute(String key ,Object value) {
		if( attributes == null ) {
			attributes = Maps.newHashMap();
		}
		attributes.put(key, value);
		return attributes;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}
	
	public static AjaxResult success(){
		AjaxResult result = new AjaxResult();
		result.setSuccess(Boolean.TRUE);
		return result;
	}
	
	public static AjaxResult error( String msg){
		AjaxResult result = new AjaxResult();
		result.setSuccess(Boolean.FALSE);
		result.setMsg(msg);
		return result;
	}
	
	/**
	 * 格式化输出 %s 代表 msg数组中的一个
	 * @param template
	 * @param msg
	 * @return
	 */
	public static AjaxResult error( String template, Object...msg){
		return error(StringUtil3.format(template, msg));
	}
	
}
