package com.tmt.base.system.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.Log;

@Repository
public class LogDao extends BaseIbatisDAO<Log, String> {

	@Override
	protected String getNamespace() {
		return "SYS_LOG";
	}
}
