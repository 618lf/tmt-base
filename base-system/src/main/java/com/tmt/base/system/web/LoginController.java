package com.tmt.base.system.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.utils.CacheUtils;
import com.tmt.base.common.utils.CookieUtils;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.User;
import com.tmt.base.system.service.UserService;
import com.tmt.base.system.utils.UserUtils;

@Controller
@RequestMapping(value = "${adminPath}")
public class LoginController extends BaseController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "")
	public String index(HttpServletRequest request, HttpServletResponse response){
		User user = UserUtils.getUser();
		// 未登录，则跳转到登录页
		if(user.getId() == null){
			return "redirect:"+Globals.getAdminPath()+"/login";
		}
		// 登录成功后，验证码计算器清零
		isValidateCodeLogin(user.getLoginName(), false, true);
		//设置session cookie 持久化 支持多游览器窗口
		//CookieUtils.setCookie(response, "JSESSIONID", ContextHolderUtils.getSession().getId(), 3600);
		return "system/SysIndex";
	}
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(HttpServletRequest request, HttpServletResponse response, Model model){
		User user = UserUtils.getUser();
		// 如果已经登录，则跳转到管理首页
		if(user.getId() != null){
			return "redirect:"+Globals.getAdminPath();
		}
		String username = CookieUtils.getCookie(request, FormAuthenticationFilter.DEFAULT_USERNAME_PARAM);
		if( username != null ) {
		   model.addAttribute(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, username);
		}
		String password = CookieUtils.getCookie(request, FormAuthenticationFilter.DEFAULT_PASSWORD_PARAM);
		if( username != null ) {
		   model.addAttribute(FormAuthenticationFilter.DEFAULT_PASSWORD_PARAM, password);
		}
		return "system/SysLogin";
	}
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
    public String login(@RequestParam(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM) String username, HttpServletRequest request, HttpServletResponse response, Model model){
		User user = UserUtils.getUser();
		// 如果已经登录，则跳转到管理首页
		if(user.getId() != null){
			return "redirect:"+Globals.getAdminPath();
		}
		model.addAttribute(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, username);
		model.addAttribute("isValidateCodeLogin", isValidateCodeLogin(username, true, false));
		return "system/SysLogin";
	}
	
	@RequestMapping(value = {"homePage"})
	public String homePage(){
		
		return "system/HomePage";
	}
	/**
	 * 是否是验证码登录
	 * @param useruame 用户名
	 * @param isFail 计数加1
	 * @param clean 计数清零
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean isValidateCodeLogin(String useruame, boolean isFail, boolean clean){
		Map<String, Integer> loginFailMap = (Map<String, Integer>)CacheUtils.get("loginFailMap");
		if (loginFailMap==null){
			loginFailMap = Maps.newHashMap();
			CacheUtils.put("loginFailMap", loginFailMap);
		}
		Integer loginFailNum = loginFailMap.get(useruame);
		if (loginFailNum==null){
			loginFailNum = 0;
		}
		if (isFail){
			loginFailNum++;
			loginFailMap.put(useruame, loginFailNum);
		}
		if (clean){
			loginFailMap.remove(useruame);
		}
		return loginFailNum >= 3;
	}
	
	@RequestMapping(value = {"/register/userRegister","/register"})
	public String userRegister(){
		return "system/SysRegister";
	}
	
	@ResponseBody
	@RequestMapping(value = {"/register/checkAccount"})
	public Boolean checkAccount(User user){
		int iCount = this.userService.checkAccountByRegister(user);
		if( iCount > 0 ) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
}
