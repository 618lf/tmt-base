package com.tmt.base.system.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.Dict;

@Repository
public class DictDao extends BaseIbatisDAO<Dict, String>{

	@Override
	protected String getNamespace() {
		return "SYS_DICT";
	}

}
