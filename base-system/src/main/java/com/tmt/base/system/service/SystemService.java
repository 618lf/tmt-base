package com.tmt.base.system.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.tmt.base.common.security.Digests;
import com.tmt.base.common.utils.Encodes;
import com.tmt.base.system.dao.MenuDao;
import com.tmt.base.system.dao.RoleMenuDao;
import com.tmt.base.system.dao.UserDao;
import com.tmt.base.system.dao.UserRoleDao;
import com.tmt.base.system.entity.Menu;
import com.tmt.base.system.entity.User;
import com.tmt.base.system.utils.UserUtils;

/**
 * 系统服务：用户，角色
 * @author lifeng
 */
@Service
public class SystemService {
	
	public static final String HASH_ALGORITHM = "SHA-1";
	public static final int HASH_INTERATIONS = 1024;
	public static final int SALT_SIZE = 8;
	
	@Autowired
	private UserRoleDao roleUserDao;
	@Autowired
	private MenuDao   menuDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private RoleMenuDao menuAuthorityDao;
	
	public List<Menu> getMenuList(){
		return UserUtils.getMenuList();
	}
	
	public User getUserByAccount(String account){
		return userDao.findByAccount(account);
	}
	
	public Menu getMenu( String Id ) {
		List<Menu> menuList = this.getMenuList();
		for( Menu menu:menuList ){
			if( menu.getId().compareTo(Id) == 0 ) {
				return menu;
			}
		}
		return menuDao.findByPk(Id);
	}
	
	public List<Menu> findParentMenus( Menu menu ) {
		List<Menu> menuList = this.getMenuList();
		List<Menu> parents = Lists.newArrayList();
		for( Menu item:menuList ){
			if( (","+menu.getParentIds()+",").indexOf(","+item.getId()+",") != -1
				&& 	item.getLevel() != 0) {
				parents.add(item);
			}
		}
		Collections.sort(parents, new Comparator<Menu>(){
			@Override
			public int compare(Menu o1, Menu o2) {
				if(o1.equals(o2)) {
					return 0;
				}
				if(o1.getLevel() < o2.getLevel()) {
					return -1;
				} else if( o1.getLevel() == o2.getLevel()) {
					return o1.getSort()<= o2.getSort()?-1:1;
				} else {
					return 1;
				}
			}
		});
		return parents;
	}
	
	public List<Menu> findChildrenMenus( Menu menu ) {
		List<Menu> menuList = this.getMenuList();
		List<Menu> children = Lists.newArrayList();
		for( Menu item:menuList ){
			if(item.getParentId().compareTo(menu.getParentId()) == 0
				&& item.getId().compareTo(menu.getId()) != 0 ) {
				children.add(item);
			}
		}
		Collections.sort(children, new Comparator<Menu>(){
			@Override
			public int compare(Menu o1, Menu o2) {
				if(o1.equals(o2)) {
					return 0;
				}
				if(o1.getLevel() < o2.getLevel()) {
					return -1;
				} else if( o1.getLevel() == o2.getLevel()) {
					return o1.getSort()<= o2.getSort()?-1:1;
				} else {
					return 1;
				}
			}
		});
		return children;
	}
	
	/**
	 * 生成安全的密码，生成随机的16位salt并经过1024次 sha-1 hash
	 */
	public static String entryptPassword(String plainPassword) {
		byte[] salt = Digests.generateSalt(SALT_SIZE);
		byte[] hashPassword = Digests.sha1(plainPassword.getBytes(), salt, HASH_INTERATIONS);
		return Encodes.encodeHex(salt) + Encodes.encodeHex(hashPassword);
	}
	
	/**
	 * 验证密码
	 * @param plainPassword 明文密码
	 * @param password 密文密码
	 * @return 验证成功返回true
	 */
	public static boolean validatePassword(String plainPassword, String password) {
		byte[] salt = Encodes.decodeHex(password.substring(0,16));
		byte[] hashPassword = Digests.sha1(plainPassword.getBytes(), salt, HASH_INTERATIONS);
		return password.equals(Encodes.encodeHex(salt)+Encodes.encodeHex(hashPassword));
	}
}
