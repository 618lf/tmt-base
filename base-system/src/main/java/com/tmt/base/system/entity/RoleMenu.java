package com.tmt.base.system.entity;

import com.tmt.base.common.entity.IdEntity;

/**
 * 菜单权限
 * @author lifeng
 */
public class RoleMenu extends IdEntity<String>{
  
	private static final long serialVersionUID = 2956898261035481196L;
	
	private String roleId;
    private String menuId;
    
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
    
}