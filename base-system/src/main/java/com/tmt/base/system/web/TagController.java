package com.tmt.base.system.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;

@Controller
@RequestMapping(value = "${adminPath}/system/tag")
public class TagController extends BaseController{

	@RequestMapping(value = "treeselect")
	public String treeselect(HttpServletRequest request, Model model) {
		String extId = request.getParameter("extId");
		extId = StringUtil3.isBlank(extId)?IdGen.INVALID_ID:extId;
		model.addAttribute("url", request.getParameter("url")); 	// 树结构数据URL
		model.addAttribute("extId", extId); // 排除的编号ID
		model.addAttribute("checked", request.getParameter("checked")); // 是否可复选
		model.addAttribute("selectIds", request.getParameter("selectIds")); // 指定默认选中的ID
		model.addAttribute("module", request.getParameter("module"));	// 过滤栏目模型（仅针对CMS的Category树）
		return "/system/Treeselect";
	}
	
	@RequestMapping(value = "iconselect")
	public String iconselect(HttpServletRequest request, Model model) {
		model.addAttribute("value", request.getParameter("value"));
		return "/system/Iconselect";
	}
}
