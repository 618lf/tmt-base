package com.tmt.base.system.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.dao.MenuDao;
import com.tmt.base.system.dao.RoleMenuDao;
import com.tmt.base.system.entity.Menu;
import com.tmt.base.system.entity.RoleMenu;
import com.tmt.base.system.entity.TreeVO;
import com.tmt.base.system.security.AuthenticationRealm;

@Service
public class MenuService extends BaseService<Menu, String>{

	@Autowired
	private MenuDao menuDao;
	@Autowired
	private RoleMenuDao roleMenuDao;
	@Autowired
	private AuthenticationRealm authenticationRealm;
	
	@Override
	protected BaseIbatisDAO<Menu, String> getEntityDao() {
		return menuDao;
	}
	
	@Transactional(readOnly = false)
	public String save( Menu menu ) {
		Menu parent = this.get(menu.getParentId());
		String oldParentIds = menu.getParentIds();
		menu.fillByParent(parent);
		if( !StringUtil3.isBlank(menu.getHref()) ) {
			menu.setType(2);
		}
		if( IdGen.isInvalidId(menu.getId()) ) {
		    this.insert(menu);
		} else {
			this.update(menu);
			List<Menu> children = this.findByParent(menu);
			for( Menu e : children ) {
				e.setParentIds(e.getParentIds().replace(","+oldParentIds, "," + menu.getParentIds()));
			}
			this.batchUpdate(children);
		}
		//清空缓存
		this.authenticationRealm.clearAllCachedAuthorizationInfo();
		return menu.getId();
	}
	
	@Transactional(readOnly = false)
	public void batchDelete( List<Menu> menus ) {
		if( menus != null && menus.size() != 0 ) {
			List<RoleMenu> allMenuAuths = Lists.newArrayList();
			for( Menu menu : menus ) {
				// 删除此菜单的权限
				List<RoleMenu> menuAuths = roleMenuDao.queryForList("findByMenuId", menu.getId());
				allMenuAuths.addAll(menuAuths);
			}
			if( allMenuAuths.size() != 0) {
				roleMenuDao.batchDelete("delete", allMenuAuths);
			}
			super.batchDelete(menus);
			//清空缓存
			this.authenticationRealm.clearAllCachedAuthorizationInfo();
		}
	}
	
	@Transactional(readOnly = false)
	public void updateSort( List<Menu> menus ) {
		this.batchUpdate("updateSort", menus);
		//清空缓存
		this.authenticationRealm.clearAllCachedAuthorizationInfo();
	}
	
	public List<Menu> findByParent(Menu parent){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("PARENT_ID",  parent.getId());
		return this.queryForList("findByCondition", params);
	}
	public List<Menu> findByCondition(Map<String,Object> params) {
		return this.queryForList("findByCondition", params);
	}
	
	public List<Menu> findAllWithRoot(Map<String,Object> params){
		return this.queryForList("findAllWithRoot", params);
	}
	
	public int treeMenuCheck( String menuId){
		return this.countByCondition("treeMenuCheck", menuId);
	}
	public int deleteMenuCheck(Menu menu){
		return this.countByCondition("deleteMenuCheck", menu);
	}
	public List<TreeVO> findTreeList(Map<String,Object> params){
		return this.queryForGenericsList("findTreeList", params);
	}
}
