package com.tmt.base.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.system.dao.OfficeDao;
import com.tmt.base.system.entity.Office;
import com.tmt.base.system.entity.TreeVO;

@Service
public class OfficeService extends BaseService<Office, String>{

	@Autowired
	private OfficeDao officeDao;
	
	@Override
	protected BaseIbatisDAO<Office, String> getEntityDao() {
		return this.officeDao;
	}
	
	@Transactional
	public String save(Office office){
		Office parent = this.get(office.getParentId());
		String oldParentIds = office.getParentIds();
		office.fillByParent(parent);
		if( IdGen.isInvalidId(office.getId()) ) {
		    this.insert(office);
		} else {
			this.update(office);
			List<Office> children = this.findByParent(office);
			for( Office o : children ) {
				 o.updatePathByParent(parent, oldParentIds);
			}
			this.batchUpdate(children);
		}
		return office.getId();
	}
	
	public List<Office> findByParent(Office parent){
		QueryCondition qc = new QueryCondition();
		Criteria c = qc.createCriteria();
		c.andEqualTo("PARENT_ID", "PARENT_ID", parent.getId());
		return this.queryByCondition(qc);
	}
	public List<TreeVO> findTreeList(Map<String,Object> params) {
		return this.queryForGenericsList("findTreeList", params);
	}
	public int deleteOfficeCheck(Office entity){
		return this.countByCondition("deleteOfficeCheck",entity);
	}
}
