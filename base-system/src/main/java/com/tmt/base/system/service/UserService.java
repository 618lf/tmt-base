package com.tmt.base.system.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.dao.UserDao;
import com.tmt.base.system.dao.UserRoleDao;
import com.tmt.base.system.entity.TreeVO;
import com.tmt.base.system.entity.User;
import com.tmt.base.system.entity.UserRole;

@Service
public class UserService extends BaseService<User,String>{
	
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserRoleDao userRoleDao;
	
	@Override
	protected BaseIbatisDAO<User, String> getEntityDao() {
		return this.userDao;
	}
	public User isValidUser( String account , String password ) {
		if( StringUtils.isEmpty(account) || StringUtils.isEmpty(password)) {
			return null;
		}
		User newUser = null;
		try{
			newUser = this.findUserByAccount(account);
			if( newUser != null){
				//应有加密的转换
				if(password.equals(newUser.getPassword())){
					return newUser;
				}
			}
		}catch(Exception e){
			logger.debug("",e);
			newUser = null;
		}
		return null;
	}
	
	@Transactional(readOnly = false)
	public String save( User user ) {
		if(IdGen.isInvalidId(user.getId())) {
		   user.setPassword(SystemService.entryptPassword("hello"));//默认密码
		   this.insert(user);
		} else {
			String roleIdStrs = user.getRoleIds();
			List<UserRole> userTemps = this.userRoleDao.queryForList("findByUserId", user.getId());
			this.userRoleDao.batchDelete("delete", userTemps);
			if( roleIdStrs != null ) {
				List<UserRole> roleUsers = Lists.newArrayList();
				String[] roleIds = roleIdStrs.split(",");
				UserRole roleUser = null;
				for( String roleId :roleIds ) {
					if(StringUtil3.isNotEmpty(roleId)) {
						roleUser = new UserRole();
						roleUser.setRoleId(roleId);
						roleUser.setUserId(user.getId());
						roleUsers.add(roleUser);
					}
				}
				this.userRoleDao.batchInsert("insert", roleUsers);
			}
			this.update(user);
		}
		return user.getId();
	}
	
	@Transactional
	public void batchDelete(List<User> users){
		//删除用户
		super.batchDelete(users);
		//删除权限
		List<UserRole> roleUsers = Lists.newArrayList();
		for( User user:users ) {
			List<UserRole> userTemps = this.userRoleDao.queryForList("findByUserId", user.getId());
			if(userTemps != null) {
				roleUsers.addAll(userTemps);
			}
		}
		this.userRoleDao.batchDelete("delete", roleUsers);
	}
	
	@Transactional
	public void lockUser(List<User> users) {
		this.batchUpdate("updateStatus", users);
	}
	
	@Transactional
	public void unLockUser(List<User> users) {
		this.batchUpdate("updateStatus", users);
	}
	
	@Transactional
	public void updatePassWord( User user ) {
		this.userDao.update("updatePassWord",user);
	}
	
	@Transactional
	public void updateStatus( User user ) {
		this.userDao.update("updateStatus",user);
	}
	
	public int checkAccountByRegister(User user){
		return this.countByCondition("checkAccountByRegister",user);
	}
	
	public List<TreeVO> findTreeList(Map<String,Object> params) {
		return this.queryForGenericsList("findTreeList", params);
	}
	
	public int officeSelectCheck(String userId){
		return this.countByCondition("officeSelectCheck",userId);
	}
	
	public int checkAccount(User user) {
		return this.countByCondition("checkAccount",user);
	}
	
	public int checkUserCode(User user){
		return this.countByCondition("checkUserCode",user);
	}
	
	public User findUserByAccount(String account){
		return this.queryForObject("findUserByAccount", account);
	}
}
