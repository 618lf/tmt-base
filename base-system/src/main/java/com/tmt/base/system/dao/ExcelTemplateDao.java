package com.tmt.base.system.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.ExcelTemplate;

@Repository
public class ExcelTemplateDao extends BaseIbatisDAO<ExcelTemplate, String> {

	@Override
	protected String getNamespace() {
		return "SYS_EXCEL_TEMPLATE";
	}
}
