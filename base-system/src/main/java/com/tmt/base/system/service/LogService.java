package com.tmt.base.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.system.dao.LogDao;
import com.tmt.base.system.entity.Log;

@Service
public class LogService extends BaseService<Log,String>{
	
	@Autowired
	private LogDao LogDao;
	
	@Override
	protected BaseIbatisDAO<Log, String> getEntityDao() {
		return this.LogDao;
	}
}
