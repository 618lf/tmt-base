package com.tmt.base.system.entity;

import java.io.Serializable;

import com.tmt.base.common.persistence.incrementer.IdGen;

/**
 * 针对有 parent属性的实体
 * 
 * @author liFeng 2014年5月22日
 */
public class BaseParentEntity<PK> extends BaseEntity<PK> implements
		Serializable {

	private static final long serialVersionUID = 36681857725403035L;

	protected Integer level;
	protected String parentId;
	protected String parentIds;

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	/**
	 * 将父区域中的属性添加到子区域中
	 * 
	 * @param parent
	 */
	public void fillByParent(BaseParentEntity<PK> parent) {
		int level = parent == null ? 0 : parent.getLevel();
		String parentIds = parent == null ? IdGen.INVALID_ID : (parent.getParentIds() + this.getParentId());
		this.setLevel(level + 1);
		this.setParentIds(parentIds + IDS_SEPARATE);
	}

	/**
	 * 父节点的Path修改之后，所有的子节点都要修改
	 * 
	 * @param parent
	 * @param oldParentIds
	 */
	public void updatePathByParent(BaseParentEntity<PK> parent, String oldParentIds) {
		this.setParentIds(this.getParentIds().replace(IDS_SEPARATE + oldParentIds, IDS_SEPARATE + parent.getParentIds()));
	}

}
