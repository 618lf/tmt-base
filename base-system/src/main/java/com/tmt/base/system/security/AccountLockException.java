package com.tmt.base.system.security;

import org.apache.shiro.authc.AuthenticationException;

/**
 * 账户锁定异常
 * @author lifeng
 *
 */
public class AccountLockException extends AuthenticationException {

	private static final long serialVersionUID = 4579680236680135972L;

	public AccountLockException() {
		super();
	}

	public AccountLockException(String message, Throwable cause) {
		super(message, cause);
	}

	public AccountLockException(String message) {
		super(message);
	}

	public AccountLockException(Throwable cause) {
		super(cause);
	}
}
