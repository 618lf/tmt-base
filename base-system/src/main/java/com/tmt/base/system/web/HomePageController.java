package com.tmt.base.system.web;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.Menu;
import com.tmt.base.system.entity.User;
import com.tmt.base.system.utils.UserUtils;

/**
 * 用户首页
 * @author liFeng
 * 2014年8月20日
 */
@Controller
@RequestMapping(value = "${adminPath}/system/homePage")
public class HomePageController extends BaseController {

	/**
	 * 用户个性化配置 -- 菜单设置
	 * @param user
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"homePageConfig"})
	public String homePageConfig(User user, Model model){
		if(user == null || IdGen.isInvalidId(user.getId())) {
			user = UserUtils.getUser();
		}
		List<Menu> menus = UserUtils.getMenuList();
		model.addAttribute("menus", menus);
		return "/system/HomePageConfig";
	}
	
}
