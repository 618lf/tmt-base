package com.tmt.base.system.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.entity.Area;
import com.tmt.base.system.entity.TreeVO;
import com.tmt.base.system.service.AreaService;

@Controller
@RequestMapping(value = "${adminPath}/system/area")
public class AreaController extends BaseController{

	@Autowired
	private AreaService areaService;
	
	@RequestMapping(value = {"initList", ""})
	public String initList(Area area, Model model) {
		if(area != null && area.getId() != null) {
			model.addAttribute("id", area.getId());
		}
		return "/system/AreaList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Area area, Model model) {
		Map<String,Object> params = new HashMap<String,Object>();
		if( area != null && !StringUtil3.isBlank(area.getName())) {
			params.put("AREA_NAME", area.getName());
			QueryCondition qc = new QueryCondition();
			Criteria c = qc.createCriteria();
			c.andLike("NAME", "NAME", "%"+area.getName()+"%");
			List<Area> areas = this.areaService.queryByCondition(qc);
			if( areas != null && areas.size() != 0 ) {
				StringBuffer sb = new StringBuffer(",");
				for( Area orgItem: areas ) {
					 sb.append(orgItem.getParentIds());
					 sb.append(orgItem.getId()).append(",");
				}
				sb.append("-1");
				params.clear();
				params.put("AREA_IDS", sb.toString());
				//选中
				area = new Area();
				area.setId(areas.get(0).getId());
			}
		}
		List<TreeVO> trees = this.areaService.findTreeList(params);
		trees = TreeVO.sort(trees);
		if( trees != null && trees.size() != 0 && area != null && area.getId() != null && !StringUtil3.isBlank(area.getId()) ) {
			area = this.areaService.get(area.getId());
			if(area!=null) {
				for( TreeVO treeVO : trees ){
					if( (","+area.getParentIds()).indexOf(","+treeVO.getId()+",") != -1) {
						 treeVO.setExpanded(Boolean.TRUE);
					}
				}
			}
		} else if( trees != null && trees.size() != 0 ){
			trees.get(0).setExpanded(Boolean.TRUE);
		}
		Page pageList = new Page();
		pageList.setData(trees);
		return pageList;
	}
	
	@RequestMapping(value = {"form"})
	public String form(Area area, Model model) {
		if( area != null && area.getId() != null) {
			area = this.areaService.get(area.getId());
			Area parent = this.areaService.get(area.getParentId());
			if(parent != null) {
				area.setParentId(parent.getId());
				area.setParentName(parent.getName());
			}
		} else {
			area.setId(IdGen.INVALID_ID);
			if( area.getParentId() == null ){
				area.setParentId(IdGen.INVALID_ID);
			} else {
				Area parent = this.areaService.get(area.getParentId());
				area.setParentId(parent.getId());
				area.setParentName(parent.getName());
			}
		}
		//组织类型
		model.addAttribute("area", area);
		return "/system/AreaForm";
	}
	
	@ResponseBody
	@RequestMapping(value = {"treeSelect"})
	public List<Map<String, Object>> treeSelect(@RequestParam(required=false)String extId, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<TreeVO> trees = this.areaService.findTreeList(new HashMap<String,Object>());
		for (int i=0; i<trees.size(); i++){
			TreeVO e = trees.get(i);
			if (extId == null || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParent());
				map.put("name", e.getTreeName());
				mapList.add(map);
			}
		}
		return mapList;
	}
	
	@RequestMapping(value = {"save"})
	public String save(Area area, Model model,RedirectAttributes redirectAttributes){
		if ( !beanValidator(model, area) ){
			return form(area, model);
		}
		String id = this.areaService.save(area);
		addMessage(redirectAttributes, "保存菜单'" + area.getName() + "'成功");
		redirectAttributes.addAttribute("id", id);
		return "redirect:"+Globals.getAdminPath()+"/system/area/initList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model,HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Area> areas = Lists.newArrayList();
		Boolean bFalg = Boolean.TRUE;
		Area oneParent = null;
		for( String id:idList ) {
			 Area org = this.areaService.get(id);
			 //验证是否可以删除 -- 用户
			 int iCount = this.areaService.deleteAreaCheck(org);
			 if( iCount > 0){
				 bFalg = Boolean.FALSE;break;
			 }
			 areas.add(org);
			 if(oneParent == null){
				 oneParent = new Area();
				 oneParent.setId(org.getParentId());
			 }
		}
		if(!bFalg) {//删除失败
			return AjaxResult.error("要删除的区域存在子区域或存在组织结构!");
		}
		this.areaService.batchDelete(areas);
		AjaxResult result = AjaxResult.success();
		result.setObj(oneParent);
		return result;
	}
	
}
