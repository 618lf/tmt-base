package com.tmt.base.system.enums;

/**
 * 用户状态
 * @author lifeng
 */
public enum UserStatus {

	MD_P("待修改密码",1),
	LOCK_U("锁定",2),
	MD_P_A_LOCK("待修改密码-锁定",3),
	NARMAL("正常",4),
	OTHER("其他",-1);
	
	private String name;
	private int value;
	
	private UserStatus( String name ,int value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public static String valueOf(int value) {
		for( UserStatus status:UserStatus.values() ) {
			if( status.getValue() == value) {
				return status.getName();
			}
		}
		return UserStatus.OTHER.getName();
	}
}
