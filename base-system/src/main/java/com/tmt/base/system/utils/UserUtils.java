package com.tmt.base.system.utils;

import java.util.List;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.UnavailableSecurityManagerException;
import org.apache.shiro.subject.Subject;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.system.dao.MenuDao;
import com.tmt.base.system.dao.UserDao;
import com.tmt.base.system.entity.Menu;
import com.tmt.base.system.entity.User;
import com.tmt.base.system.security.AuthenticationRealm.Principal;

/**
 * 用户工具类
 * @author lifeng
 *
 */
public class UserUtils {
	
	public static final String CACHE_USER = "user";
	public static final String CACHE_MENU_LIST = "menuList";
	public static final String CACHE_AREA_LIST = "areaList";
	public static final String CACHE_OFFICE_LIST = "officeList";
	
	private static UserDao userDao = SpringContextHolder.getBean(UserDao.class);
	private static MenuDao menuDao = SpringContextHolder.getBean(MenuDao.class);
	
	/**
	 * 用户拥有的顶层菜单
	 * @return
	 */
	public static List<Menu> getTopMenuList(){
		List<Menu> menuList = getMenuList();
		List<Menu> topMenus = Lists.newArrayList();
		if(menuList != null && menuList.size() != 0) {
			for(Menu menu: menuList) {
				if( menu.getLevel() == 1 && "1".equals(menu.getIsShow())) {
					topMenus.add(menu);
				}
			}
		}
		return topMenus;
	}
	
	/**
	 * 用户的菜单
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Menu> getMenuList(){
		List<Menu> menuList = (List<Menu>)getCache(CACHE_MENU_LIST);
		if( menuList == null) {
			User user = getUser();
			if(user.getStatus() == 3){
				menuList = menuDao.queryForList("findByAuthority", user.getId());
			} else if("SuperAdmin".equals(user.getLoginName())) {//系统管理员
				menuList = menuDao.findAll();
			} else {
				menuList = menuDao.queryForList("findByAuthority", user.getId());
			}
			if ( user.getStatus() != 3 ){
				putCache(CACHE_MENU_LIST, menuList);
			}
		}
		return menuList;
	}
	
	public static User getUser(){
		User user = (User)getCache(CACHE_USER);
		if (user == null){
			Principal principal = (Principal)SecurityUtils.getSubject().getPrincipal();
			if (principal!=null){
				user = userDao.findByAccount(principal.getLoginName());
				putCache(CACHE_USER, user);
			}
		}
		if (user == null){
			user = new User();
			SecurityUtils.getSubject().logout();
		}
		return user;
	}
	
	// ============== User Cache ==============
	public static Object getCache(String key) {
		return getCache(key, null);
	}
	
	public static Object getCache(String key, Object defaultValue) {
		Object obj = getCacheMap().get(key);
		return obj==null?defaultValue:obj;
	}

	public static void putCache(String key, Object value) {
		getCacheMap().put(key, value);
	}

	public static void removeCache(String key) {
		getCacheMap().remove(key);
	}
	
	public static Map<String, Object> getCacheMap(){
		Map<String, Object> map = Maps.newHashMap();
		try{
			Subject subject = SecurityUtils.getSubject();
			Principal principal = (Principal)subject.getPrincipal();
			return principal!=null?principal.getCacheMap():map;
		}catch (UnavailableSecurityManagerException e) {
			return map;
		}
	}
	
}
