package com.tmt.base.system.service;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 缓存服务
 * 
 * @author liFeng 2014年6月21日
 */
@Service
public class CacheService {

	@Autowired
	private CacheManager cacheManager;

	public String getDiskStorePath() {
		return this.cacheManager.getConfiguration().getDiskStoreConfiguration()
				.getPath();
	}

	public int getCacheSize() {
		int i = 0;
		String[] arrayOfString1 = this.cacheManager.getCacheNames();
		if (arrayOfString1 != null)
			for (String str : arrayOfString1) {
				Ehcache localEhcache = this.cacheManager.getEhcache(str);
				if (localEhcache != null)
					i += localEhcache.getSize();
			}
		return i;
	}

	public void clear() {
		
	}
}
