package com.tmt.base.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.ehcache.annotations.Cacheable;
import com.googlecode.ehcache.annotations.TriggersRemove;
import com.tmt.base.common.listen.AttributeChangeListen;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.security.Cryptos;
import com.tmt.base.common.service.BaseObserveService;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.system.dao.DictDao;
import com.tmt.base.system.entity.Dict;
import com.tmt.base.system.entity.TreeVO;

public class DictService extends BaseObserveService<Dict,String>{
	
	@Autowired
	private DictDao dictDao;
	
	@Override
	protected BaseIbatisDAO<Dict, String> getEntityDao() {
		return this.dictDao;
	}
	@Transactional
	@TriggersRemove(cacheName="dictCache",removeAll=true)
	public String save(Dict dict){
		Dict parent = this.get(dict.getParentId());
		dict.fillByParent(parent);
		if(IdGen.isInvalidId(dict.getId())) {
			this.insert(dict);
		}else {
			this.update(dict);
		}
		if("1".equals(dict.getEncrypt())) {//加密存储
			dict.setValue(Cryptos.encrypt(dict.getValue(), dict.getId()));
		}
		return dict.getId();
	}
	@Cacheable(cacheName="dictCache")
	public List<Dict> getDictsByType( String dictType ) {
		return this.dictDao.queryForList("findByDictType", dictType);
	}
	@Cacheable(cacheName="dictCache")
	public List<Dict> getDictsByParent( Dict dictId ) {
		return this.dictDao.queryForList("findByParent", dictId);
	}
	@Cacheable(cacheName="dictCache")
	public List<Dict> getDictsByParentKey( String  parentKey ) {
		return this.dictDao.queryForList("findByParentKey", parentKey);
	}
	@Cacheable(cacheName="dictCache")
	public Dict getDictByCode( String  dictCode ) {
		return decrypt(this.dictDao.queryForObject("findByCode", dictCode));
	}
	@Cacheable(cacheName="dictCache")
	public Dict getDictByValue( String  dictValue ) {
		return decrypt(this.dictDao.queryForObject("findByValue", dictValue));
	}
	
	/**
	 * 解密
	 * @param dict
	 * @return
	 */
	public Dict decrypt(Dict dict ) {
		if("1".equals(dict.getEncrypt())) {//解密
			dict.setValue(Cryptos.decrypt(dict.getValue(), dict.getId()));
		}
		return dict;
	}
	
	public List<TreeVO> findTreeList(Map<String,Object> params){
		return this.queryForGenericsList("findTreeList", params);
	}
	
	public int checkDictCode(Dict dict){
		return this.countByCondition("checkDictCode",dict);
	}
	
	@SuppressWarnings("unchecked")
	public void setListens(String ... names) {
		for(String name: names){
			AttributeChangeListen<Dict> listen = (AttributeChangeListen<Dict>)SpringContextHolder.getBean(name);
			this.addAttributeChangeListen(listen);
		}
	}
}
