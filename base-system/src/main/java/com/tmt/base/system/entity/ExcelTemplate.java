package com.tmt.base.system.entity;

import java.util.List;

public class ExcelTemplate extends BaseEntity<String>{
   
	private static final long serialVersionUID = 7028547710749969137L;
    private String templateName;
    private String templateType;
    private String targetClass;
    private Byte startRow;
    private Byte effectiveFlag;
    private String extendAttr;
    private List<ExcelItem> items;
    
	public List<ExcelItem> getItems() {
		return items;
	}
	public void setItems(List<ExcelItem> items) {
		this.items = items;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getTemplateType() {
		return templateType;
	}
	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}
	public String getTargetClass() {
		return targetClass;
	}
	public void setTargetClass(String targetClass) {
		this.targetClass = targetClass;
	}
	public Byte getStartRow() {
		return startRow;
	}
	public void setStartRow(Byte startRow) {
		this.startRow = startRow;
	}
	public Byte getEffectiveFlag() {
		return effectiveFlag;
	}
	public void setEffectiveFlag(Byte effectiveFlag) {
		this.effectiveFlag = effectiveFlag;
	}
	public String getExtendAttr() {
		return extendAttr;
	}
	public void setExtendAttr(String extendAttr) {
		this.extendAttr = extendAttr;
	}
}