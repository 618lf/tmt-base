package com.tmt.base.system.entity;

import com.tmt.base.common.entity.IdEntity;

/**
 * 用户权限
 * @author lifeng
 */
public class UserRole extends IdEntity<String>{
	
	private static final long serialVersionUID = 3500438053558301875L;
	
	private String userId;
    private String roleId;
    
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

}