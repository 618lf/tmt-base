package com.tmt.base.system.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.ExcelItem;

@Repository
public class ExcelItemDao extends BaseIbatisDAO<ExcelItem,String> {

	@Override
	protected String getNamespace() {
		return "SYS_EXCEL_ITEM";
	}
	
}
