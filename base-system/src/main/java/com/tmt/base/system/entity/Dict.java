package com.tmt.base.system.entity;



/**
 * 数据字典
 * @author lifeng
 */
public class Dict extends BaseParentEntity<String>{
   
	private static final long serialVersionUID = -5781019254623705594L;
	private String label;
	private String code;
    private String value;
    private String type;
    private String encrypt;
    private Boolean isLeaf;
    
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Boolean getIsLeaf() {
		return isLeaf;
	}
	public void setIsLeaf(Boolean isLeaf) {
		this.isLeaf = isLeaf;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEncrypt() {
		return encrypt;
	}
	public void setEncrypt(String encrypt) {
		this.encrypt = encrypt;
	}
}