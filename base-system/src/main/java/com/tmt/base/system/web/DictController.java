package com.tmt.base.system.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.Dict;
import com.tmt.base.system.entity.TreeVO;
import com.tmt.base.system.service.DictService;

@Controller
@RequestMapping(value = "${adminPath}/system/dict")
public class DictController extends BaseController {
	
	@Autowired
	private DictService dictService;
	
	@RequestMapping(value = {"treeFrame"})
	public String treeFrame(Model model){
		model.addAttribute("treeUrl", "/system/dict/dictTree");
		return "/system/TreeFrame";
	}
	
	@RequestMapping(value = {"dictTree"})
	public String dictTree(Model model){
		List<TreeVO> trees = this.dictService.findTreeList(new HashMap<String,Object>());
		model.addAttribute("items", trees);
		model.addAttribute("checked", Boolean.FALSE);
		model.addAttribute("treeUrl", "/system/dict/initList?parentId=");
		return "/system/DictTree";
	}
	
	@RequestMapping(value = {"form"})
	public String form(Dict dict,Model model) {
		if( dict != null && dict.getId() != null) {
			dict = this.dictService.get(dict.getId());
		} else {
			if(dict == null) {
				dict = new Dict();
			}
			dict.setId(IdGen.INVALID_ID);
			if(dict.getParentId() != null) {
				Dict parent = this.dictService.get(dict.getParentId());
				if( parent != null) {
					dict.setType(parent.getType());
				}
			}else {
				dict.setParentId(IdGen.INVALID_ID);
			}
		}
		model.addAttribute("dict", dict);
		model.addAttribute("parentId", IdGen.isInvalidId(dict.getId()) ? dict.getParentId():dict.getId());
		return "/system/DictForm";
	}
	
	@RequestMapping(value = {"save"})
	public String save(Dict dict,Model model, RedirectAttributes redirectAttributes){
		//后台验证
		if ( !beanValidator(model, dict) ){
			return form(dict, model);
		}
		this.dictService.save(dict);
		addMessage(redirectAttributes, "保存字典项'" + dict.getLabel() + "'成功");
		redirectAttributes.addAttribute("id", dict.getId());
		return "redirect:"+Globals.getAdminPath()+"/system/dict/form";
	}
	
	@RequestMapping(value = {"initList"})
	public String initList(Dict dict,Model model){
		if(dict!=null && dict.getParentId() != null) {
			model.addAttribute("parentId", dict.getParentId());
		}
		//叶子节点
		if(dict!=null && dict.getIsLeaf() != null && dict.getIsLeaf()) {
			dict.setId(dict.getParentId());
			return form(dict, model);
		}
		return "/system/DictList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Dict dict,Model model,Page page) {
		Map<String,Object> params = new HashMap<String,Object>();
		if( dict != null && !StringUtil3.isBlank(dict.getLabel())) {
			params.put("LABEL", dict.getLabel());
			QueryCondition qc = new QueryCondition();
			Criteria c = qc.createCriteria();
			c.andLike("LABEL", "LABEL", "%"+dict.getLabel()+"%");
			List<Dict> dicts = this.dictService.queryByCondition(qc);
			if( dicts != null && dicts.size() != 0 ) {
				StringBuffer sb = new StringBuffer(",");
				for( Dict orgItem: dicts ) {
					 sb.append(orgItem.getParentIds());
					 sb.append(orgItem.getId()).append(",");
				}
				sb.append("-1");
				params.clear();
				params.put("DICT_IDS", sb.toString());
				//选中
				dict = new Dict();
				dict.setId(dicts.get(0).getId());
			}
		}
		List<TreeVO> trees = this.dictService.findTreeList(params);
		trees = TreeVO.sort(trees);
		if( trees != null && trees.size() != 0 && dict != null && dict.getId() != null && !StringUtil3.isBlank(dict.getId())) {
			dict = this.dictService.get(dict.getId());
			if(dict!=null) {
				for( TreeVO treeVO : trees ){
					if( (","+dict.getParentIds()).indexOf(","+treeVO.getId()+",") != -1) {
						 treeVO.setExpanded(Boolean.TRUE);
					}
				}
			}
		} else {
			if( trees != null && trees.size() != 0 ) {
				trees.get(0).setExpanded(Boolean.TRUE);
			}
		}
		Page pageList = new Page();
		pageList.setData(trees);
		return pageList;
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public Boolean delete(String[] idList , Model model,HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Dict> dicts = Lists.newArrayList();
		Boolean bFalg = Boolean.TRUE;
		for( String id:idList ) {
			 Dict dict = new Dict();
			 dict.setId(id);
			 dicts.add(dict);
		}
		this.dictService.batchDelete(dicts);
		return bFalg;
	}
	
	@ResponseBody
	@RequestMapping(value = {"checkDictCode"})
	public Boolean checkDictCode(Dict dict,Model model) {
		Boolean bFalg = Boolean.TRUE;
		int iCount = this.dictService.checkDictCode(dict);
		if( iCount > 0) {
			bFalg = Boolean.FALSE;
		}
		return bFalg;
	}
}
