package com.tmt.base.system.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.entity.Area;
import com.tmt.base.system.entity.Office;
import com.tmt.base.system.entity.TreeVO;
import com.tmt.base.system.service.AreaService;
import com.tmt.base.system.service.OfficeService;

@Controller
@RequestMapping(value = "${adminPath}/system/office")
public class OfficeController extends BaseController{

	@Autowired
	private OfficeService officeService;
	@Autowired
	private AreaService areaService;
	
	@RequestMapping(value = {"initList", ""})
	public String initList(Office office, Model model) {
		if(office != null && office.getId() != null) {
			model.addAttribute("id", office.getId());
		}
		return "/system/OfficeList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Office office, Model model) {
		Map<String,Object> params = new HashMap<String,Object>();
		if( office != null && !StringUtil3.isBlank(office.getName())) {
			params.put("ORG_NAME", office.getName());
			QueryCondition qc = new QueryCondition();
			Criteria c = qc.createCriteria();
			c.andLike("NAME", "NAME", "%"+office.getName()+"%");
			List<Office> offices = this.officeService.queryByCondition(qc);
			if( offices != null && offices.size() != 0 ) {
				StringBuffer sb = new StringBuffer(",");
				for( Office orgItem: offices ) {
					 sb.append(orgItem.getParentIds());
					 sb.append(orgItem.getId()).append(",");
				}
				sb.append("-1");
				params.clear();
				params.put("ORG_IDS", sb.toString());
				//选中
				office = new Office();
				office.setId(offices.get(0).getId());
			}
		}
		List<TreeVO> trees = this.officeService.findTreeList(params);
		trees = TreeVO.sort(trees);
		if( trees != null && trees.size() != 0 && office != null && office.getId() != null && !StringUtil3.isBlank(office.getId())) {
			office = this.officeService.get(office.getId());
			if(office!=null) {
				for( TreeVO treeVO : trees ){
					if( (","+office.getParentIds()).indexOf(","+treeVO.getId()+",") != -1) {
						 treeVO.setExpanded(Boolean.TRUE);
					}
				}
			}
		} else if(trees != null && trees.size() != 0){
			trees.get(0).setExpanded(Boolean.TRUE);
		}
		Page pageList = new Page();
		pageList.setData(trees);
		return pageList;
	}
	
	@RequestMapping(value = {"form"})
	public String form(Office office, Model model) {
		Office parent = null; Area area = null;
		if( office != null && office.getId() != null && !StringUtil3.isBlank(office.getId()) ) {
			office = this.officeService.get(office.getId());
			parent = this.officeService.get(office.getParentId());
			area = areaService.get(office.getAreaId());
		} else {
			office.setId(IdGen.INVALID_ID);
			if(office.getParentId() == null){
			   office.setParentId(IdGen.INVALID_ID);
			} else {
				parent = this.officeService.get(office.getParentId());
				if( parent != null) {
					area = areaService.get(parent.getAreaId());
				}
			}
		}
		if(parent != null) {
			office.setParentId(parent.getId());
			office.setParentName(parent.getName());
		}
		if( area != null ) {
			office.setAreaId(area.getId());
			office.setAreaName(area.getName());
		}
		//组织类型
		model.addAttribute("office", office);
		return "/system/OfficeForm";
	}
	
	@ResponseBody
	@RequestMapping(value = {"treeSelect"})
	public List<Map<String, Object>> treeSelect(@RequestParam(required=false)String extId, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<TreeVO> trees = this.officeService.findTreeList(new HashMap<String,Object>());
		for (int i=0; i<trees.size(); i++){
			TreeVO e = trees.get(i);
			if (extId == null || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParent());
				map.put("name", e.getTreeName());
				mapList.add(map);
			}
		}
		return mapList;
	}
	@RequestMapping(value = {"save"})
	public String save(Office office, Model model,RedirectAttributes redirectAttributes){
		if ( !beanValidator(model, office) ){
			return form(office, model);
		}
		String Id = this.officeService.save(office);
		addMessage(redirectAttributes, "保存菜单'" + office.getName() + "'成功");
		redirectAttributes.addAttribute("Id", Id);
		return "redirect:"+Globals.getAdminPath()+"/system/office/initList";
	}
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model,HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Office> offices = Lists.newArrayList();
		Boolean bFalg = Boolean.TRUE;
		Office oneParent = null;
		for( String id:idList ) {
			 Office org = this.officeService.get(id);
			 //验证是否可以删除 -- 用户
			 int iCount = this.officeService.deleteOfficeCheck(org);
			 if( iCount > 0){
				 bFalg = Boolean.FALSE;break;
			 }
			 offices.add(org);
			 if(oneParent == null){
				 oneParent = new Office();
				 oneParent.setId(org.getParentId());
			 }
		}
		if(!bFalg) {//删除失败
			return AjaxResult.error("要删除的组织结构存在子组织结构或存在用户!");
		}
		this.officeService.batchDelete(offices);
		AjaxResult result = AjaxResult.success();
		result.setObj(oneParent);
		return result;
	}
}
