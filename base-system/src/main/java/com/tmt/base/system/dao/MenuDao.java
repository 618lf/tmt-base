package com.tmt.base.system.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.Menu;

@Repository
public class MenuDao extends BaseIbatisDAO<Menu,String>{

	@Override
	protected String getNamespace() {
		return "SYS_MENU";
	}
}
