package com.tmt.base.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.dao.MenuDao;
import com.tmt.base.system.dao.RoleDao;
import com.tmt.base.system.dao.RoleMenuDao;
import com.tmt.base.system.dao.UserDao;
import com.tmt.base.system.dao.UserRoleDao;
import com.tmt.base.system.entity.Menu;
import com.tmt.base.system.entity.Role;
import com.tmt.base.system.entity.RoleMenu;
import com.tmt.base.system.entity.TreeVO;
import com.tmt.base.system.entity.User;
import com.tmt.base.system.entity.UserRole;
import com.tmt.base.system.security.AuthenticationRealm;

@Service
public class RoleService extends BaseService<Role,String>{
	
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private MenuDao menuDao;
	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private RoleMenuDao roleMenuDao;
	@Autowired
	private AuthenticationRealm authenticationRealm;
	
	@Override
	protected BaseIbatisDAO<Role, String> getEntityDao() {
		return this.roleDao;
	}
	
	@Override
	public Role get(String id) {
		Role role = super.get(id);
		List<User> roleUsers = userDao.queryForList("findUsersByRoleId", id);
		List<Menu> RoleMenus = menuDao.queryForList("findMenusByRoleId", id);
		if( roleUsers != null) {
			StringBuffer sbUserIds = new StringBuffer(100);
			StringBuffer sbUserNames = new StringBuffer(100);
			for(User u :roleUsers) {
				sbUserIds.append(u.getId()).append(",");
				sbUserNames.append(u.getName()).append(",");
			}
			role.setUserIds(sbUserIds.toString());
			role.setUserNames(sbUserNames.toString());
		}
		if( RoleMenus != null) {
			StringBuffer sbMenuIds = new StringBuffer(100);
			StringBuffer sbMenuNames = new StringBuffer(100);
			StringBuffer sbOptionIds = new StringBuffer(100);
			StringBuffer sbOptionNames = new StringBuffer(100);
			for(Menu m :RoleMenus) {
				if( m.getType() == 2) {
					sbMenuIds.append(m.getId()).append(",");
					sbMenuNames.append(m.getName()).append(",");
				} else {
					sbOptionIds.append(m.getId()).append(",");
					sbOptionNames.append(m.getName()).append(",");
				}
			}
			role.setMenuIds(sbMenuIds.toString());
			role.setMenuNames(sbMenuNames.toString());
			role.setOptionIds(sbOptionIds.toString());
			role.setOptionNames(sbOptionNames.toString());
		}
		return role;
	}
	
	@Transactional
	public String save(Role role){
		if( IdGen.isInvalidId(role.getId())) {
			this.insert(role);
		} else {
			this.update(role);
		}
		String Id = role.getId();
		//删除权限
		List<UserRole> roleUsers = this.userRoleDao.findByRoleId(role.getId());
		List<RoleMenu> roleMenus = this.roleMenuDao.findByRoleId(role.getId());
		this.userRoleDao.batchDelete("delete", roleUsers);
		this.roleMenuDao.batchDelete("delete", roleMenus);
		//存储新权限
		roleUsers = Lists.newArrayList();
		roleMenus = Lists.newArrayList();
		if( StringUtil3.isNotEmpty( role.getUserIds()) ) {
			String[] userIds = role.getUserIds().split(",");
			UserRole roleUser = null;
			for( String userId :userIds ) {
				if(StringUtil3.isNotEmpty(userId)) {
					roleUser = new UserRole();
					roleUser.setUserId(userId);
					roleUser.setRoleId(Id);
					roleUsers.add(roleUser);
				}
			}
		}
		//菜单
		if( StringUtil3.isNotEmpty( role.getMenuIds()) ) {
			String[] menuIds = role.getMenuIds().split(",");
			RoleMenu roleMenu = null;
			for( String menuId :menuIds ) {
				if(StringUtil3.isNotEmpty(menuId)) {
					roleMenu = new RoleMenu();
					roleMenu.setMenuId(menuId);
					roleMenu.setRoleId(Id);
					roleMenus.add(roleMenu);
				}
			}
		}
		//操作
		if( StringUtil3.isNotEmpty( role.getOptionIds()) ) {
			String[] menuIds = role.getOptionIds().split(",");
			RoleMenu roleMenu = null;
			for( String menuId :menuIds ) {
				if(StringUtil3.isNotEmpty(menuId)) {
					roleMenu = new RoleMenu();
					roleMenu.setMenuId(menuId);
					roleMenu.setRoleId(Id);
					roleMenus.add(roleMenu);
				}
			}
		}
		this.roleMenuDao.batchInsert("insert", roleMenus);
		this.userRoleDao.batchInsert("insert", roleUsers);
		//清空缓存
		this.authenticationRealm.clearAllCachedAuthorizationInfo();
		return role.getId();
	}
	
	@Transactional
	public void batchDelete( List<Role> roles ) {
		for( Role role: roles ) {
			//删除权限
			List<UserRole> roleUsers = this.userRoleDao.findByRoleId(role.getId());
			List<RoleMenu> roleMenus = this.roleMenuDao.findByRoleId(role.getId());
			this.userRoleDao.batchDelete("delete", roleUsers);
			this.roleMenuDao.batchDelete("delete", roleMenus);
		}
		super.batchDelete(roles);
		//清空缓存
		this.authenticationRealm.clearAllCachedAuthorizationInfo();
	}
	
	public List<TreeVO> findTreeList(Map<String,Object> params){
		return this.queryForGenericsList("findTreeList", params);
	}
	
	public int officeSelectCheck( String id ){
		return this.countByCondition("officeSelectCheck",id);
	}
	
	public List<Role> findByUserId( String userId){
		return this.queryForList("findByUserId", userId);
	}
}
