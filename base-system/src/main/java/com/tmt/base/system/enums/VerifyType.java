package com.tmt.base.system.enums;

public enum VerifyType {
	OTHER("无"),UNIQUE("唯一"), NOT_NULL("不为空");
	private String name;
	private VerifyType( String name ) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
