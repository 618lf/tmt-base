package com.tmt.base.system.utils;

import java.util.List;

import com.tmt.base.system.entity.Menu;

/**
 * 菜单显示策略
 * @author lifeng
 */
public interface IMenuDisplay {

	/**
	 * 返回显示Menu的字符串
	 * @param menuList
	 * @param screenSize
	 * @return
	 */
	public String getUIMenu(List<Menu> menuList, double screenSize);
}
