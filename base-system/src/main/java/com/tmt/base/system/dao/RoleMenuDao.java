package com.tmt.base.system.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.system.entity.RoleMenu;

@Repository
public class RoleMenuDao extends BaseIbatisDAO<RoleMenu, String>{

	@Override
	protected String getNamespace() {
		return "SYS_ROLE_MENU";
	}
	
	public List<RoleMenu> findByRoleId(String roleId){
		return this.queryForList("findByRoleId", roleId);
	}
}
