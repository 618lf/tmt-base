package com.tmt.base.system.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.mapper.JsonMapper;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.entity.Menu;
import com.tmt.base.system.entity.TreeVO;
import com.tmt.base.system.service.MenuService;
import com.tmt.base.system.service.SystemService;
import com.tmt.base.system.utils.MenuUtils;

@Controller
@RequestMapping(value = "${adminPath}/system/menu")
public class MenuController extends BaseController {

	@Autowired
	private SystemService systemService;
	@Autowired
	private MenuService menuService;
	
	/**
	 * 左边菜单
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = {"userMenu"})
	public String userMenu(Menu parent, Model model,HttpServletRequest request){
		List<Menu> menuList = systemService.getMenuList();
		List<Menu> children = Lists.newArrayList();
		if(parent != null && StringUtil3.isNotBlank(parent.getId()) && menuList != null && menuList.size() != 0){
			for(Menu menu: menuList) {
				if( menu.getParentId().compareTo(parent.getId()) == 0 
						|| menu.getId().compareTo(parent.getId()) == 0
						|| StringUtil3.contains(menu.getParentIds(), ","+parent.getId()+",")) {
					children.add(menu);
				}
			}
		} else {
			children = menuList;
		}
		model.addAttribute("menuList", children);
		model.addAttribute("screenSize",request.getParameter("screenSize"));
		return "/system/UserMenu";
	} 
	 
	@ResponseBody
	@RequestMapping(value = {"menuNav"})
	public AjaxResult menuNav(String id, HttpServletResponse response){
		response.setContentType("application/json; charset=UTF-8");
		Menu menu = this.systemService.getMenu(id);
		List<Menu> parents = this.systemService.findParentMenus(menu);
		List<Menu> brothers = this.systemService.findChildrenMenus(menu);
		AjaxResult jsonObj = new AjaxResult();
		jsonObj.setObj(menu);
		jsonObj.setAttribute("parents",parents);
		jsonObj.setAttribute("brothers", brothers);
		return jsonObj;
	}
	
	@RequestMapping(value = {"treeFrame"})
	public String treeFrame(Model model){
		model.addAttribute("treeUrl", "/system/menu/menuTree");
		return "/system/TreeFrame";
	}
	
	@RequestMapping(value = {"menuTree"})
	public String menuTree(Model model){
		List<TreeVO> trees = this.menuService.findTreeList(new HashMap<String,Object>());
		model.addAttribute("items", trees);
		model.addAttribute("checked", Boolean.FALSE);
		model.addAttribute("treeUrl", "/system/menu/form?id=");
		return "/system/MenuTree";
	}
	
	@RequestMapping(value = {"initList", ""})
	public String initList(Menu menu, Model model) {
		if(menu != null && menu.getId() != null) {
			model.addAttribute("id", menu.getId());
		}
		return "/system/MenuList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Menu menu,Model model) {
		Map<String,Object> params = new HashMap<String,Object>();
		if(menu!=null && !StringUtil3.isBlank(menu.getName())) {
			params.put("MENU_NAME", menu.getName());
		}
		if(menu!=null && !StringUtil3.isBlank(menu.getHref())) {
			params.put("MENU_HREF", menu.getHref());
		}
		List<Menu> menus = this.menuService.findByCondition(params);
		if( menus != null && menus.size() != 0 ) {
			StringBuffer sb = new StringBuffer(100);
			for( Menu menuItem: menus ) {
				 sb.append(menuItem.getParentIds());
				 sb.append(menuItem.getId()).append(",");
			}
			sb.append("-1");
			params.clear();
			params.put("MENU_IDS", sb.toString());
		}
		if(menu!=null && menu.getId() != null){
			menu = this.menuService.get(menu.getId());
		}
		menus = this.menuService.findByCondition(params);
		if( menus != null) {
			for( Menu menuItem: menus ) {
				 menuItem.setId(menuItem.getId());
				 menuItem.setParent(menuItem.getParentId());
				 menuItem.setLevel(menuItem.getLevel());
				 menuItem.setExpanded(Boolean.FALSE);
				 menuItem.setLoaded(Boolean.TRUE);
				 menuItem.setIsLeaf(Boolean.TRUE);
				 if( menu!=null && menu.getId() != null && ( (","+menu.getParentIds()+",").indexOf(","+menuItem.getId()+",") != -1)) {
					 menuItem.setExpanded(Boolean.TRUE);
				 }
			}
		}
		List<Menu> copyMenus = MenuUtils.sort(menus);
		if( copyMenus != null && copyMenus.size() != 0 && !(menu!=null && menu.getId() != null)) {
			copyMenus.get(0).setExpanded(Boolean.TRUE);
		}
		Page pageList = new Page();
		pageList.setData(copyMenus);
		return pageList;
	}
	
	@RequestMapping(value = {"form"})
	public String form(Menu menu , Model model) {
		if(menu != null && menu.getId() != null) {
			menu = this.menuService.get(menu.getId());
		} else {
			menu.setId(IdGen.INVALID_ID); 
			if(menu.getParentId() == null){
				menu.setParentId(IdGen.ROOT_ID);
			}
		}
		Menu parent = this.menuService.get(menu.getParentId());
		menu.setParentMenu(parent);
		model.addAttribute("menu", menu);
		return "/system/MenuForm";
	}
	
	@ResponseBody
	@RequestMapping(value = {"treeSelect"})
	public List<Map<String, Object>> treeSelect(@RequestParam(required=false)String extId, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<Menu> list = this.menuService.findAllWithRoot(new HashMap<String,Object>());
		for (int i=0; i<list.size(); i++){
			Menu e = list.get(i);
			if (extId == null || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)
				&& e.getType() != 3){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("name", e.getName());
				if(e.getType() == 1) {//目录判断是否可以选择
					map.put("chkDisabled", Boolean.TRUE);
					int iCount = this.menuService.treeMenuCheck(e.getId());
				    if(iCount >0) {
				    	map.put("chkDisabled", Boolean.FALSE);
				    }
				}
				mapList.add(map);
			}
		}
		return mapList;
	}
	
	/**
	 * 菜单 细分权限的选择
	 * @param extId
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = {"treeSelect/option"})
	public List<Map<String, Object>> treeSelect4Option(@RequestParam(required=false)String extId, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<Menu> list = this.menuService.findAllWithRoot(new HashMap<String,Object>());
		for (int i=0; i<list.size(); i++){
			Menu e = list.get(i);
			if (extId == null || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("name", e.getName());
				if(e.getType() == 1 || e.getType() == 2) {//目录判断是否可以选择
					map.put("chkDisabled", Boolean.TRUE);
					int iCount = this.menuService.treeMenuCheck(e.getId());
				    if(iCount >0) {
				    	map.put("chkDisabled", Boolean.FALSE);
				    }
				}
				mapList.add(map);
			}
		}
		return mapList;
	}
	
	@RequestMapping(value = {"save"})
	public String save(Menu menu, Model model, RedirectAttributes redirectAttributes) {
		if ( !beanValidator(model, menu) ){
			return form(menu, model);
		}
		String id = this.menuService.save(menu);
		addMessage(redirectAttributes, "保存菜单'" + menu.getName() + "'成功");
		redirectAttributes.addAttribute("id", id);
		return "redirect:"+Globals.getAdminPath()+"/system/menu/form";
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model,HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<Menu> menus = Lists.newArrayList();
		Boolean bFalg = Boolean.TRUE;
		for( String id:idList ) {
			Menu menu = new Menu();
			menu.setId(id);
			//验证是否可以删除
			int count = this.menuService.deleteMenuCheck(menu);
			if( count > 0 ) {
				bFalg = Boolean.FALSE;break;
			}
			menus.add(menu);
		}
		if(!bFalg) {//删除失败
			return AjaxResult.error("要删除的菜单中存在子菜单，或分配了用户,不能删除");
		}
		this.menuService.batchDelete(menus);
		return AjaxResult.success();
	}
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = {"sort"})
	public AjaxResult sort(Model model, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		String postData = request.getParameter("postData");
		List<Map<String,String>> maps = JsonMapper.getInstance().fromJson(postData, ArrayList.class);
		if(maps != null && maps.size() != 0){
			List<Menu> menus = new ArrayList<Menu>();
			for(Map<String,String> map: maps) {
				Menu menu = new Menu();
				menu.setId(map.get("id"));
				menu.setSort(Integer.parseInt(map.get("sort")));
				menus.add(menu);
			}
			this.menuService.updateSort(menus);
			return AjaxResult.success();
		}
		return AjaxResult.error("没有需要保存的数据");
	}
}
