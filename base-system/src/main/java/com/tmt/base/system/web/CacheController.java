package com.tmt.base.system.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tmt.base.common.config.Globals;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.service.CacheService;

/**
 * 缓存管理
 * 
 * @author liFeng 2014年6月21日
 */
@Controller
@RequestMapping(value = "${adminPath}/system/cache")
public class CacheController extends BaseController {

	@Autowired
	private CacheService cacheService;
	
	@RequestMapping(value = { "/clear" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	public String clear(ModelMap model) {
		Long localLong1 = null;
		Long localLong2 = null;
		Long localLong3 = null;
		try {
			localLong1 = Long.valueOf(Runtime.getRuntime().totalMemory() / 1024L / 1024L);
			localLong2 = Long.valueOf(Runtime.getRuntime().maxMemory() / 1024L / 1024L);
			localLong3 = Long.valueOf(Runtime.getRuntime().freeMemory() / 1024L / 1024L);
		} catch (Exception localException) {
		}
		model.addAttribute("totalMemory", localLong1);
		model.addAttribute("maxMemory", localLong2);
		model.addAttribute("freeMemory", localLong3);
		model.addAttribute("cacheSize",Integer.valueOf(this.cacheService.getCacheSize()));
		model.addAttribute("diskStorePath", this.cacheService.getDiskStorePath());
		return "system/clear";
	}
	
	@RequestMapping(value = { "/clear" }, method = { org.springframework.web.bind.annotation.RequestMethod.POST })
	public String clear(RedirectAttributes redirectAttributes) {
		this.cacheService.clear();
		return "redirect:"+Globals.getAdminPath()+"system/cache/clear";
	}

}
