package com.tmt.base.system.entity;

import com.tmt.base.system.enums.DataType;
import com.tmt.base.system.enums.VerifyType;

public class ExcelItem extends BaseEntity<String>{
    
	private static final long serialVersionUID = 5012884923584792525L;
	private String itemName;
    private String templateId;
    private String columnName;
    private String property;
    private DataType dataType;
    private String dataFormat;
    private VerifyType verifyType;
    private String verifyFormat;
    
	public VerifyType getVerifyType() {
		return verifyType;
	}
	public String getVerifyTypeStr() {
		if(verifyType != null) {
			return verifyType.getName();
		}
		return null;
	}
	public void setVerifyType(VerifyType verifyType) {
		this.verifyType = verifyType;
	}
	public String getVerifyFormat() {
		return verifyFormat;
	}
	public void setVerifyFormat(String verifyFormat) {
		this.verifyFormat = verifyFormat;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public DataType getDataType() {
		return dataType;
	}
	public String getDataTypeStr(){
		if( dataType != null) {
			return dataType.getName();
		}
		return null;
	}
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}
	public String getDataFormat() {
		return dataFormat;
	}
	public void setDataFormat(String dataFormat) {
		this.dataFormat = dataFormat;
	}

}