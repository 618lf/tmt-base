package com.tmt.base.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.system.dao.AreaDao;
import com.tmt.base.system.entity.Area;
import com.tmt.base.system.entity.TreeVO;

@Service
public class AreaService extends BaseService<Area, String>{

	@Autowired
	private AreaDao areaDao;
	
	@Override
	protected BaseIbatisDAO<Area, String> getEntityDao() {
		return areaDao;
	}
	
	@Transactional
	public String save(Area area){
		String oldParentIds = area.getParentIds();
		Area parent = this.get(area.getParentId());
		area.fillByParent(parent);
		if( IdGen.isInvalidId( area.getId()) ) {
		    this.insert(area);
		} else {
			this.update(area);
			List<Area> children = this.findByParent(area);
			for( Area o : children ) {
				o.updatePathByParent(area, oldParentIds);
			}
			this.batchUpdate(children);
		}
		return area.getId();
	}
	
	/**
	 * 通过父区域查询所有的子区域
	 * @param parent
	 * @return
	 */
	public List<Area> findByParent(Area parent){
		QueryCondition qc = new QueryCondition();
		Criteria c = qc.createCriteria();
		c.andEqualTo("PARENT_ID", "PARENT_ID", parent.getId());
		return this.queryByCondition(qc);
	}
	
	/**
	 * 通过查询条件查询
	 * @param params
	 * @return
	 */
	public List<TreeVO> findTreeList(Map<String,Object> params){
		return this.queryForGenericsList("findTreeList", params);
	}
	
	public int deleteAreaCheck(Area entity){
		return this.countByCondition("deleteAreaCheck",entity);
	}
}
