package com.tmt.base.system.security;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;

import com.tmt.base.common.utils.CookieUtils;

public class AuthenticationFilter extends FormAuthenticationFilter{
	
	public static final String DEFAULT_CAPTCHA_PARAM = "validateCode";
	private String captchaParam = DEFAULT_CAPTCHA_PARAM;
	
	public String getCaptchaParam() {
		return captchaParam;
	}
	
	protected String getCaptcha(ServletRequest request) {
		return WebUtils.getCleanParam(request, getCaptchaParam());
	}
	
	protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
		String username = this.getUsername(request);
		String password = this.getPassword(request);
		if (password==null){
			password = "";
		}
		boolean rememberMe = this.isRememberMe(request);
		String host = this.getHost(request);
		String captcha = this.getCaptcha(request);
		if( rememberMe ) {
			CookieUtils.setCookie((HttpServletResponse)response, FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, username);
			CookieUtils.setCookie((HttpServletResponse)response, FormAuthenticationFilter.DEFAULT_PASSWORD_PARAM, password);
		} else {
			CookieUtils.remove((HttpServletRequest)request, (HttpServletResponse)response, FormAuthenticationFilter.DEFAULT_USERNAME_PARAM);
			CookieUtils.remove((HttpServletRequest)request, (HttpServletResponse)response, FormAuthenticationFilter.DEFAULT_PASSWORD_PARAM);
		}
		return new UsernamePasswordToken(username, password.toCharArray(), rememberMe, host, captcha);
	}
}
