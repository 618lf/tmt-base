package com.tmt.base.system.entity;

import java.util.List;

import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.incrementer.IdGen;

/**
 * 菜单
 * @author lifeng
 */
public class Menu extends BaseTreeEntity<String>{
	
	private static final long serialVersionUID = 8430566005417611507L;
	private String parentId;
    private String parentIds;
    private String name;
    private String href;
    private String target;
    private String iconClass;
    private Integer sort;
    private Integer level;
    private String isShow;
    private String isActiviti;
    private String permission;
    private Integer type;//菜单类型 ： 1 目录 。。。
    private Boolean isLeaf;
    private Integer quickMenu;//是否快捷菜单
    private Integer topMenu;//是否置顶菜单
    
	private int childrenCount;
    private List<Menu> children;
    private Menu parentMenu;

	public Integer getQuickMenu() {
		return quickMenu;
	}
	public void setQuickMenu(Integer quickMenu) {
		this.quickMenu = quickMenu;
	}
	
	public Integer getTopMenu() {
		return topMenu;
	}
	public void setTopMenu(Integer topMenu) {
		this.topMenu = topMenu;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Menu getParentMenu() {
		return parentMenu;
	}
	public void setParentMenu(Menu parentMenu) {
		this.parentMenu = parentMenu;
	}
	public List<Menu> getChildren() {
		return children;
	}
	public void setChildren(List<Menu> children) {
		this.children = children;
	}
	
	public void addChild( Menu child) {
		if( this.children == null ) {
			this.children = Lists.newArrayList();
		}
		this.children.add(child);
	}
	public int getChildrenCount() {
		return childrenCount;
	}
	public void setChildrenCount(int childrenCount) {
		this.childrenCount = childrenCount;
	}
	public Boolean getIsLeaf() {
		return isLeaf;
	}
	public void setIsLeaf(Boolean isLeaf) {
		this.isLeaf = isLeaf;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getParentIds() {
		return parentIds;
	}
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getIconClass() {
		return iconClass;
	}
	public void setIconClass(String icon) {
		this.iconClass = icon;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getIsShow() {
		return isShow;
	}
	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}
	public String getIsActiviti() {
		return isActiviti;
	}
	public void setIsActiviti(String isActiviti) {
		this.isActiviti = isActiviti;
	}
	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	
	public void fillByParent(Menu parent){
		int level = parent == null?-1:parent.getLevel();
		String parentIds = parent == null?IdGen.INVALID_ID:(parent.getParentIds() + this.getParentId());
		this.setLevel(level + 1);
		this.setParentIds(parentIds + IDS_SEPARATE);
	}
    
}