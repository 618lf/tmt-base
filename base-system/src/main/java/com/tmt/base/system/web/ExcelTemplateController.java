package com.tmt.base.system.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.tmt.base.common.mapper.JsonMapper;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.entity.ExcelTemplate;
import com.tmt.base.system.service.ExcelTemplateService;

@Controller
@RequestMapping(value = "${adminPath}/system/excel")
public class ExcelTemplateController extends BaseController{
	
	@Autowired
	private ExcelTemplateService excelTemplateService;
	
	@RequestMapping(value = {"initList", ""})
	public String initList(){
		return "/system/ExcelTemplateList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Model model, Page page, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		page  = this.excelTemplateService.queryForPage(qc, param);
		return page;
	}
	
	@RequestMapping(value = {"excelForm"})
	public String excelForm(ExcelTemplate template,Model model) {
		if( template != null && template.getId() != null) {
			template = this.excelTemplateService.get(template.getId());
		} else {
			template = new ExcelTemplate();
			template.setId(IdGen.INVALID_ID);
		}
		model.addAttribute("template", template);
		return "/system/ExcelTemplateForm";
	}
	
	@ResponseBody
	@RequestMapping(value = {"save"})
	public AjaxResult save(ExcelTemplate template, Model model, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		String postData = request.getParameter("postData");
		template =  JsonMapper.getInstance().fromJson(postData, ExcelTemplate.class);
		if ( !beanValidator(model, template) ){
			return AjaxResult.error("修改失败");
		}
		this.excelTemplateService.saveTemplate(template);
		return AjaxResult.success();
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList , Model model, HttpServletResponse response) {
		response.setContentType("application/json; charset=UTF-8");
		List<ExcelTemplate> templates = Lists.newArrayList();
		for(String id: idList ) {
			ExcelTemplate template = new ExcelTemplate();
			template.setId(id);
			templates.add(template);
		}
		this.excelTemplateService.delete(templates);
		return AjaxResult.success();
	}
}
