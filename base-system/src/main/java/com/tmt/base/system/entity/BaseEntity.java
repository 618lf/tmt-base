package com.tmt.base.system.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tmt.base.common.entity.IdEntity;
import com.tmt.base.common.utils.DateUtil3;
import com.tmt.base.system.utils.UserUtils;

/**
 * 基础实体类
 * @author lifeng
 * @param <PK>
 */
public class BaseEntity<PK> extends IdEntity<PK> {

	private static final long serialVersionUID = -26225184078729957L;

	//path 中的分隔
	public static final String PATH_SEPARATE = "/";
	//IDS 中的分隔
	public static final String IDS_SEPARATE = ",";
	// 显示/隐藏
	public static final String SHOW = "1";
	public static final String HIDE = "0";
	
	// 是/否
	public static final String YES = "1";
	public static final String NO = "0";

	// 删除标记（0：正常；1：删除；2：审核；）
	public static final String DEL_FLAG = "delFlag";
	public static final String DEL_FLAG_NORMAL = "0";
	public static final String DEL_FLAG_DELETE = "1";
	public static final String DEL_FLAG_AUDIT = "2";
	
	//公有字段
	protected String createId;//创建人ID
	protected String createName;//创建人名称
	protected Date createDate;//创建时间
    protected String updateId;//修改人ID
    protected String updateName;//修改人名称
	protected Date updateDate;//修改时间
    protected String delFlag; // 删除标记（0：正常；1：删除；2：审核）
    protected String remarks;//描述
    
    public BaseEntity() {
		super();
		this.delFlag = DEL_FLAG_NORMAL;
	}
    
    public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getUpdateId() {
		return updateId;
	}

	public void setUpdateId(String updateId) {
		this.updateId = updateId;
	}

	public String getUpdateName() {
		return updateName;
	}

	public void setUpdateName(String updateName) {
		this.updateName = updateName;
	}

	public Date getCreateDate() {
		return createDate;
	}
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	@Override
	public PK prePersist(Serializable key ) {
		PK pk = super.prePersist(key);
		this.createId = UserUtils.getUser().getId();
		this.createName = UserUtils.getUser().getName();
		this.createDate = DateUtil3.getTimeStampNow();
		return pk;
	}
	@Override
	public void preUpdate() {
		this.updateId = UserUtils.getUser().getId();
		this.updateName = UserUtils.getUser().getName();
		this.updateDate = DateUtil3.getTimeStampNow();
	}
}
