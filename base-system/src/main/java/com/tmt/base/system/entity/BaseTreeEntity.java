package com.tmt.base.system.entity;

import java.io.Serializable;

/**
 * jqGrid Tree 基础节点
 * @author lifeng
 *
 */
public class BaseTreeEntity<PK> extends BaseEntity<PK> implements Serializable{

	private static final long serialVersionUID = -7343926348509677125L;
	
	//jqGrid tree 节点
	private String parent;
	private Integer level; //从0 开始
	private Boolean isLeaf = Boolean.TRUE; //是否子节点
	private Boolean expanded = Boolean.FALSE;//是否展开
	private Boolean loaded = Boolean.TRUE;//是否不动态加载 
	private String parentIds;
	
	public String getParentIds() {
		return parentIds;
	}
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Boolean getIsLeaf() {
		return isLeaf;
	}
	public void setIsLeaf(Boolean isLeaf) {
		this.isLeaf = isLeaf;
	}
	public Boolean getExpanded() {
		return expanded;
	}
	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}
	public Boolean getLoaded() {
		return loaded;
	}
	public void setLoaded(Boolean loaded) {
		this.loaded = loaded;
	}
}
