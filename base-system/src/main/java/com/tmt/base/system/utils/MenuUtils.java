package com.tmt.base.system.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.tmt.base.system.entity.Menu;

/**
 * 菜单展示工具类
 * @author lifeng
 */
public class MenuUtils {
	
	public static Map<Integer,List<Menu>> classifyByLevelOnlyShow(List<Menu> menuList){
		return classifyByLevel(menuList, new Filter(){
			@Override
			public Boolean filter(Menu menu) {
				return menu != null && "1".equals(menu.getIsShow())?Boolean.TRUE:Boolean.FALSE;
			}
		});
	}
	//按级别存储
	public static Map<Integer,List<Menu>> classifyByLevel(List<Menu> menuList){
		return classifyByLevel(menuList, new Filter(){
			@Override
			public Boolean filter(Menu menu) {
				return Boolean.TRUE;
			}
		});
	}
	//按级别存储
	public static Map<Integer,List<Menu>> classifyByLevel(List<Menu> menuList, Filter filter){
		Map<Integer,List<Menu>> menuMap = new HashMap<Integer,List<Menu>>();
		for( Menu menu: menuList ) {
			if(filter.filter(menu)) { //按条件过滤
				if( !menuMap.containsKey(menu.getLevel()) ) {
					List<Menu> menus = new ArrayList<Menu>();
					menuMap.put(menu.getLevel(), menus);
				}
				menuMap.get(menu.getLevel()).add(menu);
			}
		}
		return menuMap;
	}
	//处理子节点
	public static void classifyByLevel(Menu parent,int level,Map<Integer,List<Menu>> menuMap) {
		List<Menu> firstMenu = menuMap.get(level); //level从1开始
		if(firstMenu == null){ return;}
		for(Menu menu: firstMenu) {
			if(menu.getParentId().compareTo(parent.getId()) == 0) {
				parent.addChild(menu);
				classifyByLevel(menu,level+1,menuMap);
			}
		}
	}
	//排序 并放入 copyMenus
	public static List<Menu> sort( List<Menu> menus ) {
		List<Menu> copyMenus = Lists.newArrayList();
		Map<Integer,List<Menu>> menuMap = classifyByLevel(menus);
		int firstLevel = 1;
		List<Menu> menuList = menuMap.get(firstLevel);
		if( menuList != null ) {
			for( Menu menu: menuList ) {
				 copyMenus.add(menu);
				 List<Menu> child = sort(menu,firstLevel+1,menuMap);
				 if( child != null && child.size() != 0 ) {
					 copyMenus.addAll(child);
					 menu.setIsLeaf(Boolean.FALSE);
				 }
			}
		}
		return copyMenus;
	}
	public static List<Menu> sort( Menu parent, int level,Map<Integer,List<Menu>> menuMap ) {
		List<Menu> copyMenus = Lists.newArrayList();
		List<Menu> menuList = menuMap.get(level);
		if( menuList != null ) {
			for( Menu menu: menuList ) {
				if(menu.getParentId().compareTo(parent.getId()) == 0) {
					 copyMenus.add(menu);
					 List<Menu> child = sort(menu,level+1,menuMap);
					 if( child != null && child.size() != 0 ) {
						 copyMenus.addAll(child);
						 menu.setIsLeaf(Boolean.FALSE);
					 }
				}
			}
		}
		return copyMenus;
	}
	
	public static interface Filter{
		public Boolean filter(Menu menu);
	}
}
