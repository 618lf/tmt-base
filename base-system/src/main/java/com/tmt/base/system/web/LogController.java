package com.tmt.base.system.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.Log;
import com.tmt.base.system.service.LogService;

@Controller
@RequestMapping(value = "${adminPath}/system/log")
public class LogController extends BaseController{

	@Autowired
	private LogService logService;
	
	@RequestMapping(value = {"initList", ""})
	public String initList() {
		return "/system/LogList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Log log,Model model,Page page) {
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		qc.setOrderByClause(" CREATE_DATE DESC ");
		Criteria c = qc.createCriteria();
		if(StringUtil3.isNotBlank(log.getCreateName())) {
			c.andEqualTo("CREATE_ID", "CREATE_ID", log.getCreateName());
		}
		if(StringUtil3.isNotBlank(log.getType())) {
			c.andEqualTo("TYPE", "TYPE", log.getType());
		}
		if(StringUtil3.isNotBlank(log.getRemoteAddr())) {
			c.andEqualTo("REMOTE_ADDR", "REMOTE_ADDR", log.getRemoteAddr());
		}
		if(StringUtil3.isNotBlank(log.getRequestUri())) {
			c.andEqualTo("REQUEST_URI", "REQUEST_URI", log.getRequestUri());
		}
		if(StringUtil3.isNotBlank(log.getUserAgent())) {
			c.andEqualTo("USER_AGENT", "USER_AGENT", log.getUserAgent());
		}
		page  = this.logService.queryForPage(qc, param);
		return page;
	}
	
	@RequestMapping(value = {"form"})
	public String form(Log log, Model model){
		if( log != null && log.getId() != null ) {
			log = this.logService.get(log.getId());
		}
		model.addAttribute("log", log);
		return "/system/LogForm";
	}
}
