package com.tmt.base.system.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.system.dao.ExcelItemDao;
import com.tmt.base.system.entity.ExcelItem;
import com.tmt.base.system.entity.ExcelTemplate;

@Service
public class ExcelItemService extends BaseService<ExcelItem,String>{

	@Autowired
	private ExcelItemDao excelItemDao;
	
	@Override
	protected BaseIbatisDAO<ExcelItem, String> getEntityDao() {
		return this.excelItemDao;
	}
	
	@Transactional
	public void save( ExcelTemplate template) {
		List<ExcelItem> oldItems = this.findByTempleteId(template.getId());
		if(oldItems != null) {
			this.batchDelete(oldItems);
		}
		if( template.getItems() != null ) {
			for(ExcelItem item : template.getItems()) {
				item.setTemplateId(template.getId());
			}
			this.batchInsert(template.getItems());
		}
	}
	
	public List<ExcelItem> findByTempleteId( String templateId ) {
		return this.queryForList("findByTempleteId", templateId);
	}
}
