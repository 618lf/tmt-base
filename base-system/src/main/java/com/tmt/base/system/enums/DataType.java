package com.tmt.base.system.enums;

public  enum DataType{
	MONEY("金额"), NUMBER("数字"),
	STRING("文本"), DATE("日期"), BOOLEAN("布尔");
	private String name;
	private DataType( String name ) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
