package com.tmt.base.system.entity;


/**
 * 地区
 * 
 * @author lifeng
 */
public class Area extends BaseParentEntity<String> {

	private static final long serialVersionUID = -4665339784591518513L;

	private String code;
	private String name;
	private String type;
	private String parentName;
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 将父区域中的属性添加到子区域中
	 * 
	 * @param parent
	 */
	public void fillByParent(Area parent) {
		super.fillByParent(parent);
		this.setPath((parent == null?"":parent.getPath())  + PATH_SEPARATE + this.getName());
	}

	/**
	 * 父节点的Path修改之后，所有的子节点都要修改
	 * 
	 * @param parent
	 * @param oldParentIds
	 */
	public void updatePathByParent(Area parent, String oldParentIds) {
		super.updatePathByParent(parent, oldParentIds);
		this.setPath(parent.getPath() + PATH_SEPARATE + this.getName());
	}

}