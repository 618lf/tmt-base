package com.tmt.base.component.excel.imp;

import java.util.Map;

/**
 * 泛型实体是 Map<String,Object>
 * @author liFeng
 * 2014年9月25日
 */
public abstract class MapObjExcelMapper extends AbstractExcelMapper<Map<String,Object>>{

	public IReceiver<Map<String,Object>> getReceiver() {
		if( receiver == null){
			receiver = new IReceiver<Map<String,Object>>(){
				@Override
				public Map<String, Object> receive( Map<String, Object> valueMap,
						Class<Map<String, Object>> clazz) {
					return valueMap;
				}
			};
		}
		return receiver;
	}
}
