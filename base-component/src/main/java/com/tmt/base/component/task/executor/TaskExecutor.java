package com.tmt.base.component.task.executor;

/**
 * 定时任务任务执行器
 * @author lifeng
 */
public interface TaskExecutor {
	//执行类调用的方法
	public abstract Boolean doTask();
}
