package com.tmt.base.component.email;

import java.io.File;
import java.util.Map;

import javax.mail.internet.MimeMessage;

/**
 * 邮件服务基类
 * @author lifeng
 */
public interface BaseMailService {

	public Boolean sendNotificationMail( String[] toArray , String[]  CcArray ,Map<String,Object> context );
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 不带附件
	 * toArray : 发送
	 * CcArray : 抄送 
	 */
	public Boolean sendNotificationMail( String[] toArray , String[]  CcArray ,Map<String,Object> context , File[] attachments );
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 不带附件
	 * toArray : 发送
	 * CcArray : 抄送
	 */
	public Boolean sendNotificationMail( String subject , String[] toArray , String[]  CcArray ,String templateName , Map<String,Object> context );
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 并带附件  基础
	 */
	public Boolean sendNotificationMail( String subject , String[] toArray , String[]  CcArray ,String templateName ,Map<String,Object> context , File[] attachments );
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 不带附件
	 * toArray : 发送
	 * CcArray : 抄送
	 */
	public Boolean sendNotificationMail( String[] toArray , String[]  CcArray ,String templateName , Map<String,Object> context );
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 并带附件  基础
	 */
	public Boolean sendNotificationMail( String[] toArray , String[]  CcArray ,String templateName ,Map<String,Object> context , File[] attachments );
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 自定义格式
	 */
	public Boolean sendNotificationMail( MimeMessage msg );
}
