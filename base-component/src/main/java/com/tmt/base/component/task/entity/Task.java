package com.tmt.base.component.task.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tmt.base.common.orm.annotion.Entity;
import com.tmt.base.system.entity.BaseEntity;
/**
 * 任务
 * @author lifeng
 */
public class Task extends BaseEntity<String>{
	
	private static final long serialVersionUID = -1301574146709137781L;
	private String name;
    private TaskType type;
    private String cronExpression;
    private Integer allowExecuteCount;
    private Integer yetExecuteCount;
    private Integer failExecuteCount;
    private Date preExecuteTime;
    private Date nextExecuteTime;
    private String businessObject;
    private String businessObjectName;
    private Short taskStatus;
    private Integer manualOperation;
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TaskType getType() {
		return type;
	}
	public void setType(TaskType type) {
		this.type = type;
	}
	public String getCronExpression() {
		return cronExpression;
	}
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	public Integer getAllowExecuteCount() {
		return allowExecuteCount;
	}
	public void setAllowExecuteCount(Integer allowExecuteCount) {
		this.allowExecuteCount = allowExecuteCount;
	}
	public Integer getYetExecuteCount() {
		return yetExecuteCount;
	}
	public void setYetExecuteCount(Integer yetExecuteCount) {
		this.yetExecuteCount = yetExecuteCount;
	}
	public Integer getFailExecuteCount() {
		return failExecuteCount;
	}
	public void setFailExecuteCount(Integer failExecuteCount) {
		this.failExecuteCount = failExecuteCount;
	}
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8:00")
	public Date getPreExecuteTime() {
		return preExecuteTime;
	}
	public void setPreExecuteTime(Date preExecuteTime) {
		this.preExecuteTime = preExecuteTime;
	}
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8:00")
	public Date getNextExecuteTime() {
		return nextExecuteTime;
	}
	public void setNextExecuteTime(Date nextExecuteTime) {
		this.nextExecuteTime = nextExecuteTime;
	}
	public String getBusinessObject() {
		return businessObject;
	}
	public void setBusinessObject(String businessObject) {
		this.businessObject = businessObject;
	}
	public String getBusinessObjectName() {
		return businessObjectName;
	}
	public void setBusinessObjectName(String businessObjectName) {
		this.businessObjectName = businessObjectName;
	}
	public Short getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(Short taskStatus) {
		this.taskStatus = taskStatus;
	}
	public Integer getManualOperation() {
		return manualOperation;
	}
	public void setManualOperation(Integer manualOperation) {
		this.manualOperation = manualOperation;
	}
	
	@Entity(noCache=true)
	public enum TaskType {
		
		 PERIOD_TASK("周期任务"),ONCE_TASK("一次性任务");
		 
		 private String desc;
		 
		 private TaskType(String desc) {
				this.desc = desc;
		 }
		 public String getDesc() {
				return desc;
		 }
	}
	
	
}