package com.tmt.base.component.excel.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 
 * @ClassName: ExcelUtils 
 * @author 李锋
 * @date 2013-4-26 下午10:18:05 
 */
public abstract class ExcelUtils {

    private ExcelUtils(){}
    
    /**
     * 将Excel列头转换为序号，如B->2
     * @param column
     * @return
     */
    public static int columnToIndex(String column) {
        int index = 0;
        char[] chars = column.toUpperCase().toCharArray();
        for (int i = 0; i < chars.length; i++) {
            index += ((int) chars[i] - (int) 'A' + 1) * (int) Math.pow(26, chars.length - i - 1);
        }
        return index;
    }

    /**
     * 将Excel列序号转换为列头，如2->B
     * @param index
     * @return
     */
    public static String indexToColumn(int index) {
        String rs = "";
        do {
            index--;
            rs = ((char) (index % 26 + (int) 'A')) + rs;
            index = (int) ((index - index % 26) / 26);
        } while (index > 0);
        return rs;
    }
    
    /**
     * 从路径加载Excel
     * @param filePath
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws BiffException
     */
    public static Workbook loadExcelFile(String filePath) throws FileNotFoundException, IOException {
        return loadExcelFile(new File(filePath));
    }

    /**
     * 从文件加载Excel
     * @param file
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws BiffException
     */
    public static Workbook loadExcelFile(File file) throws IOException {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new IOException("指定的Excel数据文件不存在",e);
        }
        return loadExcelFile(fileInputStream);
    }

    /**
     * 从流加载Excel
     * @param inputStream
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws BiffException
     */
    public static Workbook loadExcelFile(InputStream  inputStream ) throws IOException {
        try {
        	Workbook wb = null;
            if(!inputStream.markSupported()) {
            	inputStream = new PushbackInputStream(inputStream);
            }
            if(POIFSFileSystem.hasPOIFSHeader(inputStream)){
            	wb = new HSSFWorkbook(inputStream);
            } else if(POIXMLDocument.hasOOXMLHeader(inputStream)){
            	wb = new XSSFWorkbook(inputStream);
            }
        	return wb;
        } catch (IOException e) {
            throw new IOException("加载Excel数据文件异常",e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }
    
    /**
     * 是否是日期列
     * @param value
     * @return
     */
    public static Boolean isValidExcelDate(double value){
    	return DateUtil.isValidExcelDate(value);
    }
    
    /**
     * 是否是日期列
     * @param value
     * @return
     */
    public static Boolean isADateFormat(int i, String f){
    	return DateUtil.isADateFormat(i, f);
    }
}
