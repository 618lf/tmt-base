package com.tmt.base.component.email;

import java.io.File;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * MIME邮件服务类.
 * 
 * 由Freemarker引擎生成的的html格式邮件, 并带有附件.
 * 
 * @author LIFNG
 */
public class MimeMailService implements BaseMailService{

	private static final String DEFAULT_ENCODING = "UTF-8";
	private static Logger logger = LoggerFactory.getLogger(MimeMailService.class);
	private JavaMailSender mailSender;
	private MailTemplate mailTemplate;

	/**
	 * 发送MIME格式的用户修改通知邮件. 不带附件
	 * toArray : 发送
	 * CcArray : 抄送
	 */
	public Boolean sendNotificationMail( String[] toArray , String[]  CcArray ,Map<String,Object> context ) {
		return this.sendNotificationMail(toArray,CcArray,mailTemplate.getDefaultTemplate(), context, null);
	}
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 不带附件
	 * toArray : 发送
	 * CcArray : 抄送 
	 */
	public Boolean sendNotificationMail( String[] toArray , String[]  CcArray ,Map<String,Object> context , File[] attachments ) {
		return this.sendNotificationMail(toArray,CcArray,mailTemplate.getDefaultTemplate(), context, attachments);
	}
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 不带附件
	 * toArray : 发送
	 * CcArray : 抄送
	 */
	public Boolean sendNotificationMail( String subject , String[] toArray , String[]  CcArray ,String templateName , Map<String,Object> context ) {
		return this.sendNotificationMail(subject,toArray,CcArray,templateName, context, null);
	}
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 并带附件  基础
	 */
	public Boolean sendNotificationMail( String subject , String[] toArray , String[]  CcArray ,String templateName ,Map<String,Object> context , File[] attachments ) {
		MimeMessage msg = this.generateMimeMessage( subject,toArray,CcArray,templateName ,context,attachments);
		return this.sendNotificationMail(msg);
	}
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 不带附件
	 * toArray : 发送
	 * CcArray : 抄送
	 */
	public Boolean sendNotificationMail( String[] toArray , String[]  CcArray ,String templateName , Map<String,Object> context ) {
		return this.sendNotificationMail(toArray,CcArray,templateName, context, null);
	}
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 并带附件  基础
	 */
	public Boolean sendNotificationMail( String[] toArray , String[]  CcArray ,String templateName ,Map<String,Object> context , File[] attachments ) {
		MimeMessage msg = this.generateMimeMessage(toArray,CcArray,templateName ,context,attachments);
		return this.sendNotificationMail(msg);
	}
	
	/**
	 * 发送MIME格式的用户修改通知邮件. 自定义格式
	 */
	public Boolean sendNotificationMail( MimeMessage msg ) {
		try {
			mailSender.send(msg);
			logger.debug("HTML版邮件已发送");
			return Boolean.TRUE;
		}catch (Exception e) {
			logger.error("发送邮件失败", e);
			return Boolean.FALSE;
		}
	}
	
	/**
	 * 默认主题
	 * @param toArray
	 * @param CcArray
	 * @param templateName
	 * @param context
	 * @param attachments
	 * @return
	 */
	private MimeMessage generateMimeMessage( String[] toArray , String[]  CcArray , String templateName, Map<String,Object> context, File[] attachments) {
		return this.generateMimeMessage(mailTemplate.getSubject(),toArray, CcArray, templateName, context,attachments);
	}
	private MimeMessage generateMimeMessage(String subject , String[] toArray , String[]  CcArray , String templateName, Map<String,Object> context, File[] attachments) {
		try {
			MimeMessage msg = this.generateMimeMessage(subject,toArray,CcArray,templateName,context);
			if( msg != null && attachments != null && attachments.length != 0 ) {
				MimeMessageHelper helper = new MimeMessageHelper(msg, true, DEFAULT_ENCODING);
				for( File attachment : attachments ) {
					helper.addAttachment(attachment.getName(), attachment);
			    }
			}
			return msg;
		} catch (MessagingException e) {
			logger.error("构造邮件失败", e);
			return null;
		} 
	}
	private MimeMessage generateMimeMessage(String subject , String[] toArray , String[]  CcArray ,String templateName, Map<String,Object> context) {
		try {
			MimeMessage msg = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, true, DEFAULT_ENCODING);
			
			helper.setFrom(mailTemplate.getFrom());
			helper.setTo(toArray);
			helper.setCc(CcArray == null ?new String[0]:CcArray);
			helper.setSubject(subject);
			
			String content = mailTemplate.generateContent(templateName,context);
			helper.setText(content, true);
			return msg;
		} catch (Exception e) {
			logger.error("构造邮件失败", e);
			return null;
		} 
	}
	
	/**
	 * Spring的MailSender.
	 */
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	/**
	 * 邮件模版
	 */
	public void setMailTemplate(MailTemplate mailTemplate) {
		this.mailTemplate = mailTemplate;
	}
	
}
