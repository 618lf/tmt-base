package com.tmt.base.component.task.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.component.task.entity.Task;

@Repository
public class TaskDao extends BaseIbatisDAO<Task, String> {

	@Override
	protected String getNamespace() {
		return "SYS_TASK";
	}
}
