package com.tmt.base.component.tld;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author TMT
 * 展现页面的路径
 */
@SuppressWarnings("serial")
public class PageInfoTag extends TagSupport{
	
	// 日志记录器
    private static Logger logger = LoggerFactory.getLogger(PageInfoTag.class);
    
    private String type;
    
    public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int doStartTag() throws JspException {

        if( null != type && "filePath".equals(type)){
        	printStringByLoggerLevel(filePath());
        }
        return 0;
	}
	
	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}
	
	//打印信息
    private void printStringByLoggerLevel(String string) throws JspException
    {
	    try
	    {
	        if(logger.isDebugEnabled()){
	           pageContext.getOut().print(string);
	        }
	    }
	    catch(IOException ex)
	    {
	        logger.error("IOError:" + ex.getMessage(), ex);
	        throw new JspException("IOError:" + ex.getMessage());
	    }
	}
    //显示文件路径信息
	private String filePath()
	{
	        HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
	        StringBuffer sbInfo = new StringBuffer(256);
	        sbInfo.append("<!--[if !IE]>\u6587\u4EF6\u8DEF\u5F84\uFF1A");
	        sbInfo.append(request.getRequestURI());
	        sbInfo.append("<![endif]-->");
	        return sbInfo.toString();
   }

}
