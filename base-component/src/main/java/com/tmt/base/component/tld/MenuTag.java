package com.tmt.base.component.tld;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.system.entity.Menu;
import com.tmt.base.system.utils.IMenuDisplay;

/**
 * 用户菜单
 * @author lifeng
 */
public class MenuTag extends TagSupport{

	private static final long serialVersionUID = 1L;
	private List<Menu> menuList;
	private String style;//样式
	private double screenSize;//用户屏幕尺寸
	
	@Override
	public int doStartTag() throws JspException {
		return EVAL_PAGE;
	}
	@Override
	public int doEndTag() throws JspException {
		try {
			JspWriter out = this.pageContext.getOut();
			out.print(displayMenu());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return EVAL_PAGE;
	}
	private String displayMenu() {
		IMenuDisplay menuDisplay = SpringContextHolder.getBean(IMenuDisplay.class);
		StringBuffer sb = new StringBuffer();
		if( menuDisplay != null) {
			sb.append(menuDisplay.getUIMenu(menuList, screenSize));
		}
		return sb.toString();
	}
	public List<Menu> getMenuList() {
		return menuList;
	}
	public void setMenuList(List<Menu> menuList) {
		this.menuList = menuList;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public double getScreenSize() {
		return screenSize;
	}
	public void setScreenSize(double screenSize) {
		this.screenSize = screenSize;
	}
}
