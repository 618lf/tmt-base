package com.tmt.base.component.excel.imp;

import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.system.entity.ExcelItem;
import com.tmt.base.system.entity.ExcelTemplate;
import com.tmt.base.system.service.ExcelTemplateService;


/**
 * Excel Template Mapper
 * excel 模版映射
 * @author liFeng
 * 2014年9月23日
 */
public class TemplateExcelMapper<T> extends AbstractExcelMapper<T>{
	
	//Excel模版
	private ExcelTemplate template;
	private ExcelTemplateService templateService = null;
	
	public TemplateExcelMapper(String templateId){
		templateService = SpringContextHolder.getBean(ExcelTemplateService.class);
		template = templateService.getWithItems(templateId);
	}
	
    public TemplateExcelMapper(ExcelTemplate template){
		this.template = template;
	}
    
	@Override
	public int getStartRow() {
		return template.getStartRow();
	}
	
	/**
	 * 初始化配置文件信息用到的时候会加载
	 */
	protected void initRowMapper(){
		if(!(this.template != null && this.template.getItems() != null && 
				this.template.getItems().size() != 0 )) {
		   return;
		}
		//取数据
		for(ExcelItem item: this.template.getItems()) {
			ColumnMapper mapper = new ColumnMapper();
			mapper.setColumn(item.getColumnName());
			mapper.setDataFormat(item.getDataFormat());
			mapper.setDataType(item.getDataType());
			mapper.setProperty(item.getProperty());
			mapper.setVerifyFormat(item.getVerifyFormat());
			mapper.setVerifyType(item.getVerifyType());
			rowMapper.put(mapper.getColumn(), mapper);
		}
	}
}
