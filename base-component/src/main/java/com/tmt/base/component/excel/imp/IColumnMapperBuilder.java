package com.tmt.base.component.excel.imp;

import java.util.List;

/**
 * 获取Column
 * @author liFeng
 * 2014年9月23日
 */
public interface IColumnMapperBuilder {

	public List<ColumnMapper> getColumnMappers();
}
