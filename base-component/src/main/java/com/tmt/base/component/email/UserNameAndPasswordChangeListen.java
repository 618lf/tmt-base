package com.tmt.base.component.email;

import org.springframework.stereotype.Component;

import com.tmt.base.common.config.Dicts;
import com.tmt.base.common.listen.AttributeChangeEvent;
import com.tmt.base.common.listen.AttributeChangeListen;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.system.entity.Dict;

/**
 * 属性变化监听器
 * 
 * @author lifeng
 */
@Component
public class UserNameAndPasswordChangeListen implements AttributeChangeListen<Dict> {

	private JavaMailSender sender;
	private MailTemplate template;

	@Override
	public void fireAttributeChange(AttributeChangeEvent<Dict> event) {
		if (sender == null) {
			sender = SpringContextHolder.getBean(JavaMailSender.class);
		}
		if (template == null) {
			template = SpringContextHolder.getBean(MailTemplate.class);
		}

		Dict dict = event.getSource();
		if (Dicts.COM_SYSTEM_MAIL_PASSWORD.equals(dict.getCode())
				|| Dicts.COM_SYSTEM_MAIL_PASSWORD.equals(dict.getCode())
				|| Dicts.COM_SYSTEM_MAIL_USERNAME.equals(dict.getCode())) {
			try {
				sender.initialize();
			} catch (Exception e) {
				logger.error("",e);
			}
		} else if (template.getDictConfig().get("from_dict")
				.equals(dict.getCode())
				|| template.getDictConfig().get("default_template_dict")
						.equals(dict.getCode())) {
			try {
				template.initialize();
			} catch (Exception e) {
				logger.error("",e);
			}
		}
	} 
}
