package com.tmt.base.component.excel.imp;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.tmt.base.common.utils.DateUtil3;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.component.excel.util.ExcelUtils;


/**
 * 默认的excel执行器  --- 单例模式
 * @author liFeng
 * 2014年9月22日
 */
public class DefaultExcelExecuter implements IExcelExecuter{

	private static DefaultExcelExecuter EXECUTER = new DefaultExcelExecuter();
	
	private DefaultExcelExecuter(){}
	
	public static IExcelExecuter getInstance(){
		return EXECUTER;
	}
	
	@Override
	public <T> ImportResult<T> getExcelData(IExcelMapper<T> mapper, Sheet sheet) {
		ImportResult<T> result = new ImportResult<T>();
		int iStartRow = mapper.getStartRow();
		int iEndRow = sheet.getLastRowNum();
		try {
			for( int i = iStartRow, j = iEndRow; i< j; i++ ) {
				Boolean bFlag = processRow(mapper, result, i, sheet.getRow(i));
				if( mapper.returnWhenError() && !bFlag ) {
					break;
				}
			}
		} catch(Exception e){
			result = ImportResult.error(e.getMessage());
		}
		return result;
	}
	
	private <T> Boolean processRow(IExcelMapper<T> mapper, ImportResult<T> result, int iRow, Row row) throws Exception{
		Boolean isEmptyRow = Boolean.TRUE; // 是否空行
		if(row == null) {
		   result.addError(iRow, "为空行");
		   return Boolean.FALSE;
		}
		Map<String,Object> valueMap = new HashMap<String,Object>();
		for( int i = row.getFirstCellNum(), j = row.getLastCellNum(); i< j; i++) {
			Iterable<ColumnMapper> columnMappers = mapper.getColumnMappers(ExcelUtils.indexToColumn(i));
			if( columnMappers == null) {
				continue;
			}
			
			Object tempValue = getCellValue(row.getCell(i));
			String cellvalue = null;
			
			if( tempValue == null ) {//无值
				cellvalue = "";
			} else{
				cellvalue = String.valueOf(tempValue);
			}
			Boolean isDefaultFormat = Boolean.FALSE;
			Iterator<ColumnMapper> it = columnMappers.iterator();
			while( it.hasNext() ) {
				ColumnMapper columnMapper = it.next();
				if( tempValue instanceof Date) {
					String pattern = columnMapper.getDataFormat();
					Date date = (Date)tempValue;
					if(StringUtil3.isNotBlank(pattern)) {
						cellvalue = DateUtil3.formatDate(date, pattern);
					} else if( !isDefaultFormat){
						cellvalue = DateUtil3.formatDate(date);
						isDefaultFormat = Boolean.TRUE;
					}
				}
				//去空
				cellvalue = StringUtil3.trimToNull(cellvalue);
				//这列不为空
				if( StringUtil3.isNotBlank(cellvalue)) {
					isEmptyRow = Boolean.FALSE;
				}
				//设置的格式的验证
				
				//特殊的验证
				
				//保存数据
				valueMap.put(columnMapper.getProperty(), cellvalue);
			}
		}
		if( isEmptyRow ) { //空行就返回
			result.addError(iRow, "为空行");
			return Boolean.FALSE;
		}
		result.addSucessRow(mapper.receive(valueMap));
		return Boolean.TRUE;
	}
	
	/**
	 * cell 的值
	 * @param cell
	 * @param columnMapper
	 * @return
	 * @throws Exception 
	 */
	private Object getCellValue(Cell cell){
		if(cell == null) { return "";}
		FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();  
		String strValue = null;
		if(isDateCell(cell) ) { //日期格式
			return cell.getDateCellValue();
		} else if( Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {//数字
			strValue = String.valueOf(cell.getNumericCellValue());
		} else if( Cell.CELL_TYPE_BOOLEAN == cell.getCellType()){
			strValue = String.valueOf(cell.getBooleanCellValue());
		} else if( Cell.CELL_TYPE_BLANK == cell.getCellType()){
			strValue = "";
		} else if( Cell.CELL_TYPE_FORMULA == cell.getCellType()) {
			evaluator.evaluateFormulaCell(cell);
			strValue = String.valueOf(cell.getNumericCellValue());
		} else if( Cell.CELL_TYPE_ERROR == cell.getCellType()) {
			strValue = String.valueOf(cell.getErrorCellValue());
		} else{
			cell.setCellType(Cell.CELL_TYPE_STRING);
			strValue = cell.getStringCellValue();
		}
		return strValue;
	}
	
	/**
	 * 是否是日期列
	 * @param cell
	 * @return
	 */
	private Boolean isDateCell(Cell cell){
		if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			double value = cell.getNumericCellValue();
			if(ExcelUtils.isValidExcelDate(value)) {
				CellStyle style = cell.getCellStyle();
				if(style != null){
					int i = style.getDataFormat();
					String f = style.getDataFormatString();
					return ExcelUtils.isADateFormat(i, f);
				}
			}
		}
		return Boolean.FALSE;
	}
}
