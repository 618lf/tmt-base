package com.tmt.base.component.excel.imp;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.ss.usermodel.Sheet;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public abstract class AbstractExcelMapper<T> implements IExcelMapper<T>, IReceiver<T> {

	protected Class<T> clazz;
	protected Multimap<String,ColumnMapper> rowMapper = null;
	protected IReceiver<T> receiver;
	
	@Override
	public Boolean returnWhenError() {
		return Boolean.FALSE;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Class<T> getTargetClass(){
		if( clazz == null) {
			Type type = super.getClass().getGenericSuperclass() ;
			if(type instanceof ParameterizedType){  
	            ParameterizedType ptype = ((ParameterizedType)type);  
	            Type[] args = ptype.getActualTypeArguments();  
	            clazz = (Class<T>) args[0].getClass();  
	        }  
		}
		return clazz;
	}

	@Override
	public T receive(Map<String, Object> valueMap) {
		return (T) this.receive(valueMap, this.getTargetClass());
	}
	
	@Override
	public T receive(Map<String, Object> valueMap, Class<T> clazz) {
		return this.getReceiver().receive(valueMap, clazz);
	}
	
	public ImportResult<T> getExcelData(Sheet sheet){
		return DefaultExcelExecuter.getInstance().getExcelData(this, sheet);
	}
	
	//初始化
	protected abstract void initRowMapper();
	
	@Override
	public Iterable<ColumnMapper> getColumnMappers(String column) {
		if( rowMapper == null ) {
			rowMapper = ArrayListMultimap.create();
			this.initRowMapper();
		}
		return rowMapper.get(column);
	}
	
	public IReceiver<T> getReceiver() {
		if( receiver == null){
			receiver = new DefaulReceiver<T>();
		}
		return receiver;
	}

	//默认的转换器
	public static class DefaulReceiver<T> implements IReceiver<T>{
		@Override
		public T receive(Map<String, Object> valueMap, Class<T> clazz) {
			T obj = null;
	        try {
	            obj = (T)clazz.newInstance();
	            BeanUtils.populate(obj, valueMap);
	        } catch (Exception e) {}
			return obj;
		}
	}
}
