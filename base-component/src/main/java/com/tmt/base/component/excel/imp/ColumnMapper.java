package com.tmt.base.component.excel.imp;

import com.tmt.base.system.enums.DataType;
import com.tmt.base.system.enums.VerifyType;

/**
 * 
 * @ClassName: ColumnMapper 
 * @author 李锋
 * @date 2013-4-26 下午09:52:05 
 *
 */
public class ColumnMapper {

    private int cellIndex;
    private String column;
    private DataType dataType;
    private String dataFormat;
    private String property;
    private VerifyType verifyType;
    private String verifyFormat;
    
    public ColumnMapper(){
    	
    }
    
    public ColumnMapper(String column, DataType dataType, String property) {
        this.column = column;
        this.dataType = dataType;
        this.property = property;
    }
    
    public String getDataFormat() {
		return dataFormat;
	}

	public void setDataFormat(String dataFormat) {
		this.dataFormat = dataFormat;
	}

    public int getCellIndex() {
		return cellIndex;
	}

	public void setCellIndex(int cellIndex) {
		this.cellIndex = cellIndex;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public VerifyType getVerifyType() {
		return verifyType;
	}

	public void setVerifyType(VerifyType verifyType) {
		this.verifyType = verifyType;
	}

	public String getVerifyFormat() {
		return verifyFormat;
	}

	public void setVerifyFormat(String verifyFormat) {
		this.verifyFormat = verifyFormat;
	}
}
