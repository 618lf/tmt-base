package com.tmt.base.component.excel.test;

import java.util.Map;

import com.tmt.base.component.excel.imp.AbstractExcelMapper;

/**
 * 测试类
 * @author liFeng
 * 2014年9月25日
 */
public class UserInfoExcelMapper extends AbstractExcelMapper<Map<String,Object>>{

	@Override
	protected void initRowMapper() {
		
	}

	@Override
	public int getStartRow() {
		return 2;
	}
	
	public static void main( String[] args) {
		UserInfoExcelMapper mapper = new UserInfoExcelMapper();
		mapper.getExcelData(null);
	}
}
