package com.tmt.base.component.task.web;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.tmt.base.common.config.Dicts;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.component.task.entity.Task;
import com.tmt.base.component.task.service.TaskService;
import com.tmt.base.system.entity.Dict;
import com.tmt.base.system.service.DictService;

/**
 * 定时任务
 * @author lifeng
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/system/task")
public class TaskController extends BaseController{

	@Autowired
	private TaskService taskService;
	
	@Autowired
	private DictService dictService;
	
	@RequestMapping(value = {"initList", ""})
	public String initList(){
		return "system/TaskList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Task task,Model model,Page page) {
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		page  = this.taskService.queryForPage(qc, param);
		return page;
	}
	@RequestMapping(value = {"form"})
	public String form(Task task,Model model){
		List<Dict> dicts = dictService.getDictsByType(Dicts.COM_SYSTEM_TASK_ITEMS);
		if( task != null && task.getId() != null ) {
			task = this.taskService.get(task.getId());
		} else {
			if( task == null) {
				task = new Task();
			}
			task.setId(IdGen.INVALID_ID);
		}
		model.addAttribute("task", task);
		model.addAttribute("business", dicts);
		return "system/TaskForm";
	}
	@RequestMapping(value = {"save"})
	public String save(Task task,Model model,RedirectAttributes redirectAttributes) {
		//后台验证
		if ( !beanValidator(model, task) ){
			return form(task, model);
		}
		//用value去查询
		Dict dict = this.dictService.getDictByValue(task.getBusinessObject());
		if( dict != null) {
			task.setBusinessObject(dict.getValue());
			task.setBusinessObjectName(dict.getLabel());
		}
		this.taskService.save(task);
		addMessage(redirectAttributes, "保存定时任务'" + task.getName() + "'成功");
		redirectAttributes.addAttribute("id", task.getId());
		return "redirect:"+Globals.getAdminPath()+"/system/task/form";
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public Boolean delete(String[] idList , Model model,HttpServletResponse response) {
		List<Task> tasks = Lists.newArrayList();
		Boolean bFalg = Boolean.TRUE;
		for( String id:idList ) {
			 Task task = new Task();
			 task.setId(id);
			 tasks.add(task);
		}
		this.taskService.batchDelete(tasks);
		return bFalg;
	}
	
	@ResponseBody
	@RequestMapping(value = {"execute"})
	public Boolean execute(String[] idList , Model model,HttpServletResponse response){
		Boolean bFalg = Boolean.TRUE;
		List<Task> tasks = Lists.newArrayList();
		for( String id:idList ) {
			 Task task = new Task();
			 task.setId(id);
			 tasks.add(task);
		}
		this.taskService.batchExecute(tasks);
		return bFalg;
	}
}
