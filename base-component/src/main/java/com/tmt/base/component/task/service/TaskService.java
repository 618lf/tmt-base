package com.tmt.base.component.task.service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.quartz.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.DateUtil3;
import com.tmt.base.common.utils.SpringContextHolder;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.component.task.dao.TaskDao;
import com.tmt.base.component.task.entity.Task;
import com.tmt.base.component.task.executor.TaskExecutor;

/**
 * 解决一些小的临时任务， 避免配置过多的任务
 * @author lifeng
 */
@Service
public class TaskService extends BaseService<Task, String>{

	@Autowired
	private TaskDao taskDao;
	
	private ConcurrentHashMap<String,TaskExecutor> businessMap = new ConcurrentHashMap<String,TaskExecutor>();
	private ConcurrentHashMap<String,CronExpression> cronEMap = new ConcurrentHashMap<String,CronExpression>();
	
	public synchronized void executeByQuartzLocalJob(){
		//所有的任务
		List<Task> allTasks = this.taskDao.queryForList("findActiveTasks");
		for( Task task: allTasks ) {
			if( this.needToExecute(task) ) {
				this.doTask(task);
			}
		}
	}
	
	//是否需要执行 比较麻烦
	private Boolean needToExecute( Task task ) {
		//是否处理执行的状态
		if( !(task.getTaskStatus() <= 0) && ( task.getManualOperation() == null || task.getManualOperation() <= 0 )) {
			return Boolean.FALSE;
		}
		//是否需要去执行
		if( task.getType() == Task.TaskType.PERIOD_TASK ) {
			if( task.getManualOperation() != null && task.getManualOperation() > 0) {
				return Boolean.TRUE;
			}
			if( !StringUtil3.isBlank(task.getCronExpression()) && task.getPreExecuteTime() != null ) {
				//比较上次执行的时间 和即将执行的时间
				Date nextValidTime = this.getNextValidTime(task);
				if( !nextValidTime.before(new Date(System.currentTimeMillis())) ) {
					return Boolean.FALSE;
				}
			}
		}
		return Boolean.TRUE;
	}
	//执行任务
	private Boolean doTask( Task task ) {
		TaskExecutor executor = this.getExecutor(task);
		if( executor != null ) {
			task.setTaskStatus(Short.valueOf("1"));
			this.update("updateStatus", task);
		    executor.doTask();
		    task.setYetExecuteCount(task.getYetExecuteCount() == null?1:(task.getYetExecuteCount() + 1));
			if( task.getAllowExecuteCount() != null && task.getYetExecuteCount() >= task.getAllowExecuteCount() ) {
				task.setTaskStatus(Short.valueOf("2"));
			} else {
				task.setTaskStatus(Short.valueOf("0"));
			}
			task.setPreExecuteTime(DateUtil3.getTimeStampNow());
			Date nextExecuteTime = this.getNextValidTime(task);
			task.setNextExecuteTime(nextExecuteTime);
			//手动执行的次数
			Integer manualOperation = task.getManualOperation();
			task.setManualOperation((manualOperation != null?manualOperation - 1:0));
			this.update("updateExecuteStatus", task);
		}
		return Boolean.TRUE;
	}
	
	private TaskExecutor getExecutor( Task task ){
		String Id = "BUSINESS_" + task.getId();
		try{
			TaskExecutor executor = businessMap.get(Id);
			if( executor == null) {
				executor = SpringContextHolder.getBean(task.getBusinessObject());
				businessMap.put(Id, executor);
			}
			return executor;
		}catch(Exception e) {
			return null;
		}
	}
	
	private CronExpression getCronE( Task task ) {
		String Id = "CRON_E_" + task.getId();
		try{
			CronExpression cronE = cronEMap.get(Id);
			if( cronE == null) {
				cronE = new CronExpression(task.getCronExpression());
				cronEMap.put(Id, cronE);
			}
			return cronE;
		}catch(Exception e) {
			return null;
		}
	}
	
	private Date getNextValidTime( Task task ) {
		CronExpression cronE = this.getCronE(task);
		Date nextValidTime = null;
		if( cronE != null) {
			//比较上次执行的时间 和即将执行的时间
			nextValidTime = cronE.getNextValidTimeAfter(task.getPreExecuteTime());
		}
		return nextValidTime;
	}
	@Override
	protected BaseIbatisDAO<Task, String> getEntityDao() {
		return taskDao;
	}
	
	@Transactional
	public String save( Task task ) {
		if( IdGen.isInvalidId(task.getId())) {
			this.insert(task);
		} else {
			Task taskTemp = this.get(task.getId());
			taskTemp.setName(task.getName());
			taskTemp.setType(task.getType());
			taskTemp.setBusinessObject(task.getBusinessObject());
			taskTemp.setBusinessObjectName(task.getBusinessObjectName());
			taskTemp.setTaskStatus(task.getTaskStatus());
			taskTemp.setCronExpression(task.getCronExpression());
			taskTemp.setAllowExecuteCount(task.getAllowExecuteCount());
			this.update(taskTemp);
			this.businessMap.clear();
			this.cronEMap.clear();
		}
		return task.getId();
	}
	
	/**
	 * 现阶段值能执行一次周期任务
	 * @param tasks
	 */
	@Transactional
	public void batchExecute( List<Task> tasks ) {
		//执行完之后的属性设置 锁定当前对象
		synchronized(this){
			List<Task> mTask = Lists.newArrayList();
			for( Task task: tasks ) {
				task = this.get(task.getId());
				//是否需要去执行
				if ( task.getTaskStatus() != 1  ) {
					 Integer manualOperation = task.getManualOperation();
					 task.setManualOperation( ((manualOperation != null&&manualOperation>0)?manualOperation+1:1) );
					 mTask.add(task);
				}
			}
			this.batchUpdate("updateManualOperation", mTask);
		}
	}
}
