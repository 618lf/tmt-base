package com.tmt.base.component.email;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.tmt.base.system.entity.Dict;
import com.tmt.base.system.service.DictService;

/**
 * 邮件服务
 * @author lifeng
 */
public class JavaMailSender extends org.springframework.mail.javamail.JavaMailSenderImpl{

	private String encryptMethod;
	private Map<String,String> dictConfig;
	
	@Autowired
	private DictService dictService;
    
	@PostConstruct
    public void initialize() throws Exception {
		String host = this.getHost();
		String username = this.getUsername();
		String password = this.getPassword();
		if( dictConfig != null && !dictConfig.isEmpty()) {
			
			host = this.dictConfig.get("host_dict");
			username = this.dictConfig.get("username_dict");
			password = this.dictConfig.get("password_dict");
			
			Dict dict = this.dictService.getDictByCode(host);
			if( dict != null ) {
				host = dict.getValue();
				this.setHost(host);
			}
			dict = this.dictService.getDictByCode(username);
			if( dict != null ) {
				username = dict.getValue();
				this.setUsername(username);
			}
			
			dict = this.dictService.getDictByCode(password);
			if( dict != null ) {
				password = dict.getValue();
				this.setPassword(password);
			}
		}
	}
	
	public Map<String, String> getDictConfig() {
		return dictConfig;
	}

	public void setDictConfig(Map<String, String> dictConfig) {
		this.dictConfig = dictConfig;
	}
	public String getEncryptMethod() {
		return encryptMethod;
	}

	public void setEncryptMethod(String encryptMethod) {
		this.encryptMethod = encryptMethod;
	}
	
}
