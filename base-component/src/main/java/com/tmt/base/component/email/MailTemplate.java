package com.tmt.base.component.email;

import java.io.IOException;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.tmt.base.system.entity.Dict;
import com.tmt.base.system.service.DictService;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;

public class MailTemplate {

	private static Logger logger = LoggerFactory.getLogger(MailTemplate.class);
	
	//默认编码
	private static final String DEFAULT_ENCODING = "UTF-8";
    private String from; //邮件内容 - 发送人
	private String subject;//邮件内容 - 默认主题
	private String defaultTemplate;//默认的邮件模板
	private Map<String,String> dictConfig;
	
	private Configuration configuration;
	
	@Autowired
	private DictService dictService;
	
	public String generateContent( String templateName , Map<String,Object> context ) throws MessagingException{
		try {
			return FreeMarkerTemplateUtils.processTemplateIntoString(configuration.getTemplate(templateName, DEFAULT_ENCODING), context);
		} catch (IOException e) {
			logger.error("生成邮件内容失败, FreeMarker模板不存在", e);
			throw new MessagingException("FreeMarker模板不存在", e);
		} catch (TemplateException e) {
			logger.error("生成邮件内容失败, FreeMarker处理失败", e);
			throw new MessagingException("FreeMarker处理失败", e);
		}
	}
	
	@PostConstruct
    public void initialize() throws Exception {
		
		String from = this.getFrom();
		String defaultTemplate = this.getDefaultTemplate();
		if( dictConfig != null && !dictConfig.isEmpty()) {
			from = dictConfig.get("from_dict");
			defaultTemplate = dictConfig.get("default_template_dict");
			
			Dict dict = this.dictService.getDictByCode(from);
			if( dict != null ) {
				from = dict.getValue();
				this.setFrom(from);
			}
			
			dict = this.dictService.getDictByCode(defaultTemplate);
			if( dict != null ) {
				defaultTemplate = dict.getValue();
				this.setDefaultTemplate(defaultTemplate);
			}
		}
	}
	
	/**
	 * 注入Freemarker引擎配置,
	 */
	public void setFreemarkerConfiguration(Configuration freemarkerConfiguration) throws IOException {
		configuration = freemarkerConfiguration;
	}
	
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getDefaultTemplate() {
		return defaultTemplate;
	}

	public void setDefaultTemplate(String defaultTemplate) {
		this.defaultTemplate = defaultTemplate;
	}
	public Map<String, String> getDictConfig() {
		return dictConfig;
	}

	public void setDictConfig(Map<String, String> dictConfig) {
		this.dictConfig = dictConfig;
	}
	
}
