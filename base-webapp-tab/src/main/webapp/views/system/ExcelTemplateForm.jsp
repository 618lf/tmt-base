<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<link href="${ctxStatic}/jquery-jqGrid/custom/jqgrid.css" rel="stylesheet"/>
<script src="${ctxStatic}/jquery-jqGrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jqGrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-form/json2.js" type="text/javascript"></script>
<script type="text/javascript">
var originalData = {
    id: -1,
    entries: [{
        id: "1"
    }, {
        id: "2"
    }, {
        id: "3"
    }, {
        id: "4"
    }, {
        id: "5"
    }, {
        id: "6"
    }, {
        id: "7"
    }, {
        id: "8"
    }]
};
var checkOptionsFmt = function(){
	return {
		value:{'OTHER':'无','UNIQUE':'唯一','NOT_NULL':'不为空'}
	};
};
var dataTypesFmt = function(){
	return {
		value:{'STRING':'文本','DATE':'日期','MONEY':'金额','NUMBER':'数值','BOOLEAN':'布尔值'}
	};
};
var curRow, curCol;
var THISPAGE = {
	newId:9,
	_init : function(t){
		this.loadGrid(t);
		this.addEvent();
	},
	loadGrid : function(t){
		var init = Public.setGrid();
		$('#grid').jqGrid(
		  Public.defaultEditGrid({   
			  data: t,
			  shrinkToFit:true,
			  width:init.w,
              colModel:[
                 {name:'id', index:'itemId', label: "ID", title: !0, width:150,hidden:true,sortable:false,editable: !0, edittype:'text'},
                 {name:'operating', index:'', label: " ", width: 40, align:'center',fixed:!0, formatter: Public.billsOper},
                 {name:'itemName', index:'itemName', label: "Excel列名称", width:150,sortable:false,editable: !0, edittype:'text'},
 				 {name:'columnName', index:'columnName', label: "Excel列序号", align:'center',width:150,sortable:false,editable: !0, edittype:'text'},
 				 {name:'property', index:'property', label: "类属性", align:'center', width:150,sortable:false,editable: !0, edittype:'text'},
 				 {name:'dataTypeStr', index:'dataTypeStr',label: "数据类型", align:'center',width:150,sortable:false,editable: !0, edittype:'select',editoptions:dataTypesFmt()},
 				 {name:'dataFormat', index:'dataFormat',label: "格式化", align:'center',width:150,sortable:false,editable: !0, edittype:'text'},
 				 {name:'verifyTypeStr', index:'verifyTypeStr',label: "校验", align:'center',width:150,sortable:false,editable: !0, edittype:'select',editoptions:checkOptionsFmt()},
 				 {name:'verifyFormat', index:'verifyFormat',label: "校验表达式", align:'center',width:150,sortable:false,editable: !0, edittype:'text'}
             ]
		}));
		$('#grid').jqGrid('setFrozenColumns');
		$("#grid").jqGrid("setGridParam", { cellEdit: !0  });
	},
	addEvent : function(){
		var that = this;
		Public.billsEvent(this);
		$("#btnSubmit").on('click',function(){
			var t = that.getPostData();
			var i = {
				id:($('#templateId').val()),
				templateName:($('#templateName').val()),
				templateType:($('#templateType').val()),
				targetClass:($('#targetClass').val()),
				startRow:($('#startRow').val()),
				items:t
			};
			Public.executex("确定保存配置","${ctx}/system/excel/save",{
				 postData: JSON.stringify(i)
			},function(data){
				if(data && data.success){
					Public.success("保存成功");
				} else {
					Public.error(data.msg);
				}
			});
		});
        $("#btnBack").on('click',function(){
        	window.location.href = "${ctx}/system/excel/initList";
		});
	},
	getPostData : function(){
		var getDataType = function(text){
			var name;
			var dataTypes = dataTypesFmt();
			$.each(dataTypes.value,function(key,value){
				if(!name && text === value){
					name = key;
				}
			});
			return name;
		};
		var getVerifyType = function(text){
			var name;
			var dataTypes = checkOptionsFmt();
			$.each(dataTypes.value,function(key,value){
				if(!name && text === value){
					name = key;
				}
			});
			return name;
		};
		for (var t = [], e = $("#grid").jqGrid("getDataIDs"), i = 0, a = e.length; a > i; i++) {
			var r, n = e[i],  o = $("#grid").jqGrid("getRowData", n);
	        if( !!o.columnName && !!o.property && !!o.dataTypeStr && !!o.verifyTypeStr){
	        	r = {
    	            id: o.id,
    	            itemName: o.itemName,
    	            columnName: o.columnName,
    	            property: o.property,
    	            dataType: getDataType(o.dataTypeStr),
    	            dataFormat: o.dataFormat,
    	            verifyType: getVerifyType(o.verifyTypeStr),
    	            verifyFormat: o.verifyFormat
    	        };
    	        t.push(r);
	        }
		}
		return t;
	}
};
$(function(){
	var param = [];
	    param.push({name:'id',value:($('#templateId').val())});
	Public.postAjax('${ctx}/system/excel/item/items',param, function(data){
		var entries = originalData.entries;
		if(data && data.obj && data.obj.length) {
			entries = data.obj;
		}
		THISPAGE._init(entries);
	});
});
</script>
</head>
<body>
    <input name="id" id="templateId" value="${template.id}" maxlength="50" class="required" type="hidden"/>
	<div class="row-fluid wrapper">
	   <div class="bills" style="width: 1200px;">
	     <div class="con-header">
	         <table class="main-table">
	            <tr>
	                <td width="10%">模版名称:</td>
	                <td><input type='text' name="templateName" id="templateName" value="${template.templateName}"/></td>
	                <td width="10%">模版类型:</td>
	                <td><input type='text' name="templateType" id="templateType" value="${template.templateType}"/></td>
	            </tr>
	            <tr>
	                <td>目标类:</td>
	                <td><input type='text' name="targetClass" id="targetClass" value="${template.targetClass}"/></td>
	                <td>开始行:</td>
	                <td><input type='text' name="startRow" id="startRow" value="${template.startRow}"/></td>
	            </tr>
	         </table>
	     </div>
	     <div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
		 </div> 
		 <div class="form-actions">
				<input id="btnSubmit" class="ui-btn-href" type="button" value="保 存"/>
				<input id="btnBack" class="ui-btn" type="button" value="返回"/>
		 </div>
	   </div>
	</div>
</body>
</html>