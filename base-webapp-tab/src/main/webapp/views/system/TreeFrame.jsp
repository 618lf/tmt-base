<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>TreeFrame</title>
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
</head>
<div id="content" class="row-fluid">
	<div id="left">
		<iframe id="frameTree" name="frameTree" src="${ctx}/${treeUrl}" style="overflow:visible;"
			scrolling="yes" frameborder="0" width="100%"></iframe>
	</div>
	<div id="openClose" class="close">&nbsp;</div>
	<div id="right">
		<iframe id="frameMain" name="frameMain" src="" style="overflow:visible;"
			scrolling="yes" frameborder="0" width="100%"></iframe>
	</div>
</div>
<script type="text/javascript"> 
	var leftWidth = "160"; // 左侧窗口大小
	function wSize(){
		var strs = getWindowSize().toString().split(",");
		$("#frameTree, #frameMain, #openClose").height(strs[0]-5);
		$("#right").width($("body").width()-$("#left").width()-$("#openClose").width()-5);
	}
</script>
<script src="${ctxStatic}/common/wsize.min.js" type="text/javascript"></script>
</html>