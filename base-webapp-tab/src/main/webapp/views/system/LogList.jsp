<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var typesFmt = function(cellvalue, options, rowObject) {
			if(cellvalue==1) { return "访问";}
			return "异常";
		};
		var optionsFmt = function (cellvalue, options, rowObject) {
			return "<i class='detail' data-id='"+rowObject.id+"' title='明细'></i>";
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/system/log/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:true, 
				rownumbers: !0,//序号
				multiselect:false,//定义是否可以多选
				multiboxonly: false,
				colNames: ['日志类型','操作用户','操作时间','操作用户的IP地址', '操作的URI', '操作的方式', '操作提交的数据', '操作用户代理信息', '异常信息', ''],
				colModel: [
	                {name:'type', index:'type', width:80,sortable:false,formatter:typesFmt},
	                {name:'createName', index:'createName', width:100,sortable:false},
	                {name:'createDate', index:'createDate', width:100,sortable:false},
					{name:'remoteAddr', index:'remoteAddr', width:100,sortable:false},
					{name:'requestUri', index:'requestUri',width:150,sortable:false},
					{name:'method', index:'method',align:'center',width:80,sortable:false},
					{name:'params', index:'params', align:'center', width:150,sortable:false},
					{name:'userAgent', index:'userAgent',align:'center',width:150,sortable:false},	
					{name:'exception', index:'exception',align:'center',width:200,sortable:false},	
					{name:'options', index:'options',align:'center',width:40,sortable:false,formatter:optionsFmt}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		$('#dataGrid').on('click','.detail',function(e){
			Public.openWindow('iframe:${ctx}/system/log/form?id='+$(this).attr('data-id'),'错误日志明细',700,450);
		});
		Public.combo('#type');
		Public.autoCombo('#createName','${ctx}/system/user/jSonList');
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow: hidden;">
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
					<div class="fl">
					  <strong>日志查询：</strong>
					  <div class="ui-btn-menu">
					      <span class="ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu" style="width: 320px;">
					           <div class="control-group formSep">
									<label class="control-label">操作用户:</label>
									<div class="controls">
										<input type="text" name="createName" id='createName'/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">日志类型:</label>
									<div class="controls">
										<select id='type' name='type'>
										    <option value=''></option>
										    <option value='1'>访问</option>
										    <option value='2'>异常</option>
										</select>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">操作用户:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="remoteAddr"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">操作URL:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="requestUri"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">用户代理:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="userAgent"/>
									</div>
							   </div>
						       <div class="ui-btns"> 
					              <input class="ui-btn-href query" type="button" value="查询"/>
					              <input class="ui-btn reset" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" class="ui-btn-href" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()">
					</div>
					<div class="fr">
					   <input type="button" class="ui-btn"  onclick="optionsFmt4D()" value="&nbsp;导&nbsp;出&nbsp;">
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>