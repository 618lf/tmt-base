<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<html>
<head>
<title>TMT基础开发平台</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/login.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/bootstrap/2.3.2/js/bootstrap.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.method.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#loginForm").validate({
			rules: {
				validateCode: {remote: "${pageContext.request.contextPath}/servlet/validateCodeServlet"}
			},
			messages: {
				username: {required: "请填写用户名."},password: {required: "请填写密码."},
				validateCode: {remote: "验证码不正确.", required: "请填写验证码."}
			},
			errorLabelContainer: "#messageBox",
			errorPlacement: function(error, element) {
				error.appendTo($("#loginError").parent());
			} 
		});
	});
	//如果在框架中，则跳转刷新上级页面
	if(self.frameElement && (self.frameElement.tagName=="IFRAME"||self.frameElement.tagName=="FRAME")){
		top.location.reload();
	}
	function register(){
		//window.location.href="${ctx}/register/userRegister";
	}
</script>
</head>
<body>
	<div class="header"></div>
    <div class="main" id="mainBg" style="background-color: #f5f5f5;">
		<div><%String error = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);%>
			<div id="messageBox" class="alert alert-error <%=error==null?"hide":""%>"><button data-dismiss="alert" class="close">×</button>
				<label id="loginError" class="error"><%=error==null?"":"com.tmt.base.system.security.CaptchaException".equals(error)?"验证码错误, 请重试.":"用户或密码错误, 请重试." %></label>
			</div>
		</div>
		<div class="login">
			<div class="loginForm">
			    <form id="loginForm" class="form-signin" action="${ctx}/login" method="post">
			        <tags:token/>
			        <h2 class="form-signin-heading">TMT基础开发平台</h2>
			        <div class="control-group">
			           <label class="input-label control-label" for="username">登录名</label>
			           <div class="controls">
			             <input type="text" id="username" name="username" class="input-block-level required" value="${username}">
			           </div>
			        </div>
			        <div class="control-group">
				        <label class="input-label control-label" for="password">密码</label>
						<div class="controls">
						  <input type="password" id="password" name="password" class="input-block-level required" value="${password}">
						</div>
			        </div>
			        <c:if test="${isValidateCodeLogin}">
			        <div class="control-group validateCode">
			            <label class="input-label mid" for="validateCode" style="display: inline-block;">验证码</label>
						<tags:validateCode name="validateCode" inputCssStyle="margin-bottom:0;"/>
			        </div>
					</c:if>
					<div class="control-group clearfix">
						<div class="forgetPwdLine" style="display: inline-block;float: left;">						
							 <label for="rememberMe" title="下次不需要再登录" style="display: inline-block;"> 
							 <input type="checkbox" id="rememberMe" name="rememberMe"/> 记住我（公共环境慎用）
							 </label>
						</div>
						<div class="forgetPwdLine" style="display: inline-block;float: right;">	
							<label for="rememberMe" title="下次不需要再登录" style="display: inline-block;"> 
							 <a class="forgetPwd" target="_blank" title="找回密码">忘记密码了?</a></label>					
						</div>
					</div>
					<div class="control-group clearfix">
						<input class="btn btn-large btn-primary" type="submit" value="登 录" style="width:100%;float: left;"/>
					</div>
			    </form>
			</div>
		</div>
    </div>
    <div id="footer" class="footer">
		<div class="footer-inner" id="footerInner">
		    <a href="${ctx}/guestbook" target="_blank">公共留言</a> | <a href="${ctx}/search" target="_blank">全站搜索</a> | <a target="_blank">站点地图</a> | <a href="mailto:thinkgem@163.com">技术支持</a> | <a href="${pageContext.request.contextPath}${fns:getAdminPath()}" target="_blank">后台管理</a>
		</div>
	</div>
</body>
</html>
