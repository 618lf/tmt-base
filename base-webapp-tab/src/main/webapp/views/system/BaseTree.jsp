<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String defaulExpandLevel = String.valueOf(request.getParameter("defaulExpandLevel"));
	if(defaulExpandLevel == null || "null".equals(defaulExpandLevel)) { defaulExpandLevel = "0"; }
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>智信移单位个人所得税管理软件V1.0.0</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<link href="${ctxStatic}/jquery-ztree/3.5.12/css/zTreeStyle/zTreeStyle.min.css" rel="stylesheet" type="text/css"/>
<script src="${ctxStatic}/jquery-ztree/3.5.12/js/jquery.ztree.core-3.5.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-ztree/3.5.12/js/jquery.ztree.excheck-3.5.min.js" type="text/javascript"></script>
<script type="text/javascript">
  var zTreeNodes = [];
  var firstNodeId = '${firstNode}';
  var treeUrl = '${ctx}/${treeUrl}';
  var defaulExpandLevel = 0||"<%=defaulExpandLevel%>";
  var tree, setting = {view:{selectedMulti:false},check:{enable:("${checked}" == "true"),nocheckInherit:true},
			data:{simpleData:{enable:true}},
			view:{
				fontCss:function(treeId, treeNode) {
					return (!!treeNode.highlight) ? {"font-weight":"bold"} : {"font-weight":"normal"};
				}
			},
			callback:{beforeClick:function(id, node){
				if("${checked}" == "true"){
					tree.checkNode(node, !node.checked, true, true);
					return false;
				}
			}, 
			onClick:function(event, treeId, treeNode){
				if(treeNode.id) {
					frameMainShow(treeNode);
				}
            },
			onDblClick:function(){
            }
  }};
  function selectNode(nodeId) {
	  var node = tree.getNodeByParam("id", nodeId);
	  if("${checked}" == "true"){
			try{tree.checkNode(node, true, true);}catch(e){}
			tree.selectNode(node, false);
	  }else{
			tree.selectNode(node, true);
	  }
	  tree.setting.callback.onClick(null, tree.setting.treeId, node);
  }
  function frameMainShow(treeNode){
	  if(typeof(treeNode) == 'object'){
		  parent.frameMain.document.location.href = treeUrl + treeNode.id+"&isLeaf=" +((treeNode.isParent)?0:1);
	  }else{
		  parent.frameMain.document.location.href = treeUrl + treeNode;
	  }
  }
  $(function(){
	  tree = $.fn.zTree.init($("#tree"), setting, zTreeNodes);
	  // 默认展开一级节点
	  var nodes = tree.getNodesByParam("level", defaulExpandLevel);
	  for(var i=0; i<nodes.length; i++) {
		  tree.expandNode(nodes[i], true, false, false);
		  if(!firstNodeId) {
			  firstNodeId = nodes[i].id;
		  }
	  }
	  //展示默认节点
	  if(!!firstNodeId) {
		  selectNode(firstNodeId);
	  } else {
		  frameMainShow('-1');
	  }
	  //自动调整大小
	  wSize();
  });
  function wSize(){
	$(".ztree").width($(window).width()-16).height($(window).height() - 43);
	$(".ztree").css({"overflow":"auto","overflow-x":"auto","overflow-y":"auto"});
	$("html,body").css({"overflow":"hidden","overflow-x":"hidden","overflow-y":"hidden"});
  }
</script>
<style type="text/css">
.ztree {
	overflow: auto;
	margin: 0;
	_margin-top: 10px;
	padding: 10px 0 0 10px;
}
.mod-box {
 margin-top: 1px;
}
</style>
</head>
<body style="overflow: hidden;">
	<div class="mod-box" id="fixedAssets-box">
		<div class="hd">组织结构</div>
		<div class="bd">
		     <div id="tree" class="ztree"></div>
		</div>
	</div>
	<script type="text/javascript">
		<!--//
			<c:forEach items="${items}" var="tree">
			   zTreeNodes.push({id:'${tree.id}',pId:'${tree.parent}',name:'${tree.treeName}',type:'${tree.treeType}'});
		    </c:forEach>
		//-->
	</script>
</body>
</html>