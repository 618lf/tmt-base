<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script src="${ctxStatic}/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-jbox/2.3/Skins/Bootstrap/jbox.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jbox/2.3/i18n/jquery.jBox-zh-CN.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
<link href="${ctxStatic}/common/plugins.css" rel="stylesheet" /> 
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#name").focus();
		$("#inputForm").validate(
			Public.validate({
				rules:{
					loginName:{
						 required: true,
						 remote: {
							    url: "${ctx}/system/user/checkAccount", 
							    type: "post",  
							    dataType: "json", 
							    data: {     
							    	userId: function() {
							            return $("#id").val();
							        },
							        account: function() {
							            return $("#loginName").val();
							        }
							    }
						 }
					},
					no:{
						 required: true,
						 remote: {
							    url: "${ctx}/system/user/checkNo", 
							    type: "post",  
							    dataType: "json", 
							    data: {     
							    	userId: function() {
							            return $("#id").val();
							        },
							        account: function() {
							            return $("#no").val();
							        }
							    }
						 }
					}
				},
				messages: {
					loginName:{ remote:'账户名重复！' },
					no:{ remote:'用户编码重复！'}
				}
			})
		);
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/system/user/initList?id="+$('#id').val();
		});
		$(document).on('blur','#newPassWord',function(){
			$(this).next("label").remove();
		});
		$(document).on('blur','#rNewPassWord',function(){
			$(this).next("label").remove();
		});
		$(document).on('click','#initPBtn',function(){
			Public.openWindow($("#initPasswordDiv").val(),"初始化密码",500,230,{
				buttons:{"确定":"mok", "关闭":true},
				submit:function(v, h, f){
					var returnValue = false;
					if (v=="mok"){
						$.ajaxSetup({async: false});
						$("#initPassword").ajaxSubmit({
							url: '${ctx}/system/user/initPassword',
							beforeSubmit:function(){
								var bFalg = true;
								//判断必填项
								$("#initPassword .required").each(function(index, item){
									if( !($(item).val())) {
										if( !$(item).parent().find("label").eq(0).get(0) ){
											$("<label for='password' class='error'>必填信息</label>").insertAfter(item);
										}
										bFalg = false;
									}
								});
								//判断是否一致
								if(bFalg && !($('#newPassWord').val() === $('#rNewPassWord').val())) {
									if( !$('#rNewPassWord').parent().find("label").eq(0).get(0) ) {
										$("<label for='password' class='error'>确认密码不一致</label>").insertAfter($('#rNewPassWord'));
									}
									bFalg = false;
								}
								return bFalg;
							},
							success: function(data){
								if(data) {
									Public.success("初始化密码成功，结果已经发送邮件通知用户!");
								} else {
									Public.error("初始化密码成功，通知邮件发送失败，请检查用户名和密码以及网络状况!");
								}
								returnValue = true; 
							}
					    });
						$.ajaxSetup({async: true});
					} else {
						returnValue = true;
					}
					return returnValue;
				}
			});
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
<style type="text/css">
   #initPassword .control-label{width: 100px;}
   #initPassword .controls{margin-left: 120px;}
</style>
</head>
<body>
	<tags:message content="${message}"/>
	<div class="row-fluid wrapper">
	   <div class="bills">
	    <div class="page-header">
	       <h3>个人信息 <small> &gt;&gt; 编辑</small></h3>
	    </div>
		<form:form id="inputForm" modelAttribute="user" action="${ctx}/system/user/selfInfoSave" method="post" class="form-horizontal">
			<tags:token/>
			<form:hidden path="id"/>
			<div class="control-group formSep">
				<label class="control-label">用户姓名:</label>
				<div class="controls">
					<form:input path="name" htmlEscape="false" maxlength="50" class="required realName"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">用户帐号:</label>
				<div class="controls">
					<form:input path="loginName" htmlEscape="false" maxlength="50" class='userName'/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">用户编码:</label>
				<div class="controls">
					<form:input path="no" htmlEscape="false" maxlength="50" class="required abc"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">所属组织:</label>
				<div class="controls">
					<tags:treeselect id="office" name="officeId" value="${user.office.id}" labelName="officeName" labelValue="${user.office.name}"
					      title="所属组织" url="${ctx}/system/office/treeSelect" cssClass="required" disabled="disabled"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">职务:</label>
				<div class="controls">
					<form:input path="duty" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">生日:</label>
				<div class="controls">
				    <input type="text" name="birthday" value="<fmt:formatDate value="${user.birthday}" pattern="yyyy-MM-dd HH:mm:ss"/>" readonly="readonly" maxlength="50" class="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});">
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">电话:</label>
				<div class="controls">
				    <form:input path="phone"  maxlength="50" class="digits"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">手机:</label>
				<div class="controls">
				    <form:input path="mobile"  maxlength="50" class="digits"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">地址:</label>
				<div class="controls">
				    <form:input path="address" maxlength="50"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">电子邮件:</label>
				<div class="controls">
				    <div class="input-prepend">
					  <span class="add-on">@</span>
					  <form:input path="email"  maxlength="50"/>
					</div>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">备注:</label>
				<div class="controls">
				    <form:textarea path="remarks"  maxlength="255" cssStyle="width:350px;" rows="4" />
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">用户状态:</label>
				<div class="controls">
				     <c:if test="${user.status == 1}">待修改密码</c:if>
				     <c:if test="${user.status == 2}">锁定</c:if>
				     <c:if test="${user.status == 3}">待修改密码-锁定</c:if>
				     <c:if test="${user.status == 4}">正常</c:if>
				     <form:hidden path="status"/>
				</div>
			</div>
			<div class="control-group formSep">
			    <label class="control-label">用户角色:</label>
				<div class="controls">
				     ${user.roleNames}
				     <form:hidden path="roleIds"/>
				</div>
			</div>
			<div class="form-actions">
				<input id="submitBtn" class="ui-btn-href" type="submit" value="保 存"/>
				<shiro:hasPermission name="selfinfo:md:password">
				  <input id="initPBtn" class="ui-btn" type="button" value="初始化密码" onclick="initPasswordDiv()"/>
				</shiro:hasPermission>
			</div>
		</form:form>
	   </div>
	</div>
<textarea id="initPasswordDiv" style="display: none;">
	<div class="row-fluid">
	   <form id="initPassword" name="initPassword" action="${ctx}/system/user/initPassword" class="form-horizontal" method="post">
		 <input type="hidden" name="id" class="required" value="${user.id}"/>
		 <div class="control-group formSep">
			<label class="control-label">新密码:</label>
			<div class="controls">
				<input type="text" name="newPassWord" id="newPassWord" class="required"/>
			</div>
		 </div>
		 <div class="control-group formSep">
			<label class="control-label">确认密码:</label>
			<div class="controls">
				<input type="text" name="rNewPassWord" id="rNewPassWord" class="required"/>
			</div>
		 </div>
       </form>
	</div>
</textarea>
</body>
</html>