<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#name").focus();
		$("#inputForm").validate(
			Public.validate()
		);
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/system/area/initList?id="+$('#id').val();
		});
		Public.combo('#type');
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body>
    <tags:message content="${message}"/>
	<div class="row-fluid wrapper">
	    <div class="bills">
	      <div class="page-header">
	         <h3>区域管理 <small> &gt;&gt; 编辑</small></h3>
	      </div>
	      <form:form id="inputForm" modelAttribute="area" action="${ctx}/system/area/save" method="post" class="form-horizontal">
			<tags:token/>
			<form:hidden path="id"/>
			<form:hidden path="parentIds"/>
			<div class="control-group formSep">
				<label class="control-label">上级区域:</label>
				<div class="controls">
				    <tags:treeselect id="parent" name="parentId" value="${area.parentId}" labelName="parentName" labelValue="${area.parentName}"
					      title="区域" url="${ctx}/system/area/treeSelect" extId="${area.id}" />
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">区域名称:</label>
				<div class="controls">
					<form:input path="name" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">区域CODE:</label>
				<div class="controls">
					<form:input path="code" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">区域类型:</label>
				<div class="controls">
				    <form:select path="type" class="required">
				       <form:option value="1">省</form:option>
				       <form:option value="2">市</form:option>
				       <form:option value="3">其他</form:option>
				    </form:select>
				</div>
			</div>
			<div class="form-actions">
				<input id="submitBtn" class="ui-btn-href" type="submit" value="保 存"/>
				<input id="cancelBtn" class="ui-btn" type="button" value="返 回"/>
			</div>
		  </form:form>
	    </div>
	</div>
</body>
</html>