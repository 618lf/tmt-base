<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
</script>
</head>
<body>
<div class="row-fluid wrapper white">
	<form:form id="inputForm" modelAttribute="log" action="${ctx}/system/area/save" method="post" class="form-horizontal">
	 	<form:hidden path="id"/>
		<div class="control-group formSep">
			<label class="control-label">日志类型:</label>
			<div class="controls">
				<form:select path="type" disabled="true">
	                 <form:option value="1">访问</form:option>
	                 <form:option value="2">异常</form:option>
	            </form:select>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">操作用户:</label>
			<div class="controls">
				<form:input path="createName"/>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">操作用户的IP地址:</label>
			<div class="controls">
				<form:input path="remoteAddr" cssStyle="width:75%"/>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">操作的URI:</label>
			<div class="controls">
				<form:input path="requestUri" cssStyle="width:75%"/>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">操作的方式:</label>
			<div class="controls">
				<form:input path="method" cssStyle="width:75%"/>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">操作提交的数据:</label>
			<div class="controls">
			    <form:textarea path="params" cols="40" rows="3" cssStyle="width:75%"/>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">操作用户代理信息:</label>
			<div class="controls">
			    <form:textarea path="userAgent" cols="40" rows="3" cssStyle="width:75%"/>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">异常信息:</label>
			<div class="controls">
			    <form:textarea path="exception" cols="40" rows="10" cssStyle="width:75%"/>
			</div>
		</div>
	</form:form>
	</div>
</body>
</html>