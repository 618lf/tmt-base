<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<link href="${ctxStatic}/common/font-awesome.min.css" rel="stylesheet" />
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#name").focus();
		$("#inputForm").validate(
			Public.validate()
		);
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/system/menu/initList?id="+$('#id').val();
		});
		Public.combo('#type,#isShow');
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body>
    <tags:message content="${message}"/>
	<div class="row-fluid wrapper">
	    <div class="bills">
	      <div class="page-header">
	         <h3>菜单管理 <small> &gt;&gt; 编辑</small></h3>
	      </div>
	      <form:form id="inputForm" modelAttribute="menu" action="${ctx}/system/menu/save" method="post" class="form-horizontal">
			<tags:token/>
			<form:hidden path="id"/>
			<form:hidden path="parentIds"/>
			<div class="control-group formSep">
				<label class="control-label">上级菜单:</label>
				<div class="controls">
				    <tags:treeselect id="parent" name="parentId" value="${menu.parentId}" labelName="parentName" labelValue="${menu.parentMenu.name}"
					      title="菜单" url="${ctx}/system/menu/treeSelect" extId="${menu.id}" cssClass="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">菜单名称:</label>
				<div class="controls">
					<form:input path="name" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">菜单类型:</label>
				<div class="controls">
				    <form:select path="type" class="required">
				       <form:option value="1">目录</form:option>
				       <form:option value="2">菜单</form:option>
				       <form:option value="3">权限</form:option>
				    </form:select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">链接:</label>
				<div class="controls">
					<form:input path="href" htmlEscape="false" maxlength="200"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">权限标识:</label>
				<div class="controls">
					<form:input path="permission" htmlEscape="false" maxlength="50"/>
				</div>
			</div> 
			<div class="control-group formSep">
				<label class="control-label">菜单图标:</label>
				<div class="controls">
				     <tags:iconselect id="iconClass" name="iconClass" value="${menu.iconClass}" style="font-size: 24px;"></tags:iconselect>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">同级排序:</label>
				<div class="controls">
					<form:input path="sort" htmlEscape="false" value="${menu.sort}" maxlength="50" class="required digits" />
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">是否显示:</label>
				<div class="controls">
					<form:select path="isShow" class="required">
				       <form:option value="1">显示</form:option>
				       <form:option value="0">隐藏</form:option>
				    </form:select>
				</div>
			</div>
			<div class="form-actions">
				<input id="submitBtn" class="ui-btn-href" type="submit" value="保 存"/>
				<input id="cancelBtn" class="ui-btn" type="button" value="返 回"/>
			</div>
		  </form:form>
	    </div>
	</div>
</body>
</html>