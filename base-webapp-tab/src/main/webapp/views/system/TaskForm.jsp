<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#name").focus();
		$("#inputForm").validate(Public.validate());
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/system/task/initList";
		});
		Public.combo('#type,#businessObject');
		Public.uniform();
	}
};
$(function(){
	THISPAGE._init();
});	
</script>
</head>
<body>
	<div class="row-fluid wrapper">
	    <div class="bills">
	      <div class="page-header">
	       <h3>任务管理 <small> &gt;&gt; 编辑</small></h3>
	      </div>
	      <form:form id="inputForm" modelAttribute="task" action="${ctx}/system/task/save" method="post" class="form-horizontal">
			<tags:token/>
			<form:hidden path="id"/>
			<tags:message content="${message}"/>
			<div class="control-group formSep">
				<label class="control-label">任务名称:</label>
				<div class="controls">
					<form:input path="name" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">任务类型:</label>
				<div class="controls">
				    <form:select path="type" class="required">
				       <form:option value="PERIOD_TASK">周期任务</form:option>
				       <form:option value="ONCE_TASK">一次性任务</form:option>
				    </form:select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">执行间隔（分 时 天 周 月 年）:</label>
				<div class="controls">
				    <form:input path="cronExpression" htmlEscape="false" maxlength="200"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">允许执行的次数:</label>
				<div class="controls">
					<form:input path="allowExecuteCount" htmlEscape="false" maxlength="200"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">业务对象:</label>
				<div class="controls">
					<form:select path="businessObject" items="${business}" itemLabel="label" itemValue="value"/>
				</div>
			</div> 
			<div class="control-group formSep">
				<label class="control-label">状态:</label>
				<div class="controls">
					 <label class='radio inline'>
				       <form:radiobutton path="taskStatus" value="-1" data-form='uniform'/>&nbsp;待启动
				     </label>
				     <label class='radio inline'>
				       <form:radiobutton path="taskStatus" value="0" data-form='uniform'/>&nbsp;就绪
				     </label>
				     <label class='radio inline'>
				       <form:radiobutton path="taskStatus" value="1" data-form='uniform'/>&nbsp;执行中
				     </label>
				     <label class='radio inline'>
				       <form:radiobutton path="taskStatus" value="2" data-form='uniform'/>&nbsp;暂停
				     </label>
				     <label class='radio inline'>
				       <form:radiobutton path="taskStatus" value="3" data-form='uniform'/>&nbsp;执行完成
				     </label>
				</div>
			</div> 
			<div class="form-actions">
				<input id="btnSubmit" class="ui-btn-href" type="submit" value="保 存"/>
				<input id="cancelBtn" class="ui-btn" type="button" value="返 回" onclick="history.go(-1)"/>
			</div>
		  </form:form>
	    </div>
	</div>
</body>
</html>