<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
//初始化Grid
function initGrid(){ 
	var init = Public.setGrid();
	$('#grid').jqGrid(
		Public.defaultGrid({
			url: '${ctx}/system/task/jSonList?timeid='+ Math.random(),
			height:init.h,
			shrinkToFit:true, 
			colNames: ['任务Id','任务名称', '任务类型', '计划执行次数', '已经执行次数', '上次执行时间','预计下次执行时间', '业务对象','状态', '立即执行', '操作'],
			colModel: [
                {name:'id', index:'id', width:150,sortable:false,hidden:true},
				{name:'name', index:'name', width:150,sortable:false},
				{name:'type', index:'type',width:100,sortable:false,formatter:taskTypeFmt},
				{name:'allowExecuteCount', index:'allowExecuteCount',align:'center',width:120,sortable:false,formatter:allowExecuteFmt},
				{name:'yetExecuteCount', index:'yetExecuteCount', align:'center', width:100,sortable:false},
				{name:'preExecuteTime', index:'preExecuteTime',align:'center',width:150,sortable:false},
				{name:'nextExecuteTime', index:'nextExecuteTime',align:'center',width:150,sortable:false,formatter:nextExecuteTimeFmt},
				{name:'businessObjectName', index:'businessObjectName',align:'center',width:150,sortable:false},
				{name:'taskStatus', index:'taskStatus',align:'center',width:80,sortable:false,formatter:taskStatusFmt},
				{name:'manualOperation', index:'manualOperation',align:'center',width:80,sortable:false,formatter:manualFmt},
				{name:'options', index:'options',align:'center',width:80,sortable:false,formatter:optionsFmt}
			]
		})		
	);
	$('#grid').jqGrid('setFrozenColumns');
}
function nextExecuteTimeFmt(cellvalue, options, rowObject){
	if(rowObject.manualOperation >=1){
		return "等待立即执行";
	}
	return cellvalue;
}
function manualFmt(cellvalue, options, rowObject){
	if( cellvalue <= 0 ) {
		return '否';
	} else {
		return '是';
	}
}
function optionsFmt(cellvalue, options, rowObject) {
	var text =  "<a href='${ctx}/system/task/form?id="+rowObject.id+"'>编辑</a>&nbsp;&nbsp;";
	return text;
}
function taskTypeFmt(cellvalue, options, rowObject){
	if( cellvalue == 'PERIOD_TASK' ) {
		return '周期任务';
	} else {
		return '一次性任务';
	}
}
function allowExecuteFmt(cellvalue, options, rowObject){
	if( !cellvalue ) {
		return '无限制';
	} else {
		return cellvalue;
	}
}
function taskStatusFmt(cellvalue, options, rowObject){
	var text = "";
	if(cellvalue == -1){
		text = '新建';
	} else if(cellvalue == 0){
		text = '等待执行';
	}else if(cellvalue == 1){
		text = '执行中';
	} else if(cellvalue == 2){
		text = '暂停';
	} else {
		text = '执行完毕';
	}
	if( !!rowObject.manualOperation && cellvalue != 1) {
		text = '等待执行';
	}
	return text;
}
function optionsFmt4E() {
	window.location.href = "${ctx}/system/task/form";
}
function optionsFmt4D() {
	var param = [];
	var checkeds = $('#grid').getGridParam('selarrrow');
	if( !!checkeds ) {
		$.each(checkeds,function(index,item){
			var taskId = $('#grid').getRowData(item).id;
			param.push({name:'idList',value:taskId});
		});
		Public.deletex("确定删除选中的定时任务？","${ctx}/system/task/delete",param,query);
	} else {
		Public.error("请选择要删除的定时任务!");
	}
}
function optionsFmt4Exc(){
	var param = [];
	var checkeds = $('#grid').getGridParam('selarrrow');
	if( !!checkeds && checkeds.length) {
		$.each(checkeds,function(index,item){
			var taskId = $('#grid').getRowData(item).id;
			param.push({name:'idList',value:taskId});
		});
		Public.executex("确定执行选中的定时任务？","${ctx}/system/task/execute",param,query);
	} else {
		Public.error("请选择要执行的任务!");
	}
}
function query(){
	$('#grid').jqGrid('setGridParam',{page:1}).trigger("reloadGrid");
}
function openDiv(){
	var option = {
		title:'测试'
	};
	Public.openWindowFrame(option,{});
}
$(function(){
	initGrid();
	Public.initBtnMenu();
});
</script>
</head>
<body style="overflow: hidden;">
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
			        <input type="hidden" name="orgId" id="orgId" value="${orgId}"/>
					<div class="fl">
					  <strong>定时任务：</strong>
					  <div class="ui-btn-menu">
					      <span class="ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu" style="width: 320px;">
					           <div class="control-group formSep">
									<label class="control-label">任务名称:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="name"/>
									</div>
							   </div>
						       <div class="ui-btns"> 
					              <input class="ui-btn-href" type="button" onclick="Public.doQuery()" value="查询"/>
					              <input class="ui-btn" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" class="ui-btn-href" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()">
					</div>
					<div class="fr">
					   <input type="button" class="ui-btn-href" onclick="optionsFmt4E()" value="&nbsp;添&nbsp;加&nbsp;">
					   <input type="button" class="ui-btn" onclick="optionsFmt4Exc()" value="&nbsp;立即执行&nbsp;">
					   <input type="button" class="ui-btn" onclick="optionsFmt4Lock()" value="&nbsp;暂&nbsp;停&nbsp;">
					   <input type="button" class="ui-btn" onclick="optionsFmt4UnLock()" value="&nbsp;恢&nbsp;复&nbsp;">
					   <input type="button" class="ui-btn"  onclick="optionsFmt4D()" value="&nbsp;删&nbsp;除&nbsp;">
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>