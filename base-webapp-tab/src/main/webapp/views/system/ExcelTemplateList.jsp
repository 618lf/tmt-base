<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var optionsFmt = function (cellvalue, options, rowObject) {
			var text =  "<i class='edit' data-id='"+rowObject.id+"' title='编辑'></i>";
			    text += "<i class='delete' data-id='"+rowObject.id+"' title='删除'></i>";
			return text;
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/system/excel/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:true, 
				colNames: ['模板名称', '模板类型', '目标类名称', '开始行号', '创建人', '创建日期', '特殊属性','操作'],
				colModel: [
					{name:'templateName', index:'templateName', width:150,sortable:false},
					{name:'templateType', index:'templateType',width:150,sortable:false},
					{name:'targetClass', index:'targetClass',align:'center',width:150,sortable:false},
					{name:'startRow', index:'startRow', align:'center', width:150,sortable:false},
					{name:'createName', index:'createName',align:'center',width:150,sortable:false},
					{name:'createDate', index:'createDate',align:'center',width:150,sortable:false},
					{name:'extendAttr', index:'extendAttr',align:'center',width:150,sortable:false},
					{name:'options', index:'options',align:'center',width:80,sortable:false,formatter:optionsFmt}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		var that = this;
		Public.initBtnMenu();
		var deleteTemplate = function(checkeds){
			var param = [];
			if(typeof(checkeds) === 'object'){//数组
				$.each(checkeds,function(index,item){
					var id = $('#grid').getRowData(item).id;
					param.push({name:'idList',value:id});
				});
			} else {
				param.push({name:'idList',value:checkeds});
			}
			if(param.length != 0){
				Public.deletex("确定删除选中的模板？","${ctx}/system/excel/delete",param,function(){
					Public.success("删除成功！");
					Public.doQuery();
				});
			}else {
				Public.error("请选择要删除的模板!");
			}
		};
		$(document).on('click','#addBtn',function(){
			window.location.href = "${ctx}/system/excel/excelForm";
		});
		$(document).on('click','#refreshBtn',function(){
			Public.doQuery();
		});
		$(document).on('click','#delBtn',function(){
			var checkeds = $('#grid').getGridParam('selarrrow');
			deleteTemplate(checkeds);
		});
        $('#dataGrid').on('click','.delete',function(e){
        	deleteTemplate($(this).attr('data-id'));
		});
        $('#dataGrid').on('click','.edit',function(e){
        	window.location.href = "${ctx}/system/excel/excelForm?id="+$(this).attr('data-id');
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow: hidden;">
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
			        <input type="hidden" name="orgId" id="orgId" value="${orgId}"/>
					<div class="fl">
					  <strong>模板查询：</strong>
					  <div class="ui-btn-menu">
					      <span class="ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu " style="width: 320px;">
					           <div class="control-group formSep">
									<label class="control-label">模板名称:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="menuName"/>
									</div>
							   </div>
						       <div class="ui-btns"> 
					              <input class="ui-btn-href query" type="button" value="查询"/>
					              <input class="ui-btn reset" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" id="refreshBtn" class="ui-btn-href" value="&nbsp;刷&nbsp;新&nbsp;">
					</div>
					<div class="fr">
					   <input type="button" id="addBtn" class="ui-btn-href" value="&nbsp;添&nbsp;加&nbsp;"/>
					   <input type="button" id="delBtn" class="ui-btn"  value="&nbsp;删&nbsp;除&nbsp;"/>
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>