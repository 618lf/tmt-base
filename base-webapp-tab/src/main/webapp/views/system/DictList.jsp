<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="list"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var menuTypeFmt = function(cellvalue, options, rowObject) {
			var text = "";
			if( cellvalue == 1) {
				text = "目录";
			} else if( cellvalue == 2 ) {
				text = "菜单";
			} else if( cellvalue == 3 ) {
				text = "权限";
			}
			return text;
		};
		var iconUrlFmt = function(cellvalue, options, rowObject) {
			return "<i class='"+ cellvalue +"' style='font-size: 16px;'></i>";
		};
		var optionsFmt = function (cellvalue, options, rowObject) {
			var text =  "<i class='edit' data-id='"+rowObject.id+"' title='编辑'></i>";
			    text += "<i class='add'  data-id='"+rowObject.id+"' title='添加子节点'></i>";
			    text += "<i class='delete' data-id='"+rowObject.id+"' title='删除'></i>";
			return text;
		};
		$('#grid').jqGrid(
			Public.treeGrid({
				url: '${ctx}/system/dict/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:true, 
				colNames: ['字典名称', '类别', '键', '值', '描述', '操作'],
				colModel: [
					{name:'treeName', index:'label', width:150,sortable:false},
					{name:'treeType', index:'type', align:'left', width:150,sortable:false},
					{name:'treeCode', index:'code', align:'left',width:150,sortable:false},
					{name:'treePath', index:'value',align:'left',width:150,sortable:false},
					{name:'treeDesc', index:'value',align:'left',width:250,sortable:false},
					{name:'options', index:'options',align:'center',width:100,sortable:false,formatter:optionsFmt}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		Public.initBtnMenu();
		var deleteDict = function(checkeds){
			if( !!checkeds ) {
				var param = [];
				param.push({name:'idList',value:checkeds});
				Public.deletex("确定删除选中的参数配置？","${ctx}/system/dict/delete",param,function(data){
					Public.doQuery();
				});
			} else {
				Public.error("请选择要删除的参数配置!");
			}
		};
		$(document).on('click','#addBtn',function(){
			var checkeds = $('#grid').getGridParam('selrow');
			if(!!checkeds) {
				window.location.href = "${ctx}/system/dict/form?parentId="+checkeds;
			} else {
				window.location.href = "${ctx}/system/dict/form";
			}
		});
		$(document).on('click','#refreshBtn',function(){
			Public.doQuery();
		});
		$(document).on('click','#delBtn',function(){
			var checkeds = $('#grid').getGridParam('selrow');
			deleteDict(checkeds);
		});
		$('#dataGrid').on('click','.add',function(e){
			window.location.href = "${ctx}/system/dict/form?parentId="+$(this).attr('data-id');
		});
        $('#dataGrid').on('click','.delete',function(e){
        	deleteDict($(this).attr('data-id'));
		});
        $('#dataGrid').on('click','.edit',function(e){
        	window.location.href = "${ctx}/system/dict/form?id="+$(this).attr('data-id');
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow: hidden;">
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
			        <input type="hidden" name="parentId" id="parentId" value="${parentId}"/>
					<div class="fl">
					  <strong>字典查询：</strong>
					  <div class="ui-btn-menu">
					      <span class="ui-ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu" style="width: 320px;">
					           <div class="control-group formSep">
									<label class="control-label">字典名称:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="label"/>
									</div>
							   </div>
						       <div class="ui-btns"> 
					              <input class="ui-btn-href query" type="button" value="查询"/>
					              <input class="ui-btn reset" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" class="ui-btn-href" id="refreshBtn" value="&nbsp;刷&nbsp;新&nbsp;" onclick="Public.doQuery()">
					</div>
					<div class="fr">
					   <input type="button" id="addBtn" class="ui-btn-href" value="&nbsp;添&nbsp;加&nbsp;">
					   <input type="button" id="delBtn" class="ui-btn"  value="&nbsp;删&nbsp;除&nbsp;">
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>