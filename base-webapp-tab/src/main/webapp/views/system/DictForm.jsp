<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#label").focus();
		$("#inputForm").validate(
			Public.validate({
				rules:{
					code:{
						 required: true,
						 remote: {
							    url: "${ctx}/system/dict/checkDictCode", 
							    type: "post",  
							    dataType: "json", 
							    data: {     
							    	id: function() {
							            return $("#id").val();
							        },
							        dictCode: function() {
							            return $("#code").val();
							        }
							    }
						 }
					}
				},
				messages: {
					code:{ remote:'键重复！' }
				}
			})
		);
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/system/dict/initList?id="+$('#id').val();
		});
		$(document).on('click','#encrypt',function(){
			var isEncrypt = $(this).attr("checked");
			if( !!isEncrypt ){
				var p = $("#value").parent();
				var v = $("#value").val();
				$("#value").remove();
				$('<input type="password" id="value" name="value" value="" class="required"/>').val(v).appendTo(p);
			}else {
				var p = $("#value").parent();
				var v = $("#value").val();
				$("#value").remove();
				$('<input type="text" id="value" name="value" value="" class="required"/>').val(v).appendTo(p);
			}
		});
		Public.uniform();
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow-x: hidden;">
	<div class="row-fluid wrapper">
	   <div class="bills">
	    <div class="page-header">
	         <h3>字典管理 <small> &gt;&gt; 编辑</small></h3>
	    </div>
		<form:form id="inputForm" modelAttribute="dict" action="${ctx}/system/dict/save" method="post" class="form-horizontal">
			<tags:token/>
			<form:hidden path="id"/>
			<form:hidden path="parentId"/>
			<tags:message content="${message}"/>
			<div class="control-group formSep">
				<label class="control-label">名称:</label>
				<div class="controls">
					<form:input path="label" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">类别:</label>
				<div class="controls">
					<form:input path="type" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">键:</label>
				<div class="controls">
					<form:input path="code" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">值:</label>
				<div class="controls">
					<input type="text" id="value" name="value" value="${dict.value}" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">加密存储:</label>
				<div class="controls">
				    <label class='checkbox inline'>
				       <form:checkbox path="encrypt" value="1" data-form='uniform'/>&nbsp;是
				    </label>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">描述:</label>
				<div class="controls">
				   <textarea rows="5" name="remarks" style="width: 300px;">${dict.remarks}</textarea>
				</div>
			</div>
			<div class="form-actions">
				<input id="submitBtn" class="ui-btn-href" type="submit" value="保 存"/>
				<input id="cancelBtn" class="ui-btn" type="button" value="返 回"/>
			</div>
		</form:form>
	   </div>
	</div>
</body>
</html>