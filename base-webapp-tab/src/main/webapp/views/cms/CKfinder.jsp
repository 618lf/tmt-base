<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>智信移单位个人所得税管理软件V1.0.0</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-cookie/jquery.cookie.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<link href="${ctxStatic}/jquery-jqGrid/custom/jqgrid.css" rel="stylesheet"/>
<script src="${ctxStatic}/jquery-jqGrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jqGrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
<script src="${ctxStatic}/ckeditor/ckeditor.js" type="text/javascript" ></script>
<script src="${ctxStatic}/ckfinder/ckfinder.js" type="text/javascript" ></script>
<script src="${ctxStatic}/ckfinder/ckfinder.js" type="text/javascript" ></script>
<script type="text/javascript">
function decode(value, id) {// value传入值,id接受值
	var last = value.lastIndexOf("/");
	var filename = value.substring(last + 1, value.length);
	$("#" + id).text(decodeURIComponent(filename));
}
function browse(inputId, file, type) {// 文件管理器，可多个上传共用
	var finder = new CKFinder();
	finder.selectActionFunction = function(fileUrl, data) {// 设置文件被选中时的函数
		$("#" + inputId).attr("value", fileUrl);
		if ("Images" != type) { // equals函数无效? 改为符号判断 --- Alexander
			$("#" + file).attr("href", fileUrl);
			decode(fileUrl, file);
		} else {
			$("#" + file).attr("src", fileUrl);
			$("#" + file).attr("hidden", false);
		}
	};
	finder.resourceType = type;// 指定ckfinder只为type进行管理
	finder.selectActionData = inputId; // 接收地址的input ID
	finder.removePlugins = 'help';// 移除帮助(只有英文)
	finder.defaultLanguage = 'zh-cn';
	finder.popup();
}
</script>
</head>
<body style="overflow: hidden;">
	<form action="cgformFtlControllerr.do?saveCkeditor" method="post" id="myCkeditorForm">
		<input type="hidden" value="${cgformFtlEntity.id}" name="id" />
		<table style="width: 870px; height: 500px;" cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				<td align="right"><label class="Validform_label"> 图片: </label></td>
				<td class="value"><ssi:ckfinder name="image" uploadType="Images" value="${jeecgDemoCkfinderPage.image}" width="100" height="60" buttonClass="ui-button" buttonValue="上传图片"></ssi:ckfinder></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label"> 附件: </label></td>
				<td class="value"><ssi:ckfinder name="attachment" uploadType="Files" value="${jeecgDemoCkfinderPage.attachment}" buttonClass="ui-button" buttonValue="上传附件"></ssi:ckfinder></td>
			</tr>
			<tr>
				<td align="right"><label class="Validform_label"> 备注: </label></td>
				<td class="value"><ssi:ckeditor name="remark" isfinder="true" value="${jeecgDemoCkfinderPage.remark}" type="width:750"></ssi:ckeditor></td>
			</tr>
		</table>
	</form>
</body>
</html>