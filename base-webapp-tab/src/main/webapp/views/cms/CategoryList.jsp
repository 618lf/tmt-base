<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>智信移单位个人所得税管理软件V1.0.0</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-cookie/jquery.cookie.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<link href="${ctxStatic}/jquery-jqGrid/custom/jqgrid.css" rel="stylesheet"/>
<script src="${ctxStatic}/jquery-jqGrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jqGrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
<link href="${ctxStatic}/treeTable/themes/vsStyle/treeTable.min.css" rel="stylesheet" type="text/css" />
<script src="${ctxStatic}/treeTable/jquery.treeTable.min.js" type="text/javascript"></script>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var optionsFmt = function (cellvalue, options, rowObject) {
			var text =  "<i class='edit' data-id='"+rowObject.id+"' title='编辑'></i>";
			    text +=  "<i class='add' data-id='"+rowObject.id+"' title='添加'></i>";
			    text += "<i class='delete' data-id='"+rowObject.id+"' title='删除'></i>";
			return text;
		};
		$('#grid').jqGrid(
			Public.treeGrid({
				url: '${ctx}/cms/category/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:true, 
				rownumbers:!1,
				colNames: ['栏目名称', 'ID', '归属机构', '栏目模型', '排序', '导航菜单', '栏目列表','展现方式', '操作'],
				colModel: [
					{name:'name', index:'name', width:150,sortable:false},
					{name:'id', index:'id', width:150,sortable:false,hidden:true},
					{name:'officeName', index:'officeName',width:150,sortable:false},
					{name:'moduleLabel', index:'module',align:'center',width:150,sortable:false},
					{name:'sort', index:'sort',align:'center',width:150,sortable:false},
					{name:'inMenuLabel', index:'inMenu',align:'center',width:150,sortable:false},
					{name:'inListLabel', index:'inList',align:'center',width:150,sortable:false},	
					{name:'showModesLabel', index:'showModes',align:'center',width:150,sortable:false},
					{name:'options', index:'options',align:'center',width:150,sortable:false,formatter:optionsFmt}
				]
			})
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		Public.initBtnMenu();
		var deleteCategory = function(checkeds){
			if( !!checkeds && checkeds.length ) {
				var param = [];
				if(typeof(checkeds) === 'object'){
					$.each(checkeds,function(index,item){
						var userId = $('#grid').getRowData(item).id;
						param.push({name:'idList',value:userId});
					});
				} else {
					param.push({name:'idList',value:checkeds});
				}
				Public.deletex("确定删除选中的栏目？","${ctx}/cms/category/delete",param,function(data){
					if(!!data.success) {
						Public.success('删除栏目成功');
						Public.doQuery();
					} else {
						Public.error(data.msg);
					}
				});
			} else {
				Public.error("请选择要删除的栏目!");
			}
		};
		$(document).on('click','#addBtn',function(){
			window.location.href = "${ctx}/cms/category/form";
		});
		$(document).on('click','#refreshBtn',function(){
			Public.doQuery();
		});
		$(document).on('click','#delBtn',function(){
			var checkeds = $('#grid').getGridParam('selrow');
			deleteCategory(checkeds);
		});
		$(document).on('click','#sortBtn',function(){
			var checkeds = $('#grid').getGridParam('selarrrow');
		});
        $('#dataGrid').on('click','.delete',function(e){
        	deleteCategory($(this).attr('data-id'));
		});
        $('#dataGrid').on('click','.add',function(e){
        	window.location.href = "${ctx}/cms/category/form?parentId="+$(this).attr('data-id');
		});
        $('#dataGrid').on('click','.edit',function(e){
        	window.location.href = "${ctx}/cms/category/form?id="+$(this).attr('data-id');
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body style="overflow: hidden;">
    <tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
					<div class="fl">
					  <strong>栏目查询：</strong>
					  <div class="ui-btn-menu">
					      <span class="ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu" style="width: 320px;">
					           <div class="control-group formSep">
									<label class="control-label">栏目名称:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="name"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">归属机构:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="officeName"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">栏目模型:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="module"/>
									</div>
							   </div>
						       <div class="ui-btns"> 
					              <input class="ui-btn-href query" type="button" value="查询"/>
					              <input class="ui-btn reset" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" id="refreshBtn" class="ui-btn-href" value="&nbsp;刷&nbsp;新&nbsp;">
					</div>
					<div class="fr">
					   <input type="button" id="addBtn" class="ui-btn-href" value="&nbsp;添&nbsp;加&nbsp;">
					   <input type="button" id="sortBtn" class="ui-btn-href" value="&nbsp;保存排序&nbsp;">
					   <input type="button" id="delBtn" class="ui-btn"  value="&nbsp;删&nbsp;除&nbsp;">
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>