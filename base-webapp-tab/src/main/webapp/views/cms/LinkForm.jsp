<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#name").focus();
		$("#inputForm").validate(
			Public.validate({
				submitHandler: function(form){
                    if ($("#categoryId").val()==""){
                        $("#categoryName").focus();
                        top.$.jBox.tip('请选择归属栏目','warning');
                    }else if (CKEDITOR.instances.content.getData()=="" && $("#link").val().trim()==""){
                        top.$.jBox.tip('请填写正文','warning');
                    }else{
                    	Public.loading('正在提交，请稍等...');
            			form.submit();
                    }
				}
			})		
		);
	},
	addEvent : function(){
		 if($("#link").val()){
             $('#linkBody').show();
             $('#url').attr("checked", true);
         }
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/cms/article/initList";
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body>
<div class="row-fluid wrapper">
   <div class="bills">
	<form:form id="inputForm" modelAttribute="link" action="${ctx}/cms/link/save" method="post" class="form-horizontal">
		<tags:token/>
		<tags:message content="${message}"/>
		<form:hidden path="id"/>
		<div class="control-group formSep">
			<label class="control-label">归属栏目:</label>
			<div class="controls">
                <tags:treeselect id="category" name="categoryId" value="${article.categoryId}" labelName="categoryName" labelValue="${article.categoryName}"
					  title="栏目" url="${ctx}/cms/category/treeSelect" module="link" selectScopeModule="true" notAllowSelectRoot="false" notAllowSelectParent="true" cssClass="required"/>&nbsp;
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">名称:</label>
			<div class="controls">
				<form:input path="title" htmlEscape="false" maxlength="200" class="input-xxlarge required measure-input"/>
				&nbsp;<label>颜色:</label>
				<form:select path="color" class="input-mini">
					<form:option value="" label="默认"/>
					<form:options items="${fns:getDictList('SYS_CONFIG_COLOR')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">链接图片:</label>
			<div class="controls">
				<form:hidden path="image" htmlEscape="false" maxlength="255" class="input-xlarge"/>
				<tags:ckfinder input="image" type="images" uploadPath="/cms/link" selectMultiple="false"/>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">链接地址:</label>
			<div class="controls">
				<form:input path="href" htmlEscape="false" maxlength="255" class="input-xxlarge"/>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">权重:</label>
			<div class="controls">
				<form:input path="weight" htmlEscape="false" maxlength="200" class="input-mini required digits"/>&nbsp;
				<span>
					<input id="weightTop" type="checkbox" onclick="$('#weight').val(this.checked?'999':'0')"><label for="weightTop">置顶</label>
				</span>
				&nbsp;过期时间：
				<input id="weightDate" name="weightDate" type="text" readonly="readonly" maxlength="20" class="input-small Wdate"
					value="<fmt:formatDate value="${link.weightDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:true});"/>
				<span class="help-inline">数值越大排序越靠前，过期时间可为空，过期后取消置顶。</span>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">备注:</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="200" class="input-xxlarge"/>
			</div>
		</div>
		<div class="control-group formSep">
			<label class="control-label">发布状态:</label>
			<div class="controls">
				<form:radiobuttons path="delFlag" items="${fns:getDictList('SYS_CONFIG_DEL_FALG')}" itemLabel="label" itemValue="value" htmlEscape="false" class="required"/>
				<span class="help-inline"></span>
			</div>
		</div>
		<div class="form-actions">
			<input id="btnSubmit" class="ui-btn-href" type="submit" value="保 存"/>&nbsp;
			<input id="cancelBtn" class="ui-btn" type="button" value="返 回"/>
		</div>
	</form:form>
	</div>
</div>
</body>
</html>