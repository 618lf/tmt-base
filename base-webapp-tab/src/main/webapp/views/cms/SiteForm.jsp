<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>智信移单位个人所得税管理软件V1.0.0</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.method.min.js" type="text/javascript"></script>
<link href="${ctxStatic}/font/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#name").focus();
		$("#inputForm").validate(
			Public.validate({})		
		);
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/cms/site/initList";
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body>
    <tags:message content="${message}"/>
    <div class="row-fluid wrapper">
	   <div class="bills">
	        <form:form id="inputForm" modelAttribute="site" action="${ctx}/cms/site/save" method="post" class="form-horizontal">
				<form:hidden path="id"/>
				<tags:token/>
				<div class="control-group formSep">
					<label class="control-label">站点名称:</label>
					<div class="controls">
						<form:input path="name" htmlEscape="false" maxlength="200" class="input-xlarge required"/>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">站点标题:</label>
					<div class="controls">
						<form:input path="title" htmlEscape="false" maxlength="200" class="input-xlarge required"/>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">站点Logo:</label>
					<div class="controls">
						<form:hidden path="logo" htmlEscape="false" maxlength="255" class="input-xlarge"/>
						<tags:ckfinder input="logo" type="images" uploadPath="/cms/site"/>
						<span class="help-inline">建议Logo大小：1000 × 145（像素）</span>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">描述:</label>
					<div class="controls">
						<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="200" class="input-xxlarge"/>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">关键字:</label>
					<div class="controls">
						<form:input path="keywords" htmlEscape="false" maxlength="200"/>
						<span class="help-inline">填写描述及关键字，有助于搜索引擎优化</span>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">默认主题:</label>
					<div class="controls">
						<form:select path="theme">
							<form:options items="${fns:getDictList('SYS_CONFIG_CMS_THEME')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">版权信息:</label>
					<div class="controls">
						<form:textarea id="copyright" htmlEscape="true" path="copyright" rows="4" maxlength="200" class="input-xxlarge"/>
						<tags:ckeditor replace="copyright" uploadPath="/cms/site" />
					</div>
				</div>
				<div class="control-group formSep">
					<label class="control-label">自定义首页视图:</label>
					<div class="controls">
						<form:input path="customIndexView" htmlEscape="false" maxlength="200"/>
					</div>
				</div>
				<div class="form-actions">
					<input id="submitBtn" class="ui-btn-href" type="submit" value="保 存"/>
				    <input id="cancelBtn" class="ui-btn" type="button" value="返 回"/>
				</div>
			</form:form>
	   </div>
	</div>
</body>
</html>