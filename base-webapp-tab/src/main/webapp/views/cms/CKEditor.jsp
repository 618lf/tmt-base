<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>智信移单位个人所得税管理软件V1.0.0</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-cookie/jquery.cookie.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<link href="${ctxStatic}/jquery-jqGrid/custom/jqgrid.css" rel="stylesheet"/>
<script src="${ctxStatic}/jquery-jqGrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jqGrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
<script src="${ctxStatic}/ckeditor/ckeditor.js" type="text/javascript" ></script>
<script type="text/javascript">
</script>
</head>
<body style="overflow: hidden;">
	<form action="cgformFtlControllerr.do?saveCkeditor" method="post" id="myCkeditorForm">
		<input type="hidden" value="${cgformFtlEntity.id}" name="id" />
		<textarea cols="80" id="editor1" name="contents" rows="100">1232323</textarea>
		<script>
			CKEDITOR.replace('editor1',{
				height : 350,
				customConfig : '${ctxStatic}/ckeditor/ftlConfig.js'
			});
		</script>
	</form>
</body>
</html>