<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>智信移单位个人所得税管理软件V1.0.0</title>
<meta name="decorator" content="form"/>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#title").focus();
		$("#inputForm").validate(
			Public.validate({
				submitHandler: function(form){
                    if ($("#categoryId").val()==""){
                        $("#categoryName").focus();
                        top.$.jBox.tip('请选择归属栏目','warning');
                    }else if (CKEDITOR.instances.content.getData()=="" && $("#link").val().trim()==""){
                        top.$.jBox.tip('请填写正文','warning');
                    }else{
                    	Public.loading('正在提交，请稍等...');
            			form.submit();
                    }
				}
			})		
		);
	},
	addEvent : function(){
		 if($("#link").val()){
             $('#linkBody').show();
             $('#url').attr("checked", true);
         }
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/cms/article/initList";
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body>
<div class="row-fluid wrapper">
   <div class="bills">
        <form:form id="inputForm" modelAttribute="article" action="${ctx}/cms/article/save" method="post" class="form-horizontal">
			<tags:token/>
			<tags:message content="${message}"/>
			<form:hidden path="id"/>
			<div class="control-group formSep">
				<label class="control-label">归属栏目:</label>
				<div class="controls">
	                <tags:treeselect id="category" name="categoryId" value="${article.categoryId}" labelName="categoryName" labelValue="${article.categoryName}"
						title="栏目" url="${ctx}/cms/category/treeSelect" module="article" selectScopeModule="true" notAllowSelectRoot="false" notAllowSelectParent="true" cssClass="required"/>&nbsp;
	                <span>
	                    <input id="url" type="checkbox" onclick="if(this.checked){$('#linkBody').show()}else{$('#linkBody').hide()}$('#link').val()"><label for="url">外部链接</label>
	                </span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">标题:</label>
				<div class="controls">
					<form:input path="title" htmlEscape="false" maxlength="200" class="input-xxlarge measure-input required"/>
					&nbsp;<label>颜色:</label>
					<form:select path="color" class="input-mini">
						<form:option value="" label="默认"/>
						<form:options items="${fns:getDictList('SYS_CONFIG_COLOR')}" itemLabel="label" itemValue="value" htmlEscape="false" />
					</form:select>
				</div>
			</div>
	        <div id="linkBody" class="control-group formSep" style="display:none">
	            <label class="control-label">外部链接:</label>
	            <div class="controls">
	                <form:input path="link" htmlEscape="false" maxlength="200" class="input-xlarge"/>
	                <span class="help-inline">绝对或相对地址。</span>
	            </div>
	        </div>
			<div class="control-group formSep">
				<label class="control-label">关键字:</label>
				<div class="controls">
					<form:input path="keywords" htmlEscape="false" maxlength="200" class="input-xlarge"/>
					<span class="help-inline">多个关键字，用空格分隔。</span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">权重:</label>
				<div class="controls">
					<form:input path="weight" htmlEscape="false" maxlength="200" class="input-mini required digits"/>&nbsp;
					<span>
						<input id="weightTop" type="checkbox" onclick="$('#weight').val(this.checked?'999':'0')"><label for="weightTop">置顶</label>
					</span>
					&nbsp;过期时间：
					<input id="weightDate" name="weightDate" type="text" readonly="readonly" maxlength="20" class="input-small Wdate"
						value="<fmt:formatDate value="${article.weightDate}" pattern="yyyy-MM-dd"/>"
						onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:true});"/>
					<span class="help-inline">数值越大排序越靠前，过期时间可为空，过期后取消置顶。</span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">摘要:</label>
				<div class="controls">
					<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="200" class="input-xxlarge"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">缩略图:</label>
				<div class="controls">
	                <input type="hidden" id="image" name="image" value="${article.image}" />
					<tags:ckfinder input="image" type="thumb" uploadPath="/cms/article" selectMultiple="false"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">正文:</label>
				<div class="controls">
					<form:textarea id="content" htmlEscape="true" path="articleData.content" rows="10" maxlength="200" class="input-xxlarge"/>
					<tags:ckeditor replace="content" uploadPath="/cms/article" />
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">来源:</label>
				<div class="controls">
					<form:input path="articleData.copyfrom" htmlEscape="false" maxlength="200" class="input-xlarge"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">相关文章:</label>
				<div class="controls">
					<form:hidden id="articleDataRelation" path="articleData.relation" htmlEscape="false" maxlength="200" class="input-xlarge"/>
					<ol id="articleSelectList"></ol>
					<a id="relationButton" href="javascript:" class="btn">添加相关</a>
					<script type="text/javascript">
						var articleSelect = [];
						function articleSelectAddOrDel(id,title){
							var isExtents = false, index = 0;
							for (var i=0; i<articleSelect.length; i++){
								if (articleSelect[i][0]==id){
									isExtents = true;
									index = i;
								}
							}
							if(isExtents){
								articleSelect.splice(index,1);
							}else{
								articleSelect.push([id,title]);
							}
							articleSelectRefresh();
						}
						function articleSelectRefresh(){
							$("#articleDataRelation").val("");
							$("#articleSelectList").children().remove();
							for (var i=0; i<articleSelect.length; i++){
								$("#articleSelectList").append("<li>"+articleSelect[i][1]+"&nbsp;&nbsp;<a href=\"javascript:\" onclick=\"articleSelectAddOrDel('"+articleSelect[i][0]+"','"+articleSelect[i][1]+"');\">×</a></li>");
								$("#articleDataRelation").val($("#articleDataRelation").val()+articleSelect[i][0]+",");
							}
						}
						$.getJSON("${ctx}/cms/article/findByIds",{ids:$("#articleDataRelation").val()},function(data){
							for (var i=0; i<data.length; i++){
								articleSelect.push([data[i][1],data[i][2]]);
							}
							articleSelectRefresh();
						});
						$("#relationButton").click(function(){
							top.$.jBox.open("iframe:${ctx}/cms/article/selectList?pageSize=8", "添加相关",$(top.document).width()-220,$(top.document).height()-180,{
								buttons:{"确定":true}, loaded:function(h){
									$(".jbox-content", top.document).css("overflow-y","hidden");
								}
							});
						});
					</script>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">是否允许评论:</label>
				<div class="controls">
					<form:radiobuttons path="articleData.allowComment" items="${fns:getDictList('SYS_CONFIG_YES_NO')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">推荐位:</label>
				<div class="controls">
					<form:checkboxes path="posidList" items="${fns:getDictList('SYS_CONFIG_CMS_POSID')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">发布时间:</label>
				<div class="controls">
					<input id="createDate" name="createDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
						value="<fmt:formatDate value="${article.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
						onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
				</div>
			</div>
			<div class="control-group formSep">
					<label class="control-label">发布状态:</label>
					<div class="controls">
						<form:radiobuttons path="delFlag" items="${fns:getDictList('SYS_CONFIG_DEL_FALG')}" itemLabel="label" itemValue="value" htmlEscape="false" class="required"/>
						<span class="help-inline"></span>
					</div>
			</div>
            <div class="control-group formSep">
                <label class="control-label">自定义内容视图:</label>
                <div class="controls">
                      <form:select path="customContentView">
                          <form:option value="" label="默认视图"/>
                          <form:options items="${contentViewList}" htmlEscape="false"/>
                      </form:select>
                      <span class="help-inline">自定义内容视图名称必须以"${article_DEFAULT_TEMPLATE}"开始</span>
                </div>
            </div>
            <div class="control-group formSep">
                <label class="control-label">自定义视图参数:</label>
                <div class="controls">
                      <form:input path="viewConfig" htmlEscape="true"/>
                      <span class="help-inline">视图参数例如: {count:2, title_show:"yes"}</span>
                </div>
            </div>
			<c:if test="${not empty article.id}">
				<div class="control-group formSep">
					<label class="control-label">查看评论:</label>
					<div class="controls">
						<input id="btnComment" class="btn" type="button" value="查看评论" onclick="viewComment('${ctx}/cms/comment/?module=article&contentId=${article.id}&status=0')"/>
						<script type="text/javascript">
							function viewComment(href){
								top.$.jBox.open('iframe:'+href,'查看评论',$(top.document).width()-220,$(top.document).height()-180,{
									buttons:{"关闭":true},
									loaded:function(h){
										$(".jbox-content", top.document).css("overflow-y","hidden");
										$(".nav,.form-actions,[class=btn]", h.find("iframe").contents()).hide();
										$("body", h.find("iframe").contents()).css("margin","10px");
									}
								});
								return false;
							}
						</script>
					</div>
				</div>
			</c:if>
			<div class="form-actions">
				<input id="btnSubmit" class="ui-btn-href" type="submit" value="保 存"/>&nbsp;
				<input id="cancelBtn" class="ui-btn" type="button" value="返 回"/>
			</div>
		</form:form>
   </div>
</div>
</body>
</html>