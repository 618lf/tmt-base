<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>智信移单位个人所得税管理软件V1.0.0</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-cookie/jquery.cookie.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<link href="${ctxStatic}/jquery-jqGrid/custom/jqgrid.css" rel="stylesheet"/>
<script src="${ctxStatic}/jquery-jqGrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jqGrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.loadGrid();
		this.addEvent();
	},
	loadGrid : function(){
		var init = Public.setGrid();
		var optionsFmt = function (cellvalue, options, rowObject) {
			var text =  "<i class='edit' data-id='"+rowObject.id+"' title='编辑'></i>";
			    text += "<i class='delete' data-id='"+rowObject.id+"' title='删除'></i>";
			return text;
		};
		$('#grid').jqGrid(
			Public.defaultGrid({
				url: '${ctx}/cms/site/jSonList?timeid='+ Math.random(),
				height:init.h,
				shrinkToFit:true, 
				rownumbers:!0,
				colNames: ['站点Id','名称', '标题', '描述', '关键字', '主题', '操作'],
				colModel: [
	                {name:'id', index:'id', width:150,sortable:false,hidden:true},
					{name:'name', index:'name', width:150,sortable:false, formatter:function(cellvalue, options, rowObject){
						if(rowObject.currentSiteId == rowObject.id) {
							return cellvalue + "【当前站点】";
						}
						return cellvalue;
					}},
					{name:'title', index:'title',width:150,sortable:false},
					{name:'remarks', index:'remarks',align:'left',width:150,sortable:false},
					{name:'keywords', index:'keywords',align:'left',width:150,sortable:false},
					{name:'theme', index:'theme',align:'left',width:150,sortable:false},	
					{name:'options', index:'options',align:'center',width:80,sortable:false,formatter:optionsFmt}
				]
			})		
		);
		$('#grid').jqGrid('setFrozenColumns');
	},
	addEvent : function(){
		Public.initBtnMenu();
		var deleteSite = function(checkeds){
			if( !!checkeds && checkeds.length ) {
				var param = [];
				if(typeof(checkeds) === 'object'){
					$.each(checkeds,function(index,item){
						var userId = $('#grid').getRowData(item).id;
						param.push({name:'idList',value:userId});
					});
				} else {
					param.push({name:'idList',value:checkeds});
				}
				Public.deletex("确定删除选中的站点？","${ctx}/cms/site/delete",param,function(data){
					if(!!data.success) {
						Public.success('删除站点成功');
						Public.doQuery();
					} else {
						Public.error(data.msg);
					}
				});
			} else {
				Public.error("请选择要删除的站点!");
			}
		};
		var selectSite = function(checkeds){
			if( !!checkeds && checkeds.length && checkeds.length != 1) {
				Public.info("只能选择一个站点");
			} else if( !!checkeds && checkeds.length ) {
				var param = [];
				param.push({name:'id',value:checkeds});
				Public.executex("确定切换选择的站点","${ctx}/cms/site/select",param,function(){
					Public.success('切换站点成功');
					Public.doQuery();
				});
			} else {
				Public.error("请选择要却换的站点!");
			}
		};
		$(document).on('click','#addBtn',function(){
			window.location.href = "${ctx}/cms/site/form";
		});
		$(document).on('click','#refreshBtn',function(){
			Public.doQuery();
		});
		$(document).on('click','#delBtn',function(){
			var checkeds = $('#grid').getGridParam('selarrrow');
			deleteSite(checkeds);
		});
		$(document).on('click','#selectBtn',function(){
			var checkeds = $('#grid').getGridParam('selarrrow');
			selectSite(checkeds);
		});
        $('#dataGrid').on('click','.delete',function(e){
        	deleteSite($(this).attr('data-id'));
		});
        $('#dataGrid').on('click','.edit',function(e){
        	window.location.href = "${ctx}/cms/site/form?id="+$(this).attr('data-id');
		});
	}
};
$(function(){
	THISPAGE._init();
}); 
</script>
</head>
<body style="overflow: hidden;">
    <tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
					<div class="fl">
					  <strong>站点查询：</strong>
					  <div class="ui-btn-menu">
					      <span class="ui-btn ui-menu-btn ui-btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu" style="width: 320px;">
					           <div class="control-group formSep">
									<label class="control-label">站点名称:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="name"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">站点标题:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="title"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">站点描述:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="remarks"/>
									</div>
							   </div>
							   <div class="control-group formSep">
									<label class="control-label">关键字:</label>
									<div class="controls">
										<input type="text" class="input-txt" name="keywords"/>
									</div>
							   </div>
						       <div class="ui-btns"> 
					              <input class="ui-btn-href query" type="button" value="查询"/>
					              <input class="ui-btn reset" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" id="refreshBtn" class="ui-btn-href" value="&nbsp;刷&nbsp;新&nbsp;">
					</div>
					<div class="fr">
					   <input type="button" id="addBtn" class="ui-btn-href" value="&nbsp;添&nbsp;加&nbsp;">
					   <input type="button" id="selectBtn" class="ui-btn-href" value="&nbsp;切换站点&nbsp;">
					   <input type="button" id="delBtn" class="ui-btn"  value="&nbsp;删&nbsp;除&nbsp;">
					</div>
				</form>
			</div>
		</div>
		<div id="dataGrid" class="autoGrid">
			<table id="grid"></table>
			<div id="page"></div>
		</div> 
    </div>
</body>
</html>