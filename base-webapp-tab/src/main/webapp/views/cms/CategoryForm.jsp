<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>智信移单位个人所得税管理软件V1.0.0</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.method.min.js" type="text/javascript"></script>
<link href="${ctxStatic}/font/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<script type="text/javascript">
var THISPAGE = {
	_init : function(){
		this.bindValidate();
		this.addEvent();
	},
	bindValidate : function(){
		$("#name").focus();
		$("#inputForm").validate(
			Public.validate({})		
		);
	},
	addEvent : function(){
		$(document).on('click','#cancelBtn',function(){
			window.location.href = "${ctx}/cms/category/initList";
		});
	}
};
$(function(){
	THISPAGE._init();
});
</script>
</head>
<body>
<div class="row-fluid wrapper">
     <div class="bills">
          <form:form id="inputForm" modelAttribute="category" action="${ctx}/cms/category/save" method="post" class="form-horizontal">
			<form:hidden path="id"/>
			<tags:token/>
			<tags:message content="${message}"/>
			<div class="control-group formSep">
				<label class="control-label">归属机构:</label>
				<div class="controls">
	                <tags:treeselect id="office" name="officeId" value="${category.officeId}" labelName="officeName" labelValue="${category.officeName}"
						title="上级组织" url="${ctx}/system/office/treeSelect" cssClass="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">上级栏目:</label>
				<div class="controls">
	                <tags:treeselect id="category" name="parentId" value="${category.parentId}" labelName="parentName" labelValue="${category.parentName}"
						title="栏目" url="${ctx}/cms/category/treeSelect" extId="${category.id}" cssClass="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">栏目模型:</label>
				<div class="controls">
					<form:select path="module">
						<form:option value="" label="公共模型"/>
						<form:options items="${fns:getDictList('SYS_CONFIG_CMS_MODULE')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
					</form:select>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">栏目名称:</label>
				<div class="controls">
					<form:input path="name" htmlEscape="false" maxlength="50" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">栏目图片:</label>
				<div class="controls">
					<form:hidden path="image" htmlEscape="false" maxlength="255" class="input-xlarge"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">链接:</label>
				<div class="controls">
					<form:input path="href" htmlEscape="false" maxlength="200"/>
					<span class="help-inline">栏目超链接地址，优先级“高”</span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">目标:</label>
				<div class="controls">
					<form:input path="target" htmlEscape="false" maxlength="200"/>
					<span class="help-inline">栏目超链接打开的目标窗口，新窗口打开，请填写：“_blank”</span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">描述:</label>
				<div class="controls">
					<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="200" class="input-xxlarge"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">关键字:</label>
				<div class="controls">
					<form:input path="keywords" htmlEscape="false" maxlength="200"/>
					<span class="help-inline">填写描述及关键字，有助于搜索引擎优化</span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">排序:</label>
				<div class="controls">
					<form:input path="sort" htmlEscape="false" maxlength="11" class="required digits"/>
					<span class="help-inline">栏目的排列次序</span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">在导航中显示:</label>
				<div class="controls">
					<form:radiobuttons path="inMenu" items="${fns:getDictList('SYS_CONFIG_CMS_SHOW_HIDE')}" itemLabel="label" itemValue="value" htmlEscape="false" class="required"/>
					<span class="help-inline">是否在导航中显示该栏目</span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">在分类页中显示列表:</label>
				<div class="controls">
					<form:radiobuttons path="inList" items="${fns:getDictList('SYS_CONFIG_CMS_SHOW_HIDE')}" itemLabel="label" itemValue="value" htmlEscape="false" class="required"/>
					<span class="help-inline">是否在分类页中显示该栏目的文章列表</span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label" title="默认展现方式：有子栏目显示栏目列表，无子栏目显示内容列表。">展现方式:</label>
				<div class="controls">
					<form:radiobuttons path="showModes" items="${fns:getDictList('SYS_CONFIG_CMS_SHOW_MODES')}" itemLabel="label" itemValue="value" htmlEscape="false" class="required"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">是否允许评论:</label>
				<div class="controls">
					<form:radiobuttons path="allowComment" items="${fns:getDictList('SYS_CONFIG_YES_NO')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">是否需要审核:</label>
				<div class="controls">
					<form:radiobuttons path="isAudit" items="${fns:getDictList('SYS_CONFIG_YES_NO')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">自定义列表视图:</label>
				<div class="controls">
	                <form:select path="customListView">
	                    <form:option value="" label="默认视图"/>
	                    <form:options items="${listViewList}" htmlEscape="false"/>
	                </form:select>
	                <span class="help-inline">自定义列表视图名称必须以"${category_DEFAULT_TEMPLATE}"开始</span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">自定义内容视图:</label>
				<div class="controls">
	                <form:select path="customContentView">
	                    <form:option value="" label="默认视图"/>
	                    <form:options items="${contentViewList}" htmlEscape="false"/>
	                </form:select>
	                <span class="help-inline">自定义内容视图名称必须以"${article_DEFAULT_TEMPLATE}"开始</span>
				</div>
			</div>
			<div class="control-group formSep">
				<label class="control-label">自定义视图参数:</label>
				<div class="controls">
	                <form:input path="viewConfig" htmlEscape="true"/>
	                <span class="help-inline">视图参数例如: {count:2, title_show:"yes"}</span>
				</div>
			</div>
			<div class="form-actions">
				<input id="btnSubmit" class="ui-btn-href" type="submit" value="保 存"/>&nbsp;
				<input id="cancelBtn" class="ui-btn" type="button" value="返 回"/>
			</div>
		</form:form>
     </div>
</div>
</body>
</html>