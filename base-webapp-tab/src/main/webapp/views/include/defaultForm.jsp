<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><sitemesh:title/> - Powered By LiFeng</title>
		<!-- styles -->
		<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
		<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
		<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-cookie/jquery.cookie.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
		<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet" />
		<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.method.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
		<script src="${ctxStatic}/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
		<link href="${ctxStatic}/common/common-ui.css" type="text/css" rel="stylesheet" />
		<script src="${ctxStatic}/jquery-form/jquery.select2.min.js" type="text/javascript"></script>
		<script src="${ctxStatic}/jquery-form/jquery.uniform.min.js" type="text/javascript"></script>
		<sitemesh:head/>
	</head>
	<body>
	    <sitemesh:body/>
	</body>
</html>
