<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>智信移单位个人所得税管理软件V1.0.0</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>

<!-- common, all times required, imports -->
<script src='${ctxStatic}/processDesigner/js/draw2d/wz_jsgraphics.js'></script>          
<script src='${ctxStatic}/processDesigner/js/draw2d/moocanvas.js'></script>                        
<script src='${ctxStatic}/processDesigner/js/draw2d/draw2d.js'></script>

<!-- designer -->
<link href="${ctxStatic}/processDesigner/js/designer/designer.css" type="text/css" rel="stylesheet"/>
<script src="${ctxStatic}/processDesigner/js/designer/MyCanvas.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/ResizeImage.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/event/Start.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/event/End.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/connection/MyInputPort.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/connection/MyOutputPort.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/connection/DecoratedConnection.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/task/Task.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/task/UserTask.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/task/ManualTask.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/gateway/ExclusiveGateway.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/gateway/ParallelGateway.js"></script>
<script src="${ctxStatic}/processDesigner/js/designer/designer.js"></script>	
<script type="text/javascript">

</script>
</head>
<body style="overflow: hidden;">
    <ul class="ui nav nav-tabs">
        <li><a href="${ctx}/system/workflow/processList">我的流程</a></li>
		<li><a href="${ctx}/system/menu/menuForm">已部署流程</a></li>
		<li class="active"><a href="${ctx}/system/workflow/processDesigner">流程设计</a></li>
	</ul>
	<tags:message content="${message}"/>
	<div class="wrapper">
	    <div id="process-panel" class="wrapper-inner"  split="true"  iconCls="process-icon" title="Process">
			
		</div>
    </div>
</body>
</html>
