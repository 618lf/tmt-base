<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>智信移单位个人所得税管理软件V1.0.0</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript" ></script>
<link href="${ctxStatic}/jquery-jqGrid/custom/jqgrid.css" rel="stylesheet"/>
<script src="${ctxStatic}/jquery-jqGrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jqGrid/js/i18n/grid.locale-cn.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-form/jquery.form.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/mustache.min.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-jbox/2.3/Skins/Bootstrap/jbox.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-jbox/2.3/i18n/jquery.jBox-zh-CN.min.js" type="text/javascript"></script>
<script type="text/javascript">
//发布流程
function deployP() {
	var html= Mustache.render($("#auditTemplate").html(),{});
	Public.openWindow(html,"部署新流程",350,null,{
		submit:function(v, h, f){
			if(v == 'ok') { //点击确定
				$("#deployForm").submit();
			}
		}
	});
}
</script>
<!-- 页面模版 --> 
<script type="text/template" id="auditTemplate">
		<div><b>支持文件格式：</b>zip、bar、bpmn、bpmn20.xml</div>
		<form id="deployForm" name="deployForm" action="${ctx}/system/workflow/deploy" method="post" enctype="multipart/form-data">
            <div class="control-group">
			   <div class="controls">
			      <input type="file" name="file" />
			   </div>
		    </div>
		</form>
</script>
</head>
<body style="overflow: hidden;">
    <ul class="ui nav nav-tabs">
        <li class="active"><a href="${ctx}/system/workflow/processList">待办任务</a></li>
		<li><a href="${ctx}/system/menu/menuForm">我的任务</a></li>
		<li><a href="${ctx}/system/workflow/processList">队列</a></li>
		<li><a href="${ctx}/system/workflow/processList">受邀</a></li>
		<li><a href="${ctx}/system/workflow/processList">已归档</a></li>
	</ul>
	<tags:message content="${message}" />
	<div class="wrapper">
	    <div class="wrapper-inner">
			<div class="top">
			    <form name="queryForm" id="queryForm">
					<div class="fl">
					  <strong>流程查询：</strong>
					  <div class="btn-menu">
					      <span class="ui-btn menu-btn btn" style='vertical-align: middle;'>
					         <strong>点击查询</strong><b></b>
					      </span>
					      <div class="dropdown-menu" style="width: 280px;">
					           <ul class="filter-list">
					             <li>
					                <label>菜单名称:</label> 
					                <input type="text" class="input-txt" name="menuName"/>
					             </li>
					           </ul>
						       <div class="btns"> 
					              <input class="ui-btn-href" type="button" onclick="query()" value="查询"/>
					              <input class="ui-btn" type="button" value="重置"/>
					           </div> 
					      </div>
					  </div>
					  <input type="button" class="ui-btn-href" value="&nbsp;刷&nbsp;新&nbsp;" onclick="query()">
					</div>
					<div class="fr">
					   <input type="button" class="ui-btn-href" onclick="deployP()" value="&nbsp;部署流程&nbsp;">
					   <input type="button" class="ui-btn"  onclick="optionsFmt4D()" value="&nbsp;删&nbsp;除&nbsp;">&nbsp;
					</div>
				</form>
			</div>
		</div>
    </div>
    <div id="dataGrid" class="autoGrid">
		<table class="table table-striped table-bordered ">
		   <thead>
				<tr>
					<th>ProcessDefinitionId</th>
					<th>DeploymentId</th>
					<th>名称</th>
					<th>KEY</th>
					<th>版本号</th>
					<th>XML</th>
					<th>图片</th>
					<th>部署时间</th>
					<th>是否挂起</th>
					<th>操作</th>
				</tr>
		   </thead>
		   <tbody>
		        <c:forEach items="${objects}" var="object">
					<c:set var="process" value="${object[0] }" />
					<c:set var="deployment" value="${object[1] }" />
					<tr>
						<td>${process.id }</td>
						<td>${process.deploymentId }</td>
						<td>${process.name }</td>
						<td>${process.key }</td>
						<td>${process.version }</td>
						<td><a target="_blank" href='${ctx}/system/workflow/resource/read?processDefinitionId=${process.id}&resourceType=xml'>${process.resourceName }</a></td>
						<td><a target="_blank" href='${ctx}/system/workflow/resource/read?processDefinitionId=${process.id}&resourceType=image'>${process.diagramResourceName }</a></td>
						<td>${deployment.deploymentTime }</td>
						<td>${process.suspended} |
							<c:if test="${process.suspended }">
								<a href="processdefinition/update/active/${process.id}">激活</a>
							</c:if>
							<c:if test="${!process.suspended }">
								<a href="processdefinition/update/suspend/${process.id}">挂起</a>
							</c:if>
						</td>
						<td>
	                        <a href='${ctx }/workflow/process/delete?deploymentId=${process.deploymentId}'>删除</a>
	                        <a href='${ctx }/workflow/process/convert-to-model/${process.id}'>转换为Model</a>
	                    </td>
					</tr>
				</c:forEach>
		   </tbody>
		</table>
	</div>
</body>
</html>
