/**
 *  工作流操作类
 */
$(function(){
	//事件注册
	$(document).bind('click',function(event){
		var src = event.target;
		if( src.nodeName == 'A' && $(src).hasClass('startup-process')) { //启动工作流
			showStartupProcessDialog.call(src);
		} else if( src.nodeName == 'A' && $(src).hasClass('handle-task')) {//任务处理
			showHandleTaskDialog.call(src);
		} else if( src.nodeName == 'A' && $(src).hasClass('process-trace')) {//流程图
			showGraphTrace.call(src);
		}
	});
});

//启动工作流
function showStartupProcessDialog() {
	var $ele = $(this);
	var html = '<div id="pDContent" class="ui-loading">正在读取表单……</div>';
	var title = '启动流程['+ $ele.attr('data-pName') + ']';
	Public.openWindow(html,title,400,null,{
		submit:function(v, h, f){
			if(v == 'ok') { //点击确定
				startupProcess.call
			}
		}, loaded:function(h){//读取表单
           $(".jbox-content", top.document).css("overflow-y","hidden");
           readFormFields.call(this, $ele.attr('data-pId'));
        }
	});
}
//处理待办任务 -- 需重写
function showHandleTaskDialog() {
	
}
//展示流程图
function showGraphTrace(options){
	
	var _defaults = {
	    srcEle: this,
	    pid: $(this).attr('data-pid')
	};
	
	var opts = $.extend(true, _defaults, options);
	
	// 获取图片资源
    var imageUrl = webRoot + "/system/workflow/resource/read-pi?pid=" + opts.pid + "&type=image";
    $.getJSON( webRoot + '/system/workflow/process/trace?pid=' + opts.pid, function(infos) {
        
    	var positionHtml = "";
    	// 生成图片
        var varsArray = new Array();
        $.each(infos, function(i, v) {
            var $positionDiv = $('<div/>', {
                'class': 'activity-attr'
            }).css({
                position: 'absolute',
                left: (v.x - 1),
                top: (v.y - 1),
                width: (v.width - 2),
                height: (v.height - 2),
                backgroundColor: 'black',
                opacity: 0//,
                //zIndex: $.fn.qtip.zindex - 1
            });
            
            //节点边框
            var $border = $('<div/>', {
                'class': 'activity-attr-border'
            }).css({
                position: 'absolute',
                left: (v.x - 1),
                top: (v.y - 1),
                width: (v.width - 4),
                height: (v.height - 3)//,
                //zIndex: $.fn.qtip.zindex - 2
            });
            if (v.currentActiviti) {
                $border.addClass('ui-corner-all').css({
                    border: '3px solid red'
                });
            }
            positionHtml += $positionDiv.outerHTML() + $border.outerHTML();
            varsArray[varsArray.length] = v.vars;
        });
        var wtDialog = $('<div/>', {
            id: 'workflowTraceDialog',
            title: '查看流程（按ESC键可以关闭）',
            html: "<div  style='position:relative;'><img src='" + imageUrl + "' style='position:absolute; left:0px; top:0px;' />" +
            "<div id='processImageBorder' style='position:absolute;'>" + positionHtml + "</div>" + "</div>"
        })
        // 设置每个节点的data
        wtDialog.find('.activity-attr').each(function(i, v) {
            $(this).data('vars', varsArray[i]);
        });
    	top.$.jBox.open(wtDialog.outerHTML(),"查看流程（按ESC键可以关闭）",750,400,{
    		submit: function () {}
    		,loaded:function(h){}
    	});
    });
}
//读取表单
function readFormFields(processDefinitionId) {
	var dialog = this;
	//表单提交form
	$("#pDContent").html("<form class='dynamic-form' method='post'><table class='dynamic-form-table'></table></form>");
	var $form = $('.dynamic-form');
	
	// 设置表单提交id
	$form.attr('action', webRoot + '/form/dynamic/start-process/' + processDefinitionId);
}
//发送启动流程请求
function startupProcess() {
	$('#pDContent .dynamic-form').submit();
}

