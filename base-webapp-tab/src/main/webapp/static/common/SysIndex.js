﻿/**
 *  加载菜单
 */
var tab = {};
$(function(){
	//Tab 的大小
	setTabLayout();
	
	//加载菜单
	userMenu('');
	
	//tab初始化
	initTab();
	
	//add 首页
	tab = $("#page-tab").ligerGetTabManager();
	tab.addTabItem({tabid: 'index', text: '首页', url: webRoot + '/admin/homePage', showClose: false});
    
	//IE 版本
	if($.browser.msie && parseInt($.browser.version) < 8) {
		var Oldbrowser = {
			msie: $.browser.msie,
			version: parseInt($.browser.version),
			init: function(){
				if($.cookie('Oldbrowser') === 'onlyOne') {
					return;
				};
				this.addDom();
			},
			addDom: function() {
				var html = $('<div id="browser">您使用的浏览器版本过低，影响网页性能，建议您换用<a href="http://www.google.cn/chrome/intl/zh-CN/landing_chrome.html" target="_blank">谷歌</a>、<a href="http://download.microsoft.com/download/4/C/A/4CA9248C-C09D-43D3-B627-76B0F6EBCD5E/IE9-Windows7-x86-chs.exe" target="_blank">IE9</a>、或<a href=http://firefox.com.cn/"  target="_blank">火狐浏览器</a>，以便更好的使用！<a id="bClose" title="关闭">x</a></div>').insertBefore('#header').slideDown(500);	this._colse();
			},
			_colse: function() {
				$('#bClose').click(function(){
					$('#browser').remove();
					if(Oldbrowser.version === 7) {
						$.cookie('Oldbrowser', 'onlyOne', {expires: 1000});
					}
				});
			}
		};
		Oldbrowser.init();
	};
	//快捷菜单事件
	$(document).on('click','#indexQc',function(){
		tab.selectTabItem('index');
		tab.reload('index');
	});
	//个人信息
	$(document).on('click','#persionInfo',function(){
		var userId = $(this).attr('data-id');
		var tabid = 'persionInfo';
		if(tab.isTabItemExist(tabid)){
			tab.selectTabItem(tabid);
			tab.reload(tabid);
		} else {
			tab.addTabItem({tabid: tabid, text: '个人信息', url: webRoot + '/admin/system/user/selfInfo?id='+userId, showClose: true});
		}
	});
	//修改密码
	$(document).on('click','#mdPassword',function(){
		var tabid = 'mdPassword';
		if(tab.isTabItemExist(tabid)){
			tab.selectTabItem(tabid);
			tab.reload(tabid);
		} else {
			tab.addTabItem({tabid: tabid, text: '修改密码', url: webRoot + '/admin/homePage', showClose: true});
		}
	});
	//tmt-home事件
	$(document).on('click','#tmt-home',function(){
		$('#sidebar').slideToggle();
		$('#content').toggleClass("content-max-all");
	});
	//top menu 事件
	$(document).on('click','#topMenu .menu',function(){
		var that = $(this);
		var id = that.data('id'),type = that.data('type'), href = that.data('href'),text = that.text();
		var tabid = id;
		if(!!type&& type == 2&& !!href){
			if(tab.isTabItemExist(tabid)){
				tab.selectTabItem(tabid);
				tab.reload(tabid);
			} else {
				tab.addTabItem({tabid: tabid, text: text, url: webRoot + href, showClose: true});
			}
		} else {
			userMenu(that.data('id'));
		}
		$('#topMenu').children('li').removeClass('active');
		that.parent().addClass('active');
	});
	Public.click_event = $.fn.tap ? "tap" : "click";
});
/**
 *  加载菜单
 */
function userMenu(parentId) {
	$("div#sidebar[href]").each(function(index,item){
		var that = $(item);
		var url = that.attr('href');
		    url = ((url.indexOf("?")>-1)?(url+"&_"):(url+"?_")) +"timeid=" + Math.random();
		var param  = "&id=" + parentId;
		    param += "&screenSize=" + $("#content").height();
		if( !!url ) {
			that.load( url + param ,function(){
				//tab事件
				Public.pageTab();
				//菜单事件
				Public.handle_side_menu($);
				//展开第一个菜单,如果第一个菜单是目录
				if( that.find('ul>li:first>a').eq(0).hasClass('dropdown-toggle') ) {
					that.find('ul>li:first').eq(0).toggleClass('active open');
				}
			});
		}
	});
}
/**
 * 加载Tab
 */
function initTab(){
	$('#page-tab').ligerTab({
		height: '100%', 
		changeHeightOnResize : true,
		onBeforeAddTabItem : function(tabid){
			
		},
		onAfterSelectTabItem : function(tabid){
			
		},
		onBeforeRemoveTabItem : function(tabid){
			
		},
		onAfterLeaveTabItem: function(tabid){
			
		}
	});
}
/**
 * Tab 的大小
 */
function setTabLayout(){
	$('#section').css({height:($(window).outerHeight() - 45)});
	$('#content').css({height:($('#section').outerHeight())});
	$('#content-body').css({height:($('#content').height())});
}
//
Public.handle_side_menu = function(a) {
	a("#menu-toggler").on(Public.click_event, function() {
		a("#sidebar").toggleClass("display");
		a(this).toggleClass("display");
		return false;
	});
	var c = a("#sidebar").hasClass("menu-min");
	a("#sidebar-collapse").on(Public.click_event, function() {
		c = a("#sidebar").hasClass("menu-min");
		Public.settings.sidebar_collapsed(!c);
	});
	a(".nav-list").on(
		Public.click_event,
		function(h) {
			var g = a(h.target).closest("a");
			if (!g || g.length == 0) {
				return
			}
			c = a("#sidebar").hasClass("menu-min");
			if (!g.hasClass("dropdown-toggle")) {//菜单项不执行连接，但执行添加样式的事件
				if (c && Public.click_event == "tap"
						&& g.get(0).parentNode.parentNode == this) {
					var i = g.find(".menu-text").get(0);
					if (h.target != i && !a.contains(i, h.target)) {
						return false;
					}
				}
				//添加样式的事件，先清除所有的active
				a(".nav-list").find(".active").each(function(index, item){
					$(item).removeClass("active");
				});
				a(".nav-list").find(".open").each(function(index, item){
					$(item).addClass("active");
				});
				g.parent().addClass('active');
				return;
			}
			var f = g.next().get(0);
			if (!a(f).is(":visible")) {
				var d = a(f.parentNode).closest("ul");
				if (c && d.hasClass("nav-list")) {
					return;
				}
				d.find("> .open > .submenu").each(
					function() {
						if (this != f) {
							a(this).slideUp(200).parent().removeClass("open");
						}
		            }
			    );
			}
			if (c && a(f.parentNode.parentNode).hasClass("nav-list")) {
				return false;
			}
			a(f).slideToggle(200).parent().toggleClass("open");
			return false;
		});
};
