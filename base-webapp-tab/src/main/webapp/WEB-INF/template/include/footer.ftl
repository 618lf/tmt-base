<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="author" content="http://thinkgem.iteye.com"/>
<meta http-equiv="X-UA-Compatible" content="IE=7,IE=9,IE=10" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<script src="${base}/static/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
<link href="${base}/static/modules/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<script src="${base}/static/modules/bootstrap/3.2.0/js/bootstrap.js" type="text/javascript"></script>
<link href="${base}/static/modules/cms/front/themes/basic/style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
</script>
</head>
<body>
    <footer class="footer">
        <div class="footer_nav"><a href="" target="_blank">公共留言</a> | <a href="" target="_blank">全站搜索</a> | <a href="" target="_blank">站点地图</a> | <a href="mailto:thinkgem@163.com">技术支持</a> | <a href="" target="_blank">后台管理</a></div>
		<div class="pull-right"></div><div class="copyright"></div>
    </footer>
</body>
</html>