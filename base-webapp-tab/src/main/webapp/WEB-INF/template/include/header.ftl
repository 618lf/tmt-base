<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="author" content="http://thinkgem.iteye.com"/>
<meta http-equiv="X-UA-Compatible" content="IE=7,IE=9,IE=10" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<script src="${base}/static/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
<link href="${base}/static/modules/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<script src="${base}/static/modules/bootstrap/3.2.0/js/bootstrap.js" type="text/javascript"></script>
<link href="${base}/static/modules/cms/front/themes/basic/style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
</script>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
         <div class="container-fluid">
            <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">豆瓜之家</a>
		    </div>
		    <div class="collapse navbar-collapse">
		        <ul class="nav navbar-nav">
		            <li [#if (rootCategory == null)] class="active" [/#if]><a href="${frontBase}"><span>首页</span></a></li>
			    	[@category_root_list count = 3]
			    	    [#list categoryRoots as categoryRoot]
							<li [#if (categoryRoot.id==rootCategory.id)] class="active" [/#if]>
								<a href="${frontBase}${categoryRoot.path}" target="_self">${categoryRoot.name}</a>
							</li>
						[/#list]
			    	[/@category_root_list]
			    	<li class="dropdown">
			    	    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="站点">站点<span class="caret"></span></a>
						<ul class="dropdown-menu">
						  
						</ul>
					</li>
					<li id="themeSwitch" class="dropdown">
				       	<a class="dropdown-toggle" data-toggle="dropdown" href="#" title="主题切换">主题切换<span class="caret"></span></a>
					    <ul class="dropdown-menu">
					    </ul>
				    </li>
		        </ul>
	            <form class="navbar-form navbar-left" action="" method="get">
			        <input type="text" class="form-control" name="q" maxlength="20" style="width: 200px;" placeholder="全站搜索..." value=""/>
			    </form>
			    <ul class="nav navbar-nav navbar-right">
			        <li><a href="#">登录</a></li>
			        <li><a href="#">注册</a></li>
			    </ul>
		    </div>
         </div>
    </nav>
</body>
</html>