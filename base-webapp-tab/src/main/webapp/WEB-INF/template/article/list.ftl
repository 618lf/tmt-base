<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="author" content="http://thinkgem.iteye.com"/>
<meta http-equiv="X-UA-Compatible" content="IE=7,IE=9,IE=10" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<script src="${base}/static/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
<link href="${base}/static/modules/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<script src="${base}/static/modules/bootstrap/3.2.0/js/bootstrap.js" type="text/javascript"></script>
<link href="${base}/static/modules/cms/front/themes/basic/style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
</script>
</head>
<body>
    [#include "/include/header.ftl"/]
    <div class="container" id="content" style="margin-top: 70px;">
		  <div class="jumbotron">
	      <div class="container">
	        [@article_list count=1 orderBy="hits desc" weight="999"]
					  [#list articles as article]
					    <h1>${article.title}</h1>
			        <p>${abbreviate(article.content, 220, "...")}</p>
			        <p><a class="btn btn-primary btn-lg" href="${frontBase}${article.path}" target="_self">详情 »</a></p>
					  [/#list]
				  [/@article_list]
	      </div>
	    </div>
	    <div class="row">
	        <div class="col-md-3">
				<div class="list-group">
				  <a href="#" class="list-group-item disabled">热点分类</a>
				  [@category_children_list count = 10 categoryId=rootCategory.id]
					  [#list categoryChildren as category]
						<a href="${frontBase}${category.path}" target="_self" class="list-group-item">${category.name}</a>
					  [/#list]
				  [/@category_children_list]
				</div>
				
				<div class="list-group">
				  <a href="#" class="list-group-item disabled">热点文章</a>
				  [@article_list count=10 orderBy="hits desc"]
					  [#list articles as article]
						<a href="${frontBase}${article.path}" title="${article.title}" target="_self" class="list-group-item">${abbreviate(article.title, 80, "...")}</a>
					  [/#list]
				  [/@article_list]
				</div>
				
			</div>
			<div class="col-md-9">
				<ol class="breadcrumb">
				  <li><a href="${frontBase}">首页</a></li>
				  [@category_parent_list categoryId = category.id]
						[#list categoryParents as category]
							<li><a href="${frontBase}${category.path}">${category.name}</a></li>
						[/#list]
					[/@category_parent_list]
				  <li class="active">${category.name}</li>
				</ol>
				<div class="result">
				   [#if (page.data?size>0)]
				      <ul>
				          [#list page.data as article]
				              <li[#if !article_has_next] class="last"[/#if]>
												<a href="${frontBase}${article.path}" title="${article.title}">${abbreviate(article.title, 80, "...")}</a>
												<span title="${article.createDate?string("yyyy-MM-dd HH:mm:ss")}">${article.createDate?string("yyyy-MM-dd HH:mm:ss")}</span>
												<p>${abbreviate(article.content, 220, "...")}</p>
											</li>
				          [/#list]
				      </ul>
				   [#else]
						  
					 [/#if]
				</div>
				${page.simplePagination}
			</div>
	    </div>
    </div>
    [#include "/include/footer.ftl"/]
</body>
</html>