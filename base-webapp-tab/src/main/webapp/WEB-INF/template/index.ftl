<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="author" content="http://thinkgem.iteye.com"/>
<meta http-equiv="X-UA-Compatible" content="IE=7,IE=9,IE=10" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<script src="${base}/static/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
<link href="${base}/static/modules/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<script src="${base}/static/modules/bootstrap/3.2.0/js/bootstrap.js" type="text/javascript"></script>
<link href="${base}/static/modules/cms/front/themes/basic/style.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
</script>
</head>
<body>
    [#include "/include/header.ftl"/]
    <div class="container" id="content" style="margin-top: 70px;">
		<div class="jumbotron">
	      <div class="container">
	        <h1>Hello, world!</h1>
	        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
	        <p><a class="btn btn-primary btn-lg">Learn more »</a></p>
	      </div>
	    </div>
	    <div class="row">
	        <div class="col-md-3">
				<div class="list-group">
				  <a href="#" class="list-group-item disabled">热点分类</a>
				  [@category_root_list count = 3]
					  [#list categoryRoots as category]
						<a href="${frontBase}${category.path}" target="_blank" class="list-group-item">${category.name}</a>
					  [/#list]
				  [/@category_root_list]
				</div>
				
				<div class="list-group">
				  <a href="#" class="list-group-item disabled">热点文章</a>
				  <a href="#" class="list-group-item">精华文章 <span class="badge pull-right">42</span></a>
				  <a href="#" class="list-group-item">未解决问题</a>
				  <a href="#" class="list-group-item">商城动态</a>
				  <a href="#" class="list-group-item">用户评论</a>
				</div>
			</div>
			<div class="col-md-9">
				<ol class="breadcrumb">
				  <li><a href="${frontBase}">首页</a></li>
				  <li><a href="#">精华文章</a></li>
				  <li class="active">商城动态</li>
				</ol>
				<div class="result">
				   <ul>
						<li>
							<a href="/article/content/201306/27/1.html" title="五月靓丽女人节 呵护自己">五月靓丽女人节 呵护自己</a>
							<span title="2013-06-13 00:01:22">2013-06-13</span>
							<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 爱美的你是否早已按捺不住？五一过后就是热辣的夏天，我们为女性朋友们带来涵盖多种品牌的缤纷大促销，从鞋包到服装一应俱全，全面满足你的购物欲。4月27日至4月30日三天内，光临女人节，就有三重特惠供你选择，满199元减60元、满299...</p>
						</li>
						<li>
							<a href="/article/content/201306/28/1.html" title="电子商务未来是否需要“移动”">电子商务未来是否需要“移动”</a>
							<span title="2013-06-13 00:01:23">2013-06-13</span>
							<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2009年中，苹果公司市值超越谷歌；2010年5月，超越微软成为全球市值最高的科技公司；2012年8月12日，苹果市值超越石油巨头埃克森美孚，登顶全球市值榜首。一次次不断超越为我们展现的不仅是苹果公司活力与激情、动荡与辉煌相互交织的...</p>
						</li>
				   </ul>
				</div>
				<ul class="pagination">
				  <li class="disabled"><a href="#"><<</a></li>
				  <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
				  <li class=""><a href="#">2 <span class="sr-only">(current)</span></a></li>
				  <li class=""><a href="#">3 <span class="sr-only">(current)</span></a></li>
				  <li class=""><a href="#">4 <span class="sr-only">(current)</span></a></li>
				  <li class=""><a href="#">5 <span class="sr-only">(current)</span></a></li>
				  <li class=""><a href="#">6 <span class="sr-only">(current)</span></a></li>
				  <li><a href="#">>></a></li>
				</ul>
			</div>
	    </div>
    </div>
    [#include "/include/footer.ftl"/]
</body>
</html>