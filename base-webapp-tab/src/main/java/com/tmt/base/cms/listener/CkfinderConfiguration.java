package com.tmt.base.cms.listener;

import javax.servlet.ServletConfig;

import com.ckfinder.connector.configuration.Configuration;
import com.tmt.base.common.config.Globals;

/**
 * ckfinder监听器
 * @author liFeng
 * 2014年6月24日
 */
public class CkfinderConfiguration extends Configuration {

	private String path = "";
	
	public CkfinderConfiguration(ServletConfig servletConfig) {
		super(servletConfig);
		path = servletConfig.getServletContext().getContextPath();
	}
	
	public void init() throws Exception {
		super.init();
		String files = Globals.getConfig("ck.userfiles");
		if (files.contains("http://")){
			this.baseURL = files;
		}else {
			this.baseURL = path + "/" + files + "/";
		}
		this.baseDir = Globals.getConfig("ck.baseDir");
	}

}
