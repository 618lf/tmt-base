package com.tmt.base.cms.entity;

import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.system.entity.BaseTreeEntity;
import com.tmt.base.system.entity.Office;
import com.tmt.base.system.utils.DictUtils;

/**
 * 栏目
 * @author liFeng
 * 2014年7月5日
 */
public class Category extends BaseTreeEntity<String> {
   
	private static final long serialVersionUID = 2951276261545359351L;
	
	private String siteId;// 归属站点
    private String officeId;// 归属部门
    private String parentId;// 父级菜单
    private String module;// 栏目模型（article：文章；picture：图片；download：下载；link：链接；special：专题）
    private String name;// 栏目名称
    private String image;// 栏目图片
    private String href;// 链接
    private String target;// 目标（ _blank、_self、_parent、_top）
    private String keywords;// 关键字，填写有助于搜索引擎优化
    private Integer sort;// 排序（升序）
    private String inMenu;// 是否在导航中显示（1：显示；0：不显示）
    private String inList;// 是否在分类页中显示列表（1：显示；0：不显示）
    private String showModes;// 展现方式（0:有子栏目显示栏目列表，无子栏目显示内容列表;1：首栏目内容列表；2：栏目第一条内容）
    private String allowComment;// 是否允许评论
    private String isAudit;// 是否需要审核
    private String customListView;// 自定义列表视图
    private String customContentView;// 自定义内容视图
    private String viewConfig;	// 视图参数
    private String siteName;
    private String officeName;
    private String parentName;
    private Site site;
    private Office office;
    
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getOfficeName() {
		return officeName;
	}
	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public Site getSite() {
		return site;
	}
	public void setSite(Site site) {
		this.site = site;
	}
	public Office getOffice() {
		return office;
	}
	public void setOffice(Office office) {
		this.office = office;
	}
	public String getViewConfig() {
		return viewConfig;
	}
	public void setViewConfig(String viewConfig) {
		this.viewConfig = viewConfig;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getOfficeId() {
		return officeId;
	}
	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getInMenu() {
		return inMenu;
	}
	public void setInMenu(String inMenu) {
		this.inMenu = inMenu;
	}
	public String getInList() {
		return inList;
	}
	public void setInList(String inList) {
		this.inList = inList;
	}
	public String getShowModes() {
		return showModes;
	}
	public void setShowModes(String showModes) {
		this.showModes = showModes;
	}
	public String getAllowComment() {
		return allowComment;
	}
	public void setAllowComment(String allowComment) {
		this.allowComment = allowComment;
	}
	public String getIsAudit() {
		return isAudit;
	}
	public void setIsAudit(String isAudit) {
		this.isAudit = isAudit;
	}
	public String getCustomListView() {
		return customListView;
	}
	public void setCustomListView(String customListView) {
		this.customListView = customListView;
	}
	public String getCustomContentView() {
		return customContentView;
	}
	public void setCustomContentView(String customContentView) {
		this.customContentView = customContentView;
	}
	
	public void fillByParent(Category parent){
		int level = parent == null?-1:parent.getLevel();
		String parentIds = parent == null?IdGen.INVALID_ID:(parent.getParentIds() + this.getParentId());
		this.setLevel(level + 1);
		this.setParentIds(parentIds + IDS_SEPARATE);
	}
	
	/**
	 * 数据字典项
	 */
	public String getModuleLabel() {
		return DictUtils.getDictLabel(this.getModule(), "SYS_CONFIG_CMS_MODULE", "公共模型");
	}
	
	/**
	 * 数据字典项
	 */
	public String getInMenuLabel() {
		return DictUtils.getDictLabel(this.getInMenu(), "SYS_CONFIG_CMS_SHOW_HIDE", "隐藏");
	}
	
	/**
	 * 数据字典项
	 */
	public String getInListLabel() {
		return DictUtils.getDictLabel(this.getInList(), "SYS_CONFIG_CMS_SHOW_HIDE", "隐藏");
	}
	
	/**
	 * 数据字典项
	 */
	public String getShowModesLabel() {
		return DictUtils.getDictLabel(this.getShowModes(), "SYS_CONFIG_CMS_SHOW_MODES", "默认展现方式");
	}
	
	public String getPath() {
		 return "/article/list/" + getId() + ".html";
	}
}