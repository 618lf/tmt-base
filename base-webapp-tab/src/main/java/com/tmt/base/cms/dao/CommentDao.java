package com.tmt.base.cms.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.cms.entity.Comment;
import com.tmt.base.common.persistence.BaseIbatisDAO;

@Repository
public class CommentDao extends BaseIbatisDAO<Comment, String> {

	@Override
	protected String getNamespace() {
		return "CMS_COMMENT";
	}
}