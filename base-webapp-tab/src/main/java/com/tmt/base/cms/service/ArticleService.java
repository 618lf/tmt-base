package com.tmt.base.cms.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tmt.base.cms.dao.ArticleDao;
import com.tmt.base.cms.entity.Article;
import com.tmt.base.cms.entity.ArticleData;
import com.tmt.base.cms.entity.Category;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.StringUtil3;

@Service
public class ArticleService extends BaseService<Article, String>{

	@Autowired
	private ArticleDao articleDao;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ArticleDataService articleDataService;
	
	@Override
	protected BaseIbatisDAO<Article, String> getEntityDao() {
		return articleDao;
	}
	
	@Transactional
	public String save( Article article) {
		if (article.getArticleData().getContent()!= null){
			article.getArticleData().setContent(StringEscapeUtils.unescapeHtml4(
					article.getArticleData().getContent()));
		}
		// 如果没有审核权限，则将当前内容改为待审核状态
		if (!SecurityUtils.getSubject().isPermitted("cms:article:audit")){
			article.setDelFlag(Article.DEL_FLAG_AUDIT);
		}
		// 如果栏目不需要审核，则将该内容设为发布状态
		if (article.getCategoryId()!=null&&StringUtil3.isNotBlank(article.getCategoryId())){
			Category category = categoryService.get(article.getCategoryId());
			if (!Article.YES.equals(category.getIsAudit())){
				article.setDelFlag(Article.DEL_FLAG_NORMAL);
			}
		}
        if (StringUtil3.isNotBlank(article.getViewConfig())){
            article.setViewConfig(StringEscapeUtils.unescapeHtml4(article.getViewConfig()));
        }
        if(IdGen.isInvalidId(article.getId())) {
        	this.insert(article);
        	article.getArticleData().setArticleId(article.getId());
        	this.articleDataService.insert(article.getArticleData());
        } else {
        	this.update(article);
        	article.getArticleData().setArticleId(article.getId());
        	this.articleDataService.update(article.getArticleData());
        }
		return article.getId();
	}
	
	@Override
	public Article get(String id) {
		return this.queryForObject("findByPkWithData", id);
	}
	
	@Transactional
	public int delete(List<Article> articles){
		this.batchDelete(articles);
		List<ArticleData> datas = Lists.newArrayList();
		for( Article article: articles ){
			ArticleData newData = new ArticleData();
			newData.setArticleId(article.getId());
			datas.add(newData);
		}
		this.articleDataService.batchDelete(datas);
		return articles.size(); 
	}
	
	public List<Article> queryByCondition(Map<String,Object> param) {
		return this.queryForList("queryByMapParam", param);
	}
}
