package com.tmt.base.cms.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.cms.entity.ArticleData;
import com.tmt.base.common.persistence.BaseIbatisDAO;

@Repository
public class ArticleDataDao extends BaseIbatisDAO<ArticleData, String> {

	@Override
	protected String getNamespace() {
		return "CMS_ARTICLE_DATA";
	}
}
