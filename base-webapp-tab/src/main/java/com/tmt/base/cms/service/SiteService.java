package com.tmt.base.cms.service;

import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmt.base.cms.dao.SiteDao;
import com.tmt.base.cms.entity.Site;
import com.tmt.base.cms.utils.CmsUtils;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;

@Service
public class SiteService extends BaseService<Site, String>{

	@Autowired
	private SiteDao siteDao;
	
	@Override
	protected BaseIbatisDAO<Site, String> getEntityDao() {
		return siteDao;
	}
	
	@Transactional(readOnly = false)
	public void save(Site site) {
		if (site.getCopyright()!=null){
			site.setCopyright(StringEscapeUtils.unescapeHtml4(site.getCopyright()));
		}
		//是否是第一个站点
		if(IdGen.isInvalidId(site.getId())) {
			List<Site> sites = this.findAll();
			if(sites == null || sites.size() == 0 ) {
				site.setId(IdGen.ROOT_ID);
			}
			this.insert(site);
		} else {
			this.update(site);
		}
		CmsUtils.removeCache("site_"+site.getId());
		CmsUtils.removeCache("siteList");
	}
	
}
