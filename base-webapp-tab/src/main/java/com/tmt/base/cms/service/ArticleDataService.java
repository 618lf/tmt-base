package com.tmt.base.cms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.cms.dao.ArticleDataDao;
import com.tmt.base.cms.entity.ArticleData;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;

@Service
public class ArticleDataService extends BaseService<ArticleData, String>{

	@Autowired
	private ArticleDataDao articleDataDao;
	
	@Override
	protected BaseIbatisDAO<ArticleData, String> getEntityDao() {
		return articleDataDao;
	}
}
