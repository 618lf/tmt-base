/**
 * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.tmt.base.cms.utils;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.dom4j.io.SAXReader;
import org.springframework.core.io.ClassPathResource;

import com.google.common.collect.Maps;
import com.tmt.base.cms.entity.Site;
import com.tmt.base.cms.service.SiteService;
import com.tmt.base.common.utils.CacheUtils;
import com.tmt.base.common.utils.SpringContextHolder;

/**
 * 内容管理工具类
 * 
 * @author ThinkGem
 * @version 2013-5-29
 */
public class CmsUtils {
	
	//==============Other=================
	private static SiteService siteService = SpringContextHolder.getBean(SiteService.class);
    public static Site getSite( String siteId) {
    	return siteService.get(siteId);
    }
	//==============Template================
	private static final Map<String,Template> TEMPLATES = Maps.newHashMap();
	
	public static Template getTemplate(String templateId ) {
		return getTemplateById(templateId);
	}
	
	public static String getStaticPath( String templateId ){
		return getTemplateById(templateId).getStaticPath();
	}
	
	private static Template getTemplateById( String templateId ) {
		Template template = TEMPLATES.get(templateId);
		if(template == null){
			loadTemplates();
			template = TEMPLATES.get(templateId);
		}
		return template;
	}
	
	@SuppressWarnings("unchecked")
	private static void loadTemplates() {
		try {
			File localFile = new ClassPathResource("/cms.xml").getFile();
			org.dom4j.Document localDocument = new SAXReader().read(localFile);
			List<org.dom4j.Node> nodes = localDocument.selectNodes("/cms/template");
			for( org.dom4j.Node node: nodes ) {
				org.dom4j.Element localElement = (org.dom4j.Element)node;
				String id = localElement.attributeValue("id");
				String templatePath = localElement.attributeValue("templatePath");
				String staticPath = localElement.attributeValue("staticPath");
				TEMPLATES.put(id, new Template(id, templatePath, staticPath));
			}
		} catch (Exception localException) {
			localException.printStackTrace();
		}
	}
	
	public static class Template{
		
		private String id;
		private String templatePath;
		private String staticPath;
		
		Template(String id, String templatePath, String staticPath ) {
			this.id = id;
			this.templatePath = templatePath;
			this.staticPath = staticPath;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getTemplatePath() {
			return templatePath;
		}
		public void setTemplatePath(String templatePath) {
			this.templatePath = templatePath;
		}
		public String getStaticPath() {
			return staticPath;
		}
		public void setStaticPath(String staticPath) {
			this.staticPath = staticPath;
		}
	}
	
	//============== Cms Cache ==============
	private static final String CMS_CACHE = "cmsCache";
	
	public static Object getCache(String key) {
		return CacheUtils.get(CMS_CACHE, key);
	}

	public static void putCache(String key, Object value) {
		CacheUtils.put(CMS_CACHE, key, value);
	}

	public static void removeCache(String key) {
		CacheUtils.remove(CMS_CACHE, key);
	}
}