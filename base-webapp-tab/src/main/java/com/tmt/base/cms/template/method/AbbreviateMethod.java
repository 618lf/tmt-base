package com.tmt.base.cms.template.method;

import java.util.List;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Node;
import org.springframework.stereotype.Component;

import com.tmt.base.common.utils.StringUtil3;

import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

@Component("abbreviateMethod")
public class AbbreviateMethod implements TemplateMethodModelEx{

	//匹配中文的正则表达式
	private static final Pattern CHINESE_CHARS = Pattern.compile("[\\u4e00-\\u9fa5\\ufe30-\\uffa0]+$");
	
	@SuppressWarnings("rawtypes")
	@Override
	public Object exec(List arguments) throws TemplateModelException {
		if(arguments != null && !arguments.isEmpty() && arguments.get(0) != null
				&& StringUtil3.isNotEmpty(arguments.get(0).toString())) {
			Integer size = null;
			String str = null;
			if (arguments.size() == 2) {
				if (arguments.get(1) != null) {
					size = Integer.valueOf(arguments.get(1).toString());
				}
			}else if (arguments.size() > 2){
				if (arguments.get(1) != null) {
					size = Integer.valueOf(arguments.get(1).toString());
				} 
				if (arguments.get(2) != null) {
					str = arguments.get(2).toString();     
				}
			}
			return new SimpleScalar(limitStr(htmlToStr(arguments.get(0).toString()), size, str));	
		}
		return null;
	}
	
	//过滤出html的文本信息
	private String htmlToStr( String html) {
		org.jsoup.nodes.Document localDocument = Jsoup.parse(html);
		List<Node> nodes = localDocument.body().childNodes();
		StringBuffer localStringBuffer = new StringBuffer();
		if( nodes != null) {
			for( Node node: nodes ){
				if( node instanceof org.jsoup.nodes.Element ) {
					localStringBuffer.append(((org.jsoup.nodes.Element)node).text());
				}else if (node instanceof org.jsoup.nodes.TextNode) {
					localStringBuffer.append(((org.jsoup.nodes.TextNode)node).text());
				}
			}
		}
		return localStringBuffer.toString();
	}
	
	//长度
	private String limitStr(String paramString1, Integer paramInteger, String paramString2) {
		if (paramInteger != null) {
			int i = 0;
			int j = 0;
			while (i < paramString1.length()) {
				j = CHINESE_CHARS.matcher(String.valueOf(paramString1.charAt(i))).find() ? j + 2 : j + 1;
				if (j >= paramInteger.intValue())
					break;
				i++;
			}
			if (i < paramString1.length()) {
				if (paramString2 != null)
					return paramString1.substring(0, i + 1) + paramString2;
				return paramString1.substring(0, i + 1);
			}
			return paramString1;
		}
		if (paramString2 != null)
			return paramString1 + paramString2;
		return paramString1;
	}
}
