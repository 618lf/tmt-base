package com.tmt.base.cms.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.tmt.base.cms.dao.CategoryDao;
import com.tmt.base.cms.entity.Category;
import com.tmt.base.cms.entity.Site;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.system.entity.TreeVO;

@Service
public class CategoryService extends BaseService<Category, String>{

	public static final String CACHE_CATEGORY_LIST = "categoryList";
	
	@Autowired
	private CategoryDao categoryDao;
	
	@Override
	protected BaseIbatisDAO<Category, String> getEntityDao() {
		return categoryDao;
	}
	
	public List<Category> findByCondition(Map<String,Object> params) {
		return this.queryForList("findByCondition", params);
	}
	
	public List<Category> findByParent(Category parent){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("PARENT_ID",  parent.getId());
		return this.queryForList("findByCondition", params);
	}
	
	@Transactional
	public String save(Category category){
		category.setSiteId(Site.getCurrentSiteId());
		Category parent = this.get(category.getParentId());
		String oldParentIds = category.getParentIds();
		category.fillByParent(parent);
		if( IdGen.isInvalidId(category.getId()) ) {
		    this.insert(category);
		} else {
			this.update(category);
			List<Category> children = this.findByParent(category);
			for( Category e : children ) {
				e.setParentIds(e.getParentIds().replace(","+oldParentIds, "," + category.getParentIds()));
			}
			this.batchUpdate(children);
		}
		return category.getId();
	}
	
	public List<TreeVO> findTreeList(Map<String,Object> params) {
		return this.queryForGenericsList("findTreeList", params);
	}
	
	public int deleteCategoryCheck(Category category){
		return this.countByCondition("deleteCategoryCheck", category);
	}
	
	/**
	 * lEVEL = 1 的那层
	 * @return
	 */
	public List<Category> findMainNavList(){
		Map<String,Object> param = Maps.newHashMap();
		param.put("LEVEL", 1);
		return this.queryForList("findMainNavList", param);
	}
	
	/**
	 * 
	 * @param category
	 * @return
	 */
	public List<Category> findParents(Category category) {
		Map<String,Object> param = Maps.newHashMap();
		param.put("ID", category.getId());
		return this.queryForList("findParents", param);
	}
	
	public List<Category> findChildren(Category category) {
		Map<String,Object> param = Maps.newHashMap();
		param.put("PARENT_ID", category.getId());
		return this.queryForList("findChildren", param);
	}
	
	/**
	 * 查找层级为1的类别
	 * @param category
	 * @return
	 */
	public Category findNavCategory(Category category) {
		Map<String,Object> param = Maps.newHashMap();
		param.put("ID", category.getId());
		param.put("LEVEL", 1);
		return this.queryForObject("findNavCategory", param);
	}
}
