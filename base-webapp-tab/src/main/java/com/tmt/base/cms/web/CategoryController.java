package com.tmt.base.cms.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tmt.base.cms.entity.Category;
import com.tmt.base.cms.service.CategoryService;
import com.tmt.base.cms.service.FileTplService;
import com.tmt.base.cms.service.SiteService;
import com.tmt.base.cms.utils.CategoryUtils;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.entity.Office;
import com.tmt.base.system.entity.TreeVO;
import com.tmt.base.system.service.OfficeService;

/**
 * 栏目Controller
 * @author ThinkGem
 * @version 2013-4-21
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/category")
public class CategoryController extends BaseController {

	@Autowired
	private CategoryService categoryService;
	@Autowired
	private OfficeService officeService;
    @Autowired
   	private FileTplService fileTplService;
    @Autowired
   	private SiteService siteService;
	
    @RequestMapping(value = {"initList", ""})
	public String initList(Category category, Model model) {
		if(category != null && category.getId() != null && StringUtil3.isNotBlank(category.getId())) {
			model.addAttribute("id", category.getId());
		}
		return "/cms/CategoryList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Category category, Model model) {
		Map<String,Object> params = new HashMap<String,Object>();
		List<Category> categorys = this.categoryService.findByCondition(params);
		if( categorys != null) {
			for(Category categoryItem : categorys){
				categoryItem.setId(categoryItem.getId());
				categoryItem.setParent(categoryItem.getParentId());
				categoryItem.setLevel(categoryItem.getLevel());
				categoryItem.setExpanded(Boolean.TRUE);//默认全部展开
				categoryItem.setLoaded(Boolean.TRUE);
				categoryItem.setIsLeaf(Boolean.TRUE);
			}
		}
		categorys = CategoryUtils.sort(categorys);
		Page pageList = new Page();
		pageList.setData(categorys);
		return pageList;
	}
	
	@RequestMapping(value = "form")
	public String form(Category category, Model model) {
		if(category !=null && category.getId() != null ) {
			category = this.categoryService.get(category.getId());
		} else {
			category.setId(IdGen.INVALID_ID);
		}
		//父栏目
		if(!IdGen.isInvalidId(category.getParentId())) {
			Category parent = this.categoryService.get(category.getParentId());
			category.setParentId(parent.getId());
			category.setParentName(parent.getName());
		}
		//所属区域
		if(!IdGen.isInvalidId(category.getOfficeId())) {
			Office office = this.officeService.get(category.getOfficeId());
			category.setOfficeId(office.getId());
			category.setOfficeName(office.getName());
		}
		model.addAttribute("category", category);
		return "/cms/CategoryForm";
	}
	
	@RequestMapping(value = "save")
	public String save(Category category, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, category)){
			return form(category, model);
		}
		this.categoryService.save(category);
		addMessage(redirectAttributes, "保存栏目'" + category.getName() + "'成功");
		return "redirect:"+Globals.getAdminPath()+"/cms/category/";
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList, RedirectAttributes redirectAttributes) {
		List<Category> categorys = Lists.newArrayList();
		for( String id: idList) {
			Category category = new Category();
			category.setId(id);
			int iCount = this.categoryService.deleteCategoryCheck(category);
			if(iCount > 0) {
				return AjaxResult.error("选择的栏目中包含子栏目,不能删除");
			}
			//验证
			categorys.add(category);
		}
		this.categoryService.batchDelete(categorys);
		return AjaxResult.success();
	}

	/**
	 * 批量修改栏目排序
	 */
	@RequestMapping(value = "updateSort")
	public String updateSort(String[] ids, Integer[] sorts, RedirectAttributes redirectAttributes) {
    	
		return "redirect:"+Globals.getAdminPath()+"/cms/category/";
	}
	
	@ResponseBody
	@RequestMapping(value = "treeSelect")
	public List<Map<String, Object>> treeSelect(String extId, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<TreeVO> trees = this.categoryService.findTreeList(new HashMap<String,Object>());
		for (int i=0; i<trees.size(); i++){
			TreeVO e = trees.get(i);
			if (extId == null || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParent());
				map.put("name", e.getTreeName());
				map.put("module", e.getTreeType());
				mapList.add(map);
			}
		}
		return mapList;
	}
}
