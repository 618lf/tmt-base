package com.tmt.base.cms.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.cms.entity.GuestBook;
import com.tmt.base.common.persistence.BaseIbatisDAO;

@Repository
public class GuestBookDao extends BaseIbatisDAO<GuestBook, String> {

	@Override
	protected String getNamespace() {
		return "CMS_GUEST_BOOK";
	}
}