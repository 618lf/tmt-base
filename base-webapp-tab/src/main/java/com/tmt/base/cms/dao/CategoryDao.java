package com.tmt.base.cms.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.cms.entity.Category;
import com.tmt.base.common.persistence.BaseIbatisDAO;

@Repository
public class CategoryDao extends BaseIbatisDAO<Category, String> {

	@Override
	protected String getNamespace() {
		return "CMS_CATEGORY";
	}
}