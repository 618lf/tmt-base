package com.tmt.base.cms.entity;

import com.tmt.base.common.entity.IdEntity;

public class ArticleData extends IdEntity<String>{
   
	private static final long serialVersionUID = 620512063022157128L;
	
    private String copyfrom;// 来源
    private String relation;// 相关文章
    private String allowComment;// 是否允许评论
    private String content;// 内容
    private String articleId;//文章id
    
	public String getArticleId() {
		return articleId;
	}
	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}
	public String getCopyfrom() {
		return copyfrom;
	}
	public void setCopyfrom(String copyfrom) {
		this.copyfrom = copyfrom;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getAllowComment() {
		return allowComment;
	}
	public void setAllowComment(String allowComment) {
		this.allowComment = allowComment;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

}