package com.tmt.base.cms.template.directive;

import java.io.IOException;
import java.util.Map;

import com.tmt.base.common.utils.FreemarkerUtils;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

/**
 * 基础的指令
 * 
 * @author liFeng 2014年8月6日
 */
public abstract class BaseDirective implements TemplateDirectiveModel {

	private static final String useCache = "useCache";
	private static final String cacheRegion = "cacheRegion";
	private static final String id = "id";
	private static final String count = "count";

	protected boolean useCache(Environment paramEnvironment,
			Map<String, TemplateModel> paramMap) throws TemplateModelException {
		Boolean localBoolean = (Boolean) FreemarkerUtils.getParameter(useCache,
				Boolean.class, paramMap);
		return localBoolean != null ? localBoolean.booleanValue() : true;
	}

	protected String cacheRegion(Environment paramEnvironment,
			Map<String, TemplateModel> paramMap) throws TemplateModelException {
		String str = (String) FreemarkerUtils.getParameter(cacheRegion,
				String.class, paramMap);
		return str != null ? str : paramEnvironment.getTemplate().getName();
	}

	protected Long useCache(Map<String, TemplateModel> paramMap)
			throws TemplateModelException {
		return (Long) FreemarkerUtils.getParameter(id, Long.class, paramMap);
	}

	protected Integer cacheRegion(Map<String, TemplateModel> paramMap)
			throws TemplateModelException {
		return (Integer) FreemarkerUtils.getParameter(count, Integer.class,
				paramMap);
	}

	protected void useCache(String paramString, Object paramObject,
			Environment paramEnvironment, TemplateDirectiveBody paramTemplateDirectiveBody) throws TemplateException, IOException {
		TemplateModel localTemplateModel = FreemarkerUtils.getVariable(paramString, paramEnvironment);
		FreemarkerUtils.setVariable(paramString, paramObject, paramEnvironment);
		paramTemplateDirectiveBody.render(paramEnvironment.getOut());
		FreemarkerUtils.setVariable(paramString, localTemplateModel,paramEnvironment);
	}

}
