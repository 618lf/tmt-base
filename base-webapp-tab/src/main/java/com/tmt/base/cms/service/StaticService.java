package com.tmt.base.cms.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.google.common.collect.Maps;
import com.tmt.base.cms.entity.Article;
import com.tmt.base.cms.utils.CmsUtils;
import com.tmt.base.cms.utils.CmsUtils.Template;
import com.tmt.base.common.exception.BaseRuntimeException;

/**
 * Cms 静态化服务
 * 
 * @author liFeng 2014年8月2日
 */
@Service
public class StaticService implements ServletContextAware {
	
	protected static Logger logger = LoggerFactory.getLogger(StaticService.class);
	
	@Resource(name = "freeMarkerConfigurer")
	private FreeMarkerConfigurer freeMarkerConfigurer;
	@Autowired
	private ArticleService articleService;
	private ServletContext servletContext;

	public int build(String templatePath, String staticPath, Map<String, Object> model) {
		FileOutputStream localFileOutputStream = null;
		OutputStreamWriter localOutputStreamWriter = null;
		BufferedWriter localBufferedWriter = null;
		try {
			freemarker.template.Template localTemplate = this.freeMarkerConfigurer
					.getConfiguration().getTemplate(templatePath);
			File localFile1 = new File(
					this.servletContext.getRealPath(staticPath));
			File localFile2 = localFile1.getParentFile();
			if (!localFile2.exists()) {
				localFile2.mkdirs();
			}
			localFileOutputStream = new FileOutputStream(localFile1);
			localOutputStreamWriter = new OutputStreamWriter(
					localFileOutputStream, "UTF-8");
			localBufferedWriter = new BufferedWriter(localOutputStreamWriter);
			localTemplate.process(model, localBufferedWriter);
			localBufferedWriter.flush();
		} catch (Exception e) {
			logger.error("静态化执行异常：", e);
			throw new BaseRuntimeException("静态化执行异常");
		} finally {
			IOUtils.closeQuietly(localBufferedWriter);
			IOUtils.closeQuietly(localOutputStreamWriter);
			IOUtils.closeQuietly(localFileOutputStream);
		}
		return 1;
	}

	@Transactional(readOnly = true)
	public int build(String templatePath, String staticPath) {
		return build(templatePath, staticPath, null);
	}

	public int build(Article article) {
        this.delete(article);
        Template template = CmsUtils.getTemplate("articleContent");
        Map<String,Object> modle = Maps.newHashMap();
        modle.put("article", article);
        int i = 0;
		for (int j = 1; j <= article.getTotalPages(); j++) {
			article.setPageNumber(Integer.valueOf(j));
			i += build(template.getTemplatePath(), article.getPath(), modle);
		}
		article.setPageNumber(null);
        return i; 		
	}
	
	public int buildIndex() {
		Template template = CmsUtils.getTemplate("index");
		return build(template.getTemplatePath(), template.getStaticPath());
	}
	
	@Transactional(readOnly = true)
	public int buildAll() {
        List<Article> articles = this.articleService.findAll();
        int i = 0;
        for(Article article: articles) {
        	 i += build(article);
        }
        buildIndex();
        return i;
	}

	public int delete(String staticPath) {
		Assert.hasText(staticPath);
		File localFile = new File(this.servletContext.getRealPath(staticPath));
		if (localFile.exists()) {
			localFile.delete();
			return 1;
		}
		return 0;
	}

	@Transactional(readOnly = true)
	public int delete(Article article) {
		Assert.notNull(article);
		int i = 0;
		for (int j = 1; j <= article.getTotalPages() + 1000; j++) {
			article.setPageNumber(Integer.valueOf(j));
			int k = delete(article.getPath());
			if (k < 1)break;
			i += k;
		}
		article.setPageNumber(null);
		return i;
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
}
