package com.tmt.base.cms.web;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tmt.base.cms.entity.Category;
import com.tmt.base.cms.service.CategoryService;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.TreeVO;

/**
 * 内容管理
 * @author liFeng
 * 2014年7月7日
 */
@Controller
@RequestMapping(value = "${adminPath}/cms")
public class CmsController extends BaseController {

	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(value = "")
	public String treeFrame(Model model){
		model.addAttribute("treeUrl", "/cms/categorTree");
		return "/system/TreeFrame";
	}
	
	@RequestMapping(value = "categorTree")
	public String tree(Model model) {
		List<TreeVO> trees = this.categoryService.findTreeList(new HashMap<String,Object>());
		model.addAttribute("items", trees);
		model.addAttribute("checked", Boolean.FALSE);
		model.addAttribute("treeUrl", "/cms/redirect?id=");
		return "/cms/CmsTree";
	}
	
	/**
	 * 根据栏目类型的不同重定向到不同的url
	 * @param category
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = "redirect")
	public String redirect(Category category, Model model, RedirectAttributes redirectAttributes) {
		if(category != null && !IdGen.isInvalidId(category.getId())) {
			category = this.categoryService.get(category.getId());
			if("article".equals(category.getModule())){
				return "redirect:"+Globals.getAdminPath()+"/cms/article/initList?categoryid="+category.getId();
			}else if("link".equals(category.getModule())){
				return "redirect:"+Globals.getAdminPath()+"/cms/link/initList?categoryid="+category.getId();
			} 
		}
		model.addAttribute("msg", "xxxx");
		return "redirect:"+Globals.getAdminPath()+"/cms/none/";
	}
	
	@RequestMapping(value = "none")
	public String none() {
		return "/cms/CmsNone";
	}
	
}
