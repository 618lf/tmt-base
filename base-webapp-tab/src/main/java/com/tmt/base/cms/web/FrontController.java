package com.tmt.base.cms.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tmt.base.cms.entity.Article;
import com.tmt.base.cms.entity.Category;
import com.tmt.base.cms.entity.Site;
import com.tmt.base.cms.service.ArticleService;
import com.tmt.base.cms.service.CategoryService;
import com.tmt.base.cms.utils.CmsUtils;
import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.web.BaseController;

/**
 * 网站首页
 * @author liFeng
 * 2014年7月26日
 */
@Controller
@RequestMapping(value = "${frontPath}")
public class FrontController extends BaseController{

	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ArticleService articleService;
	
	/**
	 * 网站首页
	 */
	@RequestMapping
	public String index(Model model) {
		Site site = CmsUtils.getSite(Site.defaultSiteId());
		model.addAttribute("site", site);
		model.addAttribute("isIndex", true);
		return "index";
	}
	
	/**
	 * 文章列表
	 * 第一层级
	 * 第二层级
	 * @param id
	 * @param pageNumber
	 * @param model
	 * @return
	 */
	@RequestMapping(value={"/article/list/{id}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	public String articleList(@PathVariable String id, Integer pageNumber, Model model) {
		Category category = this.categoryService.get(id);
		if(category == null) {
			throw new BaseRuntimeException("栏目不存在");
		}
		Category rootCategory = null;//level = 1 那一层
		if( category.getLevel() > 1) {
			rootCategory = this.categoryService.findNavCategory(category);
		} else if( category.getLevel() == 1){
			rootCategory = category;
		}
		QueryCondition qc =  new QueryCondition();
		Criteria c = qc.createCriteria();
		if(category.getLevel() == 1) {//一层菜单，查询所有的子栏目的
			c.andConditionSql("C.PARENT_IDS LIKE CONCAT(CONCAT('%,',"+category.getId()+"),',%')");
		} else {
			c.andEqualTo("C.ID", "ID", category.getId());
		}
		PageParameters param = new PageParameters();
		param.setPageIndex(pageNumber==null?1:pageNumber);
		Page page = this.articleService.queryForPage(qc, param);
		model.addAttribute("rootCategory", rootCategory);
		model.addAttribute("category", category);
		model.addAttribute("page", page);
		return "article/list";
	}
	
	/**
	 * 文章内容
	 * @return
	 */
	@RequestMapping(value={"/article/content/*/{id}/{pageNumber}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	public String article(@PathVariable String id, @PathVariable Integer pageNumber, Model model){
		Article article = this.articleService.get(id);
		if( article == null) {
			throw new BaseRuntimeException("文章不存在");
		}
		Category category = this.categoryService.get(article.getCategoryId());
		if(category == null) {
			throw new BaseRuntimeException("栏目不存在");
		}
		Category rootCategory = null;//level = 1 那一层
		if( category.getLevel() > 1) {
			rootCategory = this.categoryService.findNavCategory(category);
		} else if( category.getLevel() == 1){
			rootCategory = category;
		}
		model.addAttribute("rootCategory", rootCategory);
		model.addAttribute("category", category);
		model.addAttribute("article", article);
		return "article/content";
	}
}
