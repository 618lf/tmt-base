package com.tmt.base.cms.entity;

import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.entity.BaseEntity;
import com.tmt.base.system.utils.UserUtils;

public class Site extends BaseEntity<String>{
    
	private static final long serialVersionUID = -1983279436983709244L;
	private String name; // 站点名称
    private String title;// 站点标题
    private String logo;// 站点logo
    private String domain;// 描述，填写有助于搜索引擎优化
    private String keywords;// 关键字，填写有助于搜索引擎优化
    private String theme;// 主题
    private String copyright;// 版权信息
    private String customIndexView;// 自定义首页视图文件
    
    public Site(){}
    public Site( String siteId ){
    	this.id = siteId;
    }
	public String getCopyright() {
		return copyright;
	}
	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public String getCustomIndexView() {
		return customIndexView;
	}
	public void setCustomIndexView(String customIndexView) {
		this.customIndexView = customIndexView;
	}
	
	/**
	 * 获取当前站点ID
	 * @return
	 */
	public static String getCurrentSiteId() {
		String siteId = (String)UserUtils.getCache("siteId");
		return StringUtil3.isNotBlank(siteId)?siteId:defaultSiteId();
	}
	
	/**
	 * 默认的站点ID
	 * @return
	 */
	public static String defaultSiteId(){
		return IdGen.ROOT_ID;
	}
}