/**
 * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.tmt.base.cms.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.tmt.base.cms.entity.Article;
import com.tmt.base.cms.service.ArticleService;
import com.tmt.base.cms.service.CategoryService;
import com.tmt.base.cms.service.FileTplService;
import com.tmt.base.cms.service.SiteService;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;

/**
 * 文章Controller
 * @author ThinkGem
 * @version 2013-3-23
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/article")
public class ArticleController extends BaseController {

	@Autowired
	private ArticleService articleService;
	@Autowired
	private CategoryService categoryService;
    @Autowired
   	private FileTplService fileTplService;
    @Autowired
   	private SiteService siteService;
	
    @RequestMapping(value = {"initList", ""})
	public String initList(Article article, Model model) {
		return "/cms/ArticleList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Article article, Model model, Page page) {
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		page = this.articleService.queryForPage(qc, param);
		return page;
	} 

	@RequestMapping(value = "form")
	public String form(Article article, Model model) {
		if( !IdGen.isInvalidId(article.getId()) ) {
			article = this.articleService.get(article.getId());
		}
		model.addAttribute("article", article);
		return "/cms/ArticleForm";
	}

	@RequestMapping(value = "save")
	public String save(Article article, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, article)){
			return form(article, model);
		}
		this.articleService.save(article);
		addMessage(redirectAttributes, "保存文章'" + StringUtil3.abbr(article.getTitle(),50) + "'成功");
		return "redirect:"+Globals.getAdminPath()+"/cms/article/initList/";
	}
	
	@ResponseBody
	@RequestMapping(value = "delete")
	public AjaxResult delete(String[] idList, Model model, RedirectAttributes redirectAttributes) {
		List<Article> articles = Lists.newArrayList();
		for( String id: idList) {
			 Article temp = new Article();
			 temp.setId(id);
			 articles.add(temp);
		}
		this.articleService.delete(articles);
		return AjaxResult.success();
	}
}
