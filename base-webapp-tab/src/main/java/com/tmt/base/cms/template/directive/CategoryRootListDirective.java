package com.tmt.base.cms.template.directive;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tmt.base.cms.entity.Category;
import com.tmt.base.cms.service.CategoryService;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 第一级栏目
 * @author liFeng
 * 2014年8月7日
 */
@Component("categoryRootListDirective")
public class CategoryRootListDirective extends BaseDirective{

	private final static String USE_CACHE = "categoryRoots";
	
	@Autowired
	private CategoryService categoryService;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		List<Category> categorys = this.categoryService.findMainNavList();
		this.useCache(USE_CACHE, categorys, env, body);
	}
}
