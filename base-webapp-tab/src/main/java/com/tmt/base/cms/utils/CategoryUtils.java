package com.tmt.base.cms.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.tmt.base.cms.entity.Category;

/**
 * 栏目帮助类
 * @author liFeng
 * 2014年7月5日
 */
public class CategoryUtils {
	//按级别存储
	public static Map<Integer,List<Category>> classifyByLevel(List<Category> categoryList){
		Map<Integer,List<Category>> menuMap = new HashMap<Integer,List<Category>>();
		for( Category menu: categoryList ) {
			if( !menuMap.containsKey(menu.getLevel()) ) {
				List<Category> menus = new ArrayList<Category>();
				menuMap.put(menu.getLevel(), menus);
			}
			menuMap.get(menu.getLevel()).add(menu);
		}
		return menuMap;
	}
	//排序 并放入 copyMenus
	public static List<Category> sort( List<Category> menus ) {
		List<Category> copyMenus = Lists.newArrayList();
		Map<Integer,List<Category>> menuMap = classifyByLevel(menus);
		int firstLevel = 1;
		List<Category> menuList = menuMap.get(firstLevel);
		if( menuList != null ) {
			for( Category menu: menuList ) {
				 copyMenus.add(menu);
				 List<Category> child = sort(menu,firstLevel+1,menuMap);
				 if( child != null && child.size() != 0 ) {
					 copyMenus.addAll(child);
					 menu.setIsLeaf(Boolean.FALSE);
				 }
			}
		}
		return copyMenus;
	}
	public static List<Category> sort( Category parent, int level,Map<Integer,List<Category>> menuMap ) {
		List<Category> copyMenus = Lists.newArrayList();
		List<Category> menuList = menuMap.get(level);
		if( menuList != null ) {
			for( Category menu: menuList ) {
				if(menu.getParentId().compareTo(parent.getId()) == 0) {
					 copyMenus.add(menu);
					 List<Category> child = sort(menu,level+1,menuMap);
					 if( child != null && child.size() != 0 ) {
						 copyMenus.addAll(child);
						 menu.setIsLeaf(Boolean.FALSE);
					 }
				}
			}
		}
		return copyMenus;
	}
}
