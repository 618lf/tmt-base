package com.tmt.base.cms.web;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.tmt.base.cms.entity.Site;
import com.tmt.base.cms.service.SiteService;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.persistence.QueryCondition.Criteria;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.utils.CookieUtils;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;
import com.tmt.base.system.utils.UserUtils;

@Controller
@RequestMapping(value = "${adminPath}/cms/site")
public class SiteController extends BaseController{

	@Autowired
	private SiteService siteService;
	
	@RequestMapping(value = {"initList", ""})
	public String initList(Site site, Model model) {
		if(site != null && site.getId() != null && StringUtil3.isNotBlank(site.getId())) {
			model.addAttribute("id", site.getId());
		}
		return "/cms/SiteList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Site site, Model model, Page page) {
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		Criteria c = qc.createCriteria();
		if(StringUtil3.isNotBlank(site.getName())) {
			c.andLike("NAME", "NAME", site.getName());
		}
		if(StringUtil3.isNotBlank(site.getTitle())) {
			c.andLike("TITLE", "TITLE", site.getTitle());
		}
		if(StringUtil3.isNotBlank(site.getRemarks())) {
			c.andLike("REMARKS", "REMARKS", site.getRemarks());
		}
		if(StringUtil3.isNotBlank(site.getKeywords())) {
			c.andLike("KEYWORDS", "KEYWORDS", site.getKeywords());
		}
		page = siteService.queryForPage(qc, param);
		return page;
	}

	@RequestMapping(value = "form")
	public String form(Site site, Model model) {
		if(!IdGen.isInvalidId(site.getId())) {
			site = this.siteService.get(site.getId());
		}
		model.addAttribute("site", site);
		return "/cms/SiteForm";
	}

	@RequestMapping(value = "save")
	public String save(Site site, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, site)){
			return form(site, model);
		}
		this.siteService.save(site);
		addMessage(redirectAttributes, "保存站点'" + site.getName() + "'成功");
		return "redirect:"+Globals.getAdminPath()+"/cms/site/?repage";
	}
	
	@ResponseBody
	@RequestMapping(value = {"delete"})
	public AjaxResult delete(String[] idList, RedirectAttributes redirectAttributes) {
		List<Site> sites = Lists.newArrayList();
		for( String id:idList ) {
			Site site = new Site();
			site.setId(id);
			if(IdGen.isRoot(id)){
				 return AjaxResult.error("默认站点不能删除");
			}
			sites.add(site);
		}
		this.siteService.batchDelete(sites);
		return AjaxResult.success();
	}
	
	/**
	 * 选择站点
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "select")
	public AjaxResult select(String id, HttpServletResponse response){
		if (id!=null){
			UserUtils.putCache("siteId", id);
			// 保存到Cookie中，下次登录后自动切换到该站点
			CookieUtils.setCookie(response, "siteId", id);
		}
		return AjaxResult.success();
	}
}
