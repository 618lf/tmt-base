/**
 * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.tmt.base.cms.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tmt.base.cms.entity.Link;
import com.tmt.base.cms.service.CategoryService;
import com.tmt.base.cms.service.LinkService;
import com.tmt.base.common.config.Globals;
import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.system.entity.AjaxResult;

/**
 * 链接Controller
 * @author ThinkGem
 * @version 2013-3-23
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/link")
public class LinkController extends BaseController {

	@Autowired
	private LinkService linkService;
	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(value = {"initList", ""})
	public String initList(Link link, Model model) {
		return "/cms/LinkList";
	}
	
	@ResponseBody
	@RequestMapping(value = {"jSonList"})
	public Page jSonList(Link link, Model model, Page page) {
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		page = this.linkService.queryForPage(qc, param);
		return page;
	} 

	@RequestMapping(value = "form")
	public String form(Link link, Model model) {
		model.addAttribute("link", link);
		return "/cms/LinkForm";
	}

	@RequestMapping(value = "save")
	public String save(Link link, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, link)){
			return form(link, model);
		}
		this.linkService.save(link);
		addMessage(redirectAttributes, "保存连接'" + StringUtil3.abbr(link.getTitle(),50) + "'成功");
		return "redirect:"+Globals.getAdminPath()+"/cms/link/initList/";
	}
	
	@ResponseBody
	@RequestMapping(value = "delete")
	public AjaxResult delete(String id, Long categoryId, @RequestParam(required=false) Boolean isRe, RedirectAttributes redirectAttributes) {
		addMessage(redirectAttributes, (isRe!=null&&isRe?"发布":"删除")+"链接成功");
		return AjaxResult.success();
	}

}
