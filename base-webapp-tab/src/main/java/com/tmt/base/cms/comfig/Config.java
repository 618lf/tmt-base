package com.tmt.base.cms.comfig;

import com.tmt.base.common.config.Globals;

/**
 * 
 * @author liFeng 2014年6月27日
 */
public class Config {

	/**
	 * 网站路劲
	 * @return
	 */
	public static String getFrontPath() {
		return Globals.getConfig("frontPath");
	}

	/**
	 * 
	 * @return
	 */
	public static String getUrlSuffix() {
		return Globals.getConfig("urlSuffix");
	}
}
