package com.tmt.base.cms.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tmt.base.common.web.BaseController;

@Controller
@RequestMapping(value = "${adminPath}/cms/ckfinder")
public class CkfinderController extends BaseController{

	
	@RequestMapping(params = "ckfinderList")
	public String jeecgDemoCkfinder(HttpServletRequest request) {
		return "/cms/UserMenu";
	}
}
