package com.tmt.base.cms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.cms.dao.GuestBookDao;
import com.tmt.base.cms.entity.GuestBook;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;

@Service
public class GuestBookService extends BaseService<GuestBook, String>{

	@Autowired
	private GuestBookDao guestBookDao;
	
	@Override
	protected BaseIbatisDAO<GuestBook, String> getEntityDao() {
		return guestBookDao;
	}
}
