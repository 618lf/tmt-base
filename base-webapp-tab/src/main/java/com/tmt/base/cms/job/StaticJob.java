package com.tmt.base.cms.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tmt.base.cms.service.StaticService;
import com.tmt.base.component.task.executor.TaskExecutor;

/**
 * 静态化的任务
 * @author liFeng
 * 2014年8月4日
 */
@Component
public class StaticJob implements TaskExecutor{

	@Autowired
	private StaticService staticService;
	
	@Override
	public Boolean doTask() {
		staticService.buildAll();
		return Boolean.TRUE;
	}
}
