package com.tmt.base.cms.service;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.cms.dao.LinkDao;
import com.tmt.base.cms.entity.Article;
import com.tmt.base.cms.entity.Category;
import com.tmt.base.cms.entity.Link;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.common.utils.StringUtil3;

@Service
public class LinkService extends BaseService<Link, String>{

	@Autowired
	private LinkDao linkDao;
	@Autowired
	private CategoryService categoryService;
	
	@Override
	protected BaseIbatisDAO<Link, String> getEntityDao() {
		return linkDao;
	}
	
	public String save(Link link) {
		// 如果没有审核权限，则将当前内容改为待审核状态
		if (!SecurityUtils.getSubject().isPermitted("cms:link:audit")){
			link.setDelFlag(Link.DEL_FLAG_AUDIT);
		}
		// 如果栏目不需要审核，则将该内容设为发布状态
		if (link.getCategoryId()!=null&&StringUtil3.isNotBlank(link.getCategoryId())){
			Category category = categoryService.get(link.getCategoryId());
			if (!Article.YES.equals(category.getIsAudit())){
				link.setDelFlag(Article.DEL_FLAG_NORMAL);
			}
		}
		if(IdGen.isInvalidId(link.getId())) {
			this.insert(link);
		} else {
			this.update(link);
		}
		return link.getId();
	}
}
