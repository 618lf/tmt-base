package com.tmt.base.cms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.cms.dao.CommentDao;
import com.tmt.base.cms.entity.Comment;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;

@Service
public class CommentService extends BaseService<Comment, String>{

	@Autowired
	private CommentDao commentDao;
	
	@Override
	protected BaseIbatisDAO<Comment, String> getEntityDao() {
		return commentDao;
	}
}
