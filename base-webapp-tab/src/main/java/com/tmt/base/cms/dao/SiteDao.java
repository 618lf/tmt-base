package com.tmt.base.cms.dao;

import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.tmt.base.cms.entity.Site;
import com.tmt.base.common.exception.DataAccessException;
import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.persistence.incrementer.IdGen;

@Repository
public class SiteDao extends BaseIbatisDAO<Site, String> {

	@Override
	protected String getNamespace() {
		return "CMS_SITE";
	}

	/**
	 * 重定义插入
	 */
	@Override
	public String insert(Site entity) {
		if(IdGen.isRoot(entity.getId())) {
			return this.insertNoAutoId(entity);
		}
		return super.insert(entity);
	}
	
	@SuppressWarnings("deprecation")
	public String insertNoAutoId(Site entity){
		try{
			this.getSqlRunner().insert(getStatementName(INSERT), entity);
			return entity.getId();
    	}catch(Exception ex){
    		LogFactory.getLog(this.getClass()).error(ex);
    		throw new DataAccessException(ex);
    	}
	}
}