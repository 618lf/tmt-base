package com.tmt.base.cms.template.directive;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tmt.base.cms.entity.Category;
import com.tmt.base.cms.service.CategoryService;
import com.tmt.base.common.utils.FreemarkerUtils;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 分类的父分类
 * @author liFeng
 * 2014年8月9日
 */
@Component("categoryChildrenListDirective")
public class CategoryChildrenListDirective extends BaseDirective{

	private final static String USE_CACHE = "categoryChildren";
	
	@Autowired
	private CategoryService categoryService;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		String categoryId = String.valueOf(FreemarkerUtils.getParameter("categoryId", String.class, params));
		Category category = this.categoryService.get(categoryId);
		List<Category> categorys = this.categoryService.findChildren(category);
		this.useCache(USE_CACHE, categorys, env, body);
	}

}
