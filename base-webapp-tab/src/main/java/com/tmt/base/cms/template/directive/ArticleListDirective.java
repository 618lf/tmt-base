package com.tmt.base.cms.template.directive;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.tmt.base.cms.entity.Article;
import com.tmt.base.cms.entity.Category;
import com.tmt.base.cms.service.ArticleService;
import com.tmt.base.cms.service.CategoryService;
import com.tmt.base.common.utils.FreemarkerUtils;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * 文章列表
 * @author liFeng
 * 2014年8月10日
 */
@Component("articleListDirective")
public class ArticleListDirective extends BaseDirective{

	private final static String USE_CACHE = "articles";
	
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ArticleService articleService;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		//栏目
		String categoryId = (String)FreemarkerUtils.getParameter("categoryId", String.class, params);
		Category category = this.categoryService.get(categoryId);
		Map<String,Object> param = Maps.newHashMap();
		if( category != null) {
			param.put("CATEGORY_ID", categoryId);
		} 
		//权重
		Integer weight = (Integer)FreemarkerUtils.getParameter("weight", Integer.class, params);
		if( weight != null) {
			param.put("WEIGHT", weight);
		}
		//大小
		Integer count = (Integer)FreemarkerUtils.getParameter("count", Integer.class, params);
		if( count != null) {
			param.put("limitNum", count);
		}
		List<Article> articles = this.articleService.queryByCondition(param);
		this.useCache(USE_CACHE, articles, env, body);
	}
}
