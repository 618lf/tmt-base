package com.tmt.base.cms.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.cms.entity.Article;
import com.tmt.base.common.persistence.BaseIbatisDAO;

@Repository
public class ArticleDao extends BaseIbatisDAO<Article, String> {

	@Override
	protected String getNamespace() {
		return "CMS_ARTICLE";
	}
}
