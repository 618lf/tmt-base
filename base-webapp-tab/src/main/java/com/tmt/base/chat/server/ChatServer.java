package com.tmt.base.chat.server;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.springframework.stereotype.Component;

import com.tmt.base.chat.endpoint.ChatEndPoint;

/**
 * 简单的服务器
 * @author liFeng
 * 2014年7月19日
 */
@Component
public class ChatServer {

	private static final Set<ChatEndPoint> connections = new CopyOnWriteArraySet<ChatEndPoint>();
	
	public void addClient( ChatEndPoint client) {
		connections.add(client);
	}
	
	public void removeClient(ChatEndPoint client) {
		connections.remove(client);
	}
	
	public Set<ChatEndPoint> getAll() {
		return connections;
	}
}
