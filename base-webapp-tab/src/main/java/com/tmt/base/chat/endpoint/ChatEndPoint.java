package com.tmt.base.chat.endpoint;

import java.io.IOException;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;

import com.tmt.base.chat.server.ChatServer;
/**
 * 用了这个之后，你的服务地址为ws://localhost:port/projectName/chat01
 * 
 * @author liFeng 2014年7月18日
 */
//@ServerEndpoint(value="/chat01/{userName}", configurator = SpringConfigurator.class)
public class ChatEndPoint {
	
	private String nickname;
    private Session session;
    @Autowired
    private ChatServer chatServer;
    
    @OnOpen
    public void start(@PathParam("userName") String userName, Session session) {
    	nickname = userName;
        this.session = session;
        chatServer.addClient(this);
        String message = String.format("嗨嗨，姑娘们，来接客了： %s %s", nickname, "has joined.");
        broadcast(message);
    }
 
 
    @OnClose
    public void end() {
    	chatServer.removeClient(this);
        String message = String.format("客官慢走，嘿嘿，还没付钱呢： %s %s",nickname, "has disconnected.");
        broadcast(message);
    }
 
 
    @OnMessage
    public void receive(String message) {
        String filteredMessage = String.format("您有新消息：%s: %s", nickname, "message.toString()");
        broadcast(filteredMessage);
    }
    
    private void broadcast(String msg) {
        for (ChatEndPoint client : chatServer.getAll()) {
            try {
            	client.session.getBasicRemote().sendText(msg);
            } catch (IOException e) {
            	chatServer.removeClient(client);
                try {
                    client.session.close();
                } catch (IOException e1) {
                }
                String message = String.format("* %s %s", client.nickname, "has been disconnected.");
                broadcast(message);
            }
        }
    }
    
}
