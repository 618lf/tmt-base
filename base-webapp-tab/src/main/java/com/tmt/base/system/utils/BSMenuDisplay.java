package com.tmt.base.system.utils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.tmt.base.common.config.Globals;
import com.tmt.base.common.utils.ContextHolderUtils;
import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.system.entity.Menu;

/**
 * BootStrap 显示样式
 * @author lifeng
 */
@Component
public class BSMenuDisplay implements IMenuDisplay{

	private static String DEFAULT_ICON = "icon-file-alt";
	
	/**
	 * 展示菜单
	 */
	@Override
	public String getUIMenu(List<Menu> menuList, double screenSize) {
		StringBuffer sb = new StringBuffer();
		try{
			sb.append(getQcMenu());
			sb.append("<ul class='nav nav-list'>");
			sb.append(getBSUIMenu(menuList,screenSize));
			sb.append("</ul>");
			sb.append(getCollapseMenu());
		}catch(Exception e){}
		return sb.toString();
	}
	
	private static String getQcMenu() {
		StringBuffer sb = new StringBuffer();
		sb.append("<div class='sidebar-shortcuts' id='sidebar-shortcuts'>")
		  .append("<div class='sidebar-shortcuts-large' id='sidebar-shortcuts-large'>")
		  .append("<button class='btn btn-success'><i class='icon-signal'></i></button>&nbsp;")
		  .append("<button class='btn btn-info'><i class='icon-pencil'></i></button>&nbsp;")
		  .append("<button class='btn btn-warning'><i class='icon-group'></i></button>&nbsp;")
		  .append("<button class='btn btn-danger'><i class='icon-cogs'></i></button>&nbsp;")
		  .append("</div><div class='sidebar-shortcuts-mini' id='sidebar-shortcuts-mini'>")
		  .append("<span class='btn btn-success'></span>")
		  .append("<span class='btn btn-info'></span>")
		  .append("<span class='btn btn-warning'></span>")
		  .append("<span class='btn btn-danger'></span>")
		  .append("</div></div>");
		return sb.toString();
	}
	
	private static String getCollapseMenu() {
		StringBuffer sb = new StringBuffer();
		sb.append("<div class='sidebar-collapse' id='sidebar-collapse'>")
		  .append("<i class='icon-double-angle-left' data-icon1='icon-double-angle-left' data-icon2='icon-double-angle-right'></i>")
		  .append("</div>");
		return sb.toString();
	}
	
	//menuList是按级别和排序字段排好序 只支持两级菜单  bootStrap
	public static String getBSUIMenu(List<Menu> menuList, double screenSize) throws IOException{
		Map<Integer,List<Menu>> menuMap = MenuUtils.classifyByLevelOnlyShow(menuList);
		StringBuffer menuString = new StringBuffer();
		int firstLevel = 1;
		List<Menu> firstMenu = menuMap.get(firstLevel); //level从1开始
		if(firstMenu == null){ return "";}
		for( Menu menu: firstMenu ) {
			 int deep = 1;//深度
			 menuString.append(getContentTags(menu, firstLevel, menuMap, deep));
		}
		return menuString.toString();
	}
	
	/**
	 * 得到菜单内容
	 * @param menu
	 * @param level
	 * @param menuMap
	 * @return
	 * @throws IOException
	 */
	private static String getContentTags(Menu menu,int level,Map<Integer,List<Menu>> menuMap, int deep) throws IOException{
		StringBuffer menuString = new StringBuffer();
		menuString.append("<li>");
		menuString.append(getBHrefTag(menu));
		menuString.append(getIconTag(menu, null, deep));
		menuString.append(getSpanTag(menu,deep));
		menuString.append(getEHrefTag(menu));
		if( menu.getType() == 1 ) {//目录
			deep++;
			menuString.append(getBSUIChildMenu(menu,level+1,menuMap,deep));
		}
		menuString.append("</li>");
		return menuString.toString();
	}
	
	//竖 展示子菜单
	private static String getBSUIChildMenu(Menu parent,int level,Map<Integer,List<Menu>> menuMap,int deep) throws IOException{
		List<Menu> menuList = menuMap.get(level);
		if(menuList == null){ return "";}
		StringBuffer menuString = null;
		int iCount = 0;
		for( Menu menu: menuList ) {
			if(menu.getParentId().compareTo(parent.getId()) == 0) {
				if( menuString == null) {
					menuString = new StringBuffer();
					menuString.append("<ul class='").append("submenu").append("'>");
				}
				iCount++; menu.setChildrenCount(iCount);
				menuString.append(getContentTags(menu, level, menuMap, deep));
			}
		}
		if( menuString != null) {
			return menuString.append("</ul>").toString();
		}
		return "";
	}
	
	/**
	 * 得到菜单的连接地址
	 * 如果没有配置 前缀 则自动加上
	 * @param href
	 * @return
	 */
	private static String getBHrefTag( Menu menu ) {
		String href = "javascript:void(0)";
		if( menu.getType() == 2 ) {
			href = menu.getHref();
			if( !StringUtil3.startsWith(href, Globals.getAdminPath())) {
				href = Globals.getAdminPath() + href;
			}
			href = ContextHolderUtils.getWebRoot() + href;
		}
		StringBuffer menuString = new StringBuffer();
		menuString.append("<a href='").append(href)
                  .append("' title='").append(menu.getName()).append("' data-id='").append(menu.getId());
        if( menu.getType() == 1) {//目录
       	    menuString.append("' class='dropdown-toggle");
        }
        menuString.append("' rel='pageTab'>");
		return menuString.toString();
	}
	
	/**
	 * 得到菜单的连接地址
	 * 如果没有配置 前缀 则自动加上
	 * @param href
	 * @return
	 */
	private static String getEHrefTag( Menu menu ) {
		StringBuilder menuString = new StringBuilder();
		if (menu.getType() == 1) {// 目录
			menuString.append("<b class='arrow icon-angle-down'></b>");
		}
		menuString.append("</a>");
		return menuString.toString();
	}
	
	/**
	 * 得到图标,第一层没小箭头，第二层才有小箭头
	 * @param menu
	 * @param defaulIcon
	 * @return
	 * @throws IOException 
	 */
	private static String getIconTag(Menu menu, String defaulIcon, int deep) throws IOException{
		String icon = defaulIcon== null ? DEFAULT_ICON: defaulIcon;
		       icon = menu.getIconClass() == null?icon:menu.getIconClass();
		StringBuilder icons = new StringBuilder(deep != 2?"":"<i class='icon-double-angle-right'></i>"); 
		return StringUtil3.appendTo(icons, "<i class='", icon, "'></i>").toString();
	}
	
	private static String getSpanTag(Menu menu, int deep) throws IOException{
		return StringUtil3.appendTo(new StringBuilder(), "<span class='menu-text'>", menu.getName(), "</span>").toString();
	}
}
