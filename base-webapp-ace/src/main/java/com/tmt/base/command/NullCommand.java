package com.tmt.base.command;

/**
 * 什么都不做
 * @author liFeng
 * 2014年7月24日
 */
public class NullCommand implements ICommand{

	@Override
	public String execute(String  param) {
		return "";
	}
}
