package com.tmt.base.command;

import com.tmt.base.common.utils.StringUtil3;

/**
 * 公共服务类
 * @author liFeng
 * 2014年7月24日
 */
public abstract class AbstractCommand implements ICommand{
	
	@Override
	public String execute(String  param) {
		if(!this.check(param)) {
			return "参数错误！";
		}
		return this.getLimitContext(this.executeCommand(param));
	}
	
	public Boolean check(String  param) {
		if(param == null && StringUtil3.isNotBlank(param)) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	/**
	 * 由于有字数的限制，则需要处理
	 * @param srcContext
	 * @return
	 */
	public String getLimitContext( String srcContext) {
		if( getByteSize(srcContext) > 2047) {
			return srcContext.substring(0,2047);
		}
		return srcContext;
	}
	
	public static int getByteSize(String content) {
		int size = 0;
		if (null != content) {
			try {
				size = content.getBytes("utf-8").length;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return size;
	}
	
	public abstract String executeCommand(String param);
	
}
