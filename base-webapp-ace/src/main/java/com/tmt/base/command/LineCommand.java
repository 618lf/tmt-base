package com.tmt.base.command;

import com.tmt.base.bus.service.BusService;

public class LineCommand extends AbstractCommand{

	@Override
	public String executeCommand(String  param) {
		return BusService.busLines(param).toString();
	}
}
