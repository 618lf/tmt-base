package com.tmt.base.command;

import java.util.List;

import com.tmt.base.bus.entity.Stat;
import com.tmt.base.bus.service.BusService;

/**
 * 站点查询
 * @author liFeng
 * 2014年7月25日
 */
public class StatCommand extends AbstractCommand{

	@Override
	public String executeCommand(String param) {
		List<Stat> stats = BusService.busStats(param);
		StringBuffer sb = new StringBuffer();
		sb.append("经过").append(param).append("的公交车：\n");
		for(Stat stat: stats) {
			sb.append(stat.toString());
		}
		return sb.toString();
	}
}
