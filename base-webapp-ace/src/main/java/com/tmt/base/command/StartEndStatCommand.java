package com.tmt.base.command;

import com.tmt.base.bus.service.BusService;

/**
 * 起始站点服务
 * @author liFeng
 * 2014年7月24日
 */
public class StartEndStatCommand extends AbstractCommand{

	@Override
	public Boolean check(String param) {
		if(!super.check(param) || param.indexOf("-") == -1) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	@Override
	public String executeCommand(String param) {
		String start = param.substring(0,param.indexOf("-"));
		String end = param.substring(param.indexOf("-")+1);
		return BusService.bus(start, end).toString();
	}
}