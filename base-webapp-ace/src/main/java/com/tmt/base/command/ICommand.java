package com.tmt.base.command;

/**
 * 命令
 * @author liFeng
 * 2014年7月24日
 */
public interface ICommand {
	
	/**
	 * 执行命令,通过参数
	 * @return
	 */
	public String execute(String param);
}
