package com.tmt.base.command;

public class CommandFactory {

	public static ICommand[] commands = new ICommand[]{
		new StartEndStatCommand(),new LineCommand(),
		new StatCommand(), new NullCommand()
	};
	public static String execute( String msg) {
		return getCommand(msg.substring(0, 2)).execute(msg.substring(2));
	}
	public static ICommand getCommand( String command) {
		if(command.startsWith("1")) {
			return commands[0];
		} else if(command.startsWith("2")) {
			return commands[1];
		}else if(command.startsWith("3")) {
			return commands[2];
		}
		return commands[3];
	}
}
