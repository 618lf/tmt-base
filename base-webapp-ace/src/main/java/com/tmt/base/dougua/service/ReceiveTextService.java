package com.tmt.base.dougua.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.dougua.dao.ReceiveTextDao;
import com.tmt.base.dougua.entity.ReceiveText;

@Service
public class ReceiveTextService extends BaseService<ReceiveText, String> {
	
	@Autowired
	private ReceiveTextDao receiveTextDao;

	@Override
	protected BaseIbatisDAO<ReceiveText, String> getEntityDao() {
		return receiveTextDao;
	}

}
