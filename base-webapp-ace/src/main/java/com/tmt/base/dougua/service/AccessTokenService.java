package com.tmt.base.dougua.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.common.service.BaseService;
import com.tmt.base.dougua.dao.AccessTokenDao;
import com.tmt.base.dougua.entity.AccessToken;

@Service
public class AccessTokenService extends BaseService<AccessToken, String> {
	
	@Autowired
	private AccessTokenDao accessTokenDao;

	@Override
	protected BaseIbatisDAO<AccessToken, String> getEntityDao() {
		return accessTokenDao;
	}

}
