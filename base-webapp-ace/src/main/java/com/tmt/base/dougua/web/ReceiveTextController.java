package com.tmt.base.dougua.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tmt.base.common.persistence.Page;
import com.tmt.base.common.persistence.PageParameters;
import com.tmt.base.common.persistence.QueryCondition;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.dougua.entity.ReceiveText;
import com.tmt.base.dougua.service.ReceiveTextService;

@Controller
@RequestMapping(value = "${adminPath}/weixin/rt")
public class ReceiveTextController extends BaseController{

	@Autowired
	private ReceiveTextService receiveTextService;
	
	@RequestMapping(value = "")
	public String list(ReceiveText receiveText, Page page, Model model){
		QueryCondition qc = new QueryCondition();
		PageParameters param = page.getParam();
		page = this.receiveTextService.queryForPage(qc, param);
		model.addAttribute("page", page);
		model.addAttribute("sidebar","receives");
		return "/weixin/ReceiveTextList";
	}
}
