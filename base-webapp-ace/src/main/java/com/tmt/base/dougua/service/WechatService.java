package com.tmt.base.dougua.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmt.base.common.exception.BaseRuntimeException;
import com.tmt.base.common.utils.DateUtil3;
import com.tmt.base.dougua.entity.ReceiveText;
import com.tmt.base.dougua.entity.response.TextMessageResp;
import com.tmt.base.dougua.utils.MenuUtil;
import com.tmt.base.dougua.utils.MessageUtil;

@Service
public class WechatService {
	
	protected static Logger logger = LoggerFactory.getLogger(WechatService.class);
	
    @Autowired
	private ReceiveTextService receiveTextService;
    
	public String coreService(HttpServletRequest request) {
		String respMessage = null;
		try {
			// 默认返回的文本消息内容
			String respContent = "请求处理异常，请稍候尝试！";
			// xml请求解析
			Map<String, String> requestMap = MessageUtil.parseXml(request);
			// 发送方帐号（open_id）
			String fromUserName = requestMap.get("FromUserName");
			// 公众帐号
			String toUserName = requestMap.get("ToUserName");
			// 消息类型
			String msgType = requestMap.get("MsgType");
			String msgId = requestMap.get("MsgId");
			//默认回复此文本信息
			TextMessageResp textMessage = new TextMessageResp();
			textMessage.setToUserName(fromUserName);
			textMessage.setFromUserName(toUserName);
			textMessage.setCreateTime(DateUtil3.getTimeStampNow().getTime());
			textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
			// 将文本消息对象转换成xml字符串
			respMessage = MessageUtil.textMessageToXml(textMessage);
			// 文本消息
			if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_TEXT)) {
				String content = requestMap.get("Content");
				// 保存接收到的信息
				ReceiveText receiveText = new ReceiveText();
				receiveText.setContent(content);
				receiveText.setCreateDate(DateUtil3.getTimeStampNow());
				receiveText.setFormUserName(fromUserName);
				receiveText.setToUserName(toUserName);
				receiveText.setMsgId(msgId);
				receiveText.setMsgType(msgType);
				receiveText.setResponse("0");
				receiveText.setAccountId(toUserName);
				this.receiveTextService.insert(receiveText);
				
				if( !MenuUtil.isMenuCommand(content)) {
					respContent = MenuUtil.getMainMenu();
				}
				//设置回复模版
				respContent = MenuUtil.getMenuByCommand(content);
			}
			// 图片消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_IMAGE)) {
				respContent = "您发送的是图片消息！";
			}
			// 地理位置消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {
				respContent = "您发送的是地理位置消息！";
			}
			// 链接消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LINK)) {
				respContent = "您发送的是链接消息！";
			}
			// 音频消息
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_VOICE)) {
				respContent = "您发送的是音频消息！";
			}
			// 事件推送
			else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_EVENT)) {
				String eventType = requestMap.get("Event");
				// 订阅
				if (eventType.equals(MessageUtil.EVENT_TYPE_SUBSCRIBE)) {
					respContent = MenuUtil.getMainMenu();
				}
			}
			textMessage.setContent(respContent);
			respMessage = MessageUtil.textMessageToXml(textMessage);
		} catch (Exception e) {
			throw new BaseRuntimeException("微信接收信息异常:",e);
		}
		return respMessage;
	}

}
