package com.tmt.base.dougua.web;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmt.base.common.utils.StringUtil3;
import com.tmt.base.common.web.BaseController;
import com.tmt.base.dougua.service.WechatService;
import com.tmt.base.dougua.utils.SignUtil;

@Controller
@RequestMapping(value = "${adminPath}/weixin")
public class WechatController extends BaseController {

	private String token = "douguazhijia";

	@Autowired
	private WechatService wechatService;
	
	/**
	 * 用于测试是否可以和微信接入
	 * 
	 * @param request
	 * @param response
	 * @param signature
	 * @param timestamp
	 * @param nonce
	 * @param echostr
	 */
	@ResponseBody
	@RequestMapping(value = "wechat", method = RequestMethod.GET)
	public String wechatGet(String signature,String timestamp,String nonce,String echostr, HttpServletRequest request) {
		if(StringUtil3.isNotBlank(signature) && StringUtil3.isNotBlank(nonce) && StringUtil3.isNotBlank(echostr)
				&& SignUtil.checkSignature(token, signature, timestamp, nonce) ) {
			return echostr;
		}
		return "ERROR";
	}

	/**
	 * 微信端调用的本地接口
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "wechat", method = RequestMethod.POST)
	public String wechatPost(HttpServletResponse response,
			HttpServletRequest request) throws IOException {
		return wechatService.coreService(request);
	}

}
