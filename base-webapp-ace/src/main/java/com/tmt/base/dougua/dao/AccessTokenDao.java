package com.tmt.base.dougua.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.dougua.entity.AccessToken;

@Repository
public class AccessTokenDao extends BaseIbatisDAO<AccessToken, String>{

	@Override
	protected String getNamespace() {
		return "WX_ACCESS_TOKEN";
	}

}
