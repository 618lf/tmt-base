package com.tmt.base.dougua.dao;

import org.springframework.stereotype.Repository;

import com.tmt.base.common.persistence.BaseIbatisDAO;
import com.tmt.base.dougua.entity.ReceiveText;

@Repository
public class ReceiveTextDao extends BaseIbatisDAO<ReceiveText, String>{

	@Override
	protected String getNamespace() {
		return "WX_RECEIVE_TEXT";
	}

}
