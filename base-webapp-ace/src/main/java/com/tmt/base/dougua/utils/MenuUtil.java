package com.tmt.base.dougua.utils;

import com.tmt.base.command.CommandFactory;
import com.tmt.base.common.utils.StringUtil3;

/**
 * 菜单项
 * @author liFeng
 * 2014年7月24日
 */
public class MenuUtil {

	public static String getMainMenu() {
		StringBuffer menus = new StringBuffer();
		menus.append("提供公交查询服务：\n");
		menus.append("1.起始站查询（1.xxx-xxx）\n");
		menus.append("2.线路查询(2.字母和数字)\n");
		menus.append("3.站点查询(3.xxx)\n");
		menus.append("4.周围站点查询（直接发送您的地理位置消息）\n");
		return menus.toString();
	}
	
	public static String getMenuByCommand( String msg ) {
		if( isMenuCommand(msg)){
			return CommandFactory.execute(msg);
		}
		return getMainMenu();
	}
	
	public static Boolean isMenuCommand( String msg ) {
		if( StringUtil3.isNotBlank(msg) && (msg.startsWith("1.")
				|| msg.startsWith("2.") || msg.startsWith("3."))) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
