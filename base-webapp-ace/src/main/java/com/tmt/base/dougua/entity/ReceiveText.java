package com.tmt.base.dougua.entity;

import com.tmt.base.system.entity.BaseEntity;

/**
 * 回复的文本
 * @author liFeng
 * 2014年7月12日
 */
public class ReceiveText extends BaseEntity<String>{
   
	private static final long serialVersionUID = 6457532675799376128L;

	private String content;
    private String formUserName;
    private String msgId;
    private String msgType;
    private String resContent;
    private String response;
    private String toUserName;
    private String accountId;
    
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getFormUserName() {
		return formUserName;
	}
	public void setFormUserName(String formUserName) {
		this.formUserName = formUserName;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getResContent() {
		return resContent;
	}
	public void setResContent(String resContent) {
		this.resContent = resContent;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getToUserName() {
		return toUserName;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
}