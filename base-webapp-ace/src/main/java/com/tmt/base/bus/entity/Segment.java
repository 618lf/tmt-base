package com.tmt.base.bus.entity;

/**
 * 方案
 * @author lifeng
 *
 *  <start_stat>西直门</start_stat>
    <end_stat>东四十条</end_stat>
    <line_name>地铁2号线内环(M2)(积水潭-积水潭)</line_name>
    <stats>西直门;积水潭;鼓楼大街;安定门;雍和宫;东直门;东四十条</stats>
    <line_dist>8645</line_dist>
    <foot_dist>0</foot_dist>
    <stat_xys></stat_xys>
    <line_xys></line_xys>
 */
public class Segment{

	private String start_stat;
	private String end_stat;
	private String line_name;
	private String stats;
	private String line_dist;
	private String foot_dist;
	private String stat_xys;
	private String line_xys;
	
	public String getLine_name() {
		return line_name;
	}
	public void setLine_name(String line_name) {
		this.line_name = line_name;
	}
	public String getStart_stat() {
		return start_stat;
	}
	public void setStart_stat(String start_stat) {
		this.start_stat = start_stat;
	}
	public String getEnd_stat() {
		return end_stat;
	}
	public void setEnd_stat(String end_stat) {
		this.end_stat = end_stat;
	}
	public String getStats() {
		return stats;
	}
	public void setStats(String stats) {
		this.stats = stats;
	}
	public String getLine_dist() {
		return line_dist;
	}
	public void setLine_dist(String line_dist) {
		this.line_dist = line_dist;
	}
	public String getFoot_dist() {
		return foot_dist;
	}
	public void setFoot_dist(String foot_dist) {
		this.foot_dist = foot_dist;
	}
	public String getStat_xys() {
		return stat_xys;
	}
	public void setStat_xys(String stat_xys) {
		this.stat_xys = stat_xys;
	}
	public String getLine_xys() {
		return line_xys;
	}
	public void setLine_xys(String line_xys) {
		this.line_xys = line_xys;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("公交线路：").append(this.line_name).append(" ");
		sb.append("开始站点：").append(start_stat).append(" -- ");
		sb.append("结束站点：").append(this.end_stat).append("\n");
		sb.append("途径站点：").append(this.stats).append("\n");
		return sb.toString();
	}
}
