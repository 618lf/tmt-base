package com.tmt.base.bus.entity;

/**
 *  <line>
    <name>42路(东40条桥-广外甘石桥)</name>
    <info>1-599路; 东四十条桥5:30-23:00|广外甘石桥5:30-23:00; 无人售票线路，单一票制1元，不设找赎。
		       持卡乘车普通卡0.40元/次、学生卡0.20元/次。
    </info>
    <stats> 东四十条桥;张自忠路;宽街路口东;地安门东;北海北门;厂桥路口东;平安里路口南;西四路口西;白塔寺;
			阜成门内;阜成门南;月坛体育场;北京儿童医院西门;礼士路南口;真武庙;天宁寺桥北;广安门北;广外关厢;达官营;
			广外甘石桥
    </stats>
    <stat_xys></stat_xys>
    <xys></xys>
    </line>
 * @author lifeng
 *
 */
public class Line{

	private String name;
	private String info;
	private String stats;
	private String stat_xys;
	private String xys;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getStats() {
		return stats;
	}
	public void setStats(String stats) {
		this.stats = stats;
	}
	public String getStat_xys() {
		return stat_xys;
	}
	public void setStat_xys(String stat_xys) {
		this.stat_xys = stat_xys;
	}
	public String getXys() {
		return xys;
	}
	public void setXys(String xys) {
		this.xys = xys;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("\n 线路:" + name).append(":\n");
		sb.append(" 经过站点:" + stats).append(":\n");
		return sb.toString();
	}
}
