package com.tmt.base.bus.entity;

/**
 * 公共
 * @author lifeng
 */
public class BaseEntity {

	private String result_num;
	private String web_url;
	private String wap_url;
	
	public String getResult_num() {
		return result_num;
	}
	public void setResult_num(String result_num) {
		this.result_num = result_num;
	}
	public String getWeb_url() {
		return web_url;
	}
	public void setWeb_url(String web_url) {
		this.web_url = web_url;
	}
	public String getWap_url() {
		return wap_url;
	}
	public void setWap_url(String wap_url) {
		this.wap_url = wap_url;
	}
	
}
