package com.tmt.base.bus.xml;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class NodeletParser {

	private Map<String, Nodelet> letMap = new HashMap<String, Nodelet>();
	private Boolean validation = Boolean.FALSE;
	private EntityResolver entityResolver = null;

	public void addNodelet(String xpath, Nodelet nodelet) {
		letMap.put(xpath, nodelet);
	}

	public void parse(Reader reader) {
		try {
			Document doc = createDocument(reader);
			parse(doc.getLastChild());
		} catch (Exception e) {
			throw new RuntimeException("Error parsing XPath , Cause: " + e, e);
		}
	}

	public void parse(Node node) {
		Path path = new Path();
		processNodelet(node, "/");
		process(node, path);
	}

	private void processNodelet(Node node, String pathString) {
		Nodelet nodelet = (Nodelet) letMap.get(pathString);
		if (nodelet != null) {
			try {
				nodelet.process(node);
			} catch (Exception e) {
				throw new RuntimeException("Error parsing XPath '" + pathString
						+ "'.  Cause: " + e, e);
			}
		}
	}

	private void process(Node node, Path path) {
		if (node instanceof Element) {
			// Element
			String elementName = node.getNodeName();
			path.add(elementName);
			processNodelet(node, path.toString());
			processNodelet(node, new StringBuffer("//").append(elementName)
					.toString());

			// Attribute
			NamedNodeMap attributes = node.getAttributes();
			int n = attributes.getLength();
			for (int i = 0; i < n; i++) {
				Node att = attributes.item(i);
				String attrName = att.getNodeName();
				path.add("@" + attrName);
				processNodelet(att, path.toString());
				processNodelet(node, new StringBuffer("//@").append(attrName)
						.toString());
				path.remove();
			}

			// Children
			NodeList children = node.getChildNodes();
			for (int i = 0; i < children.getLength(); i++) {
				process(children.item(i), path);
			}
			path.add("end()");
			processNodelet(node, path.toString());
			path.remove();
			path.remove();
		} else if (node instanceof Text) {
			// Text
			path.add("text()");
			processNodelet(node, path.toString());
			processNodelet(node, "//text()");
			path.remove();
		}
	}

	private Document createDocument(Reader reader)
			throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(validation);

		factory.setNamespaceAware(false);
		factory.setIgnoringComments(true);
		factory.setIgnoringElementContentWhitespace(false);
		factory.setCoalescing(false);
		factory.setExpandEntityReferences(true);

		DocumentBuilder builder = factory.newDocumentBuilder();
		builder.setEntityResolver(entityResolver);

		builder.setErrorHandler(new ErrorHandler() {
			public void error(SAXParseException exception) throws SAXException {
				throw exception;
			}

			public void fatalError(SAXParseException exception)
					throws SAXException {
				throw exception;
			}

			public void warning(SAXParseException exception)
					throws SAXException {
			}
		});

		return builder.parse(new InputSource(reader));
	}

	private static class Path {

		private List<String> nodeList = new ArrayList<String>();

		public void add(String node) {
			nodeList.add(node);
		}

		public void remove() {
			nodeList.remove(nodeList.size() - 1);
		}

		public String toString() {
			StringBuffer buffer = new StringBuffer("/");
			for (int i = 0; i < nodeList.size(); i++) {
				buffer.append(nodeList.get(i));
				if (i < nodeList.size() - 1) {
					buffer.append("/");
				}
			}
			return buffer.toString();
		}
	}
	
	public void setValidation(boolean validation) {
		this.validation = validation;
	}
	
	public void setEntityResolver(EntityResolver resolver) {
	    this.entityResolver = resolver;
	}
}
