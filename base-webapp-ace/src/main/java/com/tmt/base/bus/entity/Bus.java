package com.tmt.base.bus.entity;

import java.util.List;

/**
 * 公交
 * @author lifeng
 */
public class Bus{
	
	private String dist;
	private String time;
	private String foot_dist;
	private String last_foot_dist;
	private List<Segment> segments;
	
	public List<Segment> getSegments() {
		return segments;
	}
	public void setSegments(List<Segment> segments) {
		this.segments = segments;
	}
	public String getDist() {
		return dist;
	}
	public void setDist(String dist) {
		this.dist = dist;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getFoot_dist() {
		return foot_dist;
	}
	public void setFoot_dist(String foot_dist) {
		this.foot_dist = foot_dist;
	}
	public String getLast_foot_dist() {
		return last_foot_dist;
	}
	public void setLast_foot_dist(String last_foot_dist) {
		this.last_foot_dist = last_foot_dist;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("\n 距离 (米):" + dist).append(":\n");
		if( this.segments != null) {
			for( Segment segment: segments) {
				sb.append(segment.toString());
			}
		}
		return sb.toString();
	}
}
