package com.tmt.base.bus.parser;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.tmt.base.bus.entity.Bus;
import com.tmt.base.bus.entity.Segment;
import com.tmt.base.bus.xml.Nodelet;

public class BusParser extends BaseParser {

	private BusParser() {
        super();
		// parse comment
		parser.addNodelet("/root/buses/bus", new Nodelet() {
			
			private XPathExpression distExpr = null;
			private XPathExpression timeExpr = null;
			private XPathExpression footDistExpr = null;
			private XPathExpression lastFootDistExpr = null;
			private XPathExpression segmentExpr = null;
			private XPathExpression segmentStartStatExpr = null;
			private XPathExpression segmentEndStatExpr = null;
			private XPathExpression segmentLineNameExpr = null;
			private XPathExpression segmentStatsExpr = null;
			private XPathExpression segmentDistExpr = null;
			
			@Override
			public void process(Node node) throws Exception {
				if( distExpr == null) {
					distExpr = xpath.compile("dist/text()");
					timeExpr = xpath.compile("time/text()");
					footDistExpr = xpath.compile("foot_dist/text()");
					lastFootDistExpr = xpath.compile("last_foot_dist/text()");
					segmentExpr = xpath.compile("segments/segment");
					
					//换成方案
					segmentStartStatExpr = xpath.compile("start_stat/text()");
					segmentEndStatExpr = xpath.compile("end_stat/text()");
					segmentLineNameExpr = xpath.compile("line_name/text()");
					segmentStatsExpr = xpath.compile("stats/text()");
					segmentDistExpr = xpath.compile("line_dist/text()");
				}
				Bus bus = new Bus();
				String dist = String.valueOf(distExpr.evaluate(node,XPathConstants.STRING));
				String time = String.valueOf(timeExpr.evaluate(node,XPathConstants.STRING));
				String footDist = String.valueOf(footDistExpr.evaluate(node,XPathConstants.STRING));
				String lastFootDist = String.valueOf(lastFootDistExpr.evaluate(node,XPathConstants.STRING));
				bus.setDist(dist);
				bus.setTime(time);
				bus.setFoot_dist(footDist);
				bus.setLast_foot_dist(lastFootDist);
				
				NodeList segmentNodes = (NodeList)segmentExpr.evaluate(node, XPathConstants.NODESET);
				
				List<Segment> segments = new ArrayList<Segment>();
				for (int i = 0; i < segmentNodes.getLength(); i++) {
					String startStat = String.valueOf(segmentStartStatExpr.evaluate(segmentNodes.item(i),XPathConstants.STRING));
					String endStat = String.valueOf(segmentEndStatExpr.evaluate(segmentNodes.item(i),XPathConstants.STRING));
					String lineName = String.valueOf(segmentLineNameExpr.evaluate(segmentNodes.item(i),XPathConstants.STRING));
					String stats = String.valueOf(segmentStatsExpr.evaluate(segmentNodes.item(i),XPathConstants.STRING));
					String lineDist = String.valueOf(segmentDistExpr.evaluate(segmentNodes.item(i),XPathConstants.STRING));
					Segment segment = new Segment();
					segment.setStart_stat(startStat);
					segment.setEnd_stat(endStat);
					segment.setLine_dist(lineDist);
					segment.setStats(stats);
					segment.setLine_name(lineName);
					segments.add(segment);
				}
				bus.setSegments(segments);
				List<Bus> buses = getEntity();
				if( buses == null) {
					setEntity(new ArrayList<Bus>());
					buses = getEntity();
				}
				buses.add(bus);
			}
		});
	}

	private static BusParser busParser = new BusParser();
	public static BusParser getInstance() {
		return busParser;
	}
}
