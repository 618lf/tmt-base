package com.tmt.base.bus.parser;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Node;

import com.tmt.base.bus.entity.Line;
import com.tmt.base.bus.entity.Stat;
import com.tmt.base.bus.xml.Nodelet;

public class StatParser extends BaseParser{

	public StatParser() {
		super();
		parser.addNodelet("/root/stats/stat", new Nodelet() {

			private XPathExpression nameExpr = null;
			private XPathExpression xyExpr = null;
			private XPathExpression distExpr = null;
			private XPathExpression lineNameExpr = null;
			
			@Override
			public void process(Node node) throws Exception {
				
				if( nameExpr == null) {
					nameExpr = xpath.compile("name/text()");
					xyExpr = xpath.compile("xy/text()");
					lineNameExpr = xpath.compile("line_names/text()");
					distExpr = xpath.compile("dist/text()");
				}
				
				String name = String.valueOf(nameExpr.evaluate(node, XPathConstants.STRING));
				String xy = String.valueOf(xyExpr.evaluate(node, XPathConstants.STRING));
				String lineNames = String.valueOf(lineNameExpr.evaluate(node, XPathConstants.STRING));
				String dist = String.valueOf(distExpr.evaluate(node, XPathConstants.STRING));
				
				Stat stat = new Stat();
				stat.setName(name);
				stat.setXy(xy);
				stat.setLine_names(lineNames);
				stat.setDist(dist);
				
				List<Stat> stats = getEntity();
				if( stats == null) {
					setEntity(new ArrayList<Line>());
					stats = getEntity();
				}
				stats.add(stat);
			}
		 });
	}
	
	private static StatParser statParser = new StatParser();
	public static StatParser getInstance() {
		return statParser;
	}
}
