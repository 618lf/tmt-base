package com.tmt.base.bus.parser;

import java.io.StringReader;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import com.tmt.base.bus.xml.NodeletParser;

public class BaseParser {
	
	protected static ThreadLocal<Object>  entitys = new ThreadLocal<Object>();
	protected NodeletParser parser = new NodeletParser();
	protected XPathFactory xFactory = XPathFactory.newInstance();
	protected XPath xpath = xFactory.newXPath();
	
	protected BaseParser(){
		// 是否需要验证
		parser.setValidation(false);
	}
	
	public NodeletParser getParser() {
		return parser;
	}
	public void setParser(NodeletParser parser) {
		this.parser = parser;
	}
	public XPathFactory getxFactory() {
		return xFactory;
	}
	public void setxFactory(XPathFactory xFactory) {
		this.xFactory = xFactory;
	}
	public XPath getXpath() {
		return xpath;
	}
	public void setXpath(XPath xpath) {
		this.xpath = xpath;
	}
	
	public static void clear() {
		entitys.remove();
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getEntity() {
		return entitys.get() == null?null:((T)entitys.get());
	}
	public static <T> void setEntity(T value){
		entitys.set(value);
	}
	public static <T> T popEntity() {
		T t = getEntity();entitys.remove();
		return t;
	}
	public <T> T parse(String xml) {
		try {
			getParser().parse(new StringReader(xml));
			return popEntity();
		} catch (Exception e) {
			throw new RuntimeException("Error occurred.  Cause: " + e, e);
		}
	}
}
