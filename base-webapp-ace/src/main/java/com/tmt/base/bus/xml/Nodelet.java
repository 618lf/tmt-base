package com.tmt.base.bus.xml;

import org.w3c.dom.Node;

/**
 * Xml 解析
 * @author lifeng
 */
public interface Nodelet {

	 void process (Node node) throws Exception;
}
