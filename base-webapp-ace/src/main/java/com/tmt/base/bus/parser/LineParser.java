package com.tmt.base.bus.parser;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Node;

import com.tmt.base.bus.entity.Line;
import com.tmt.base.bus.xml.Nodelet;

/**
 * 
 * 公交线路
 * @author lifeng
 */
public class LineParser extends BaseParser{
	
	public LineParser() {
		 super();
		 parser.addNodelet("/root/lines/line", new Nodelet() {

			private XPathExpression nameExpr = null;
			private XPathExpression infoExpr = null;
			private XPathExpression statsExpr = null;
			private XPathExpression statXysExpr = null;
			private XPathExpression xysExpr = null;
			
			@Override
			public void process(Node node) throws Exception {
				
				if( nameExpr == null) {
					nameExpr = xpath.compile("name/text()");
					infoExpr = xpath.compile("info/text()");
					statsExpr = xpath.compile("stats/text()");
					statXysExpr = xpath.compile("stat_xys/text()");
					xysExpr = xpath.compile("xys/text()");
				}
				Line line = new Line();
				String name = String.valueOf(nameExpr.evaluate(node, XPathConstants.STRING));
				String info = String.valueOf(infoExpr.evaluate(node, XPathConstants.STRING));
				String stats = String.valueOf(statsExpr.evaluate(node, XPathConstants.STRING));
				String statXys = String.valueOf(statXysExpr.evaluate(node, XPathConstants.STRING));
				String xys = String.valueOf(xysExpr.evaluate(node, XPathConstants.STRING));
				line.setName(name);
				line.setInfo(info);
				line.setStats(stats);
				line.setStat_xys(statXys);
				line.setXys(xys);
				
				List<Line> lines = getEntity();
				if( lines == null) {
					setEntity(new ArrayList<Line>());
					lines = getEntity();
				}
				lines.add(line);
			}
		 });
	}
	
	private static LineParser lineParser = new LineParser();
	public static LineParser getInstance() {
		return lineParser;
	}
}
