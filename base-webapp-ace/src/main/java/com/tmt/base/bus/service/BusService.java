package com.tmt.base.bus.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.aibang.open.client.AibangApi;
import com.aibang.open.client.exception.AibangException;
import com.tmt.base.bus.entity.Bus;
import com.tmt.base.bus.entity.Line;
import com.tmt.base.bus.entity.Stat;
import com.tmt.base.bus.parser.BusParser;
import com.tmt.base.bus.parser.LineParser;
import com.tmt.base.bus.parser.StatParser;
/**
 * 公交服务
 * 
 * @author lifeng
 */
public class BusService {
	
	//这里请使用您自己申请的API KEY
	private static final String API_KEY = "c41273a7072dc3325414542c0bc4e503";
	private static final String CITY = "深圳";
	
	private static AibangApi aibang = new AibangApi(API_KEY);
	
	/**
	 * 通过  起始站查询
	 * @param start
	 * @param end
	 * @return
	 */
	public static List<Bus> bus(String start, String end){
		try{
			return BusParser.getInstance().parse(aibang.bus(CITY, start, end, null, null, null, null, null, null,null));
		}catch(Exception e){
			throw new RuntimeException("Error occurred.  Cause: " + e, e);
		}
	}
	
	/**
	 * 公交线路
	 * @param q
	 * @return
	 */
	public static List<Line> busLines(String q) {
		try {
			return LineParser.getInstance().parse(aibang.busLines(CITY, q, 0));
		} catch (AibangException e) {
			throw new RuntimeException("Error occurred.  Cause: " + e, e);
		}
	}
	
	/**
	 * 公交站点
	 * @param q
	 * @return
	 */
	public static List<Stat> busStats(String q) {
		try {
			return StatParser.getInstance().parse(aibang.busStats(CITY, q));
		} catch (AibangException e) {
			throw new RuntimeException("Error occurred.  Cause: " + e, e);
		}
	}
	
    /**
     * 周边公交站点 需要具体的经纬度,如果能获取具体的经纬度
     * @param lng
     * @param lat
     * @param dist
     * @return
     */
	public static List<Stat> busStats(Float lng, Float lat, Integer dist) {
		try {
			return StatParser.getInstance().parse(aibang.busStatsXy(CITY, lng, lat, dist));
		} catch (AibangException e) {
			throw new RuntimeException("Error occurred.  Cause: " + e, e);
		}
	}
	
	public static void main( String[] args ) throws AibangException, UnsupportedEncodingException {
		System.out.println(busStats("南头街道办"));
	}
}