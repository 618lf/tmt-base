package com.tmt.base.bus.entity;

/**
 * <stat>
	    <name>城铁五道口站</name>
	    <xy>116.33912,39.99303</xy>
	    <line_names>375路(西直门-韩家川);375路(韩家川-西直门北);398路(育新小区-5道口公交场站);
	                398路(5道口公交场站-育新小区)
	    </line_names>
	</stat>
 * @author lifeng
 */
public class Stat{

	private String name;
	private String xy;
	private String line_names;
	private String dist;
	
	public String getDist() {
		return dist;
	}
	public void setDist(String dist) {
		this.dist = dist;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getXy() {
		return xy;
	}
	public void setXy(String xy) {
		this.xy = xy;
	}
	public String getLine_names() {
		return line_names;
	}
	public void setLine_names(String line_names) {
		this.line_names = line_names;
	}
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("\n 站点:" + name).append(":\n");
		sb.append(" 经过线路:" + line_names).append(":\n");
		return sb.toString();
	}
}
