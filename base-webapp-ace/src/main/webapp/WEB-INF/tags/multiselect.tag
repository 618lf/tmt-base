<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ attribute name="id" type="java.lang.String" required="true" description="编号"%>
<%@ attribute name="name" type="java.lang.String" required="true" description="隐藏域名称（ID）"%>
<%@ attribute name="value" type="java.lang.String" required="true" description="隐藏域值（ID）"%>
<%@ attribute name="labelName" type="java.lang.String" required="true" description="输入框名称（Name）"%>
<%@ attribute name="labelValue" type="java.lang.String" required="true" description="输入框值（Name）"%>
<%@ attribute name="defaultText" type="java.lang.String" required="false" description="输入框值（Name）"%>
<%@ attribute name="selectUrl" type="java.lang.String" required="true" description="输入框值（Name）"%>
<%@ attribute name="title" type="java.lang.String" required="true" description="输入框值（Name）"%>
<%@ attribute name="checked" type="java.lang.Boolean" required="true" description="输入框值（Name）"%>
<%@ attribute name="allowClear" type="java.lang.Boolean" required="false" description="输入框值（Name）"%>
<script src="${ctxStatic}/jquery-form/jquery.multiselect.min.js" type="text/javascript"></script>
<div>
	<input id="${id}Name" name="${name}" readonly="readonly" type="text" value="${value}" maxlength="50" data-tags='${labelValue}'/>
</div>
<script type="text/javascript">
    $(function(){
    	$("#${id}Name").tagsInput({interactive:false,unique:false,width:'90%',defaultText:'${defaultText}'||'add Tag...',searcherF:function(){
    		var tagObj = $(this);
    		top.$.jBox.open("iframe:${ctx}/system/tag/treeselect?url="+encodeURIComponent("${selectUrl}")+"&module=${module}&checked=${checked}&extId=${extId}&selectIds="+$("#${id}Name").val(),"选择${title}", 300, 420,{
    			buttons:{"确定":"ok", ${allowClear?"\"清除\":\"clear\", ":""}"关闭":true}, submit:function(v, h, f){
    				if (v=="ok"){
    					var tree = h.find("iframe")[0].contentWindow.tree;//h.find("iframe").contents();
    					var ids = [], names = [], nodes = [];
    					if ("${checked}" == "true"){
    						nodes = tree.getCheckedNodes(true);
    					}else{
    						nodes = tree.getSelectedNodes();
    					}
    					for(var i=0; i<nodes.length; i++) {//<c:if test="${checked}">
    						if (nodes[i].isParent){
    							continue; // 如果为复选框选择，则过滤掉父节点
    						}//</c:if><c:if test="${notAllowSelectRoot}">
    						if (nodes[i].level == 0){
    							top.$.jBox.tip("不能选择根节点（"+nodes[i].name+"）请重新选择。");
    							return false;
    						}//</c:if><c:if test="${notAllowSelectParent}">
    						if (nodes[i].isParent){
    							top.$.jBox.tip("不能选择父节点（"+nodes[i].name+"）请重新选择。");
    							return false;
    						}//</c:if><c:if test="${not empty module && selectScopeModule}">
    						if (nodes[i].module == ""){
    							top.$.jBox.tip("不能选择公共模型（"+nodes[i].name+"）请重新选择。");
    							return false;
    						}else if (nodes[i].module != "${module}"){
    							top.$.jBox.tip("不能选择当前栏目以外的栏目模型，请重新选择。");
    							return false;
    						}//</c:if>
    						ids.push(nodes[i].id);
    						names.push(nodes[i].name);//<c:if test="${!checked}">
    						break; // 如果为非复选框选择，则返回第一个选择  </c:if>
    					}
    					var id = $(tagObj).attr('id');
    					$('#'+id+'_tagsinput .tag').remove();
    					$(tagObj).val(ids);
    					$(tagObj).attr('data-tags',names);
    					$.fn.tagsInput.importTags($(tagObj));
    				}//<c:if test="${allowClear}">
    				else if (v=="clear"){
    					var id = $(tagObj).attr('id');
    					$('#'+id+'_tagsinput .tag').remove();
    					$(tagObj).val('');
    					$(tagObj).attr('data-tags','');
                    }//</c:if>
    			}, loaded:function(h){
    				$(".jbox-content", top.document).css("overflow-y","hidden");
    			}
    		});
    	}});
    });
</script>