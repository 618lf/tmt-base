<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="navbar navbar-default navbar-fixed-top">
	<div class="navbar-container">
	     <div class="navbar-header pull-left">
			<a href="${ctx}/" class="navbar-brand">
				<small>
					<i class="icon-leaf"></i>&nbsp;豆瓜之家
				</small>
			</a>
		</div>
		<div class="navbar-header pull-right">
		     <ul class="nav ace-nav">
		         <li class="green">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<img class="nav-user-photo" src="${ctxStatic}/img/user.png" alt="Jason's Photo" />
						<span class="user-info">
							<small>欢迎光临,</small> <shiro:principal property="name"/>
						</span> 
						<i class="icon-caret-down"></i>
					</a>
					<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<li><a href="#"><i class="icon-user"></i>个人资料</a></li>
						<li class="divider"></li>
						<li><a href="${ctx}/logout"><i class="icon-off"></i>退出</a></li>
					</ul>
				</li>
		     </ul>
		</div>
	</div>
</div>