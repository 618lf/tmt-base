<!DOCTYPE html>
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<html style="overflow-x:hidden;overflow-y:auto;">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><sitemesh:title/> - Powered By LiFeng</title>
		<!-- styles -->
		<link href="${ctxStatic}/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${ctxStatic}/css/font-awesome.min.css" />
		<link rel="stylesheet" href="${ctxStatic}/css/ace-fonts.css" />
		<link rel="stylesheet" href="${ctxStatic}/css/ace.min.css" />
		<link rel="stylesheet" href="${ctxStatic}/css/ace-skins.min.css" />
		<script src="${ctxStatic}/js/jquery-1.8.3.min.js"></script>
		<script src="${ctxStatic}/js/bootstrap.min.js"></script>
		<script src="${ctxStatic}/js/ace-elements.min.js"></script>
		<script src="${ctxStatic}/js/ace.min.js"></script>
		<sitemesh:head/>
	</head>
	<body class="navbar-fixed">
	    <c:import url="/views/include/navbar.jsp" />
	    <div class="main-container">
	        <div class="main-container-inner">
	            <a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>
				<c:import url="/views/include/sidebar.jsp" />
				<div class="main-content">
				     <div class="page-content">
				         <sitemesh:body/>
				     </div>
				</div>
	        </div>
	    </div>
	</body>
</html>
