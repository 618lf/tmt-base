<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="sidebar sidebar-fixed" id="sidebar">
	<ul class="nav nav-list">
	   <li <c:if test="${not(sidebar=='receives'||sidebar=='replys')}">class="active"</c:if> >
			<a href="${ctx}/">
				<i class="icon-user"></i>
				<span class="menu-text"> 我的首页 </span>
			</a>
		</li>
		<li <c:if test="${sidebar=='receives'||sidebar=='replys'}">class="active open"</c:if>>
			<a href="#" class="dropdown-toggle">
				<i class="icon-comment"></i>
				<span class="menu-text"> 微信消息 </span>

				<b class="arrow icon-angle-down"></b>
			</a>
			<ul class="submenu">
				<li <c:if test="${sidebar=='receives'}">class="active"</c:if> >
					<a href="${ctx}/weixin/rt">
						<i class="icon-double-angle-right"></i>
						接收消息
					</a>
				</li>
				<li <c:if test="${sidebar=='replys'}">class="active"</c:if> >
					<a href="${ctx}/weixin/rt">
						<i class="icon-double-angle-right"></i>
						发送消息
					</a>
				</li>
			</ul>
		</li>
	</ul>
	<div class="sidebar-collapse" id="sidebar-collapse">
		<i class="icon-double-angle-left"></i>
	</div>
</div>