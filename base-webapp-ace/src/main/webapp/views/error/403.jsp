<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>403 - 用户权限不足</title>
<link href="${ctxStatic}/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="${ctxStatic}/css/font-awesome.min.css" />
<link rel="stylesheet" href="${ctxStatic}/css/ace-fonts.css" />
<link rel="stylesheet" href="${ctxStatic}/css/ace.min.css" />
<link rel="stylesheet" href="${ctxStatic}/css/ace-skins.min.css" />
<script src="${ctxStatic}/js/jquery-1.8.3.min.js"></script>
<script src="${ctxStatic}/js/bootstrap.min.js"></script>
<script src="${ctxStatic}/js/ace-elements.min.js"></script>
<script src="${ctxStatic}/js/ace.min.js"></script>
</head>
<body>
    <c:import url="/views/include/navbar.jsp" />
	<div class="container-fluid">
		<div class="page-header"><h1>用户权限不足.</h1></div>
		<div><a href="javascript:" onclick="history.go(-1);" class="btn">返回上一页</a></div>
		<script>try{top.$.jBox.closeTip();}catch(e){}</script>
	</div>
</body>
</html>