<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>智信移单位个人所得税管理软件V1.0.0</title>
<!-- styles -->
<link href="${ctxStatic}/bootstrap/2.3.2/css/bootstrap.css" rel="stylesheet" />
<link href="${ctxStatic}/common/common.css" rel="stylesheet" />
<link href="${ctxStatic}/common/homePage.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery/jquery-1.8.3.js" type="text/javascript"></script>
<script src="${ctxStatic}/jquery-cookie/jquery.cookie.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/common.js" type="text/javascript"></script>
<link href="${ctxStatic}/font/Font-Awesome/css/font-awesome.min.css" rel="stylesheet" />
<script type="text/javascript">
   var THISPAGE = {
		 _init : function(){
			 this.loadQuickMenu();
			 this.loadInlineUser();
			 this.addEvent();
		 },
		 loadQuickMenu : function(){
			 
		 },
		 loadInlineUser : function(){
			 var that = this;
		 },
		 addEvent : function(){
			 var that = this;
			 //绑定菜单打开事件
			 Public.pageTab();
		 }
   };
   $(function(){
	   THISPAGE._init();
   });
</script>
</head>
<body>
	<div class="wrapper cf">
		<div class="top">&nbsp;</div>
		<div class="col-main">
			<div class="main-wrap cf">
				<div class="m-top cf">
				   
				</div>
				<ul class="quick-links">
					<li class="voucher-list"><a href="${ctx}/system/menu/initList"
						rel="pageTab" data-id="menu" parentopen="true" title="菜单管理">
						<span class="helper-font-48"><i class="icon-dashboard"></i></span>菜单管理</a></li>
					<li class="subject-balance"><a
						href="${ctx}/system/area/initList" rel="pageTab"
						data-id="area" parentopen="true" title="区域管理"><span class="helper-font-48"><i class="icon-sitemap"></i></span>区域管理</a></li>

					<li class="profit-sheets"><a
						href="${ctx}/system/office/initList" rel="pageTab"
						data-id="office" parentopen="true" title="组织管理"><span class="helper-font-48"><i class="icon-group"></i></span>组织管理</a></li>

					<li class="balance-sheets"><a
						href="${ctx}/system/user/initList" rel="pageTab"
						data-id="user" parentopen="true" title="用户管理"><span class="helper-font-48"><i class="icon-user"></i></span>用户管理</a></li>

					<li class="subsidiary-ledger"><a
						href="${ctx}/system/role/initList" rel="pageTab"
						data-id="role" parentopen="true" tabtxt="权限管理"><span class="helper-font-48"><i class="icon-lock"></i></span>权限管理</a></li>

					<li class="checkout"><a href="${ctx}/system/dict/initList"
						rel="pageTab" data-id="dict" parentopen="true" tabtxt="系统配置">
						<span class="helper-font-48"><i class="icon-cogs"></i></span>系统配置</a>

					</li>

					<li class="added-service">
					    <a href="${ctx}/system/excel/initList" rel="pageTab" data-id="excel" parentopen="true" title="Excel配置">
					    <span class="helper-font-48"><i class="icon-table"></i></span>Excel配置</a>
					</li>
					
					<li class="help">
						<a href="${ctx}/system/log/initList" rel="pageTab" data-id="log" parentopen="true" title="系统监控">
						<span class="helper-font-48"><i class="icon-bar-chart"></i></span>系统监控</a>
					</li>

				</ul>

			</div>

		</div>
		<div class="col-extra">
			<div class="extra-wrap">
				<h2>在线用户</h2>
				<div class="list">

					<ul id="ajaxList" class="">
						<li><b></b><span class="es" title="现金">现金</span></li>
						<li><b>117,816.80</b><span class="es" title="银行存款">银行存款</span></li>
						<li><b>30,540.00</b><span class="es" title="存货">存货</span></li>
						<li><b>499.00</b><span class="es" title="应收账款">应收账款</span></li>
						<li><b>2,206,140.00</b><span class="es" title="应付账款">应付账款</span></li>
						<li><b></b><span class="es" title="主营业务收入">主营业务收入</span></li>
						<li><b></b><span class="es" title="销售费用">销售费用</span></li>
						<li><b></b><span class="es" title="管理费用">管理费用</span></li>
					</ul>

					<div class="sp">
						<a id="manage"><i></i>管理</a><a id="report-refresh"><i></i>刷新</a><span
							id="pageItem" style="display: none;"><i class="prev dis-p"></i><i
							class="next"></i></span>
					</div>

					<div id="listLD" class="loading"></div>

				</div>
			</div>
		</div>
	</div>
</body>
</html>