<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<html>
<head>
<title>微信公共帐号后台管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="${ctxStatic}/css/bootstrap2.min.css" rel="stylesheet" />
<script src="${ctxStatic}/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/js/bootstrap.min.js" type="text/javascript"></script>
<link href="${ctxStatic}/css/jquery.validate.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/js/jquery.validate.method.min.js" type="text/javascript"></script>
<style type="text/css">
	body, h1, h2, h3, ul, li, form, p, img { margin: 0; padding: 0; border: 0; }
	body { color: #000; font-size: 12px; line-height: 100%; text-align: center; }
	a { text-decoration: none; color: #959595; }
	.header { width: 1000px; height: 64px; position: relative; margin: 0 auto; z-index: 2; overflow: hidden; padding-top:0px;}
	.headerLogo { position: absolute; top: 17px; left: 50px;}
	.headerIntro { height: 28px; width: 144px; display: block; background-position: 0 -64px; top: 17px; left: 194px; }
    .unvisi { visibility: hidden;}
	.headerNav { top: 21px; right: 50px; width: 400px; text-align: right; color: #cfd0d0;}
	.headerNav a {  margin-left: 13px; }
	.headerLogo, .headerIntro, .headerNav, #headerEff, .footerLogo, .footerNav, .loginIcoCurrent, .loginIcoNew, .loginFormTh, .loginFormTdIpt, .domain, #loginFormSelect, #whatAutologinTip, #mobtips, #mobtips_arr, #mobtips_close {
     position: absolute;}
    .themeCtrl a, .loginFormIpt, .headerIntro, .loginIcoCurrent, .loginIcoNew, .themeText li, .domain, .whatAutologin, .btn, .dialogbox .hd .rc, .dialogbox .hd, .btn-moblogin, .btn-moblogin2, .ico, .locationTestTitle, .verSelected, .servSelected, .locationTestTitleClose, .ext-4free, #extText li, #extMobLogin li, #mobtips_arr, #mobtips_close {
      background-image: url(http://mimg.127.net/index/163/img/2013/bg_v1.png);
     }
    .main { height: 500px; background: #fff; position: relative; min-width: 1000px;}
    #mainCnt { width: 1000px; height: 500px; overflow: visible; margin: 0 auto; position: relative; clear: both; }
	.footer { height: 65px; margin: 0 auto;}
	.footer-inner { width: 1000px; height: 63px; overflow: visible; margin: 0 auto; color: #848585; position: relative;}
	.footerLogo { top: 11px; left: 35px;}
	.footerNav {top: 25px;right: 126px;}
	
	.login { height: 392px; padding: 13px 14px 15px; top: 30px;left: 50%; margin-left: 91px; text-align: left; position: absolute; z-index: 2; }
	.loginForm { position: relative; height: 313px; padding-top: 40px; }
	.form-signin{position:relative;text-align:left;width:300px;padding:25px 29px 29px;margin:0 auto 20px;background-color:#fff;border:1px solid #e5e5e5;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0077b3', GradientType=0) alpha(opacity=80);  opacity:0.85;
        	-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;-webkit-box-shadow:0 1px 2px rgba(0,0,0,.05);-moz-box-shadow:0 1px 2px rgba(0,0,0,.05);box-shadow:0 1px 2px rgba(0,0,0,.05);}
    .form-signin .checkbox{margin-bottom:10px;color:#0663a2;} .form-signin .input-label{font-size:16px;line-height:23px;color:#999;}
    .form-signin .input-block-level{font-size:16px;height:auto;margin-bottom:15px;padding:7px;*width:283px;*padding-bottom:0;_padding:7px 7px 9px 7px;}
    .form-signin .btn.btn-large{font-size:16px;} .form-signin #themeSwitch{position:absolute;right:15px;bottom:10px;}
    .form-signin div.validateCode {padding-bottom:15px;} .mid{vertical-align:middle;}
    .alert{position:relative;width:300px;margin:0 auto;*padding-bottom:0px;}
     label.error{background:none;padding:2px;font-weight:normal;color:inherit;margin:0;}
     #rememberMe {margin: 0;font-size: 11px;}.forgetPwdLine{font-size: 11px;}
     .form-signin-heading{text-align: center;}
     .clearfix:after{clear: both; content: "."; display: block; height: 0; overflow: hidden; visibility: hidden;}
     .weixin{height: 392px; padding: 13px 14px 15px; top: 30px;left: 20%; margin-left: 91px; text-align: left; position: absolute; z-index: 2; }
     .weixin-img{padding-top: 40px;}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$("#loginForm").validate({
			rules: {
				validateCode: {remote: "${pageContext.request.contextPath}/servlet/validateCodeServlet"}
			},
			messages: {
				username: {required: "请填写用户名."},password: {required: "请填写密码."},
				validateCode: {remote: "验证码不正确.", required: "请填写验证码."}
			},
			errorLabelContainer: "#messageBox",
			errorPlacement: function(error, element) {
				error.appendTo($("#loginError").parent());
			} 
		});
	});
	//如果在框架中，则跳转刷新上级页面
	if(self.frameElement && (self.frameElement.tagName=="IFRAME"||self.frameElement.tagName=="FRAME")){
		top.location.reload();
	}
	function register(){
		//window.location.href="${ctx}/register/userRegister";
	}
</script>
</head>
<body>
	<div class="header"></div>
    <div class="main" id="mainBg" style="background-color: #f5f5f5;">
		<div><%String error = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);%>
			<div id="messageBox" class="alert alert-error <%=error==null?"hide":""%>"><button data-dismiss="alert" class="close">×</button>
				<label id="loginError" class="error"><%=error==null?"":"com.tmt.base.system.security.CaptchaException".equals(error)?"验证码错误, 请重试.":"用户或密码错误, 请重试." %></label>
			</div>
		</div>
		<div class="weixin">
		     <div class="weixin-img">
		          <img src="${ctxStatic}/img/qrcode_for_gh_216295bb164d_258.jpg">
		     </div>
		</div>
		<div class="login">
			<div class="loginForm">
			    <form id="loginForm" class="form-signin" action="${ctx}/login" method="post">
			        <tags:token/>
			        <h3 class="form-signin-heading">微信公共帐号后台</h3>
			        <div class="control-group">
			           <label class="input-label control-label" for="username">登录名</label>
			           <div class="controls">
			             <input type="text" id="username" name="username" class="input-block-level required" value="${username}">
			           </div>
			        </div>
			        <div class="control-group">
				        <label class="input-label control-label" for="password">密码</label>
						<div class="controls">
						  <input type="password" id="password" name="password" class="input-block-level required" value="${password}">
						</div>
			        </div>
			        <c:if test="${isValidateCodeLogin}">
			        <div class="control-group validateCode">
			            <label class="input-label mid" for="validateCode" style="display: inline-block;">验证码</label>
						<tags:validateCode name="validateCode" inputCssStyle="margin-bottom:0;"/>
			        </div>
					</c:if>
					<div class="control-group clearfix">
						<div class="forgetPwdLine" style="display: inline-block;float: left;">						
							 <label for="rememberMe" title="下次不需要再登录" style="display: inline-block;"> 
							 <input type="checkbox" id="rememberMe" name="rememberMe"/> 记住我（公共环境慎用）
							 </label>
						</div>
					</div>
					<div class="control-group clearfix">
						<input class="btn btn-large btn-primary" type="submit" value="登 录" style="width:100%;float: left;"/>
					</div>
			    </form>
			</div>
		</div>
    </div>
    <div id="footer" class="footer">
		<div class="footer-inner" id="footerInner"></div>
	</div>
</body>
</html>
