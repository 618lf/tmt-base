<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/views/include/pageHead.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>微信  - 接收信息</title>
<meta name="decorator" content="default"/>
<script type="text/javascript">
function page(n,s){
	$("#pageIndex").val(n);
	$("#pageSize").val(s);
	$("#searchForm").submit();
	return false;
}
</script>
</head>
<body>
     <div class="page-header position-relative">
		<h1> <small> <i class="icon-user"></i> 微信接收信息 </small></h1>
	 </div>
	 <div class="row-fluid">
	      <div class="span12">
	           <form:form id="searchForm" modelAttribute="user" action="${ctx}/weixin/rt" method="post" class="breadcrumb form-search">
				<tags:token/>
				<input id="pageIndex" name="param.pageIndex" type="hidden" value="${page.param.pageIndex}"/>
				<input id="pageSize" name="param.pageSize" type="hidden" value="${page.param.pageIndex}"/>
			   </form:form>
	           <table id="sample-table-1" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th width="15%">消息内容</th>
							<th width="15%">发送方帐号</th>
							<th width="15%">发送时间</th>
							<th width="15%">回复内容</th>
							<th width="20%" >操作</th>
						</tr>
					</thead>
					<tbody>
					<c:forEach items="${page.data}"  var="vo"  >
						<tr>
							<td>${vo.content}</td>
							<td>${vo.formUserName}</td>
							<td><fmt:formatDate value="${vo.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td>${vo.resContent}</td>
							<td >
							    <a class="green" href="#" title="回复">
									<i class="icon-pencil bigger-130"></i>
								</a>
								<a class="red" href="#" title="删除">
									<i class="icon-trash bigger-130"></i>
								</a>
							</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
				<div class="dataTables_paginate paging_bootstrap">${page.simplePagination}</div>
	      </div>
     </div>
</body>
</html>