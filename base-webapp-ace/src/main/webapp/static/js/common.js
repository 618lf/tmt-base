/**
 * 公共函数库，主要是一些JS工具函数，各种插件的公共设置
 * @author HenryYan
 */
(function($) {
	/**
     * 获取元素的outerHTML
     */
    $.fn.outerHTML = function() {
        // IE, Chrome & Safari will comply with the non-standard outerHTML, all others (FF) will have a fall-back for cloning
        return (!this.length) ? this : (this[0].outerHTML ||
        (function(el) {
            var div = document.createElement('div');
            div.appendChild(el.cloneNode(true));
            var contents = div.innerHTML;
            div = null;
            return contents;
        })(this[0]));
    };
    
    //判断:当前元素是否是被筛选元素的子元素
    $.fn.isChildOf = function(b){
        return (this.parents(b).length > 0);
    };

    //判断:当前元素是否是被筛选元素的子元素或者本身
    $.fn.isChildAndSelfOf = function(b){
        return (this.closest(b).length > 0);
    };
    
    //ajax 全局设置
    $(document).ajaxSend(function(event, jqXHR, ajaxOptions) {
    	 jqXHR.setRequestHeader("token", $.cookie('token'));
    });
})(jQuery);

/**
 * 工具类
 */
var Public = Public || {};
Public.isIE6 = !window.XMLHttpRequest;

//快捷键
Public.keyCode = {
	ALT: 18,
	BACKSPACE: 8,
	CAPS_LOCK: 20,
	COMMA: 188,
	COMMAND: 91,
	COMMAND_LEFT: 91, // COMMAND
	COMMAND_RIGHT: 93,
	CONTROL: 17,
	DELETE: 46,
	DOWN: 40,
	END: 35,
	ENTER: 13,
	ESCAPE: 27,
	HOME: 36,
	INSERT: 45,
	LEFT: 37,
	MENU: 93, // COMMAND_RIGHT
	NUMPAD_ADD: 107,
	NUMPAD_DECIMAL: 110,
	NUMPAD_DIVIDE: 111,
	NUMPAD_ENTER: 108,
	NUMPAD_MULTIPLY: 106,
	NUMPAD_SUBTRACT: 109,
	PAGE_DOWN: 34,
	PAGE_UP: 33,
	PERIOD: 190,
	RIGHT: 39,
	SHIFT: 16,
	SPACE: 32,
	TAB: 9,
	UP: 38,
	F7: 118,
	F12: 123,
	S: 83,
	WINDOWS: 91 // COMMAND
};
/**
 * 节点赋100%高度
 *
 * @param {object} obj 赋高的对象
*/
Public.setAutoHeight = function(obj){
  if(!obj || obj.length < 1){
	return ;
  }
  Public._setAutoHeight(obj);
	$(window).bind('resize', function(){
		Public._setAutoHeight(obj);
  });
};
Public._setAutoHeight = function(obj){
	obj = $(obj);
	var winH = $(window).height();
	var h = winH - obj.offset().top - (obj.outerHeight() - obj.height());
	obj.height(h);
};
//Ajax请求，
//url:请求地址， params：传递的参数{...}， callback：请求成功回调  
Public.postAjax = function(url, params, callback){    
	$.ajax({  
	   type: "POST",
	   url: url,  
	   cache: false,  
	   async: true,  
	   dataType: "json",  
	   data: params,  
	   //当异步请求成功时调用  
	   success: function(data, status){  
		   callback(data);  
	   },  
	   //当请求出现错误时调用  只要状态码不是200 都会执行这个
	   error: function(x, s, e){
		    var msg = $.parseJSON(x.responseText).msg;
			Public.openWindow(msg,"系统错误",800,500,{
				buttons:{"关闭":true},
				submit:function(v, h, f){
				}, loaded:function(h){
		        }, closed:function(){ 
		        }
		    });
	   }  
	});  
};
//Ajax请求，
//url:请求地址， params：传递的参数{...}， callback：请求成功回调  
Public.getAjax = function(url, params, callback){    
	$.ajax({  
	   type: "GET",
	   url: url,  
	   cache: false,  
	   async: true,  
	   dataType: "json",  
	   data: params,  
	   //当异步请求成功时调用  
	   success: function(data, status){  
		   callback(data);  
	   },  
	   //当请求出现错误时调用  只要状态码不是200 都会执行这个
	   error: function(x, s, e){
		   var msg = $.parseJSON(x.responseText).msg;
		   Public.openWindow(msg,"系统错误",800,500,{
				buttons:{"关闭":true},
				submit:function(v, h, f){
				}, loaded:function(h){
		        }, closed:function(){ 
		        }
		   });
	   }  
	});  
};
Public.billsOper = function (val, opt, row) {
	var html_con = '<div class="operating" data-id="' + opt.rowId + '"><span class="ui-icon ui-icon-plus" title="新增行"></span><span class="ui-icon ui-icon-trash" title="删除行"></span></div>';
	return html_con;
};
//
Public.setGrid = function(param){
	var adjust = param || 60;
	var gridW = $("#dataGrid").width(), gridH = $(window).height() - $("#dataGrid").offset().top - adjust;
	return {
		w : gridW,
		h : gridH
	};
};

//默认的表格
Public.defaultGrid = function( params ){
	var options  = params||{};
	if( !options.form && !$('#queryForm') ) {
		alert("请设置form");
	}
	var formObj = options.form || $('#queryForm');
	var defaults = {
			datatype: "json",//xml，local，json，jsonnp，script，xmlstring，jsonstring，clientside
			mtype:'POST',//POST或者GET，默认GET
			rowNum:15,
			rowList:[15,25,50,100],
			viewrecords:true,//定义是否显示总记录数
			autoencode:true,//对url进行编码
			autowidth:true,//如果为ture时，则当表格在首次被创建时会根据父元素比例重新调整表格宽度。如果父元素宽度改变，为了使表格宽度能够自动调整则需要实现函数：setGridWidth
			loadtext:'数据加载中...',//当请求或者排序时所显示的文字内容
			multiselect:true,//定义是否可以多选（复选框）
			multiboxonly: true,
			altRows: true,
			gridview: true,
			rownumbers: !1,//序号
			cellEdit: !1,//是否可以编辑
			pager:"#page",
			page:1,//设置初始的页码
			pagerpos:'left',//指定分页栏的位置
			recordpos:'right',//定义了记录信息的位置： left, center, right
			recordtext:'当前显示{0} - {1} 条记录   共 {2} 条记录',//显示记录数信息。{0} 为记录数开始，{1}为记录数结束。 viewrecords为ture时才能起效，且总记录数大于0时才会显示此信息
			shrinkToFit:true,//此属性用来说明当初始化列宽度时候的计算类型，如果为ture，则按比例初始化列宽度。如果为false，则列宽度使用colModel指定的宽度
			jsonReader : { 
			      root: "data",   
			      page: "param.pageIndex",   
			      total: "param.pageCount",   
			      records: "param.recordCount",   
			      repeatitems: false   
		    },
		    prmNames:{
		    	page:"param.pageIndex",
		    	rows:'param.pageSize',
		    	sort: 'sidx',
		    	order: 'sord', 
		    	search:'search', 
		    	nd:'nd',
		    	npage:null
		    },
		    loadError:function(xhr,status,error){
		    	Public.error("数据加载失败:" + status);
		    },
		    beforeRequest:function(){
		    	var that = $(this);
		    	that.jqGrid('setGridParam',{postData:(function(form){
		    		var obj = {};
		    		$.each(form.serializeArray(),function(index,item){
		    			if(!(item.name in obj)){  
		    	            obj[item.name]=item.value;  
		    	        }  
		    		});
		    		return obj;
		    	})($(formObj))});
		    }
	};
	return $.extend({},defaults,options);
};
//可编辑的表格
Public.defaultEditGrid = function( params ){
	var options  = params||{};
	var defaults = {
			datatype: "clientSide",
			height: "100%",
            rownumbers: !0,
            gridview: !0,
            onselectrow: !1,
            cmTemplate: { sortable: !1, title: !1},
            forceFit: !0,
            rowNum: 1e3,
            cellEdit: !1,
            cellsubmit: "clientArray",
            localReader: {
                root: "rows",
                records: "records",
                repeatitems: !1,
                id: "id"
            },
            jsonReader: {
                root: "data.entries",
                records: "records",
                repeatitems: !1,
                id: "id"
            },
            loadonce: !0,
            footerrow: !1,
            userDataOnFooter: !0,
            userData: {},
            loadComplete: function (t) {},
            gridComplete: function () {},
            afterEditCell: function (t, e, i, a) {},
            formatCell: function () {},
            beforeSubmitCell: function () {},
            afterSaveCell: function (t, i, a, r, n) {},
            loadError: function (t, e) {}
	};
	return $.extend({},defaults,options);
};
//树型表格  -- 不支持冻结列
//第一列必须是 expandColumn 指定的列
Public.treeGrid = function( params ){
	var options  = params||{};
	var defaults = {
		treeGrid: true,
        treeGridModel: 'adjacency'
	};
	return Public.defaultGrid($.extend({},defaults,options));
};
//回车事件
Public.bindEnterDo = function(obj, func){
	var args = arguments;
	$(obj).on('keydown', 'input[type="text"]:visible:not(:disabled)', function(e){
		if (e.keyCode == '13') {
			if (typeof func == 'function') {
				var _args = Array.prototype.slice.call(args, 2 );
				func.apply(null,_args);
			}
		}
	});
};

//初始化查询组键事件
Public.initBtnMenu = function(){
	//菜单按钮
	$('.ui-btn-menu .ui-menu-btn').bind('click',function(e){
		if($(this).hasClass("ui-btn-dis")) {
			return false;
		}
		$(this).parent().toggleClass('ui-btn-menu-cur');
		$(this).blur();
		e.preventDefault();
	});
	
	//组合按钮
	$('.ui-btn-group .dropdown-toggle').bind('click',function(e){
		if($(this).hasClass("ui-btn-dis")) {
			return false;
		}
		$(this).parent().toggleClass('open');
		$(this).blur();
		e.preventDefault();
	});
	
	$(document).bind('click.menu',function(e){
		var target  = e.target || e.srcElement;
		$('.ui-btn-menu').each(function(){
			var menu = $(this);
			if($(target).closest(menu).length == 0 && $('.dropdown-menu',menu).is(':visible')){
				 menu.removeClass('ui-btn-menu-cur');
			};
		});
		$('.ui-btn-group').each(function(){
			var menu = $(this);
			if($(target).closest(menu).length == 0 && $('.dropdown-menu',menu).is(':visible')){
				 menu.removeClass('open');
			};
		});
		
		//查询事件
		if( $(target).hasClass("query") ) {
			Public.doQuery();
		}
		
		//重置事件
		if( $(target).hasClass("reset") ) {
			Public.resetQuery();
		}
		//更多条件
		if($(target).attr('id') == 'conditions-trigger') {
			  e.preventDefault();
			  if (!$(target).hasClass('conditions-expand')) {
					$('#more-conditions').stop().slideDown(200, function(){
					   $('#conditions-trigger').addClass('conditions-expand').html('收起更多<b></b>');
					   $('#filter-reset').css('display', 'inline');
				 	 });
			  } else {
				  	$('#more-conditions').stop().slideUp(200, function(){
					  $('#conditions-trigger').removeClass('conditions-expand').html('更多条件<b></b>');
					  $('#filter-reset').css('display', 'none');
				  	});
			  };	
		}
	});
};
Public.tipType = {
	 INFO:'info',
	 WARNING:'warning',
	 SUCCESS:'success',
	 ERROR:'error',
	 LOADING:'loading'
};
Public.alert = function(mess, type, callback){
	top.$.jBox.tip.mess = null;
	top.$.jBox.tip(mess,type,{persistent:true,opacity:0});
	if(typeof(callback) == 'function'){
		callback();
	}
};
Public.info = function(mess){
	Public.alert(mess,Public.tipType.INFO);
};
Public.error = function(mess){
	Public.alert(mess,Public.tipType.ERROR);
};
Public.success = function(mess, callback) {
	Public.alert(mess,Public.tipType.SUCCESS, callback);
};
Public.loading = function(mess) {
	Public.alert(mess||'数据加载中...',Public.tipType.LOADING);
};
Public.loaded = function() {
	setTimeout((function(){
		top.$.jBox.closeTip();
	})(),500);
};
//确认对话框
Public.confirmx = function(mess, ok, cancel){
	top.$.jBox.confirm(mess,'系统提示',function(v,h,f){
		if(v=='ok'){
			Public.loading('正在提交，请稍等...');
			if( typeof(ok) == 'function' ) {
				ok();
			}
		} else {
			if( typeof(cancel) == 'function' ) {
				cancel();
			}
		}
	},{buttonsFocus:1});
	return false;
};
//执行某个动作
Public.executex = function(mess, url, param, ok, cancel) {
	Public.confirmx(mess,function(){
		Public.postAjax(url,param,function(data){
    		if(typeof(data) == "string") {
          		 data = $.parseJSON(data);
          	}
    		Public.loaded();
    		ok(data);
        });
	}, cancel);
};
//删除
Public.deletex = function(mess, url, param, ok) {
	Public.executex(mess,url,param,ok,null);
};
//对话框
Public.openWindow = function(content,title,width,height,options){
	var defaults =  {
			buttons:{"确定":"ok", "关闭":true},
			submit:function(v, h, f){
				
			}, loaded:function(h){
	            $(".jbox-content", top.document).css("overflow-y","hidden");
	        }, closed:function(){ 
	        }
	};
	if($.jBox){
		$.jBox.open(content, title, width, height, $.extend({},defaults, options));
	} else {
		top.$.jBox.open(content, title, width, height, $.extend({},defaults, options));
	}
};
//打开一个窗体
Public.windowOpen = function(url, name, width, height){
	var top=parseInt((window.screen.height-height)/2,10),left=parseInt((window.screen.width-width)/2,10),
		options="location=no,menubar=no,toolbar=no,dependent=yes,minimizable=no,modal=yes,alwaysRaised=yes,"+
		"resizable=yes,scrollbars=yes,"+"width="+width+",height="+height+",top="+top+",left="+left;
	window.open(url ,name , options);
};
//重置查询条件框
Public.resetQuery = function(){
	$(".dropdown-menu").find("input[type='text']").val(" ");
	$(".dropdown-menu").find("select").val(" ");
	$(".dropdown-menu").find("textarea").val(" ");
};

//列表框的查询
Public.doQuery = function(gridName){
	$('#' + (gridName||'grid')).jqGrid('setGridParam',{page:1}).trigger("reloadGrid");
};

//表单列表的添加删除事件
Public.billsEvent = function(obj, type, flag){
	var _self = obj;
	//新增row
	$('#dataGrid').on('click', '.ui-icon-plus', function(e){
		var rowId = $(this).parent().data('id');
		var newId = $('#grid tbody tr').length;
		var datarow = { id: _self.newId };
		var su = $("#grid").jqGrid('addRowData', _self.newId, datarow, 'after', rowId);
		if(su) {
			$(this).parents('td').removeAttr('class');
			$(this).parents('tr').removeClass('selected-row ui-state-hover');
			$("#grid").jqGrid('resetSelection');
			_self.newId++;
		}
	});
	//删除row
	$('#dataGrid').on('click', '.ui-icon-trash', function(e){
		if($('#grid tbody tr').length === 2) {
			Public.error("至少保留一条分录！");
			return false;
		}
		var rowId = $(this).parent().data('id');
		var su = $("#grid").jqGrid('delRowData', rowId);
		if(su && _self.calTotal) {
			_self.calTotal();
		};
	});
	//取消row编辑状态
	$(document).bind('click.cancel', function(e){
		if(!$(e.target).closest(".ui-jqgrid-bdiv").length > 0 && curRow !== null && curCol !== null){
		   $("#grid").jqGrid("saveCell", curRow, curCol);
		   curRow = null;
		   curCol = null;
		};
	});
};

//动态添加行
Public.addRow = function(gridName){
	var that = $('#' + (gridName||'grid'));
	var colModel = that.jqGrid().getGridParam("colModel");  
	var cellLenth = colModel.length;
	for ( var i = 0 , j = colModel.length; i < j; i++) {  
	    colModel[i].editable = true ;  
	}
	var newRow = JSON.stringify(colModel);
	var ids = that.jqGrid('getDataIDs');
	var rowid = (ids.length ==0 ? 1: Math.max.apply(Math,ids)+1);
	that.setGridParam({cellEdit:false});
	that.jqGrid("addRowData", rowid,newRow , "last");
	that.jqGrid('editRow', rowid, false);
};

//动态修改行数据
Public.editRow = function(gridName,rowid){
	var that = $('#' + (gridName||'grid'));
	var colModel = that.jqGrid().getGridParam("colModel");  
	var cellLenth = colModel.length;
	for ( var i = 0 , j = colModel.length; i < j; i++) {  
	    colModel[i].editable = true ;  
	}
	var rows = null;
	if( !rowid ) {//取选中的行
		rows = that.getGridParam('selarrrow');
	}
	$.each(rows,function(index,item){
		var userId = $('#grid').getRowData(item).rowid;
		param.push({name:'idList',value:userId});
	});
	that.jqGrid('editRow', rowid, true);
};
/*批量绑定页签打开*/
Public.pageTab = function() {
	$(document).on('click', '[rel=pageTab]', function(e){
		e.preventDefault();
		var tabid = $(this).attr('data-id'), url = $(this).attr('href'), showClose = $(this).attr('showClose'), text = $(this).attr('title') || $(this).text(),parentOpen = $(this).attr('parentOpen');
		if(!(url === 'javascript:void(0)' || url === '#')){
			var sTab = {};
			if(parentOpen){ sTab = parent.tab; } else { sTab = tab; }
			if(sTab.isTabItemExist(tabid)){
				sTab.selectTabItem(tabid);
				sTab.reload(tabid);
			} else {
				sTab.addTabItem({tabid: tabid, text: text, url: url, showClose: showClose});
			}
		}
	});
};
/**
 * 表单的验证
 */
Public.validate = function(options){
	var defaults = {
		submitHandler: function(form){
			Public.loading('正在提交，请稍等...');
			form.submit();
		},
		errorContainer: "#messageBox",
		errorPlacement: function(error, element) {
			$("#messageBox").text("输入有误，请先更正。");
			if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
				error.appendTo(element.parent().parent());
			} else {
				error.insertAfter(element);
			}
		}
	};
	return $.extend({},defaults,options);
};
//引入js和css文件
function include(id, path, file){
	if (document.getElementById(id)==null){
        var files = typeof file == "string" ? [file] : file;
        for (var i = 0; i < files.length; i++){
            var name = files[i].replace(/^\s|\s$/g, "");
            var att = name.split('.');
            var ext = att[att.length - 1].toLowerCase();
            var isCSS = ext == "css";
            var tag = isCSS ? "link" : "script";
            var attr = isCSS ? " type='text/css' rel='stylesheet' " : " type='text/javascript' ";
            var link = (isCSS ? "href" : "src") + "='" + path + name + "'";
            document.write("<" + tag + (i==0?" id="+id:"") + attr + link + "></" + tag + ">");
        }
	}
}

